<?php

Route::group(['middleware' => ['auth:api']], function () {
    Route::group(['prefix' => 'user'], function ($api) {
        $api->get('/', ['as' => 'api.auth.user', 'uses' => 'UserController@current']);
        $api->post('modify', ['as' => 'api.auth.user', 'uses' => 'UserController@modify']);
    });
    Route::group(['prefix' => 'acc/calculations'], function ($api) {
        $api->post('{loa}/persist', ['as' => 'api.auth.acc.calculations.persist', 'uses' => AccCalculationController::class]);
    });
    Route::group(['prefix' => 'classification/unit'], function ($api) {
        $api->get('curate', ['as' => 'api.classification.unit', 'uses' => 'ClassificationUnitController@curate']);
    });
    Route::group(['prefix' => 'letter-of-authorization'], function ($api) {
        $api->get('find/{id}', ['as' => 'api.letter.of.authorization.find', 'uses' => 'LetterOfAuthorizationController@find']);
        $api->get('curate', ['as' => 'api.letter.of.authorization.curate', 'uses' => 'LetterOfAuthorizationController@curate']);
        $api->post('persist', ['as' => 'api.letter.of.authorization.persist', 'uses' => 'LetterOfAuthorizationController@persist']);
        $api->post('send/{id}/notification', ['as' => 'api.letter.of.authorization.send.notification', 'uses' => 'LetterOfAuthorizationController@sendNotification']);
        $api->post('expunge/{id}/application', ['as' => 'api.letter.of.authorization.remove.application', 'uses' => 'LetterOfAuthorizationController@expunge']);
    });
});

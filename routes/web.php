<?php

Route::group(['prefix' => 'document'], function ($router) {
    $router->get('{id}/letter-of-authority/{type?}', ['as' => 'web.document.letter-of-authority', 'uses' => 'WebLetterOfAuthorizationController@displayPdf']);
});

// Redirect all to the front-end router
Route::get('/{pattern?}', function() {
    return 'api yeah!';
})->where('pattern', '[\/\w\.-]*');

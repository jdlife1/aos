<?php

use Aos\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Service JDLife',
            'email' => 'service@jdlife.co.nz',
            'password' => 'secret'
        ]);
    }
}

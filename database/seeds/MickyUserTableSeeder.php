<?php

use Aos\Models\User;
use Illuminate\Database\Seeder;

class MickyUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Micky',
            'email' => 'mic51ngh@icloud.com',
            'password' => 'mic51ngh'
        ]);
    }
}

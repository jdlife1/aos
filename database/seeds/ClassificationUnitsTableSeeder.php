<?php

use Aos\Models\ClassificationUnit;

class ClassificationUnitsTableSeeder extends DatabaseSeeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    ClassificationUnit::query()->truncate();

    $this->units()->each(function ($unit) {
      $instance = ClassificationUnit::create(
        collect($unit)->except('activities')->all()
      );
      if ($activities = collect($unit)->get('activities')) {
        $instance->activities()->createMany($activities);
      }
    });
  }

  protected function units()
  {
    $units = array (
      array (
        'name' => 'Nursery production',
        'code' => '01110',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Bedding plant growing',
            'bic_code' => 'A011110'
          ),
          array (
            'name' => 'Bulb propagating',
            'bic_code' => 'A011120'
          ),
          array (
            'name' => 'Forest nursery operation or service',
            'bic_code' => 'A011210'
          ),
          array (
            'name' => 'Fruit tree nursery operation',
            'bic_code' => 'A011220'
          ),
          array (
            'name' => 'Nursery production (not elsewhere classified)',
            'bic_code' => 'A011230'
          ),
          array (
            'name' => 'Ornamental plant growing',
            'bic_code' => 'A051030'
          ),
          array (
            'name' => 'Perennial growing'
          ),
          array (
            'name' => 'Seedling growing'
          ),
          array (
            'name' => 'Vine stock nursery operation'
          )
        )
      ),
      array (
        'name' => 'Turf growing',
        'code' => '01111',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Turf growing for transplanting',
            'bic_code' => 'A011310'
          )
        )
      ),
      array (
        'name' => 'Floriculture production',
        'code' => '01120',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Calla lily growing',
            'bic_code' => 'A011410'
          ),
          array (
            'name' => 'Display foliage growing',
            'bic_code' => 'A011420'
          ),
          array (
            'name' => 'Flower growing'
          ),
          array (
            'name' => 'Hydrangea growing'
          ),
          array (
            'name' => 'Orchid growing'
          ),
          array (
            'name' => 'Peony growing'
          ),
          array (
            'name' => 'Seed, flower, growing'
          )
        )
      ),
      array (
        'name' => 'Mushroom growing',
        'code' => '01692',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Cultivated mushroom growing',
            'bic_code' => 'A012110'
          ),
          array (
            'name' => 'Mushroom spawn growing'
          )
        )
      ),
      array (
        'name' => 'Vegetable growing',
        'code' => '01130',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Asparagus growing',
            'bic_code' => 'A012210 '
          ),
          array (
            'name' => 'Bean growing (except dry field beans or soybeans)',
            'bic_code' => 'A012220'
          ),
          array (
            'name' => 'Capsicum growing',
            'bic_code' => 'A012230'
          ),
          array (
            'name' => 'Carrot growing',
            'bic_code' => 'A012240'
          ),
          array (
            'name' => 'Cucumber growing',
            'bic_code' => 'A012310'
          ),
          array (
            'name' => 'Garlic growing',
            'bic_code' => 'A012320'
          ),
          array (
            'name' => 'Herb growing',
            'bic_code' => 'A012330'
          ),
          array (
            'name' => 'Kumara growing',
            'bic_code' => 'A012340'
          ),
          array (
            'name' => 'Lettuce growing'
          ),
          array (
            'name' => 'Melon growing'
          ),
          array (
            'name' => 'Onion growing'
          ),
          array (
            'name' => 'Pea growing (except dry field peas)'
          ),
          array (
            'name' => 'Potato growing'
          ),
          array (
            'name' => 'Sprout growing'
          ),
          array (
            'name' => 'Sugar beet growing'
          ),
          array (
            'name' => 'Sweetcorn growing'
          ),
          array (
            'name' => 'Tomato growing'
          )
        )
      ),
      array (
        'name' => 'Grape growing',
        'code' => '01140',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Grape growing',
            'bic_code' => 'A013110'
          ),
          array (
            'name' => 'Grape sundrying',
            'bic_code' => 'A013120'
          ),
          array (
            'name' => 'Table grape growing'
          ),
          array (
            'name' => 'Vineyard operation'
          ),
          array (
            'name' => 'Wine grape growing'
          )
        )
      ),
      array (
        'name' => 'Kiwifruit growing',
        'code' => '01170',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Kiwifruit growing',
            'bic_code' => 'A013210'
          )
        )
      ),
      array (
        'name' => 'Berry fruit growing',
        'code' => '01192',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Berryfruit growing',
            'bic_code' => 'A013310'
          ),
          array (
            'name' => 'Blackberry growing',
            'bic_code' => 'A013320'
          ),
          array (
            'name' => 'Blackcurrant growing'
          ),
          array (
            'name' => 'Blueberry growing'
          ),
          array (
            'name' => 'Boysenberry growing'
          ),
          array (
            'name' => 'Cranberry growing'
          ),
          array (
            'name' => 'Gooseberry growing'
          ),
          array (
            'name' => 'Loganberry growing'
          ),
          array (
            'name' => 'Raspberry growing'
          ),
          array (
            'name' => 'Redcurrant growing'
          ),
          array (
            'name' => 'Strawberry growing'
          )
        )
      ),
      array (
        'name' => 'Apple and pear growing',
        'code' => '01150',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Apple growing',
            'bic_code' => 'A013410'
          ),
          array (
            'name' => 'Nashi pear growing',
            'bic_code' => 'A013420'
          ),
          array (
            'name' => 'Pear growing',
            'bic_code' => 'A013430'
          ),
          array (
            'name' => 'Quince growing'
          )
        )
      ),
      array (
        'name' => 'Stone fruit growing',
        'code' => '01160',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Apricot growing',
            'bic_code' => 'A013510'
          ),
          array (
            'name' => 'Cherry growing'
          ),
          array (
            'name' => 'Nectarine growing'
          ),
          array (
            'name' => 'Peach growing'
          ),
          array (
            'name' => 'Plum or prune growing'
          )
        )
      ),
      array (
        'name' => 'Citrus fruit growing',
        'code' => '01191',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Citrus fruit growing',
            'bic_code' => 'A013610'
          ),
          array (
            'name' => 'Citrus orchard operation'
          ),
          array (
            'name' => 'Grapefruit growing'
          ),
          array (
            'name' => 'Lemon growing'
          ),
          array (
            'name' => 'Mandarin growing'
          ),
          array (
            'name' => 'Orange growing'
          ),
          array (
            'name' => 'Tangelo growing'
          )
        )
      ),
      array (
        'name' => 'Olive growing',
        'code' => '01180',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Olive growing',
            'bic_code' => 'A013710'
          )
        )
      ),
      array (
        'name' => 'Fruit and tree nut growing (not elsewhere classified)',
        'code' => '01190',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Almond growing',
            'bic_code' => 'A013910'
          ),
          array (
            'name' => 'Avocado growing,',
            'bic_code' => 'A013920'
          ),
          array (
            'name' => 'Banana growing',
            'bic_code' => 'A013930'
          ),
          array (
            'name' => 'Brazil nut growing',
            'bic_code' => 'A013935'
          ),
          array (
            'name' => 'Cashew nut growing',
            'bic_code' => 'A013940'
          ),
          array (
            'name' => 'Chestnut growing',
            'bic_code' => 'A013950'
          ),
          array (
            'name' => 'Coconut growing',
            'bic_code' => 'A013960'
          ),
          array (
            'name' => 'Custard apple growing',
            'bic_code' => 'A013970'
          ),
          array (
            'name' => 'Feijoa growing',
            'bic_code' => 'A013975'
          ),
          array (
            'name' => 'Fig growing',
            'bic_code' => 'A013980'
          ),
          array (
            'name' => 'Loquat growing'
          ),
          array (
            'name' => 'Macadamia nut growing'
          ),
          array (
            'name' => 'Mango growing'
          ),
          array (
            'name' => 'Passionfruit growing'
          ),
          array (
            'name' => 'Pawpaw growing'
          ),
          array (
            'name' => 'Pecan nut growing'
          ),
          array (
            'name' => 'Persimmon growing'
          ),
          array (
            'name' => 'Pineapple growing'
          ),
          array (
            'name' => 'Tamarillo growing'
          ),
          array (
            'name' => 'Walnut growing'
          )
        )
      ),
      array (
        'name' => 'Sheep farming',
        'code' => '01240',
        'cover_plus' => '2.33',
        'cover_plus_extra' => '3.07',
        'activities' => array (
          array (
            'name' => 'Prime lamb raising',
            'bic_code' => 'A014110'
          ),
          array (
            'name' => 'Raw sheep milk production',
            'bic_code' => 'A014120'
          ),
          array (
            'name' => 'Sheep agistment service'
          ),
          array (
            'name' => 'Sheep farming'
          ),
          array (
            'name' => 'Wool growing'
          )
        )
      ),
      array (
        'name' => 'Beef cattle farming',
        'code' => '01250',
        'cover_plus' => '2.33',
        'cover_plus_extra' => '3.07',
        'activities' => array (
          array (
            'name' => 'Beef cattle farming',
            'bic_code' => 'A014210'
          ),
          array (
            'name' => 'Buffalo, domesticated, grazing',
            'bic_code' => 'A014220'
          ),
          array (
            'name' => 'Dairy cattle agistment service',
            'bic_code' => 'A014230'
          ),
          array (
            'name' => 'Dairy cattle replacement farming'
          ),
          array (
            'name' => 'Beef cattle feedlot operation'
          )
        )
      ),
      array (
        'name' => 'Sheep and beef cattle farming',
        'code' => '01230',
        'cover_plus' => '2.33',
        'cover_plus_extra' => '3.07',
        'activities' => array (
          array (
            'name' => 'Beef cattle and sheep farming',
            'bic_code' => 'A014410'
          ),
          array (
            'name' => 'Sheep and beef cattle farming',
            'bic_code' => 'A014420'
          )
        )
      ),
      array (
        'name' => 'Grain and sheep or grain and beef cattle farming',
        'code' => '01220',
        'cover_plus' => '2.34',
        'cover_plus_extra' => '3.08',
        'activities' => array (
          array (
            'name' => 'Beef cattle farming and grain growing',
            'bic_code' => 'A014510'
          ),
          array (
            'name' => 'Grain growing and sheep or beef cattle farming',
            'bic_code' => 'A014520'
          ),
          array (
            'name' => 'Prime lamb raising and grain growing'
          ),
          array (
            'name' => 'Sheep farming and grain growing'
          )
        )
      ),
      array (
        'name' => 'Grain growing',
        'code' => '01210',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Barley growing',
            'bic_code' => 'A014910'
          ),
          array (
            'name' => 'Cereal grain growing',
            'bic_code' => 'A014920'
          ),
          array (
            'name' => 'Coarse grain growing',
            'bic_code' => 'A014930'
          ),
          array (
            'name' => 'Field pea or field bean growing',
            'bic_code' => 'A014940'
          ),
          array (
            'name' => 'Grain seed growing',
            'bic_code' => 'A014950'
          ),
          array (
            'name' => 'Lupin growing',
            'bic_code' => 'A014960'
          ),
          array (
            'name' => 'Maize growing',
            'bic_code' => 'A014970'
          ),
          array (
            'name' => 'Millet growing',
            'bic_code' => 'A014980'
          ),
          array (
            'name' => 'Oat growing',
            'bic_code' => 'A014985'
          ),
          array (
            'name' => 'Oilseed growing (not elsewhere classified)',
            'bic_code' => 'A014990'
          ),
          array (
            'name' => 'Pasture seed growing'
          ),
          array (
            'name' => 'Safflower growing'
          ),
          array (
            'name' => 'Sorghum growing (except forage sorghum)'
          ),
          array (
            'name' => 'Soybean growing'
          ),
          array (
            'name' => 'Sunflower growing'
          ),
          array (
            'name' => 'Wheat growing'
          )
        )
      ),
      array (
        'name' => 'Crop growing (not elsewhere classified)',
        'code' => '01690',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Arrowroot growing',
            'bic_code' => 'A015905'
          ),
          array (
            'name' => 'Bamboo growing',
            'bic_code' => 'A015910'
          ),
          array (
            'name' => 'Flax seed growing',
            'bic_code' => 'A015920'
          ),
          array (
            'name' => 'Fodder growing',
            'bic_code' => 'A015930'
          ),
          array (
            'name' => 'Forage sorghum growing',
            'bic_code' => 'A015940'
          ),
          array (
            'name' => 'Ginger growing',
            'bic_code' => 'A015950'
          ),
          array (
            'name' => 'Hop growing',
            'bic_code' => 'A015960'
          ),
          array (
            'name' => 'Jute growing',
            'bic_code' => 'A015970'
          ),
          array (
            'name' => 'Lavender growing',
            'bic_code' => 'A015980'
          ),
          array (
            'name' => 'Lucerne growing',
            'bic_code' => 'A015985'
          ),
          array (
            'name' => 'Mustard growing',
            'bic_code' => 'A015990'
          ),
          array (
            'name' => 'Pasture growing for hay or silage'
          ),
          array (
            'name' => 'Pharmaceutical/cosmetic plant growing'
          ),
          array (
            'name' => 'Seed growing (not elsewhere classified)'
          ),
          array (
            'name' => 'Spice crop growing'
          ),
          array (
            'name' => 'Sudan grass growing'
          ),
          array (
            'name' => 'Tobacco growing'
          ),
          array (
            'name' => 'Vegetable growing for fodder'
          )
        )
      ),
      array (
        'name' => 'Dairy cattle farming',
        'code' => '01300',
        'cover_plus' => '2.29',
        'cover_plus_extra' => '3.02',
        'activities' => array (
          array (
            'name' => 'Dairy cattle farming',
            'bic_code' => 'A016010'
          ),
          array (
            'name' => 'Raw cattle milk production',
            'bic_code' => 'A016020'
          ),
          array (
            'name' => 'Sharemilking dairy cattle',
            'bic_code' => 'A016030'
          )
        )
      ),
      array (
        'name' => 'Poultry farming (meat)',
        'code' => '01410',
        'cover_plus' => '1.20',
        'cover_plus_extra' => '1.59',
        'activities' => array (
          array (
            'name' => 'Chicken farming (for meat)',
            'bic_code' => 'A017110'
          ),
          array (
            'name' => 'Duck farming',
            'bic_code' => 'A017120'
          ),
          array (
            'name' => 'Game bird farming',
            'bic_code' => 'A017130'
          ),
          array (
            'name' => 'Goose farming',
            'bic_code' => 'A017140'
          ),
          array (
            'name' => 'Poultry farming (for meat)',
            'bic_code' => 'A017150'
          ),
          array (
            'name' => 'Poultry hatchery operation (meat breeds)',
            'bic_code' => 'A017160'
          ),
          array (
            'name' => 'Turkey farming',
            'bic_code' => 'A017170'
          )
        )
      ),
      array (
        'name' => 'Poultry farming (eggs)',
        'code' => '01420',
        'cover_plus' => '1.20',
        'cover_plus_extra' => '1.59',
        'activities' => array (
          array (
            'name' => 'Egg farm operation',
            'bic_code' => 'A017210'
          ),
          array (
            'name' => 'Poultry farming (for eggs)',
            'bic_code' => 'A017220'
          ),
          array (
            'name' => 'Poultry hatchery operation (egg breeds)',
            'bic_code' => 'A017230'
          )
        )
      ),
      array (
        'name' => 'Deer farming',
        'code' => '01530',
        'cover_plus' => '1.97',
        'cover_plus_extra' => '2.60',
        'activities' => array (
          array (
            'name' => 'Deer breeding',
            'bic_code' => 'A018010'
          ),
          array (
            'name' => 'Deer farming for venison'
          ),
          array (
            'name' => 'Deer velvet production'
          )
        )
      ),
      array (
        'name' => 'Horse farming and horse agistment',
        'code' => '01520',
        'cover_plus' => '2.59',
        'cover_plus_extra' => '3.41',
        'activities' => array (
          array (
            'name' => 'Horse agistment service',
            'bic_code' => 'A019110'
          ),
          array (
            'name' => 'Horse breeding',
            'bic_code' => 'A019120'
          ),
          array (
            'name' => 'Stud farm operation (horses)',
            'bic_code' => array (
              'A019125',
              'A019130'
            )
          )
        )
      ),
      array (
        'name' => 'Pig farming',
        'code' => '01510',
        'cover_plus' => '1.97',
        'cover_plus_extra' => '2.60',
        'activities' => array (
          array (
            'name' => 'Pig farming',
            'bic_code' => 'A019210'
          ),
          array (
            'name' => 'Pig raising'
          )
        )
      ),
      array (
        'name' => 'Beekeeping',
        'code' => '01593',
        'cover_plus' => '1.97',
        'cover_plus_extra' => '2.60',
        'activities' => array (
          array (
            'name' => 'Apiculture',
            'bic_code' => 'A019310'
          ),
          array (
            'name' => 'Beekeeping',
            'bic_code' => 'A019320'
          )
        )
      ),
      array (
        'name' => 'Livestock farming (not elsewhere classified)',
        'code' => '01590',
        'cover_plus' => '1.97',
        'cover_plus_extra' => '2.60',
        'activities' => array (
          array (
            'name' => 'Alpaca farming',
            'bic_code' => 'A019910'
          ),
          array (
            'name' => 'Bird breeding (except poultry or game birds)',
            'bic_code' => 'A019920'
          ),
          array (
            'name' => 'Cat breeding',
            'bic_code' => 'A019930'
          ),
          array (
            'name' => 'Dairy goat farming',
            'bic_code' => 'A019940'
          ),
          array (
            'name' => 'Dog breeding',
            'bic_code' => 'A019950'
          ),
          array (
            'name' => 'Emu farming',
            'bic_code' => 'A019960'
          ),
          array (
            'name' => 'Fur skin animal farming',
            'bic_code' => 'A019970'
          ),
          array (
            'name' => 'Goat farming',
            'bic_code' => 'A019975'
          ),
          array (
            'name' => 'Livestock raising (not elsewhere classified)',
            'bic_code' => 'A019980'
          ),
          array (
            'name' => 'Ostrich farming',
            'bic_code' => 'A019990'
          ),
          array (
            'name' => 'Pet breeding'
          ),
          array (
            'name' => 'Rabbit farming'
          ),
          array (
            'name' => 'Worm farming'
          )
        )
      ),
      array (
        'name' => 'Offshore aquaculture',
        'code' => '04210',
        'cover_plus' => '2.21',
        'cover_plus_extra' => '2.92',
        'activities' => array (
          array (
            'name' => 'Mussel farming (longline)',
            'bic_code' => 'A020110'
          ),
          array (
            'name' => 'Offshore longline or rack aquaculture',
            'bic_code' => 'A020120'
          ),
          array (
            'name' => 'Oyster farming (rack)',
            'bic_code' => 'A020130'
          ),
          array (
            'name' => 'Paua farming (longline or rack)',
            'bic_code' => 'A020210'
          ),
          array (
            'name' => 'Pearl oyster farming (rack)'
          ),
          array (
            'name' => 'Seaweed farming (longline or rack)'
          ),
          array (
            'name' => 'Finfish farming (caged)'
          ),
          array (
            'name' => 'Salmon farming (caged)'
          ),
          array (
            'name' => 'Tuna farming'
          )
        )
      ),
      array (
        'name' => 'Onshore aquaculture',
        'code' => '04220',
        'cover_plus' => '2.21',
        'cover_plus_extra' => '2.92',
        'activities' => array (
          array (
            'name' => 'Crustacean or mollusc breeding or farming (pond or tank)',
            'bic_code' => 'A020310'
          ),
          array (
            'name' => 'Fish breeding or farming (pond or tank)',
            'bic_code' => 'A020320'
          ),
          array (
            'name' => 'Fish hatchery operation',
            'bic_code' => 'A020330'
          ),
          array (
            'name' => 'Ornamental fish farming',
            'bic_code' => 'A020340'
          ),
          array (
            'name' => 'Paua farming (pond)'
          ),
          array (
            'name' => 'Prawn farming (pond)'
          ),
          array (
            'name' => 'Salmon farming (pond or tank)'
          ),
          array (
            'name' => 'Trout farming'
          )
        )
      ),
      array (
        'name' => 'Forestry',
        'code' => '03010',
        'cover_plus' => '3.30',
        'cover_plus_extra' => '4.33',
        'activities' => array (
          array (
            'name' => 'Forestry growing operation',
            'bic_code' => 'A030120'
          )
        )
      ),
      array (
        'name' => 'Forest product and moss gathering and processing',
        'code' => '03021',
        'cover_plus' => '2.21',
        'cover_plus_extra' => '2.92',
        'activities' => array (
          array (
            'name' => 'Forest product gathering',
            'bic_code' => 'A030110'
          ),
          array (
            'name' => 'Kauri gum digging',
            'bic_code' => 'A030130'
          ),
          array (
            'name' => 'Native orchid gathering'
          ),
          array (
            'name' => 'Pine cone collecting'
          ),
          array (
            'name' => 'Resin gathering'
          ),
          array (
            'name' => 'Sphagnum moss gathering'
          )
        )
      ),
      array (
        'name' => 'Logging',
        'code' => '03020',
        'cover_plus' => '3.30',
        'cover_plus_extra' => '4.33',
        'activities' => array (
          array (
            'name' => 'Firewood cutting (forest)',
            'bic_code' => 'A030210'
          ),
          array (
            'name' => 'Logging',
            'bic_code' => 'A030220'
          ),
          array (
            'name' => 'Mine timber hewing (forest)',
            'bic_code' => 'A030230'
          ),
          array (
            'name' => 'Pole hewing (forest)',
            'bic_code' => 'A030240'
          ),
          array (
            'name' => 'Post shaping (forest)',
            'bic_code' => 'A030250'
          ),
          array (
            'name' => 'Railway sleeper hewing'
          ),
          array (
            'name' => 'Rough shaping of forest timber'
          ),
          array (
            'name' => 'Timber hewing (forest)'
          ),
          array (
            'name' => 'Tree cutting or felling'
          )
        )
      ),
      array (
        'name' => 'Rock lobster and crab fishing or potting',
        'code' => '04110',
        'cover_plus' => '2.98',
        'cover_plus_extra' => '3.92',
        'activities' => array (
          array (
            'name' => 'Crab fishing or potting',
            'bic_code' => 'A041105'
          ),
          array (
            'name' => 'Rock lobster fishing or potting',
            'bic_code' => 'A041110 '
          ),
          array (
            'name' => 'Saltwater crayfish fishing',
            'bic_code' => array (
              'A041120',
              'A041130'
            )
          )
        )
      ),
      array (
        'name' => 'Prawn fishing',
        'code' => '04120',
        'cover_plus' => '2.95',
        'cover_plus_extra' => '3.88',
        'activities' => array (
          array (
            'name' => 'Prawn fishing',
            'bic_code' => 'A041210'
          ),
          array (
            'name' => 'Scampi fishing',
            'bic_code' => 'A041220'
          )
        )
      ),
      array (
        'name' => 'Line fishing (including processing on-board)',
        'code' => '04150',
        'cover_plus' => '2.96',
        'cover_plus_extra' => '3.89',
        'activities' => array (
          array (
            'name' => 'Bottom long line fishing',
            'bic_code' => 'A041310'
          ),
          array (
            'name' => 'Line fishing',
            'bic_code' => 'A041320'
          ),
          array (
            'name' => 'Ocean trolling',
            'bic_code' => 'A041330'
          ),
          array (
            'name' => 'Squid jigging'
          ),
          array (
            'name' => 'Surface long line fishing'
          )
        )
      ),
      array (
        'name' => 'Fish trawling, seining, and netting (including processing on-board)',
        'code' => '04130',
        'cover_plus' => '2.96',
        'cover_plus_extra' => '3.89',
        'activities' => array (
          array (
            'name' => 'Beach seining, fishing',
            'bic_code' => 'A041403'
          ),
          array (
            'name' => 'Bottom gill netting, fishing',
            'bic_code' => 'A041407'
          ),
          array (
            'name' => 'Danish seining, fishing',
            'bic_code' => 'A041410'
          ),
          array (
            'name' => 'Finfish trawling'
          ),
          array (
            'name' => 'Pair trawling'
          ),
          array (
            'name' => 'Purse seining'
          ),
          array (
            'name' => 'Surface netting, fishing'
          )
        )
      ),
      array (
        'name' => 'Fishing (not elsewhere classified)',
        'code' => '04190',
        'cover_plus' => '2.96',
        'cover_plus_extra' => '3.89',
        'activities' => array (
          array (
            'name' => 'Abalone/paua fishing',
            'bic_code' => 'A041910'
          ),
          array (
            'name' => 'Freshwater eel fishing',
            'bic_code' => 'A041920'
          ),
          array (
            'name' => 'Freshwater fishing (not elsewhere classified)',
            'bic_code' => 'A041930'
          ),
          array (
            'name' => 'Marine water fishery product gathering (not elsewhere classified)',
            'bic_code' => 'A041940'
          ),
          array (
            'name' => 'Oyster catching (except from cultivated oyster beds)',
            'bic_code' => 'A041950'
          ),
          array (
            'name' => 'Pearling (except pearl oyster farming)',
            'bic_code' => 'A041960'
          ),
          array (
            'name' => 'Seaweed harvesting',
            'bic_code' => 'A041970'
          ),
          array (
            'name' => 'Spat catching',
            'bic_code' => 'A041980'
          )
        )
      ),
      array (
        'name' => 'Hunting and trapping',
        'code' => '02200',
        'cover_plus' => '3.30',
        'cover_plus_extra' => '4.33',
        'activities' => array (
          array (
            'name' => 'Bird trapping',
            'bic_code' => 'A042010'
          ),
          array (
            'name' => 'Culling of wild animals',
            'bic_code' => 'A042020'
          ),
          array (
            'name' => 'Deer hunting',
            'bic_code' => 'A042030'
          ),
          array (
            'name' => 'Fur skin animal hunting or trapping',
            'bic_code' => 'A042040'
          ),
          array (
            'name' => 'Game preserve, commercial, operation',
            'bic_code' => 'A042050'
          ),
          array (
            'name' => 'Mutton bird catching',
            'bic_code' => 'A042060'
          ),
          array (
            'name' => 'Possum hunting and trapping',
            'bic_code' => 'A042070'
          ),
          array (
            'name' => 'Rabbit hunting or trapping'
          )
        )
      ),
      array (
        'name' => 'Forestry support services (excluding tree cutting and felling)',
        'code' => '03030',
        'cover_plus' => '3.32',
        'cover_plus_extra' => '4.36',
        'activities' => array (
          array (
            'name' => 'Forest conservation service',
            'bic_code' => 'A051010'
          ),
          array (
            'name' => 'Forest pest control service (except aerial or wild animal control)',
            'bic_code' => 'A051020'
          ),
          array (
            'name' => 'Forest planting',
            'bic_code' => 'A051040'
          ),
          array (
            'name' => 'Reforestation service',
            'bic_code' => 'A051050'
          ),
          array (
            'name' => 'Silvicultural service',
            'bic_code' => 'A051055'
          ),
          array (
            'name' => 'Timber plantation maintenance',
            'bic_code' => 'A051060'
          ),
          array (
            'name' => 'Timber tract maintenance',
            'bic_code' => 'A051070'
          ),
          array (
            'name' => 'Tree pruning (forest)',
            'bic_code' => 'A051080'
          ),
          array (
            'name' => 'Tree thinning (forest)',
            'bic_code' => 'A051090'
          )
        )
      ),
      array (
        'name' => 'Shearing services',
        'code' => '02120',
        'cover_plus' => '3.18',
        'cover_plus_extra' => '4.18',
        'activities' => array (
          array (
            'name' => 'Alpaca shearing',
            'bic_code' => 'A052210'
          ),
          array (
            'name' => 'Goat shearing',
            'bic_code' => 'A052220'
          ),
          array (
            'name' => 'Sheep shearing',
            'bic_code' => 'A052990'
          )
        )
      ),
      array (
        'name' => 'Agriculture and fishing support services (not elsewhere classified)',
        'code' => '02190',
        'cover_plus' => '2.22',
        'cover_plus_extra' => '2.93',
        'activities' => array (
          array (
            'name' => 'Agricultural fencing contractor',
            'bic_code' => 'A052917'
          ),
          array (
            'name' => 'Agricultural support service (not elsewhere classified)',
            'bic_code' => 'A052920'
          ),
          array (
            'name' => 'Aquaculture support service',
            'bic_code' => 'A052923'
          ),
          array (
            'name' => 'Artificial insemination service',
            'bic_code' => 'A052927'
          ),
          array (
            'name' => 'Bush or rural land clearing',
            'bic_code' => 'A052933'
          ),
          array (
            'name' => 'Crop harvesting',
            'bic_code' => 'A052937'
          ),
          array (
            'name' => 'Dairy herd testing',
            'bic_code' => 'A052940'
          ),
          array (
            'name' => 'Farm irrigation service',
            'bic_code' => 'A052943'
          ),
          array (
            'name' => 'Fertiliser spreading (except aerial)',
            'bic_code' => 'A052947'
          ),
          array (
            'name' => 'Fishing support service',
            'bic_code' => 'A052953'
          ),
          array (
            'name' => 'Hay or silage baling or pressing',
            'bic_code' => 'A052955'
          ),
          array (
            'name' => 'Horse training (working horses)',
            'bic_code' => 'A052960'
          ),
          array (
            'name' => 'Livestock dipping',
            'bic_code' => 'A052964'
          ),
          array (
            'name' => 'Livestock drafting or droving',
            'bic_code' => 'A052967'
          ),
          array (
            'name' => 'Offal hole drilling',
            'bic_code' => 'A052970'
          ),
          array (
            'name' => 'Seed grading or cleaning',
            'bic_code' => 'E329939'
          ),
          array (
            'name' => 'Wool classing (including reclassing and bulk classing)',
            'bic_code' => array (
              'A052980',
              'A052983',
              'A052987',
              'A052993'
            )
          )
        )
      ),
      array (
        'name' => 'Coal mining',
        'code' => '11010',
        'cover_plus' => '1.24',
        'cover_plus_extra' => '1.65',
        'activities' => array (
          array (
            'name' => 'Black coal mining',
            'bic_code' => 'B060010'
          ),
          array (
            'name' => 'Brown coal mining',
            'bic_code' => 'B060020'
          ),
          array (
            'name' => 'Lignite mining',
            'bic_code' => array (
              'B060030',
              'B060040'
            )
          )
        )
      ),
      array (
        'name' => 'Oil and gas extraction',
        'code' => '12000',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Natural gas extraction',
            'bic_code' => 'B070010'
          ),
          array (
            'name' => 'Oil shale mining',
            'bic_code' => 'B070020'
          ),
          array (
            'name' => 'Petroleum gas extraction',
            'bic_code' => array (
              'B070030',
              'B070040'
            )
          )
        )
      ),
      array (
        'name' => 'Iron ore mining',
        'code' => '13110',
        'cover_plus' => '1.24',
        'cover_plus_extra' => '1.65',
        'activities' => array (
          array (
            'name' => 'Iron ore dressing or beneficiating',
            'bic_code' => 'B080105'
          ),
          array (
            'name' => 'Iron ore mining',
            'bic_code' => 'B080110'
          ),
          array (
            'name' => 'Iron sand mining'
          )
        )
      ),
      array (
        'name' => 'Gold ore mining',
        'code' => '13140',
        'cover_plus' => '1.24',
        'cover_plus_extra' => '1.65',
        'activities' => array (
          array (
            'name' => 'Alluvial gold mining',
            'bic_code' => 'B080410'
          ),
          array (
            'name' => 'Eluvial gold mining',
            'bic_code' => 'B080420'
          ),
          array (
            'name' => 'Gold bullion production',
            'bic_code' => 'B080430'
          ),
          array (
            'name' => 'Gold dredging',
            'bic_code' => 'B080440'
          ),
          array (
            'name' => 'Gold mining'
          ),
          array (
            'name' => 'Gold ore roasting and flotation extraction, including metallurgical hydro-extraction'
          ),
          array (
            'name' => 'Gold washing or sluicing'
          ),
          array (
            'name' => 'Reworking of mullock heaps or tailings for gold'
          )
        )
      ),
      array (
        'name' => 'Mineral sand mining',
        'code' => '13150',
        'cover_plus' => '1.31',
        'cover_plus_extra' => '1.74',
        'activities' => array (
          array (
            'name' => 'Ilmenite sand mining',
            'bic_code' => 'B080510'
          ),
          array (
            'name' => 'Leucoxene sand mining'
          ),
          array (
            'name' => 'Mineral sand mining'
          ),
          array (
            'name' => 'Monazite sand mining'
          ),
          array (
            'name' => 'Rutile sand mining'
          ),
          array (
            'name' => 'Synthetic rutile production'
          ),
          array (
            'name' => 'Zircon sand mining'
          )
        )
      ),
      array (
        'name' => 'Silver, lead, and zinc ore mining',
        'code' => '13170',
        'cover_plus' => '1.24',
        'cover_plus_extra' => '1.65',
        'activities' => array (
          array (
            'name' => 'Lead ore mining',
            'bic_code' => 'B080710'
          ),
          array (
            'name' => 'Silver ore mining',
            'bic_code' => 'B080720'
          ),
          array (
            'name' => 'Silver-lead-zinc ore mining',
            'bic_code' => 'B080730'
          ),
          array (
            'name' => 'Zinc ore mining'
          )
        )
      ),
      array (
        'name' => 'Metal ore mining (not elsewhere classified)',
        'code' => '13190',
        'cover_plus' => '1.24',
        'cover_plus_extra' => '1.65',
        'activities' => array (
          array (
            'name' => 'Antimony ore mining',
            'bic_code' => 'B080910'
          ),
          array (
            'name' => 'Beryllium ore mining',
            'bic_code' => 'B080920'
          ),
          array (
            'name' => 'Bismuth ore mining',
            'bic_code' => 'B080930'
          ),
          array (
            'name' => 'Iron pyrite mining'
          ),
          array (
            'name' => 'Manganese ore mining'
          ),
          array (
            'name' => 'Metallic ore mining (not elsewhere classified)'
          ),
          array (
            'name' => 'Molybdenite mining'
          ),
          array (
            'name' => 'Nickel ore mining'
          ),
          array (
            'name' => 'Platinum group metal mining'
          ),
          array (
            'name' => 'Tantalite mining'
          ),
          array (
            'name' => 'Tin ore mining'
          ),
          array (
            'name' => 'Tungsten ore mining'
          )
        )
      ),
      array (
        'name' => 'Gravel and sand quarrying',
        'code' => '14110',
        'cover_plus' => '1.24',
        'cover_plus_extra' => '1.65',
        'activities' => array (
          array (
            'name' => 'Pebble quarrying',
            'bic_code' => 'B091110'
          ),
          array (
            'name' => 'River gravel quarrying, washing or screening',
            'bic_code' => 'B091120'
          ),
          array (
            'name' => 'Rock, ornamental, gathering',
            'bic_code' => 'B091130'
          ),
          array (
            'name' => 'Sand quarrying, washing or screening',
            'bic_code' => 'B091140'
          )
        )
      ),
      array (
        'name' => 'Construction material mining (not elsewhere classified)',
        'code' => '14190',
        'cover_plus' => '1.24',
        'cover_plus_extra' => '1.65',
        'activities' => array (
          array (
            'name' => 'Aggregate quarrying',
            'bic_code' => 'B091910'
          ),
          array (
            'name' => 'Bentonite quarrying',
            'bic_code' => 'B091920'
          ),
          array (
            'name' => 'Blue metal stone quarrying',
            'bic_code' => 'B091930'
          ),
          array (
            'name' => 'Brick shale quarrying',
            'bic_code' => 'B091935'
          ),
          array (
            'name' => 'Building stone quarrying',
            'bic_code' => 'B091940'
          ),
          array (
            'name' => 'Cement clay quarrying',
            'bic_code' => 'B091950'
          ),
          array (
            'name' => 'Chalk quarrying',
            'bic_code' => 'B091960'
          ),
          array (
            'name' => 'Clay quarrying',
            'bic_code' => 'B091970'
          ),
          array (
            'name' => 'Construction material crushing or screening',
            'bic_code' => 'B091980'
          ),
          array (
            'name' => 'Dimension stone quarrying',
            'bic_code' => 'B091990'
          ),
          array (
            'name' => 'Earth, soil or filling quarrying'
          ),
          array (
            'name' => 'Fullers earth quarrying'
          ),
          array (
            'name' => 'Granite quarrying'
          ),
          array (
            'name' => 'Limestone quarrying'
          ),
          array (
            'name' => 'Marble quarrying'
          ),
          array (
            'name' => 'Road fill quarrying'
          ),
          array (
            'name' => 'Sandstone quarrying'
          ),
          array (
            'name' => 'Slate quarrying'
          ),
          array (
            'name' => 'Stone quarrying'
          ),
          array (
            'name' => 'Tile clay quarrying'
          )
        )
      ),
      array (
        'name' => 'Construction services (not elsewhere classified)',
        'code' => '42590',
        'cover_plus' => '2.26',
        'cover_plus_extra' => '2.98',
        'activities' => array (
          array (
            'name' => 'Billboard construction or erection',
            'bic_code' => 'E329910'
          ),
          array (
            'name' => 'Drilling contractor on construction site (not elsewhere classified)',
            'bic_code' => 'E329930'
          ),
          array (
            'name' => 'Film or stage rigging',
            'bic_code' => 'E329931'
          ),
          array (
            'name' => 'Labourer contracted to work on construction site (not elsewhere classified)',
            'bic_code' => 'E329935'
          ),
          array (
            'name' => 'House reblocking or underpinning',
            'bic_code' => 'E329936'
          ),
          array (
            'name' => 'Lane or road marking service',
            'bic_code' => 'E329937'
          ),
          array (
            'name' => 'Metal wall cladding fixing to buildings',
            'bic_code' => 'E329938'
          ),
          array (
            'name' => 'Non-electrical traffic signal installation',
            'bic_code' => 'E329940'
          ),
          array (
            'name' => 'Petrol bowser installation',
            'bic_code' => 'E329950'
          ),
          array (
            'name' => 'Sand blasting or steam cleaning of building exteriors',
            'bic_code' => 'E329960'
          ),
          array (
            'name' => 'Scaffolding construction',
            'bic_code' => 'E329970'
          ),
          array (
            'name' => 'Waterproofing of building',
            'bic_code' => 'E329980'
          ),
          array (
            'name' => 'Water bore drilling and construction',
            'bic_code' => array (
              'E329985',
              'E329990'
            )
          )
        )
      ),
      array (
        'name' => 'Mining and quarrying (not elsewhere classified)',
        'code' => '14200',
        'cover_plus' => '1.24',
        'cover_plus_extra' => '1.65',
        'activities' => array (
          array (
            'name' => 'Abrasives mining',
            'bic_code' => 'B060050'
          ),
          array (
            'name' => 'Alabaster mining',
            'bic_code' => 'B099010'
          ),
          array (
            'name' => 'Alum mining',
            'bic_code' => 'B099020'
          ),
          array (
            'name' => 'Alunite mining',
            'bic_code' => 'B099030'
          ),
          array (
            'name' => 'Barite mining',
            'bic_code' => 'B099040'
          ),
          array (
            'name' => 'Chrysoprase mining',
            'bic_code' => 'B099050'
          ),
          array (
            'name' => 'Diatomite mining'
          ),
          array (
            'name' => 'Felspar quarrying'
          ),
          array (
            'name' => 'Flint quarrying'
          ),
          array (
            'name' => 'Fluorspar mining'
          ),
          array (
            'name' => 'Gemstone mining'
          ),
          array (
            'name' => 'Glauconite mining'
          ),
          array (
            'name' => 'Graphite mining'
          ),
          array (
            'name' => 'Green sand mining'
          ),
          array (
            'name' => 'Gypsum mining'
          ),
          array (
            'name' => 'Horticultural peat extraction'
          ),
          array (
            'name' => 'Jade or greenstone mining'
          ),
          array (
            'name' => 'Kyanite mining'
          ),
          array (
            'name' => 'Lithium mineral mining'
          ),
          array (
            'name' => 'Magnesite mining'
          ),
          array (
            'name' => 'Mica mining'
          ),
          array (
            'name' => 'Mineral pigment mining (not elsewhere classified)'
          ),
          array (
            'name' => 'Phosphate rock mining'
          ),
          array (
            'name' => 'Quartz quarrying (not elsewhere classified)'
          ),
          array (
            'name' => 'Salt harvesting'
          ),
          array (
            'name' => 'Silica mining (for industrial purposes)'
          ),
          array (
            'name' => 'Talc quarrying'
          ),
          array (
            'name' => 'Vermiculite mining'
          ),
          array (
            'name' => 'Zeolite mining'
          )
        )
      ),
      array (
        'name' => 'Petroleum and natural gas exploration',
        'code' => '15110',
        'cover_plus' => '1.24',
        'cover_plus_extra' => '1.65',
        'activities' => array (
          array (
            'name' => 'Natural gas exploration',
            'bic_code' => 'B101110'
          ),
          array (
            'name' => 'Petroleum exploration',
            'bic_code' => 'B101120'
          )
        )
      ),
      array (
        'name' => 'Mineral exploration',
        'code' => '15130',
        'cover_plus' => '1.24',
        'cover_plus_extra' => '1.65',
        'activities' => array (
          array (
            'name' => 'Mineral exploration',
            'bic_code' => array (
              'B101210',
              'B101220'
            )
          )
        )
      ),
      array (
        'name' => 'Mining support services (not elsewhere classified)',
        'code' => '15200',
        'cover_plus' => '1.24',
        'cover_plus_extra' => '1.65',
        'activities' => array (
          array (
            'name' => 'Cementing oil and gas well casings',
            'bic_code' => 'B109005'
          ),
          array (
            'name' => 'Mine directional drilling and redrilling',
            'bic_code' => 'B109010'
          ),
          array (
            'name' => 'Mining draining and pumping service',
            'bic_code' => 'B109020'
          ),
          array (
            'name' => 'Oil and gas field support service (not elsewhere classified)',
            'bic_code' => 'B109030'
          )
        )
      ),
      array (
        'name' => 'Meat processing',
        'code' => '21110',
        'cover_plus' => '2.63',
        'cover_plus_extra' => '3.46',
        'activities' => array (
          array (
            'name' => 'Abattoir operation (except poultry)',
            'bic_code' => 'C111110'
          ),
          array (
            'name' => 'Animal meat packing and freezing',
            'bic_code' => 'C111120'
          ),
          array (
            'name' => 'Animal oil or fat, unrefined, manufacturing',
            'bic_code' => 'C111130'
          ),
          array (
            'name' => 'Lard or tallow rendering',
            'bic_code' => 'C111140'
          ),
          array (
            'name' => 'Meat extract or essence manufacturing',
            'bic_code' => 'C111150'
          ),
          array (
            'name' => 'Meat manufacturing (except bacon, ham and poultry)',
            'bic_code' => 'C111160'
          ),
          array (
            'name' => 'Meat or bone meal manufacturing (except fish or poultry meal)',
            'bic_code' => 'C111170'
          ),
          array (
            'name' => 'Meat packing (except poultry)',
            'bic_code' => 'C111180'
          ),
          array (
            'name' => 'Meat, canned, manufacturing (except poultry, bacon, ham and corned meat)'
          ),
          array (
            'name' => 'Meat, dehydrated, manufacturing (except poultry)'
          ),
          array (
            'name' => 'Meat, frozen, manufacturing (except poultry)'
          )
        )
      ),
      array (
        'name' => 'Poultry processing',
        'code' => '21120',
        'cover_plus' => '1.75',
        'cover_plus_extra' => '2.32',
        'activities' => array (
          array (
            'name' => 'Frozen poultry manufacturing',
            'bic_code' => 'C111210'
          ),
          array (
            'name' => 'Game bird (e.g. pheasant, quail) slaughtering',
            'bic_code' => 'C111220'
          ),
          array (
            'name' => 'Poultry abattoir operation',
            'bic_code' => 'C111230'
          ),
          array (
            'name' => 'Poultry croquette manufacturing',
            'bic_code' => 'C111240'
          ),
          array (
            'name' => 'Poultry meat or bone meal manufacturing',
            'bic_code' => 'C111250'
          ),
          array (
            'name' => 'Poultry meat packing',
            'bic_code' => 'C111260'
          ),
          array (
            'name' => 'Poultry meat processing (including canning)',
            'bic_code' => 'C111270'
          )
        )
      ),
      array (
        'name' => 'Cured meat and smallgoods manufacturing',
        'code' => '21130',
        'cover_plus' => '1.75',
        'cover_plus_extra' => '2.32',
        'activities' => array (
          array (
            'name' => 'Bacon manufacturing',
            'bic_code' => 'C111310'
          ),
          array (
            'name' => 'Croquette manufacturing (not elsewhere classified)',
            'bic_code' => 'C111320'
          ),
          array (
            'name' => 'Ham, canned, manufacturing',
            'bic_code' => 'C111330'
          ),
          array (
            'name' => 'Meat speciality manufacturing',
            'bic_code' => 'C111340'
          ),
          array (
            'name' => 'Pate manufacturing (except fish)',
            'bic_code' => 'C111350'
          ),
          array (
            'name' => 'Poultry smallgoods manufacturing',
            'bic_code' => 'C111360'
          ),
          array (
            'name' => 'Smallgoods manufacturing'
          )
        )
      ),
      array (
        'name' => 'Seafood processing (other than on-board vessels)',
        'code' => '21730',
        'cover_plus' => '1.75',
        'cover_plus_extra' => '2.32',
        'activities' => array (
          array (
            'name' => 'Crustacean, processed, manufacturing (including cooked and/or frozen) (not elsewhere classified)',
            'bic_code' => 'C112010'
          ),
          array (
            'name' => 'Fish cleaning or filleting',
            'bic_code' => 'C112020'
          ),
          array (
            'name' => 'Fish fillet manufacturing',
            'bic_code' => 'C112025'
          ),
          array (
            'name' => 'Fish loaf or cake manufacturing',
            'bic_code' => 'C112030'
          ),
          array (
            'name' => 'Fish paste manufacturing',
            'bic_code' => 'C112035'
          ),
          array (
            'name' => 'Fish pate manufacturing',
            'bic_code' => 'C112040'
          ),
          array (
            'name' => 'Fish, canned, manufacturing',
            'bic_code' => 'C112045'
          ),
          array (
            'name' => 'Fish, dried or smoked, manufacturing',
            'bic_code' => 'C112050'
          ),
          array (
            'name' => 'Mollusc, processed, manufacturing (including shelled)',
            'bic_code' => 'C112060'
          ),
          array (
            'name' => 'Oyster, shelling, freezing or bottling in brine',
            'bic_code' => 'C112065'
          ),
          array (
            'name' => 'Scallop, preserved, manufacturing',
            'bic_code' => 'C112070'
          ),
          array (
            'name' => 'Seafood, canned, manufacturing',
            'bic_code' => 'C112080'
          ),
          array (
            'name' => 'Seafood, preserved, manufacturing'
          ),
          array (
            'name' => 'Whole fin fish freezing'
          )
        )
      ),
      array (
        'name' => 'Milk and cream processing',
        'code' => '21210',
        'cover_plus' => '0.70',
        'cover_plus_extra' => '0.94',
        'activities' => array (
          array (
            'name' => 'Cream, pasteurised, manufacturing (except canned)',
            'bic_code' => 'C113110'
          ),
          array (
            'name' => 'Milk, low fat, manufacturing',
            'bic_code' => 'C113120'
          ),
          array (
            'name' => 'Milk, pasteurised, manufacturing'
          ),
          array (
            'name' => 'Skim milk manufacturing'
          ),
          array (
            'name' => 'Standard milk manufacturing'
          ),
          array (
            'name' => 'Ultra heat treatment milk manufacturing'
          )
        )
      ),
      array (
        'name' => 'Ice cream manufacturing',
        'code' => '21220',
        'cover_plus' => '0.70',
        'cover_plus_extra' => '0.94',
        'activities' => array (
          array (
            'name' => 'Confections, frozen manufacturing',
            'bic_code' => 'C113210'
          ),
          array (
            'name' => 'Fruit ice, frozen, manufacturing',
            'bic_code' => 'C113220'
          ),
          array (
            'name' => 'Gelato manufacturing'
          ),
          array (
            'name' => 'Ice cream manufacturing'
          ),
          array (
            'name' => 'Sorbet manufacturing'
          )
        )
      ),
      array (
        'name' => 'Cheese and other dairy product manufacturing (not elsewhere classified)',
        'code' => '21290',
        'cover_plus' => '0.70',
        'cover_plus_extra' => '0.94',
        'activities' => array (
          array (
            'name' => 'Anhydrous milk fat (butter oil) manufacturing',
            'bic_code' => 'C113310'
          ),
          array (
            'name' => 'Butter manufacturing',
            'bic_code' => 'C113320'
          ),
          array (
            'name' => 'Buttermilk manufacturing',
            'bic_code' => 'C113330'
          ),
          array (
            'name' => 'Casein manufacturing',
            'bic_code' => 'C113340'
          ),
          array (
            'name' => 'Cheese manufacturing',
            'bic_code' => 'C113350'
          ),
          array (
            'name' => 'Condensed milk manufacturing',
            'bic_code' => 'C113360'
          ),
          array (
            'name' => 'Cream, canned, manufacturing',
            'bic_code' => 'C113370'
          ),
          array (
            'name' => 'Dairy product manufacturing (not elsewhere classified)',
            'bic_code' => 'C113380'
          ),
          array (
            'name' => 'Dried ice cream, soft serve or milk shake mix manufacturing'
          ),
          array (
            'name' => 'Evaporated milk manufacturing'
          ),
          array (
            'name' => 'Flavoured milk manufacturing'
          ),
          array (
            'name' => 'Infants’ milk-based formula and food manufacturing'
          ),
          array (
            'name' => 'Lactose manufacturing'
          ),
          array (
            'name' => 'Liquid ice cream, soft serve or milk shake mix manufacturing'
          ),
          array (
            'name' => 'Malted milk powder manufacturing'
          ),
          array (
            'name' => 'Milk and coffee mixtures, condensed or concentrated, manufacturing'
          ),
          array (
            'name' => 'Milk powder manufacturing'
          ),
          array (
            'name' => 'Sour cream manufacturing'
          ),
          array (
            'name' => 'Whey or whey powder manufacturing'
          ),
          array (
            'name' => 'Yoghurt manufacturing'
          )
        )
      ),
      array (
        'name' => 'Fruit and vegetable processing',
        'code' => '21300',
        'cover_plus' => '1.02',
        'cover_plus_extra' => '1.37',
        'activities' => array (
          array (
            'name' => 'Baby food, canned or bottled, manufacturing (except milk based)',
            'bic_code' => 'C114010'
          ),
          array (
            'name' => 'Baked bean manufacturing',
            'bic_code' => 'C114015'
          ),
          array (
            'name' => 'Bean/legume, dried or canned, manufacturing',
            'bic_code' => 'C114020'
          ),
          array (
            'name' => 'Chutney or relish manufacturing',
            'bic_code' => 'C114025'
          ),
          array (
            'name' => 'Coconut, desiccated, manufacturing',
            'bic_code' => 'C114030'
          ),
          array (
            'name' => 'Fruit dehydrating or drying (except sun drying) manufacturing',
            'bic_code' => 'C114035'
          ),
          array (
            'name' => 'Fruit juice, 100 percent pure or concentrated, manufacturing',
            'bic_code' => 'C114040'
          ),
          array (
            'name' => 'Fruit pulp, puree or spread manufacturing',
            'bic_code' => 'C114045'
          ),
          array (
            'name' => 'Fruit salad manufacturing',
            'bic_code' => 'C114050'
          ),
          array (
            'name' => 'Fruit, frozen, manufacturing',
            'bic_code' => 'C114055'
          ),
          array (
            'name' => 'Fruit, preserved, manufacturing (including canned or bottled)',
            'bic_code' => 'C114060'
          ),
          array (
            'name' => 'Grape crushing',
            'bic_code' => 'C114065'
          ),
          array (
            'name' => 'Jam manufacturing (including conserves, jellies or fruit spreads)',
            'bic_code' => 'C114070'
          ),
          array (
            'name' => 'Mixed meat and vegetable manufacturing',
            'bic_code' => 'C114075'
          ),
          array (
            'name' => 'Rice preparation, canned, manufacturing',
            'bic_code' => 'C114080'
          ),
          array (
            'name' => 'Sauce manufacturing (except Worcestershire sauce)',
            'bic_code' => 'C114085'
          ),
          array (
            'name' => 'Spaghetti, canned, manufacturing',
            'bic_code' => 'C114090'
          ),
          array (
            'name' => 'Vegetable juice or soup manufacturing'
          ),
          array (
            'name' => 'Vegetable salad manufacturing'
          ),
          array (
            'name' => 'Vegetable soup manufacturing'
          ),
          array (
            'name' => 'Vegetable, frozen, manufacturing'
          ),
          array (
            'name' => 'Vegetable, preserved, manufacturing (including canned, dehydrated, dried or quick frozen)'
          ),
          array (
            'name' => 'Vinegar manufacturing (except wine vinegar)'
          )
        )
      ),
      array (
        'name' => 'Oil and fat manufacturing',
        'code' => '21400',
        'cover_plus' => '1.02',
        'cover_plus_extra' => '1.37',
        'activities' => array (
          array (
            'name' => 'Animal oil, refined, manufacturing',
            'bic_code' => 'C111190'
          ),
          array (
            'name' => 'Cotton seed oil manufacturing',
            'bic_code' => 'C115010'
          ),
          array (
            'name' => 'Deodorised vegetable oil manufacturing',
            'bic_code' => 'C115020'
          ),
          array (
            'name' => 'Edible oil or fat, blended, manufacturing',
            'bic_code' => 'C115030'
          ),
          array (
            'name' => 'Fish or other marine animal oil or meal manufacturing',
            'bic_code' => 'C115040'
          ),
          array (
            'name' => 'Lard, refined, manufacturing',
            'bic_code' => 'C115050'
          ),
          array (
            'name' => 'Margarine manufacturing'
          ),
          array (
            'name' => 'Olive oil manufacturing'
          ),
          array (
            'name' => 'Tallow, refined, manufacturing'
          ),
          array (
            'name' => 'Vegetable oil, meal or cake manufacturing'
          )
        )
      ),
      array (
        'name' => 'Grain mill product manufacturing',
        'code' => '21510',
        'cover_plus' => '1.75',
        'cover_plus_extra' => '2.32',
        'activities' => array (
          array (
            'name' => 'Arrowroot manufacturing',
            'bic_code' => 'C116110'
          ),
          array (
            'name' => 'Baking powder manufacturing',
            'bic_code' => 'C116120'
          ),
          array (
            'name' => 'Barley malt manufacturing',
            'bic_code' => 'C116130'
          ),
          array (
            'name' => 'Barley meal or flour manufacturing',
            'bic_code' => 'C116135'
          ),
          array (
            'name' => 'Cornflour manufacturing',
            'bic_code' => 'C116140'
          ),
          array (
            'name' => 'Cornmeal manufacturing',
            'bic_code' => 'C116150'
          ),
          array (
            'name' => 'Dextrin manufacturing',
            'bic_code' => 'C116160'
          ),
          array (
            'name' => 'Dextrose manufacturing (except prepared)',
            'bic_code' => 'C116170'
          ),
          array (
            'name' => 'Glucose manufacturing'
          ),
          array (
            'name' => 'Gluten manufacturing'
          ),
          array (
            'name' => 'Malt extract manufacturing'
          ),
          array (
            'name' => 'Malt manufacturing'
          ),
          array (
            'name' => 'Pollard manufacturing (from wheat, barley or rye)'
          ),
          array (
            'name' => 'Rice flour, meal or offal manufacturing'
          ),
          array (
            'name' => 'Rice manufacturing (including parboiled)'
          ),
          array (
            'name' => 'Rice starch manufacturing'
          ),
          array (
            'name' => 'Rye flour, meal or offal manufacturing'
          ),
          array (
            'name' => 'Sago manufacturing'
          ),
          array (
            'name' => 'Self-raising flour manufacturing'
          ),
          array (
            'name' => 'Semolina manufacturing'
          ),
          array (
            'name' => 'Starch manufacturing'
          ),
          array (
            'name' => 'Tapioca manufacturing'
          ),
          array (
            'name' => 'Unpopped corn manufacturing (for popcorn)'
          ),
          array (
            'name' => 'Wheat germ manufacturing'
          ),
          array (
            'name' => 'Wheaten bran manufacturing'
          ),
          array (
            'name' => 'Wheaten flour manufacturing'
          ),
          array (
            'name' => 'Wheaten malt manufacturing'
          ),
          array (
            'name' => 'Wheatmeal manufacturing'
          )
        )
      ),
      array (
        'name' => 'Cereal, pasta, and baking-mix manufacturing',
        'code' => '21520',
        'cover_plus' => '0.78',
        'cover_plus_extra' => '1.05',
        'activities' => array (
          array (
            'name' => 'Baking mix, prepared, manufacturing',
            'bic_code' => 'C116210'
          ),
          array (
            'name' => 'Bread mix, dry, manufacturing',
            'bic_code' => 'C116220'
          ),
          array (
            'name' => 'Cake mix manufacturing',
            'bic_code' => 'C116230'
          ),
          array (
            'name' => 'Cereal food manufacturing (not elsewhere classified)',
            'bic_code' => 'C116240'
          ),
          array (
            'name' => 'Coatings made from cereal food (except biscuit or breadcrumb) manufacturing'
          ),
          array (
            'name' => 'Custard powder manufacturing'
          ),
          array (
            'name' => 'Dessert, dried prepared, manufacturing'
          ),
          array (
            'name' => 'Noodle manufacturing'
          ),
          array (
            'name' => 'Oatmeal manufacturing'
          ),
          array (
            'name' => 'Oats, hulled or shelled, manufacturing'
          ),
          array (
            'name' => 'Oats, kilned or unkilned, manufacturing'
          ),
          array (
            'name' => 'Pasta, fresh or dried, manufacturing'
          ),
          array (
            'name' => 'Pastry mix manufacturing'
          ),
          array (
            'name' => 'Prepared breakfast cereal manufacturing'
          )
        )
      ),
      array (
        'name' => 'Bread manufacturing (factory-based)',
        'code' => '21610',
        'cover_plus' => '0.78',
        'cover_plus_extra' => '1.05',
        'activities' => array (
          array (
            'name' => 'Bagel manufacturing (factory based)',
            'bic_code' => 'C117110'
          ),
          array (
            'name' => 'Bread bakery operation (factory based)',
            'bic_code' => 'C117120'
          ),
          array (
            'name' => 'Bread dough, frozen, manufacturing (factory based)',
            'bic_code' => 'C117125'
          ),
          array (
            'name' => 'Bread roll manufacturing (factory based)',
            'bic_code' => 'C117130'
          ),
          array (
            'name' => 'Bread, leavened or unleavened, manufacturing (factory based)'
          ),
          array (
            'name' => 'Breadcrumb manufacturing (factory based)'
          ),
          array (
            'name' => 'English muffin manufacturing (factory based)'
          ),
          array (
            'name' => 'Fruit loaf manufacturing (factory based)'
          ),
          array (
            'name' => 'Panini manufacturing (factory based)'
          ),
          array (
            'name' => 'Pita bread manufacturing (factory based)'
          )
        )
      ),
      array (
        'name' => 'Cake and pastry manufacturing (factory-based)',
        'code' => '21620',
        'cover_plus' => '0.95',
        'cover_plus_extra' => '1.27',
        'activities' => array (
          array (
            'name' => 'Cake icing or decorating (factory based)',
            'bic_code' => 'C117205'
          ),
          array (
            'name' => 'Cake or pastry manufacturing (factory based)',
            'bic_code' => 'C117210'
          ),
          array (
            'name' => 'Cake or pastry, frozen, manufacturing (factory based)',
            'bic_code' => 'C117220'
          ),
          array (
            'name' => 'Cake or pastry-based pudding and dessert manufacturing (factory based)',
            'bic_code' => 'C117230'
          ),
          array (
            'name' => 'Cake or pastry-based slice manufacturing (factory based)',
            'bic_code' => 'C117240'
          ),
          array (
            'name' => 'Crumpet manufacturing (factory based)',
            'bic_code' => 'C117250'
          ),
          array (
            'name' => 'Doughnut manufacturing (factory based)',
            'bic_code' => 'C117260'
          ),
          array (
            'name' => 'Pastry manufacturing (includes frozen dough; factory based)',
            'bic_code' => 'C117270'
          ),
          array (
            'name' => 'Pie manufacturing (including meat, fruit or vegetable pies; factory based)',
            'bic_code' => 'C117280'
          )
        )
      ),
      array (
        'name' => 'Biscuit manufacturing (factory-based)',
        'code' => '21630',
        'cover_plus' => '0.95',
        'cover_plus_extra' => '1.27',
        'activities' => array (
          array (
            'name' => 'Biscuit dough manufacturing (factory based)',
            'bic_code' => 'C117310'
          ),
          array (
            'name' => 'Biscuit manufacturing (except pet food biscuits; factory based)',
            'bic_code' => 'C117320'
          ),
          array (
            'name' => 'Ice cream cone or wafer manufacturing (factory based)'
          )
        )
      ),
      array (
        'name' => 'Bakery product manufacturing (non-factory-based)',
        'code' => '21640',
        'cover_plus' => '0.81',
        'cover_plus_extra' => '1.09',
        'activities' => array (
          array (
            'name' => 'Manufacturing and selling bread from the same premises (non-factory based)',
            'bic_code' => 'C117410'
          ),
          array (
            'name' => 'Manufacturing and selling other bakery products from the same premises (non-factory based)',
            'bic_code' => 'C117420'
          )
        )
      ),
      array (
        'name' => 'Sugar manufacturing',
        'code' => '21710',
        'cover_plus' => '0.70',
        'cover_plus_extra' => '0.94',
        'activities' => array (
          array (
            'name' => 'Brown sugar manufacturing',
            'bic_code' => 'C118110'
          ),
          array (
            'name' => 'Cane syrup manufacturing'
          ),
          array (
            'name' => 'Caster sugar manufacturing'
          ),
          array (
            'name' => 'Icing sugar manufacturing'
          ),
          array (
            'name' => 'Molasses manufacturing'
          ),
          array (
            'name' => 'Sugar manufacturing'
          ),
          array (
            'name' => 'Treacle manufacturing'
          )
        )
      ),
      array (
        'name' => 'Confectionery manufacturing',
        'code' => '21720',
        'cover_plus' => '0.70',
        'cover_plus_extra' => '0.94',
        'activities' => array (
          array (
            'name' => 'Chewing gum manufacturing',
            'bic_code' => 'C118210'
          ),
          array (
            'name' => 'Chocolate manufacturing',
            'bic_code' => 'C118220'
          ),
          array (
            'name' => 'Cocoa product manufacturing',
            'bic_code' => 'C118230'
          ),
          array (
            'name' => 'Confectionery manufacturing',
            'bic_code' => 'C118240'
          ),
          array (
            'name' => 'Crystallised or glace fruit manufacturing'
          ),
          array (
            'name' => 'Drinking chocolate manufacturing'
          ),
          array (
            'name' => 'Licorice manufacturing'
          ),
          array (
            'name' => 'Marshmallow manufacturing'
          ),
          array (
            'name' => 'Marzipan manufacturing'
          ),
          array (
            'name' => 'Nut, candied, manufacturing'
          ),
          array (
            'name' => 'Popcorn, candied, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Potato crisps and corn crisps manufacturing',
        'code' => '21795',
        'cover_plus' => '0.49',
        'cover_plus_extra' => '0.67',
        'activities' => array (
          array (
            'name' => 'Corn chip manufacturing',
            'bic_code' => 'C119110'
          ),
          array (
            'name' => 'Crisp manufacturing'
          ),
          array (
            'name' => 'Potato crisp manufacturing'
          ),
          array (
            'name' => 'Taco, tortilla or tostada shell manufacturing'
          )
        )
      ),
      array (
        'name' => 'Prepared animal and bird feed manufacturing',
        'code' => '21740',
        'cover_plus' => '0.95',
        'cover_plus_extra' => '1.27',
        'activities' => array (
          array (
            'name' => 'Animal feed, prepared, manufacturing (except uncanned meat or bone meal or protein-enriched skim milk powder)',
            'bic_code' => 'C119210'
          ),
          array (
            'name' => 'Animal food, canned, manufacturing',
            'bic_code' => 'C119220'
          ),
          array (
            'name' => 'Bird feed manufacturing',
            'bic_code' => 'C119230'
          ),
          array (
            'name' => 'Cattle lick manufacturing',
            'bic_code' => 'C119240'
          ),
          array (
            'name' => 'Cereal meal manufacturing (for fodder, except from rice or rye)',
            'bic_code' => 'C119250'
          ),
          array (
            'name' => 'Chaff manufacturing',
            'bic_code' => 'C119260'
          ),
          array (
            'name' => 'Crushed grain manufacturing (including mixed; for fodder)'
          ),
          array (
            'name' => 'Dehydrated lucerne manufacturing'
          ),
          array (
            'name' => 'Dog and cat biscuit manufacturing'
          ),
          array (
            'name' => 'Fodder, prepared, manufacturing'
          ),
          array (
            'name' => 'Grain offal manufacturing (for fodder; except from rice or rye)'
          ),
          array (
            'name' => 'Lucerne cube manufacturing'
          ),
          array (
            'name' => 'Lucerne meal manufacturing'
          ),
          array (
            'name' => 'Pet food, canned, manufacturing'
          ),
          array (
            'name' => 'Poultry feed, prepared, manufacturing'
          ),
          array (
            'name' => 'Sheep lick manufacturing'
          )
        )
      ),
      array (
        'name' => 'Food product manufacturing (not elsewhere classified)',
        'code' => '21790',
        'cover_plus' => '0.95',
        'cover_plus_extra' => '1.27',
        'activities' => array (
          array (
            'name' => 'Coffee manufacturing',
            'bic_code' => 'C119910'
          ),
          array (
            'name' => 'Colouring, food, manufacturing',
            'bic_code' => 'C119915'
          ),
          array (
            'name' => 'Dessert mix, liquid, manufacturing',
            'bic_code' => 'C119920'
          ),
          array (
            'name' => 'Egg pulping or drying',
            'bic_code' => 'C119925'
          ),
          array (
            'name' => 'Flavoured water pack manufacturing (for freezing into flavoured ice)',
            'bic_code' => 'C119930'
          ),
          array (
            'name' => 'Food dressing manufacturing',
            'bic_code' => 'C119935'
          ),
          array (
            'name' => 'Food flavouring manufacturing',
            'bic_code' => 'C119940'
          ),
          array (
            'name' => 'Food manufacturing (not elsewhere classified)',
            'bic_code' => 'C119945'
          ),
          array (
            'name' => 'Gelatine manufacturing',
            'bic_code' => 'C119950'
          ),
          array (
            'name' => 'Ginger product manufacturing (except confectionery)',
            'bic_code' => 'C119955'
          ),
          array (
            'name' => 'Health supplement manufacturing',
            'bic_code' => 'C119960'
          ),
          array (
            'name' => 'Herb, processed, manufacturing',
            'bic_code' => 'C119965'
          ),
          array (
            'name' => 'Honey, blended, manufacturing',
            'bic_code' => 'C119970'
          ),
          array (
            'name' => 'Hop extract, concentrated, manufacturing',
            'bic_code' => 'C119975'
          ),
          array (
            'name' => 'Jelly crystal manufacturing'
          ),
          array (
            'name' => 'Pre-prepared meal, frozen, manufacturing'
          ),
          array (
            'name' => 'Rice preparation manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Salt, cooking or table, manufacturing'
          ),
          array (
            'name' => 'Savoury speciality manufacturing'
          ),
          array (
            'name' => 'Seasoning, food, manufacturing'
          ),
          array (
            'name' => 'Soya bean concentrate, isolate or textured protein manufacturing'
          ),
          array (
            'name' => 'Spice manufacturing'
          ),
          array (
            'name' => 'Tea blending'
          ),
          array (
            'name' => 'Tea manufacturing'
          ),
          array (
            'name' => 'Worcestershire sauce manufacturing'
          ),
          array (
            'name' => 'Yeast or yeast extract manufacturing'
          )
        )
      ),
      array (
        'name' => 'Soft drink, cordial, and syrup manufacturing',
        'code' => '21810',
        'cover_plus' => '0.49',
        'cover_plus_extra' => '0.67',
        'activities' => array (
          array (
            'name' => 'Carbonated water or cordial manufacturing',
            'bic_code' => 'C121110'
          ),
          array (
            'name' => 'Cider, non-alcoholic, manufacturing',
            'bic_code' => 'C121120'
          ),
          array (
            'name' => 'Cordial manufacturing',
            'bic_code' => 'C121130'
          ),
          array (
            'name' => 'Energy drink manufacturing',
            'bic_code' => 'C121140'
          ),
          array (
            'name' => 'Fruit drink, less than 100 percent pure juice,manufacturing',
            'bic_code' => 'C121150'
          ),
          array (
            'name' => 'Ginger beer, non-alcoholic, manufacturing',
            'bic_code' => 'C121160'
          ),
          array (
            'name' => 'Ice manufacturing (except dry ice)',
            'bic_code' => 'C121170'
          ),
          array (
            'name' => 'Mineral water manufacturing',
            'bic_code' => 'C121180'
          ),
          array (
            'name' => 'Powder flavour manufacturing (for soft drinks)'
          ),
          array (
            'name' => 'Purified water manufacturing'
          ),
          array (
            'name' => 'Soda water manufacturing'
          ),
          array (
            'name' => 'Soft drink manufacturing'
          ),
          array (
            'name' => 'Syrup, chocolate, caramel or vanilla, manufacturing'
          ),
          array (
            'name' => 'Syrup, fruit, manufacturing'
          ),
          array (
            'name' => 'Tonic water manufacturing'
          )
        )
      ),
      array (
        'name' => 'Beer manufacturing',
        'code' => '21820',
        'cover_plus' => '0.49',
        'cover_plus_extra' => '0.67',
        'activities' => array (
          array (
            'name' => 'Beer manufacturing (except non-alcoholic beer',
            'bic_code' => array (
              'C121210',
              'C121220'
            )
          )
        )
      ),
      array (
        'name' => 'Spirit manufacturing',
        'code' => '21840',
        'cover_plus' => '0.49',
        'cover_plus_extra' => '0.67',
        'activities' => array (
          array (
            'name' => 'Brandy manufacturing',
            'bic_code' => 'C121310'
          ),
          array (
            'name' => 'Fortified spirit manufacturing',
            'bic_code' => 'C121320'
          ),
          array (
            'name' => 'Liqueur manufacturing',
            'bic_code' => 'C121330'
          ),
          array (
            'name' => 'Potable spirit manufacturing'
          ),
          array (
            'name' => 'Spirit-based mixed drink manufacturing'
          )
        )
      ),
      array (
        'name' => 'Wine and alcoholic beverage manufacturing (not elsewhere classified)',
        'code' => '21830',
        'cover_plus' => '0.49',
        'cover_plus_extra' => '0.67',
        'activities' => array (
          array (
            'name' => 'Beverage (not elsewhere classified), alcoholic,manufacturing',
            'bic_code' => 'C121410'
          ),
          array (
            'name' => 'Carbonated wine manufacturing',
            'bic_code' => 'C121420'
          ),
          array (
            'name' => 'Cider, alcoholic, manufacturing',
            'bic_code' => 'C121430'
          ),
          array (
            'name' => 'Fortified wine manufacturing',
            'bic_code' => 'C121440'
          ),
          array (
            'name' => 'Mead manufacturing',
            'bic_code' => 'C121450'
          ),
          array (
            'name' => 'Perry, alcoholic, manufacturing',
            'bic_code' => 'C121460'
          ),
          array (
            'name' => 'Sherry manufacturing'
          ),
          array (
            'name' => 'Sparkling wine manufacturing'
          ),
          array (
            'name' => 'Unfortified wine manufacturing'
          ),
          array (
            'name' => 'Wine manufacturing'
          ),
          array (
            'name' => 'Wine vinegar manufacturing'
          ),
          array (
            'name' => 'Wine-based fruit drink ‘cooler’ manufacturing'
          )
        )
      ),
      array (
        'name' => 'Cigarette and tobacco product manufacturing',
        'code' => '21900',
        'cover_plus' => '0.49',
        'cover_plus_extra' => '0.67',
        'activities' => array (
          array (
            'name' => 'Chewing tobacco manufacturing',
            'bic_code' => 'C122010'
          ),
          array (
            'name' => 'Cigar manufacturing',
            'bic_code' => 'C122020'
          ),
          array (
            'name' => 'Cigarette manufacturing'
          ),
          array (
            'name' => 'Pipe tobacco manufacturing'
          ),
          array (
            'name' => 'Snuff manufacturing'
          ),
          array (
            'name' => 'Tobacco leaf redrying'
          ),
          array (
            'name' => 'Tobacco manufacturing'
          )
        )
      ),
      array (
        'name' => 'Wool scouring',
        'code' => '22110',
        'cover_plus' => '1.66',
        'cover_plus_extra' => '2.19',
        'activities' => array (
          array (
            'name' => 'Lanolin manufacturing',
            'bic_code' => 'C131110'
          ),
          array (
            'name' => 'Noil, wool, manufacturing',
            'bic_code' => 'C131120'
          ),
          array (
            'name' => 'Scoured wool manufacturing',
            'bic_code' => 'C131130'
          ),
          array (
            'name' => 'Slag wool manufacturing'
          ),
          array (
            'name' => 'Tops, unspun wool, manufacturing'
          ),
          array (
            'name' => 'Wool grease manufacturing'
          ),
          array (
            'name' => 'Wool wax manufacturing'
          ),
          array (
            'name' => 'Wool, carded or combed, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Wool wholesaling',
        'code' => '45111',
        'cover_plus' => '0.27',
        'cover_plus_extra' => '0.38',
        'activities' => array (
          array (
            'name' => 'Wool wholesaling',
            'bic_code' => 'F331110'
          )
        )
      ),
      array (
        'name' => 'Natural textile manufacturing',
        'code' => '22140',
        'cover_plus' => '1.28',
        'cover_plus_extra' => '1.12',
        'activities' => array (
          array (
            'name' => 'Cotton sewing thread manufacturing',
            'bic_code' => 'C131210'
          ),
          array (
            'name' => 'Tow manufacturing (from flax, hemp or jute)',
            'bic_code' => 'C131220'
          ),
          array (
            'name' => 'Tyre cord yarn or fabric, cotton, manufacturing',
            'bic_code' => 'C131230'
          ),
          array (
            'name' => 'Woven fabric, cotton, manufacturing',
            'bic_code' => 'C131235'
          ),
          array (
            'name' => 'Woven fabric, woollen or worsted wool,manufacturing',
            'bic_code' => 'C131240'
          ),
          array (
            'name' => 'Yarn, cotton, flax or silk, manufacturing',
            'bic_code' => 'C131250'
          ),
          array (
            'name' => 'Yarn, woollen, manufacturing',
            'bic_code' => 'C131260'
          )
        )
      ),
      array (
        'name' => 'Synthetic textile manufacturing',
        'code' => '22120',
        'cover_plus' => '0.83',
        'cover_plus_extra' => '1.12',
        'activities' => array (
          array (
            'name' => 'Fabric, woven, manufacturing (elastic or elastomeric',
            'bic_code' => 'C131310'
          ),
          array (
            'name' => 'Fabric, woven, manufacturing (predominantly of synthetic fibre)',
            'bic_code' => 'C131320'
          ),
          array (
            'name' => 'Fibreglass fabric manufacturing',
            'bic_code' => 'C131330'
          ),
          array (
            'name' => 'Lacing, woven, manufacturing',
            'bic_code' => 'C131340'
          ),
          array (
            'name' => 'Tyre cord yarn or fabric, synthetic fibre, manufacturing'
          ),
          array (
            'name' => 'Yarn, elastic or elastomeric, manufacturing'
          ),
          array (
            'name' => 'Yarn, synthetic fibre, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Leather tanning, fellmongery, and fur dressing',
        'code' => '22611',
        'cover_plus' => '1.66',
        'cover_plus_extra' => '2.19',
        'activities' => array (
          array (
            'name' => 'Bleaching and currying fur',
            'bic_code' => 'C132020'
          ),
          array (
            'name' => 'Currying hides',
            'bic_code' => 'C132050'
          ),
          array (
            'name' => 'Embossing hides and skins',
            'bic_code' => 'C132070'
          ),
          array (
            'name' => 'Fellmongery operation'
          ),
          array (
            'name' => 'Finishing hides and skins'
          ),
          array (
            'name' => 'Fur rug manufacturing'
          ),
          array (
            'name' => 'Fur skin dressing or dyeing'
          ),
          array (
            'name' => 'Japanning hides and skins'
          ),
          array (
            'name' => 'Pelt finishing and tanning'
          ),
          array (
            'name' => 'Pulling sheep and lamb skin'
          ),
          array (
            'name' => 'Scraping fur and pelt'
          ),
          array (
            'name' => 'Slipe wool manufacturing'
          ),
          array (
            'name' => 'Tanning hides and skins'
          )
        )
      ),
      array (
        'name' => 'Leather and leather substitute goods manufacturing',
        'code' => '22620',
        'cover_plus' => '0.53',
        'cover_plus_extra' => '0.73',
        'activities' => array (
          array (
            'name' => 'Bag, leather or leather substitute, manufacturing',
            'bic_code' => 'C132010'
          ),
          array (
            'name' => 'Handbag manufacturing (including metal mesh handbags)',
            'bic_code' => 'C132030'
          ),
          array (
            'name' => 'Harness manufacturing',
            'bic_code' => 'C132040'
          ),
          array (
            'name' => 'Leather or leather substitute goods manufacturing (not elsewhere classified)',
            'bic_code' => 'C132060'
          ),
          array (
            'name' => 'Leather packing, industrial, manufacturing',
            'bic_code' => 'C132080'
          ),
          array (
            'name' => 'Machine belting, leather or leather substitute, manufacturing',
            'bic_code' => 'C132090'
          ),
          array (
            'name' => 'Saddle manufacturing'
          ),
          array (
            'name' => 'Seat cover, sheepskin, manufacturing'
          ),
          array (
            'name' => 'Suitcase manufacturing (including canvas)'
          ),
          array (
            'name' => 'Toy, leather, manufacturing'
          ),
          array (
            'name' => 'Wallet manufacturing (including metal mesh wallets)'
          )
        )
      ),
      array (
        'name' => 'Textile floor-covering manufacturing',
        'code' => '22220',
        'cover_plus' => '0.83',
        'cover_plus_extra' => '1.12',
        'activities' => array (
          array (
            'name' => 'Carpet manufacturing',
            'bic_code' => 'C133110'
          ),
          array (
            'name' => 'Carpet tile manufacturing',
            'bic_code' => 'C133120'
          ),
          array (
            'name' => 'Floor covering, textile, manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Floor rug, textile, manufacturing'
          ),
          array (
            'name' => 'Hard fibre floor covering manufacturing (including sisal, coir and grass mat manufacturing)'
          ),
          array (
            'name' => 'Jute matting manufacturing'
          )
        )
      ),
      array (
        'name' => 'Rope, cordage, and twine manufacturing',
        'code' => '22230',
        'cover_plus' => '0.83',
        'cover_plus_extra' => '1.12',
        'activities' => array (
          array (
            'name' => 'Cable manufacturing (from natural or synthetic fibres)',
            'bic_code' => 'C133210'
          ),
          array (
            'name' => 'Cord manufacturing (except wire rope or tyre cord)',
            'bic_code' => 'C133220'
          ),
          array (
            'name' => 'Cordage manufacturing',
            'bic_code' => 'C133230'
          ),
          array (
            'name' => 'Fish net manufacturing',
            'bic_code' => 'C133240'
          ),
          array (
            'name' => 'Net manufacturing (not elsewhere classified)',
            'bic_code' => 'C133250'
          ),
          array (
            'name' => 'Netting, textile, manufacturing'
          ),
          array (
            'name' => 'Rope manufacturing (except wire rope)'
          ),
          array (
            'name' => 'String manufacturing'
          ),
          array (
            'name' => 'Twine manufacturing'
          )
        )
      ),
      array (
        'name' => 'Cut and sewn textile product manufacturing',
        'code' => '22210',
        'cover_plus' => '0.53',
        'cover_plus_extra' => '0.73',
        'activities' => array (
          array (
            'name' => 'Animal blanket/cover manufacturing',
            'bic_code' => 'C133310'
          ),
          array (
            'name' => 'Awning, textile, manufacturing',
            'bic_code' => 'C133315'
          ),
          array (
            'name' => 'Bag or sack, textile or canvas, manufacturing (for packaging)',
            'bic_code' => 'C133320'
          ),
          array (
            'name' => 'Bed linen manufacturing',
            'bic_code' => 'C133325'
          ),
          array (
            'name' => 'Blind, textile, manufacturing (including plastic coated)',
            'bic_code' => 'C133330'
          ),
          array (
            'name' => 'Canvas goods manufacturing (not elsewhere classified)',
            'bic_code' => 'C133335'
          ),
          array (
            'name' => 'Cotton textile furnishing manufacturing',
            'bic_code' => 'C133340'
          ),
          array (
            'name' => 'Curtain manufacturing',
            'bic_code' => 'C133345'
          ),
          array (
            'name' => 'Cushion manufacturing (except rubber)',
            'bic_code' => 'C133350'
          ),
          array (
            'name' => 'Flag or banner, manufacturing',
            'bic_code' => 'C133355'
          ),
          array (
            'name' => 'Hose, canvas, manufacturing',
            'bic_code' => 'C133360'
          ),
          array (
            'name' => 'Life jacket manufacturing',
            'bic_code' => 'C133365'
          ),
          array (
            'name' => 'Motor vehicle cover manufacturing',
            'bic_code' => 'C133370'
          ),
          array (
            'name' => 'Parachute manufacturing',
            'bic_code' => 'C133375'
          ),
          array (
            'name' => 'Pillow manufacturing (except rubber)',
            'bic_code' => 'C133380'
          ),
          array (
            'name' => 'Sail manufacturing ',
            'bic_code' => 'C133385'
          ),
          array (
            'name' => 'Seat cover, textile, manufacturing (except sheepskin)',
            'bic_code' => 'C133390'
          ),
          array (
            'name' => 'Sleeping bag manufacturing',
            'bic_code' => 'C133395'
          ),
          array (
            'name' => 'Soft furnishing manufacturing'
          ),
          array (
            'name' => 'Synthetic fibre textile furnishing manufacturing'
          ),
          array (
            'name' => 'Tent manufacturing (except oxygen tents or toy tents)'
          ),
          array (
            'name' => 'Textile furnishing manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Woollen textile furnishing manufacturing'
          )
        )
      ),
      array (
        'name' => 'Textile finishing and textile product manufacturing (not elsewhere classified)',
        'code' => '22290',
        'cover_plus' => '0.54',
        'cover_plus_extra' => '0.74',
        'activities' => array (
          array (
            'name' => 'Badge, woven, manufacturing',
            'bic_code' => 'C133410'
          ),
          array (
            'name' => 'Binding, textile, manufacturing',
            'bic_code' => 'C133430'
          ),
          array (
            'name' => 'Embroidered apparel manufacturing',
            'bic_code' => 'C133440'
          ),
          array (
            'name' => 'Embroidered fabric manufacturing',
            'bic_code' => 'C133450'
          ),
          array (
            'name' => 'Felt manufacturing',
            'bic_code' => 'C133455'
          ),
          array (
            'name' => 'Label, printed cloth, manufacturing',
            'bic_code' => 'C133460'
          ),
          array (
            'name' => 'Label, woven cloth, manufacturing',
            'bic_code' => 'C133470'
          ),
          array (
            'name' => 'Textile dyeing',
            'bic_code' => 'C133480'
          ),
          array (
            'name' => 'Textile fabric coating',
            'bic_code' => 'C133490'
          ),
          array (
            'name' => 'Textile printing (except screen printing)'
          ),
          array (
            'name' => 'Textile product manufacturing (not elsewhereclassified)'
          ),
          array (
            'name' => 'Underfelt manufacturing'
          )
        )
      ),
      array (
        'name' => 'Knitted product manufacturing',
        'code' => '22390',
        'cover_plus' => '0.53',
        'cover_plus_extra' => '0.73',
        'activities' => array (
          array (
            'name' => 'Clothing, knitted, manufacturing',
            'bic_code' => 'C134010'
          ),
          array (
            'name' => 'Crocheted fabric manufacturing',
            'bic_code' => 'C134020'
          ),
          array (
            'name' => 'Custom knitting of pullovers or cardigans',
            'bic_code' => 'C134025'
          ),
          array (
            'name' => 'Hosiery manufacturing',
            'bic_code' => 'C134030'
          ),
          array (
            'name' => 'Jacket, knitted, manufacturing',
            'bic_code' => 'C134040'
          ),
          array (
            'name' => 'Jersey, knitted, manufacturing',
            'bic_code' => 'C134045'
          ),
          array (
            'name' => 'Knitted fabric manufacturing',
            'bic_code' => 'C134050'
          ),
          array (
            'name' => 'Panty hose manufacturing',
            'bic_code' => 'C134060'
          ),
          array (
            'name' => 'Sock manufacturing',
            'bic_code' => 'C134070'
          ),
          array (
            'name' => 'Stocking manufacturing',
            'bic_code' => 'C134080'
          ),
          array (
            'name' => 'Tights manufacturing'
          )
        )
      ),
      array (
        'name' => 'Clothing manufacturing',
        'code' => '22420',
        'cover_plus' => '0.53',
        'cover_plus_extra' => '0.73',
        'activities' => array (
          array (
            'name' => 'Belt manufacturing (for clothing)',
            'bic_code' => 'C135103'
          ),
          array (
            'name' => 'Clothing accessory manufacturing (not elsewhere classified)',
            'bic_code' => 'C135107'
          ),
          array (
            'name' => 'Clothing manufacturing (not elsewhere classified)',
            'bic_code' => 'C135110'
          ),
          array (
            'name' => 'Clothing, fur, manufacturing',
            'bic_code' => 'C135113'
          ),
          array (
            'name' => 'Clothing, leather, manufacturing',
            'bic_code' => 'C135117'
          ),
          array (
            'name' => 'Clothing, plastic or rubber, manufacturing',
            'bic_code' => 'C135120'
          ),
          array (
            'name' => 'Glove manufacturing (except rubber)',
            'bic_code' => 'C135123'
          ),
          array (
            'name' => 'Handkerchief manufacturing',
            'bic_code' => 'C135127'
          ),
          array (
            'name' => 'Hat and cap manufacturing',
            'bic_code' => 'C135130'
          ),
          array (
            'name' => 'Headwear manufacturing',
            'bic_code' => 'C135133'
          ),
          array (
            'name' => 'Helmet, fabric or leather, manufacturing',
            'bic_code' => 'C135137'
          ),
          array (
            'name' => 'Infants’ clothing manufacturing',
            'bic_code' => 'C135140'
          ),
          array (
            'name' => 'Jeans manufacturing',
            'bic_code' => 'C135143'
          ),
          array (
            'name' => 'Laces manufacturing',
            'bic_code' => 'C135147'
          ),
          array (
            'name' => 'Men’s and boys’ wear manufacturing',
            'bic_code' => 'C135150'
          ),
          array (
            'name' => 'Outerwear manufacturing',
            'bic_code' => 'C135153'
          ),
          array (
            'name' => 'Sleepwear manufacturing',
            'bic_code' => 'C135157'
          ),
          array (
            'name' => 'Swimwear manufacturing',
            'bic_code' => 'C135160'
          ),
          array (
            'name' => 'Tie manufacturing',
            'bic_code' => 'C135163'
          ),
          array (
            'name' => 'Underwear manufacturing',
            'bic_code' => 'C135167'
          ),
          array (
            'name' => 'Uniform manufacturing',
            'bic_code' => 'C135170'
          ),
          array (
            'name' => 'Waterproof clothing manufacturing',
            'bic_code' => 'C135173'
          ),
          array (
            'name' => 'Wetsuit manufacturing',
            'bic_code' => 'C135177'
          ),
          array (
            'name' => 'Women’s and girls’ wear manufacturing',
            'bic_code' => 'C135180'
          ),
          array (
            'name' => 'Workwear manufacturing',
            'bic_code' => array (
              'C135183',
              'C135190',
              'C135193',
              'C135196',
              'C135198'
            )
          )
        )
      ),
      array (
        'name' => 'Footwear manufacturing',
        'code' => '22500',
        'cover_plus' => '0.53',
        'cover_plus_extra' => '0.73',
        'activities' => array (
          array (
            'name' => 'Boot manufacturing',
            'bic_code' => 'C135210'
          ),
          array (
            'name' => 'Footwear component manufacturing',
            'bic_code' => 'C135220'
          ),
          array (
            'name' => 'Footweasr manufacturing (including safety or protective footwear)',
            'bic_code' => 'C135230'
          ),
          array (
            'name' => 'Orthopaedic shoe manufacturing (excluding  orthopaedic extension footwear)',
            'bic_code' => 'C135240'
          ),
          array (
            'name' => 'Sandal manufacturing',
            'bic_code' => 'C135250'
          ),
          array (
            'name' => 'Shoe manufacturing',
            'bic_code' => 'C135260'
          ),
          array (
            'name' => 'Slipper manufacturing'
          )
        )
      ),
      array (
        'name' => 'Log sawmilling',
        'code' => '23110',
        'cover_plus' => '1.71',
        'cover_plus_extra' => '2.27',
        'activities' => array (
          array (
            'name' => 'Bark, ground, manufacturing',
            'bic_code' => 'C141110'
          ),
          array (
            'name' => 'Log sawmilling',
            'bic_code' => 'C141130'
          ),
          array (
            'name' => 'Rough sawn timber manufacturing',
            'bic_code' => 'C141140'
          ),
          array (
            'name' => 'Shook manufacturing (for containers)'
          )
        )
      ),
      array (
        'name' => 'Wood chipping',
        'code' => '23120',
        'cover_plus' => '1.71',
        'cover_plus_extra' => '2.27',
        'activities' => array (
          array (
            'name' => 'Hardwood wood chip manufacturing',
            'bic_code' => 'C141210'
          ),
          array (
            'name' => 'Softwood wood chip manufacturing'
          )
        )
      ),
      array (
        'name' => 'Timber resawing and dressing',
        'code' => '23130',
        'cover_plus' => '1.71',
        'cover_plus_extra' => '2.27',
        'activities' => array (
          array (
            'name' => 'Air-drying timber',
            'bic_code' => 'C141305'
          ),
          array (
            'name' => 'Building timber manufacturing',
            'bic_code' => 'C141310'
          ),
          array (
            'name' => 'Chemically preserving timber (except chemical preservation of logs sawn at the same unit)',
            'bic_code' => 'C141320'
          ),
          array (
            'name' => 'Dressed timber or moulding manufacturing',
            'bic_code' => 'C141325'
          ),
          array (
            'name' => 'Kiln drying timber'
          ),
          array (
            'name' => 'Seasoning timber'
          ),
          array (
            'name' => 'Wooden flooring manufacturing (solid timber only)'
          )
        )
      ),
      array (
        'name' => 'Timber wholesaling',
        'code' => '45310',
        'cover_plus' => '1.55',
        'cover_plus_extra' => '2.05',
        'activities' => array (
          array (
            'name' => 'Plywood wholesaling',
            'bic_code' => 'F333110'
          ),
          array (
            'name' => 'Timber dealing, wholesaling, (except firewood)',
            'bic_code' => 'F333120'
          ),
          array (
            'name' => 'Veneer, wood, wholesaling'
          )
        )
      ),
      array (
        'name' => 'Prefabricated wooden building manufacturing',
        'code' => '29190',
        'cover_plus' => '1.55',
        'cover_plus_extra' => '2.05',
        'activities' => array (
          array (
            'name' => 'Building, prefabricated wood, manufacturing',
            'bic_code' => 'C149110'
          ),
          array (
            'name' => 'Bus shelter, prefabricated wood, manufacturing',
            'bic_code' => 'C149120'
          ),
          array (
            'name' => 'Carport, prefabricated wood, manufacturing',
            'bic_code' => 'C149130'
          ),
          array (
            'name' => 'Conservatory, prefabricated wood, manufacturing',
            'bic_code' => 'C149140'
          ),
          array (
            'name' => 'Garage, prefabricated wood, manufacturing',
            'bic_code' => 'C222240'
          ),
          array (
            'name' => 'Gazebo, prefabricated wood, manufacturing'
          ),
          array (
            'name' => 'Kit set home, prefabricated wood, manufacturing'
          ),
          array (
            'name' => 'Shed, prefabricated wood, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Wooden structural fittings and components manufacturing',
        'code' => '23230',
        'cover_plus' => '1.55',
        'cover_plus_extra' => '2.05',
        'activities' => array (
          array (
            'name' => 'Finger-jointing manufacturing',
            'bic_code' => 'C149210'
          ),
          array (
            'name' => 'Glue laminated lumber (Glulam) manufacturing',
            'bic_code' => 'C149220'
          ),
          array (
            'name' => 'Roof truss, wooden, manufacturing',
            'bic_code' => 'C149230'
          ),
          array (
            'name' => 'Wood or wood-framed door manufacturing',
            'bic_code' => 'C149240'
          ),
          array (
            'name' => 'Wooden kitchen cabinet manufacturing',
            'bic_code' => 'C149250'
          ),
          array (
            'name' => 'Wooden structural component/fitting manufacturing',
            'bic_code' => 'C149260'
          ),
          array (
            'name' => 'Wood-framed window manufacturing'
          )
        )
      ),
      array (
        'name' => 'Veneer and plywood manufacturing',
        'code' => '23210',
        'cover_plus' => '1.55',
        'cover_plus_extra' => '2.05',
        'activities' => array (
          array (
            'name' => 'Core, plywood or veneer, manufacturing',
            'bic_code' => 'C149305'
          ),
          array (
            'name' => 'Laminated veneer lumber (LVL) manufacturing',
            'bic_code' => 'C149310'
          ),
          array (
            'name' => 'Plywood manufacturing',
            'bic_code' => 'C149320'
          ),
          array (
            'name' => 'Veneer manufacturing'
          )
        )
      ),
      array (
        'name' => 'Reconstituted wood product manufacturing',
        'code' => '23220',
        'cover_plus' => '0.64',
        'cover_plus_extra' => '0.87',
        'activities' => array (
          array (
            'name' => 'Chip board manufacturing',
            'bic_code' => 'C149410'
          ),
          array (
            'name' => 'Corestock manufacturing',
            'bic_code' => 'C149420'
          ),
          array (
            'name' => 'Fibreboard manufacturing',
            'bic_code' => 'C149430'
          ),
          array (
            'name' => 'Hardboard manufacturing',
            'bic_code' => 'C149440'
          ),
          array (
            'name' => 'Laminations of timber and non-timber materialsmanufacturing',
            'bic_code' => 'C149450'
          ),
          array (
            'name' => 'Medium density fibreboard (MDF) manufacturing'
          ),
          array (
            'name' => 'Oriented strand board (OSB) manufacturing'
          ),
          array (
            'name' => 'Particleboard manufacturing'
          )
        )
      ),
      array (
        'name' => 'Wood product manufacturing (not elsewhere classified)',
        'code' => '23290',
        'cover_plus' => '1.55',
        'cover_plus_extra' => '2.05',
        'activities' => array (
          array (
            'name' => 'Blind or shutter, wooden, manufacturing',
            'bic_code' => 'C149910'
          ),
          array (
            'name' => 'Container, wooden, manufacturing',
            'bic_code' => 'C149915'
          ),
          array (
            'name' => 'Ornamental woodwork manufacturing',
            'bic_code' => 'C149920'
          ),
          array (
            'name' => 'Pallet (wooden) manufacturing',
            'bic_code' => 'C149930'
          ),
          array (
            'name' => 'Picture or mirror frame, wooden, manufacturing',
            'bic_code' => 'C149940'
          ),
          array (
            'name' => 'Tool handle, wooden, manufacturing',
            'bic_code' => 'C149960'
          ),
          array (
            'name' => 'Trellis, wooden, manufacturing',
            'bic_code' => 'C251910'
          ),
          array (
            'name' => 'Wood product manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Wood turning'
          )
        )
      ),
      array (
        'name' => 'Pulp, paper, and paperboard manufacturing',
        'code' => '23310',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Newsprint manufacturing',
            'bic_code' => 'C151010'
          ),
          array (
            'name' => 'Paper manufacturing',
            'bic_code' => 'C151020'
          ),
          array (
            'name' => 'Paper pulp manufacturing',
            'bic_code' => 'C151030'
          ),
          array (
            'name' => 'Paperboard manufacturing',
            'bic_code' => 'C151040'
          ),
          array (
            'name' => 'Wood pulp manufacturing',
            'bic_code' => array (
              'C151050',
              'C151060'
            )
          )
        )
      ),
      array (
        'name' => 'Corrugated paperboard and paperboard container manufacturing',
        'code' => '23330',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Corrugated paperboard container manufacturing',
            'bic_code' => 'C152110'
          ),
          array (
            'name' => 'Corrugated paperboard manufacturing',
            'bic_code' => 'C152120'
          ),
          array (
            'name' => 'Paperboard container manufacturing',
            'bic_code' => 'C152130'
          )
        )
      ),
      array (
        'name' => 'Paper bag and sack manufacturing',
        'code' => '23340',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Paper bag manufacturing',
            'bic_code' => array (
              'C152210',
              'C152220'
            )
          )
        )
      ),
      array (
        'name' => 'Paper stationery manufacturing',
        'code' => '24110',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Paper label manufacturing (except adhesive)',
            'bic_code' => 'C152310'
          ),
          array (
            'name' => 'Paper stationery manufacturing',
            'bic_code' => 'C152320'
          ),
          array (
            'name' => 'Paperboard game manufacturing',
            'bic_code' => 'C152330'
          ),
          array (
            'name' => 'Paperboard toy manufacturing',
            'bic_code' => 'C152340'
          ),
          array (
            'name' => 'Playing cards manufacturing'
          )
        )
      ),
      array (
        'name' => 'Sanitary paper product manufacturing',
        'code' => '23391',
        'cover_plus' => '0.64',
        'cover_plus_extra' => '0.87',
        'activities' => array (
          array (
            'name' => 'Disposable paper nappy (cellulose-based) manufacturing',
            'bic_code' => 'C152410'
          ),
          array (
            'name' => 'Facial tissue manufacturing',
            'bic_code' => 'C152420'
          ),
          array (
            'name' => 'Paper napkin manufacturing',
            'bic_code' => 'C152430'
          ),
          array (
            'name' => 'Paper towel manufacturing',
            'bic_code' => 'C152440'
          ),
          array (
            'name' => 'Sanitary napkin (cellulose-based) manufacturing',
            'bic_code' => 'C152450'
          ),
          array (
            'name' => 'Sanitary paper product manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Tampon (cellulose-based) manufacturing'
          ),
          array (
            'name' => 'Toilet tissue manufacturing'
          )
        )
      ),
      array (
        'name' => 'Converted paper product manufacturing (not elsewhere classified)',
        'code' => '23390',
        'cover_plus' => '0.64',
        'cover_plus_extra' => '0.87',
        'activities' => array (
          array (
            'name' => 'Adhesive paper label manufacturing',
            'bic_code' => 'C152910'
          ),
          array (
            'name' => 'Moulded paper pulp product (e.g. egg trays or cartons) manufacturing',
            'bic_code' => 'C152920'
          ),
          array (
            'name' => 'Paper product manufacturing (not elsewhere classified)',
            'bic_code' => 'C152930'
          ),
          array (
            'name' => 'Wallpaper manufacturing',
            'bic_code' => 'C182910'
          )
        )
      ),
      array (
        'name' => 'Printing',
        'code' => '24120',
        'cover_plus' => '0.33',
        'cover_plus_extra' => '0.47',
        'activities' => array (
          array (
            'name' => 'Digital printing',
            'bic_code' => 'C161110'
          ),
          array (
            'name' => 'Off-set lithographic printing',
            'bic_code' => 'C161120'
          ),
          array (
            'name' => 'Photocopying service',
            'bic_code' => 'C161130'
          ),
          array (
            'name' => 'Relief printing, including letterpress and flexographic printing',
            'bic_code' => 'C161140'
          ),
          array (
            'name' => 'Screen printing on made-up clothing',
            'bic_code' => 'C161150'
          ),
          array (
            'name' => 'Seriography (screen printing)',
            'bic_code' => array (
              'C161160',
              'J541120',
              'J541220',
              'J541207',
              'J541320'
            )
          )
        )
      ),
      array (
        'name' => 'Printing support services',
        'code' => '24130',
        'cover_plus' => '0.33',
        'cover_plus_extra' => '0.47',
        'activities' => array (
          array (
            'name' => 'Book repair service',
            'bic_code' => 'C161210'
          ),
          array (
            'name' => 'Bookbinding service',
            'bic_code' => 'C161220'
          ),
          array (
            'name' => 'Colour separation service, printing',
            'bic_code' => 'C161230'
          ),
          array (
            'name' => 'Image setting service, printing',
            'bic_code' => 'C161240'
          ),
          array (
            'name' => 'Laser engraving',
            'bic_code' => 'C161250'
          ),
          array (
            'name' => 'Platemaking service, printing',
            'bic_code' => 'C161255'
          ),
          array (
            'name' => 'Pre-press printing service',
            'bic_code' => 'C161260'
          ),
          array (
            'name' => 'Printing support service (not elsewhere classified)',
            'bic_code' => 'C161270'
          ),
          array (
            'name' => 'Typesetting service',
            'bic_code' => 'C161280'
          )
        )
      ),
      array (
        'name' => 'Reproduction of recorded media',
        'code' => '24300',
        'cover_plus' => '0.20',
        'cover_plus_extra' => '0.29',
        'activities' => array (
          array (
            'name' => 'Audio tape, pre-recorded, reproduction',
            'bic_code' => 'C162010'
          ),
          array (
            'name' => 'Cassette tape, pre-recorded audio, reproduction',
            'bic_code' => 'C162020'
          ),
          array (
            'name' => 'CD-ROM software, pre-recorded, reproduction',
            'bic_code' => 'C162030'
          ),
          array (
            'name' => 'Compact disc, pre-recorded, reproduction',
            'bic_code' => 'C162040'
          ),
          array (
            'name' => 'Computer tape or disk, pre-recorded, reproduction',
            'bic_code' => 'C162050'
          ),
          array (
            'name' => 'Digital video disc (DVD), pre-recorded, reproduction',
            'bic_code' => 'C162060'
          ),
          array (
            'name' => 'Video tape, pre-recorded, reproduction',
            'bic_code' => 'C162070'
          )
        )
      ),
      array (
        'name' => 'Petroleum refining and petroleum fuel manufacturing',
        'code' => '25100',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Automotive diesel manufacturing',
            'bic_code' => 'C170110'
          ),
          array (
            'name' => 'Automotive petroleum refining',
            'bic_code' => 'C170120'
          ),
          array (
            'name' => 'Aviation fuel (Avgas) manufacturing',
            'bic_code' => 'C170130'
          ),
          array (
            'name' => 'Blending petroleum fuel with ethanol',
            'bic_code' => 'C170140'
          ),
          array (
            'name' => 'Fuel oil manufacturing',
            'bic_code' => 'C170150'
          ),
          array (
            'name' => 'Gas oil manufacturing'
          ),
          array (
            'name' => 'Heating oil manufacturing'
          ),
          array (
            'name' => 'Industrial diesel manufacturing'
          ),
          array (
            'name' => 'Jet fuel manufacturing'
          ),
          array (
            'name' => 'Kerosene manufacturing'
          ),
          array (
            'name' => 'Liquefied petroleum gas (LPG) manufacturing (inconjunction with petroleum refining)'
          ),
          array (
            'name' => 'Motor spirit manufacturing'
          ),
          array (
            'name' => 'Oil or grease base stock manufacturing'
          ),
          array (
            'name' => 'Petroleum refining or blending'
          )
        )
      ),
      array (
        'name' => 'Petroleum and coal product manufacturing (not elsewhere classified)',
        'code' => '25200',
        'cover_plus' => '0.82',
        'cover_plus_extra' => '1.10',
        'activities' => array (
          array (
            'name' => 'Aromatic hydrocarbon manufacturing',
            'bic_code' => 'C170910'
          ),
          array (
            'name' => 'Asphalt and bituminous material manufacturing (except hot-mix bituminous paving)',
            'bic_code' => 'C170920'
          ),
          array (
            'name' => 'Benzene manufacturing',
            'bic_code' => 'C170930'
          ),
          array (
            'name' => 'Bituminous adhesive or mastic manufacturing',
            'bic_code' => 'C170940'
          ),
          array (
            'name' => 'Blending of tar, asphalt and/or bitumen material',
            'bic_code' => 'C170950'
          ),
          array (
            'name' => 'Brake fluid manufacturing',
            'bic_code' => 'C170960'
          ),
          array (
            'name' => 'Char manufacturing'
          ),
          array (
            'name' => 'Emulsion, bituminous, manufacturing'
          ),
          array (
            'name' => 'Fuel briquette manufacturing (except charcoal)'
          ),
          array (
            'name' => 'Grinding oil manufacturing'
          ),
          array (
            'name' => 'Hydraulic fluid manufacturing'
          ),
          array (
            'name' => 'Lubricating oil and grease manufacturing'
          ),
          array (
            'name' => 'Motor oil manufacturing'
          ),
          array (
            'name' => 'Petroleum oil blending'
          ),
          array (
            'name' => 'Recovery of lubricating oil or grease from used petroleum waste products'
          ),
          array (
            'name' => 'Rust arresting compound manufacturing'
          ),
          array (
            'name' => 'Synthetic motor oil manufacturing'
          ),
          array (
            'name' => 'Tar, refined, manufacturing'
          ),
          array (
            'name' => 'Transmission fluid manufacturing'
          )
        )
      ),
      array (
        'name' => 'Industrial gas manufacturing',
        'code' => '25320',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Acetylene gas manufacturing',
            'bic_code' => 'C181110'
          ),
          array (
            'name' => 'Ammonia gas manufacturing',
            'bic_code' => 'C181120'
          ),
          array (
            'name' => 'Argon gas manufacturing',
            'bic_code' => 'C181130'
          ),
          array (
            'name' => 'Arsine gas manufacturing',
            'bic_code' => 'C181140'
          ),
          array (
            'name' => 'Butane gas manufacturing',
            'bic_code' => 'C181150'
          ),
          array (
            'name' => 'Carbon dioxide manufacturing'
          ),
          array (
            'name' => 'Carbon monoxide manufacturing'
          ),
          array (
            'name' => 'Chlorine gas manufacturing'
          ),
          array (
            'name' => 'Dry ice manufacturing'
          ),
          array (
            'name' => 'Hydrogen chloride gas manufacturing'
          ),
          array (
            'name' => 'Hydrogen manufacturing'
          ),
          array (
            'name' => 'Industrial gas manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Inorganic gas manufacturing'
          ),
          array (
            'name' => 'Liquefied natural gas manufacturing'
          ),
          array (
            'name' => 'Medicinal gas manufacturing'
          ),
          array (
            'name' => 'Methane manufacturing'
          ),
          array (
            'name' => 'Nitrogen (gas and liquid) manufacturing'
          ),
          array (
            'name' => 'Nitrous oxide manufacturing'
          ),
          array (
            'name' => 'Organic gas manufacturing'
          ),
          array (
            'name' => 'Oxygen manufacturing'
          ),
          array (
            'name' => 'Propane gas manufacturing'
          )
        )
      ),
      array (
        'name' => 'Basic organic chemical manufacturing',
        'code' => '25340',
        'cover_plus' => '0.27',
        'cover_plus_extra' => '0.38',
        'activities' => array (
          array (
            'name' => 'Acetaldehyde manufacturing',
            'bic_code' => 'C181210'
          ),
          array (
            'name' => 'Acid, acetic, manufacturing',
            'bic_code' => 'C181220'
          ),
          array (
            'name' => 'Acid, organic, manufacturing',
            'bic_code' => 'C181230'
          ),
          array (
            'name' => 'Activated carbon/charcoal manufacturing'
          ),
          array (
            'name' => 'Carbon black manufacturing'
          ),
          array (
            'name' => 'Charcoal briquette manufacturing'
          ),
          array (
            'name' => 'Citric acid manufacturing'
          ),
          array (
            'name' => 'Ethanol manufacturing'
          ),
          array (
            'name' => 'Ether manufacturing'
          ),
          array (
            'name' => 'Ethylene glycol manufacturing'
          ),
          array (
            'name' => 'Extraction and/or distillation of wood and gum'
          ),
          array (
            'name' => 'Formaldehyde manufacturing'
          ),
          array (
            'name' => 'Glycol manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Gum chemical manufacturing'
          ),
          array (
            'name' => 'Industrial alcohol manufacturing'
          ),
          array (
            'name' => 'Lactic acid manufacturing'
          ),
          array (
            'name' => 'Lake colour manufacturing'
          ),
          array (
            'name' => 'Methanol manufacturing'
          ),
          array (
            'name' => 'Organic dye or pigment manufacturing'
          ),
          array (
            'name' => 'Tall oil manufacturing'
          ),
          array (
            'name' => 'Tanning extract, organic, manufacturing'
          ),
          array (
            'name' => 'Turpentine (except mineral turpentine) manufacturing'
          ),
          array (
            'name' => 'Vinyl chloride manufacturing'
          ),
          array (
            'name' => 'Wood tar manufacturing'
          )
        )
      ),
      array (
        'name' => 'Basic inorganic chemical manufacturing',
        'code' => '25350',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Acid, inorganic, manufacturing (not elsewhere classified)',
            'bic_code' => 'C181310'
          ),
          array (
            'name' => 'Alkaline salt manufacturing (not elsewhere classified)',
            'bic_code' => 'C181320'
          ),
          array (
            'name' => 'Aluminium hydroxide manufacturing',
            'bic_code' => 'C181330'
          ),
          array (
            'name' => 'Ammonium hydroxide manufacturing',
            'bic_code' => 'C181340'
          ),
          array (
            'name' => 'Calcium chloride (lime) manufacturing',
            'bic_code' => 'C181350'
          ),
          array (
            'name' => 'Chromium sulphate manufacturing (for application in leather tanning)'
          ),
          array (
            'name' => 'Fluoride manufacturing'
          ),
          array (
            'name' => 'Hydrochloric acid manufacturing'
          ),
          array (
            'name' => 'Hydrofluoric acid manufacturing'
          ),
          array (
            'name' => 'Hydrogen peroxide manufacturing'
          ),
          array (
            'name' => 'Hypophosphite manufacturing'
          ),
          array (
            'name' => 'Industrial salt manufacturing'
          ),
          array (
            'name' => 'Inorganic dye or pigment manufacturing'
          ),
          array (
            'name' => 'Nitric acid manufacturing'
          ),
          array (
            'name' => 'Nitrite manufacturing'
          ),
          array (
            'name' => 'Phosphoric acid manufacturing'
          ),
          array (
            'name' => 'Silicate manufacturing'
          ),
          array (
            'name' => 'Sodium bicarbonate manufacturing'
          ),
          array (
            'name' => 'Sodium carbonate manufacturing'
          ),
          array (
            'name' => 'Sodium hydroxide manufacturing'
          ),
          array (
            'name' => 'Sulphide manufacturing'
          ),
          array (
            'name' => 'Sulphur compound manufacturing'
          ),
          array (
            'name' => 'Sulphuric acid manufacturing (except smelter byproduct)'
          ),
          array (
            'name' => 'Zinc oxide manufacturing'
          ),
          array (
            'name' => 'Zinc peroxide manufacturing'
          )
        )
      ),
      array (
        'name' => 'Synthetic resin and synthetic rubber manufacturing',
        'code' => '25330',
        'cover_plus' => '0.82',
        'cover_plus_extra' => '1.10',
        'activities' => array (
          array (
            'name' => 'Cellulosic resin manufacturing',
            'bic_code' => 'C182110'
          ),
          array (
            'name' => 'Cresol formaldehyde manufacturing',
            'bic_code' => 'C182120'
          ),
          array (
            'name' => 'Dendritic polymer (dendrimer) manufacturing',
            'bic_code' => 'C182130'
          ),
          array (
            'name' => 'Melamine formaldehyde manufacturing',
            'bic_code' => 'C182140'
          ),
          array (
            'name' => 'Non-cellulose resin manufacturing'
          ),
          array (
            'name' => 'Non-vulcanisable elastomer manufacturing'
          ),
          array (
            'name' => 'Phenol formaldehyde manufacturing'
          ),
          array (
            'name' => 'Polyacrylate manufacturing'
          ),
          array (
            'name' => 'Polybutadiene manufacturing'
          ),
          array (
            'name' => 'Polycarbonate manufacturing (except polycarbonate sheet)'
          ),
          array (
            'name' => 'Polymethacrylate manufacturing'
          ),
          array (
            'name' => 'Polypropylene manufacturing'
          ),
          array (
            'name' => 'Polystyrene manufacturing'
          ),
          array (
            'name' => 'Polyurethane manufacturing'
          ),
          array (
            'name' => 'Polyvinyl acetate manufacturing'
          ),
          array (
            'name' => 'Polyvinylchoride (PVC) manufacturing'
          ),
          array (
            'name' => 'Rubber recycling (excluding collection)'
          ),
          array (
            'name' => 'Synthetic resin manufacturing'
          ),
          array (
            'name' => 'Synthetic rubber composite manufacturing'
          ),
          array (
            'name' => 'Synthetic rubber manufacturing'
          ),
          array (
            'name' => 'Urea formaldehyde manufacturing'
          )
        )
      ),
      array (
        'name' => 'Basic polymer manufacturing (not elsewhere classified)',
        'code' => '25360',
        'cover_plus' => '0.82',
        'cover_plus_extra' => '1.10',
        'activities' => array (
          array (
            'name' => 'Basic polymer manufacturing (not elsewhere classified)',
            'bic_code' => 'C182905'
          ),
          array (
            'name' => 'Carbon fibre manufacturing (including kevlar material manufacturing',
            'bic_code' => 'C182920'
          ),
          array (
            'name' => 'Cellulose acetate manufacturing',
            'bic_code' => 'C182930'
          ),
          array (
            'name' => 'Cellulose fibre or filament manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Ethyl cellulose manufacturing'
          ),
          array (
            'name' => 'Methyl cellulose manufacturing'
          ),
          array (
            'name' => 'Methylstyrene manufacturing'
          ),
          array (
            'name' => 'Non-cellulose fibre or filament manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Nylon manufacturing'
          ),
          array (
            'name' => 'Plastic recycling (excluding collection)'
          ),
          array (
            'name' => 'Polyester manufacturing'
          ),
          array (
            'name' => 'Polyolefin manufacturing'
          ),
          array (
            'name' => 'Rayon manufacturing'
          ),
          array (
            'name' => 'Synthetic fibre or filament manufacturing'
          )
        )
      ),
      array (
        'name' => 'Fertiliser manufacturing',
        'code' => '25310',
        'cover_plus' => '0.82',
        'cover_plus_extra' => '1.10',
        'activities' => array (
          array (
            'name' => 'Ammonium phosphate manufacturing',
            'bic_code' => 'C183110'
          ),
          array (
            'name' => 'Ammonium sulphate manufacturing',
            'bic_code' => 'C183120'
          ),
          array (
            'name' => 'Animal and vegetable fertiliser manufacturing'
          ),
          array (
            'name' => 'Bonedust manufacturing'
          ),
          array (
            'name' => 'Bonemeal fertiliser manufacturing'
          ),
          array (
            'name' => 'Calcium sulphate manufacturing'
          ),
          array (
            'name' => 'Controlled release fertiliser preparation manufacturing'
          ),
          array (
            'name' => 'Fertiliser manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Fishmeal fertiliser manufacturing'
          ),
          array (
            'name' => 'Humic substance manufacturing'
          ),
          array (
            'name' => 'Nitrogenous fertiliser material manufacturing'
          ),
          array (
            'name' => 'Phosphate fertiliser material manufacturing'
          ),
          array (
            'name' => 'Potash fertiliser manufacturing'
          ),
          array (
            'name' => 'Potassium chloride fertiliser manufacturing'
          ),
          array (
            'name' => 'Potting mix manufacturing and packaging'
          ),
          array (
            'name' => 'Prilled ammonium nitrate manufacturing'
          ),
          array (
            'name' => 'Sodium nitrate fertiliser manufacturing'
          ),
          array (
            'name' => 'Sulphuric lime manufacturing'
          ),
          array (
            'name' => 'Superphosphate manufacturing'
          ),
          array (
            'name' => 'Urea, fertiliser grade, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Pesticide manufacturing',
        'code' => '25440',
        'cover_plus' => '0.38',
        'cover_plus_extra' => '0.53',
        'activities' => array (
          array (
            'name' => 'Animal dip manufacturing',
            'bic_code' => 'C183210'
          ),
          array (
            'name' => 'Animal spray manufacturing',
            'bic_code' => 'C183220'
          ),
          array (
            'name' => 'Flyspray manufacturing',
            'bic_code' => 'C183230'
          ),
          array (
            'name' => 'Formulated pest control product manufacturing',
            'bic_code' => 'C183240'
          ),
          array (
            'name' => 'Fungicide manufacturing',
            'bic_code' => 'C183250'
          ),
          array (
            'name' => 'Insect repellent manufacturing',
            'bic_code' => 'C183260'
          ),
          array (
            'name' => 'Insecticide manufacturing'
          ),
          array (
            'name' => 'Pesticide manufacturing (not elsewhere classified'
          ),
          array (
            'name' => 'Rat poison manufacturing'
          ),
          array (
            'name' => 'Soil fumigant manufacturing'
          ),
          array (
            'name' => 'Weedkiller manufacturing'
          )
        )
      ),
      array (
        'name' => 'Human pharmaceutical and medicinal product manufacturing',
        'code' => '25430',
        'cover_plus' => '0.38',
        'cover_plus_extra' => '0.53',
        'activities' => array (
          array (
            'name' => 'Ampoule manufacturing',
            'bic_code' => 'C184110'
          ),
          array (
            'name' => 'Analgesic manufacturing',
            'bic_code' => 'C184120'
          ),
          array (
            'name' => 'Anthelmintic manufacturing',
            'bic_code' => 'C184130'
          ),
          array (
            'name' => 'Antibacterial manufacturing',
            'bic_code' => 'C184140'
          ),
          array (
            'name' => 'Antibiotic manufacturing'
          ),
          array (
            'name' => 'Antibody manufacturing'
          ),
          array (
            'name' => 'Antigen manufacturing'
          ),
          array (
            'name' => 'Antitoxin manufacturing'
          ),
          array (
            'name' => 'Biotechnological manufacture of pharmaceutical and medicinal products'
          ),
          array (
            'name' => 'Blood serum manufacturing'
          ),
          array (
            'name' => 'Contraceptive, medicinal, manufacturing (except rubber contraceptives)'
          ),
          array (
            'name' => 'Diagnostic substance manufacturing'
          ),
          array (
            'name' => 'Drug manufacturing (except veterinary'
          ),
          array (
            'name' => 'Herbal drug manufacturing'
          ),
          array (
            'name' => 'Hormone manufacturing (except veterinary'
          ),
          array (
            'name' => 'Medicinal capsule manufacturing'
          ),
          array (
            'name' => 'Medicinal chemical manufacturing'
          ),
          array (
            'name' => 'Medicinal ointment manufacturing'
          ),
          array (
            'name' => 'Medicine manufacturing (except veterinary)'
          ),
          array (
            'name' => 'Morphine manufacturing'
          ),
          array (
            'name' => 'Saccharin manufacturing'
          ),
          array (
            'name' => 'Serum manufacturing'
          ),
          array (
            'name' => 'Vaccine manufacturing (except veterinary)'
          ),
          array (
            'name' => 'Vial manufacturing'
          ),
          array (
            'name' => 'Vitamin product manufacturing'
          )
        )
      ),
      array (
        'name' => 'Veterinary pharmaceutical and medicinal product manufacturing',
        'code' => '25431',
        'cover_plus' => '0.38',
        'cover_plus_extra' => '0.53',
        'activities' => array (
          array (
            'name' => 'Veterinary drug manufacturing',
            'bic_code' => 'C184210'
          ),
          array (
            'name' => 'Veterinary medicinal preparation manufacturing (not elsewhere classified)'
          )
        )
      ),
      array (
        'name' => 'Cleaning-compound manufacturing',
        'code' => '25450',
        'cover_plus' => '0.38',
        'cover_plus_extra' => '0.53',
        'activities' => array (
          array (
            'name' => 'Candle manufacturing',
            'bic_code' => 'C185110'
          ),
          array (
            'name' => 'Denture cleaner manufacturing',
            'bic_code' => 'C185120'
          ),
          array (
            'name' => 'Detergent manufacturing',
            'bic_code' => 'C185130'
          ),
          array (
            'name' => 'Dishwashing detergent manufacturing',
            'bic_code' => 'C185140'
          ),
          array (
            'name' => 'Disinfectant manufacturing',
            'bic_code' => 'C185150'
          ),
          array (
            'name' => 'Emulsifier manufacturing',
            'bic_code' => 'C185160'
          ),
          array (
            'name' => 'Glycerine manufacturing',
            'bic_code' => 'C185170'
          ),
          array (
            'name' => 'Hypochlorite-based bleach manufacturing',
            'bic_code' => 'C185180'
          ),
          array (
            'name' => 'Laundry detergent manufacturing'
          ),
          array (
            'name' => 'Penetrant manufacturing'
          ),
          array (
            'name' => 'Peroxide preparation manufacturing'
          ),
          array (
            'name' => 'Polish manufacturing'
          ),
          array (
            'name' => 'Scouring compound manufacturing'
          ),
          array (
            'name' => 'Soap manufacturing'
          ),
          array (
            'name' => 'Toothpaste manufacturing'
          )
        )
      ),
      array (
        'name' => 'Cosmetic and toiletry preparation manufacturing',
        'code' => '25460',
        'cover_plus' => '0.38',
        'cover_plus_extra' => '0.53',
        'activities' => array (
          array (
            'name' => 'After-shave lotion manufacturing',
            'bic_code' => 'C185210'
          ),
          array (
            'name' => 'Barrier cream manufacturing',
            'bic_code' => 'C185220'
          ),
          array (
            'name' => 'Cosmetic deodorant manufacturing',
            'bic_code' => 'C185230'
          ),
          array (
            'name' => 'Depilatory manufacturing',
            'bic_code' => 'C185240 '
          ),
          array (
            'name' => 'Eye shadow manufacturing',
            'bic_code' => 'C185250'
          ),
          array (
            'name' => 'Face cream and lotion manufacturing',
            'bic_code' => 'C185260'
          ),
          array (
            'name' => 'Hair preparation manufacturing'
          ),
          array (
            'name' => 'Lip balm manufacturing'
          ),
          array (
            'name' => 'Lipstick manufacturing'
          ),
          array (
            'name' => 'Mascara manufacturing'
          ),
          array (
            'name' => 'Nail polish preparation manufacturing'
          ),
          array (
            'name' => 'Perfume manufacturing'
          ),
          array (
            'name' => 'Shaving preparation manufacturing'
          ),
          array (
            'name' => 'Sunscreen preparation manufacturing'
          ),
          array (
            'name' => 'Talcum powder manufacturing'
          ),
          array (
            'name' => 'Toilet lanolin manufacturing'
          )
        )
      ),
      array (
        'name' => 'Explosives manufacturing',
        'code' => '25410',
        'cover_plus' => '0.22',
        'cover_plus_extra' => '0.32',
        'activities' => array (
          array (
            'name' => 'Ammonium nitrate, explosive, manufacturing',
            'bic_code' => 'C189210'
          ),
          array (
            'name' => 'Blasting powder manufacturing',
            'bic_code' => 'C189220'
          ),
          array (
            'name' => 'Cellulose nitrate manufacturing',
            'bic_code' => 'C189230'
          ),
          array (
            'name' => 'Detonator manufacturing (cap or fuse)',
            'bic_code' => 'C189240'
          ),
          array (
            'name' => 'Dynamite manufacturing'
          ),
          array (
            'name' => 'Explosive fuse manufacturing'
          ),
          array (
            'name' => 'Fireworks manufacturing'
          ),
          array (
            'name' => 'Gun cotton manufacturing'
          ),
          array (
            'name' => 'Match manufacturing'
          ),
          array (
            'name' => 'Propellent powder manufacturing'
          ),
          array (
            'name' => 'Pyrotechnic goods manufacturing'
          ),
          array (
            'name' => 'Pyrotechnic manufacturing'
          ),
          array (
            'name' => 'Safety fuse manufacturing'
          ),
          array (
            'name' => 'Signal flare manufacturing'
          )
        )
      ),
      array (
        'name' => 'Basic chemical product manufacturing (not elsewhere classified)',
        'code' => '25490',
        'cover_plus' => '0.22',
        'cover_plus_extra' => '0.32',
        'activities' => array (
          array (
            'name' => 'Antifreeze manufacturing',
            'bic_code' => 'C189910'
          ),
          array (
            'name' => 'Beeswax manufacturing',
            'bic_code' => 'C189920'
          ),
          array (
            'name' => 'Concrete additive or masonry surface treatment  manufacturing',
            'bic_code' => 'C189930'
          ),
          array (
            'name' => 'Dry cleaning compound manufacturing'
          ),
          array (
            'name' => 'Embalming compound manufacturing (formaldehyde and additives)'
          ),
          array (
            'name' => 'Eucalyptus oil distilling'
          ),
          array (
            'name' => 'Extraction of essential oils'
          ),
          array (
            'name' => 'Flux manufacturing (welding and soldering)'
          ),
          array (
            'name' => 'Sandalwood oil distilling'
          ),
          array (
            'name' => 'Tea-tree oil distilling'
          )
        )
      ),
      array (
        'name' => 'Polymer film and sheet packaging material manufacturing',
        'code' => '25630',
        'cover_plus' => '0.74',
        'cover_plus_extra' => '0.99',
        'activities' => array (
          array (
            'name' => 'Bag, plastic, manufacturing',
            'bic_code' => 'C191110'
          ),
          array (
            'name' => 'Bag, sack or packet (plastic film or sheeting), manufacturing',
            'bic_code' => 'C191120'
          ),
          array (
            'name' => 'Bubble packaging manufacturing',
            'bic_code' => 'C191130'
          ),
          array (
            'name' => 'Film, plastic, manufacturing'
          ),
          array (
            'name' => 'Food wrapping, plastic, manufacturing'
          ),
          array (
            'name' => 'Garbage bag, plastic, manufacturing'
          ),
          array (
            'name' => 'Plastic lamination with paper'
          )
        )
      ),
      array (
        'name' => 'Rigid and semi-rigid polymer product manufacturing',
        'code' => '25610',
        'cover_plus' => '0.74',
        'cover_plus_extra' => '0.99',
        'activities' => array (
          array (
            'name' => 'Badge, plastic, manufacturing',
            'bic_code' => 'C191210'
          ),
          array (
            'name' => 'Bathtub, plastic, manufacturing',
            'bic_code' => 'C191220'
          ),
          array (
            'name' => 'Bottle, plastic, manufacturing',
            'bic_code' => 'C191230'
          ),
          array (
            'name' => 'Bucket, plastic, manufacturing',
            'bic_code' => 'C191240'
          ),
          array (
            'name' => 'Clothes peg, plastic, manufacturing',
            'bic_code' => 'C191250'
          ),
          array (
            'name' => 'Cultured marble surfacing product manufacturing',
            'bic_code' => 'C191260'
          ),
          array (
            'name' => 'Dinnerware, plastic, manufacturing',
            'bic_code' => 'C191270'
          ),
          array (
            'name' => 'Drinking fountain, plastic, manufacturing',
            'bic_code' => 'C191275'
          ),
          array (
            'name' => 'Electrical insulation box, polymer, manufacturing',
            'bic_code' => 'C191280'
          ),
          array (
            'name' => 'Food container, plastic, manufacturing (including microwave safe)',
            'bic_code' => 'C191290'
          ),
          array (
            'name' => 'Furniture, plastic, manufacturing'
          ),
          array (
            'name' => 'Gutter and spout, plastic, manufacturing'
          ),
          array (
            'name' => 'Light switch and plug, polymer, manufacturing'
          ),
          array (
            'name' => 'Pipe fittings, plastic, manufacturing'
          ),
          array (
            'name' => 'Pipe, plastic, manufacturing'
          ),
          array (
            'name' => 'Plastic framed doors and windows, manufacturing'
          ),
          array (
            'name' => 'Plastic union manufacturing'
          ),
          array (
            'name' => 'Plumbing fittings, plastic, manufacturing (including joints, elbows and flanges)'
          ),
          array (
            'name' => 'Polycarbonate sheet manufacturing'
          ),
          array (
            'name' => 'Polymer container manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Profile shapes, plastic, manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Rod or tube, plastic, manufacturing'
          ),
          array (
            'name' => 'Safety goggle, plastic, manufacturing'
          ),
          array (
            'name' => 'Shower stall, plastic, manufacturing'
          ),
          array (
            'name' => 'Toilet fixture, plastic, manufacturing'
          ),
          array (
            'name' => 'Toilet, plastic, manufacturing'
          ),
          array (
            'name' => 'Watering can, plastic, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Polymer foam product manufacturing',
        'code' => '25650',
        'cover_plus' => '0.74',
        'cover_plus_extra' => '0.99',
        'activities' => array (
          array (
            'name' => 'Bicycle safety helmet manufacturing',
            'bic_code' => 'C191310'
          ),
          array (
            'name' => 'Cooler and ice chest, polymeric foam, manufacturing'
          ),
          array (
            'name' => 'Cup, polymeric foam, manufacturing'
          ),
          array (
            'name' => 'Food container, polymeric foam, manufacturing'
          ),
          array (
            'name' => 'Insulation and cushioning material, polymer, manufacturing'
          ),
          array (
            'name' => 'Polymeric foam product manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Sheet foam manufacturing'
          )
        )
      ),
      array (
        'name' => 'Tyre manufacturing',
        'code' => '25510',
        'cover_plus' => '0.54',
        'cover_plus_extra' => '0.74',
        'activities' => array (
          array (
            'name' => 'Aircraft tyre manufacturing',
            'bic_code' => 'C191410'
          ),
          array (
            'name' => 'Inner tube manufacturing',
            'bic_code' => 'C191420'
          ),
          array (
            'name' => 'Motor vehicle tyre manufacturing',
            'bic_code' => 'C191430'
          ),
          array (
            'name' => 'Retread or rebuilt tyre manufacturing',
            'bic_code' => 'C191440'
          ),
          array (
            'name' => 'Tyre manufacturing (pneumatic or semi-pneumatic)'
          )
        )
      ),
      array (
        'name' => 'Adhesive manufacturing',
        'code' => '25491',
        'cover_plus' => '0.22',
        'cover_plus_extra' => '0.32',
        'activities' => array (
          array (
            'name' => 'Adhesive manufacturing',
            'bic_code' => 'C191510'
          ),
          array (
            'name' => 'Casein glue manufacturing',
            'bic_code' => 'C191520'
          ),
          array (
            'name' => 'Glue manufacturing'
          ),
          array (
            'name' => 'Rubber adhesives manufacturing'
          )
        )
      ),
      array (
        'name' => 'Paint and coatings manufacturing',
        'code' => '25420',
        'cover_plus' => '0.22',
        'cover_plus_extra' => '0.32',
        'activities' => array (
          array (
            'name' => 'Carbon ink manufacturing',
            'bic_code' => 'C191605'
          ),
          array (
            'name' => 'Caulking compound manufacturing',
            'bic_code' => 'C191610'
          ),
          array (
            'name' => 'Drawing ink manufacturing',
            'bic_code' => 'C191620'
          ),
          array (
            'name' => 'Enamel manufacturing',
            'bic_code' => 'C191630'
          ),
          array (
            'name' => 'Filler and putty manufacturing (including spray forms)',
            'bic_code' => 'C191640'
          ),
          array (
            'name' => 'Ink manufacturing (not elsewhere classified)',
            'bic_code' => 'C191650'
          ),
          array (
            'name' => 'Inkjet ink manufacturing'
          ),
          array (
            'name' => 'Lacquer manufacturing'
          ),
          array (
            'name' => 'Paint or varnish remover manufacturing'
          ),
          array (
            'name' => 'Paint tinting manufacturing'
          ),
          array (
            'name' => 'Primer manufacturing'
          ),
          array (
            'name' => 'Printing ink manufacturing'
          ),
          array (
            'name' => 'Rubbing compound (frits) manufacturing'
          ),
          array (
            'name' => 'Shellac manufacturing'
          ),
          array (
            'name' => 'Silk screen ink manufacturing'
          ),
          array (
            'name' => 'Stain manufacturing (including decking stains and oils'
          ),
          array (
            'name' => 'Toner manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Undercoat and top coat paint manufacturing'
          ),
          array (
            'name' => 'Varnish manufacturing'
          ),
          array (
            'name' => 'Water repellent coating manufacturing (for concrete and masonry)'
          ),
          array (
            'name' => 'Writing ink manufacturing'
          )
        )
      ),
      array (
        'name' => 'Polymer product manufacturing (not elsewhere classified)',
        'code' => '25661',
        'cover_plus' => '0.74',
        'cover_plus_extra' => '0.99',
        'activities' => array (
          array (
            'name' => 'Awning, fibreglass, manufacturing',
            'bic_code' => 'C191140'
          ),
          array (
            'name' => 'Conveyor belt, plastic or composite, manufacturing',
            'bic_code' => 'C191910'
          ),
          array (
            'name' => 'Floor covering, resilient polymer, manufacturing',
            'bic_code' => 'C191920'
          ),
          array (
            'name' => 'Garbage bin, plastic, manufacturing',
            'bic_code' => 'C191930'
          ),
          array (
            'name' => 'Garden hose, plastic or composite, manufacturing',
            'bic_code' => 'C191940'
          ),
          array (
            'name' => 'Gloves, plastic, manufacturing',
            'bic_code' => 'C191950'
          ),
          array (
            'name' => 'High-density safety equipment manufacturing (e.g. military helmets)',
            'bic_code' => 'C191960'
          ),
          array (
            'name' => 'Hose, plastic or composite, manufacturing',
            'bic_code' => 'C191970'
          ),
          array (
            'name' => 'Hull, boat building, manufacturing',
            'bic_code' => 'C191980'
          ),
          array (
            'name' => 'Motor vehicle and boat parts, fibreglass, manufacturing'
          ),
          array (
            'name' => 'Plastic laminate fabrication'
          ),
          array (
            'name' => 'Polymer product manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Radiator and heating hose, plastic or composite, manufacturing'
          ),
          array (
            'name' => 'Refrigeration container insulation sheet manufacturing'
          ),
          array (
            'name' => 'Transmission belt, plastic or composite, manufacturing'
          ),
          array (
            'name' => 'Vacuum cleaner belt, plastic or composite, manufacturing'
          ),
          array (
            'name' => 'V-belt, plastic or composite, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Natural rubber product manufacturing',
        'code' => '25590',
        'cover_plus' => '0.53',
        'cover_plus_extra' => '0.73',
        'activities' => array (
          array (
            'name' => 'Bath mat, natural rubber, manufacturing',
            'bic_code' => 'C192010'
          ),
          array (
            'name' => 'Condom, natural rubber, manufacturing',
            'bic_code' => 'C192020'
          ),
          array (
            'name' => 'Conveyor belt, natural rubber, manufacturing',
            'bic_code' => 'C192030'
          ),
          array (
            'name' => 'Diaphragm, natural rubber, manufacturing',
            'bic_code' => 'C192040'
          ),
          array (
            'name' => 'Dummy, natural rubber, manufacturing',
            'bic_code' => 'C192050'
          ),
          array (
            'name' => 'Floor covering or underlay, resilient natural rubber, manufacturing',
            'bic_code' => 'C192060'
          ),
          array (
            'name' => 'Garden hose, natural rubber, manufacturing'
          ),
          array (
            'name' => 'Hose, natural rubber, manufacturing, (not elsewhere classified)'
          ),
          array (
            'name' => 'Hot water bottle, natural rubber, manufacturing'
          ),
          array (
            'name' => 'Mattress protector, natural rubber, manufacturing'
          ),
          array (
            'name' => 'Pillow or cushion, natural rubber, manufacturing'
          ),
          array (
            'name' => 'Plug, natural rubber, manufacturing'
          ),
          array (
            'name' => 'Rubber balloon, natural rubber, manufacturing'
          ),
          array (
            'name' => 'Rubber band, natural rubber, manufacturing'
          ),
          array (
            'name' => 'Rubber glove, natural rubber, manufacturing'
          ),
          array (
            'name' => 'Sponge, natural rubber, manufacturing'
          ),
          array (
            'name' => 'Teething ring, natural rubber, manufacturing'
          ),
          array (
            'name' => 'Tubing, natural rubber, manufacturing'
          ),
          array (
            'name' => 'Tyre manufacturing (solid)'
          ),
          array (
            'name' => 'Washer, natural rubber, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Glass and glass product manufacturing',
        'code' => '26100',
        'cover_plus' => '1.26',
        'cover_plus_extra' => '1.68',
        'activities' => array (
          array (
            'name' => 'Automotive glass manufacturing',
            'bic_code' => 'C201005'
          ),
          array (
            'name' => 'Bead, glass, manufacturing',
            'bic_code' => 'C201010'
          ),
          array (
            'name' => 'Block, glass, manufacturing',
            'bic_code' => 'C201015'
          ),
          array (
            'name' => 'Bottle, glass, manufacturing',
            'bic_code' => 'C201020'
          ),
          array (
            'name' => 'Container, glass, manufacturing',
            'bic_code' => 'C201025'
          ),
          array (
            'name' => 'Crystal glass manufacturing',
            'bic_code' => 'C201030'
          ),
          array (
            'name' => 'Domestic glassware manufacturing',
            'bic_code' => 'C201035'
          ),
          array (
            'name' => 'Drinking glass manufacturing',
            'bic_code' => 'C201040'
          ),
          array (
            'name' => 'Flat glass manufacturing',
            'bic_code' => 'C201045'
          ),
          array (
            'name' => 'Glass or glass product manufacturing (except glass wool or glass wool products)',
            'bic_code' => 'C201050'
          ),
          array (
            'name' => 'Glass, sheet, manufacturing',
            'bic_code' => 'C201055'
          ),
          array (
            'name' => 'Insulator, glass, manufacturing',
            'bic_code' => 'C201060'
          ),
          array (
            'name' => 'Jar, glass, manufacturing',
            'bic_code' => 'C201065'
          ),
          array (
            'name' => 'Kitchenware, glass, manufacturing',
            'bic_code' => 'C201070'
          ),
          array (
            'name' => 'Laboratory glassware manufacturing'
          ),
          array (
            'name' => 'Laminated sheet glass manufacturing'
          ),
          array (
            'name' => 'Mirror manufacturing'
          ),
          array (
            'name' => 'Optical glass or blanks for lenses manufacturing'
          ),
          array (
            'name' => 'Ornamental glassware manufacturing'
          ),
          array (
            'name' => 'Ovenware, glass, manufacturing'
          ),
          array (
            'name' => 'Plate glass manufacturing'
          ),
          array (
            'name' => 'Safety glass manufacturing'
          ),
          array (
            'name' => 'Scientific glassware manufacturing'
          ),
          array (
            'name' => 'Stained glass sheet manufacturing'
          ),
          array (
            'name' => 'Tubing, glass, manufacturing'
          ),
          array (
            'name' => 'Window glass manufacturing'
          ),
          array (
            'name' => 'Windscreen glass manufacturing'
          )
        )
      ),
      array (
        'name' => 'Clay brick manufacturing',
        'code' => '26210',
        'cover_plus' => '1.26',
        'cover_plus_extra' => '1.68',
        'activities' => array (
          array (
            'name' => 'Brick, clay, manufacturing (excluding refractory bricks)',
            'bic_code' => 'C202110'
          ),
          array (
            'name' => 'Brick, face or texture, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Cereal grain wholesaling',
        'code' => '45120',
        'cover_plus' => '0.69',
        'cover_plus_extra' => '0.93',
        'activities' => array (
          array (
            'name' => 'Cereal grain wholesaling',
            'bic_code' => 'F331210'
          ),
          array (
            'name' => 'Rice wholesaling'
          ),
          array (
            'name' => 'Wheat wholesaling'
          )
        )
      ),
      array (
        'name' => 'Ceramic product manufacturing (not elsewhere classified)',
        'code' => '26290',
        'cover_plus' => '1.93',
        'cover_plus_extra' => '2.55',
        'activities' => array (
          array (
            'name' => 'Bathroom fixture, vitreous china, manufacturing',
            'bic_code' => 'C202903'
          ),
          array (
            'name' => 'Brick, fireclay, manufacturing',
            'bic_code' => 'C202907'
          ),
          array (
            'name' => 'Brick, refractory, manufacturing',
            'bic_code' => 'C202910'
          ),
          array (
            'name' => 'Cement, refractory, manufacturing',
            'bic_code' => 'C202913'
          ),
          array (
            'name' => 'Ceramic goods manufacturing',
            'bic_code' => 'C202917'
          ),
          array (
            'name' => 'Clay paver manufacturing',
            'bic_code' => 'C202920'
          ),
          array (
            'name' => 'Clay, refractory, manufacturing',
            'bic_code' => 'C202923'
          ),
          array (
            'name' => 'Crockery manufacturing',
            'bic_code' => 'C202927'
          ),
          array (
            'name' => 'Crucible, refractory, manufacturing',
            'bic_code' => 'C202930'
          ),
          array (
            'name' => 'Earthenware, table or kitchen, manufacturing',
            'bic_code' => 'C202933'
          ),
          array (
            'name' => 'Flower pot, ceramic, manufacturing',
            'bic_code' => 'C202937'
          ),
          array (
            'name' => 'Graphite crucible or foundry accessory manufacturing',
            'bic_code' => 'C202940'
          ),
          array (
            'name' => 'Insulator, porcelain, manufacturing',
            'bic_code' => 'C202943'
          ),
          array (
            'name' => 'Pipe, ceramic, manufacturing (including vitreous china or porcelain)',
            'bic_code' => 'C202947'
          ),
          array (
            'name' => 'Porcelain goods manufacturing',
            'bic_code' => 'C202950'
          ),
          array (
            'name' => 'Pottery goods manufacturing',
            'bic_code' => 'C202953'
          ),
          array (
            'name' => 'Refractory product manufacturing',
            'bic_code' => 'C202957'
          ),
          array (
            'name' => 'Roof tile, clay or terracotta, manufacturing',
            'bic_code' => 'C202960'
          ),
          array (
            'name' => 'Silica brick, refractory, manufacturing',
            'bic_code' => 'C202963'
          ),
          array (
            'name' => 'Stoneware pipe or fittings manufacturing',
            'bic_code' => 'C202967'
          ),
          array (
            'name' => 'Terracotta goods manufacturing',
            'bic_code' => 'C202970'
          ),
          array (
            'name' => 'Tile, ceramic, wall or floor, manufacturing',
            'bic_code' => 'C202973'
          ),
          array (
            'name' => 'Vitreous china goods manufacturing',
            'bic_code' => 'C202977'
          ),
          array (
            'name' => 'Zirconia, ceramic, manufacturing',
            'bic_code' => 'C202980'
          )
        )
      ),
      array (
        'name' => 'Cement and lime manufacturing',
        'code' => '26310',
        'cover_plus' => '1.26',
        'cover_plus_extra' => '1.68',
        'activities' => array (
          array (
            'name' => 'Agricultural lime manufacturing',
            'bic_code' => 'C203110'
          ),
          array (
            'name' => 'Burnt lime manufacturing',
            'bic_code' => 'C203120'
          ),
          array (
            'name' => 'Cement manufacturing (except adhesive or refractory'
          ),
          array (
            'name' => 'Hydrated lime manufacturing'
          ),
          array (
            'name' => 'Hydraulic cement manufacturing'
          ),
          array (
            'name' => 'Lime manufacturing'
          ),
          array (
            'name' => 'Portland cement manufacturing'
          ),
          array (
            'name' => 'Quick hydrated lime manufacturing'
          ),
          array (
            'name' => 'Slag cement manufacturing'
          ),
          array (
            'name' => 'Slake lime manufacturing'
          )
        )
      ),
      array (
        'name' => 'Plaster and gypsum product manufacturing',
        'code' => '26320',
        'cover_plus' => '1.26',
        'cover_plus_extra' => '1.68',
        'activities' => array (
          array (
            'name' => 'Acoustic tile, plaster, manufacturing',
            'bic_code' => 'C203210'
          ),
          array (
            'name' => 'Cornice, plaster, manufacturing',
            'bic_code' => 'C203220'
          ),
          array (
            'name' => 'Display model, plaster, manufacturing',
            'bic_code' => 'C203230'
          ),
          array (
            'name' => 'Fibrous plaster product manufacturing',
            'bic_code' => 'C203240'
          ),
          array (
            'name' => 'Insulating board, plaster, manufacturing',
            'bic_code' => 'C203250'
          ),
          array (
            'name' => 'Moulding, plaster, manufacturing',
            'bic_code' => 'C203260'
          ),
          array (
            'name' => 'Plaster of paris manufacturing'
          ),
          array (
            'name' => 'Plaster product manufacturing (except dental or medical plasters)'
          ),
          array (
            'name' => 'Plasterboard manufacturing'
          ),
          array (
            'name' => 'Sheet, plaster, manufacturing'
          ),
          array (
            'name' => 'Tile, plaster, manufacturing'
          ),
          array (
            'name' => 'Wall or ceiling board, plaster, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Ready-mixed concrete manufacturing',
        'code' => '26330',
        'cover_plus' => '1.93',
        'cover_plus_extra' => '2.55',
        'activities' => array (
          array (
            'name' => 'Concrete slurry manufacturing',
            'bic_code' => 'C203310'
          ),
          array (
            'name' => 'Dry mix concrete manufacturing'
          ),
          array (
            'name' => 'Mortar, dry mix, manufacturing'
          ),
          array (
            'name' => 'Ready-mixed concrete manufacturing'
          )
        )
      ),
      array (
        'name' => 'Concrete product manufacturing',
        'code' => '26350',
        'cover_plus' => '1.26',
        'cover_plus_extra' => '1.68',
        'activities' => array (
          array (
            'name' => 'Autoclaved aerated concrete product manufacturing',
            'bic_code' => 'C203405'
          ),
          array (
            'name' => 'Block, concrete, manufacturing',
            'bic_code' => 'C203410'
          ),
          array (
            'name' => 'Box culvert, concrete, manufacturing',
            'bic_code' => 'C203415'
          ),
          array (
            'name' => 'Brick, concrete, manufacturing',
            'bic_code' => 'C203420'
          ),
          array (
            'name' => 'Building board, cement based, manufacturing',
            'bic_code' => 'C203425'
          ),
          array (
            'name' => 'Cistern, concrete, manufacturing',
            'bic_code' => 'C203430'
          ),
          array (
            'name' => 'Concrete/styrofoam composite product manufacturing',
            'bic_code' => 'C203435'
          ),
          array (
            'name' => 'Exterior cement-based cladding manufacturing',
            'bic_code' => 'C203440'
          ),
          array (
            'name' => 'Fibre cement exterior wall manufacturing',
            'bic_code' => 'C203445'
          ),
          array (
            'name' => 'Floor or wall tile, concrete, manufacturing',
            'bic_code' => 'C203450'
          ),
          array (
            'name' => 'Flower pot, concrete, manufacturing',
            'bic_code' => 'C203455'
          ),
          array (
            'name' => 'Manhole cover, concrete, manufacturing',
            'bic_code' => 'C203460'
          ),
          array (
            'name' => 'Meter box, concrete, manufacturing',
            'bic_code' => 'C203465'
          ),
          array (
            'name' => 'Monument or grave marker, concrete, manufacturing',
            'bic_code' => 'C203470'
          ),
          array (
            'name' => 'Moulding, concrete, manufacturing',
            'bic_code' => 'C203475'
          ),
          array (
            'name' => 'Ornamental concrete or terrazzo product manufacturing',
            'bic_code' => 'C203480'
          ),
          array (
            'name' => 'Panel or section, prefabricated concrete, manufacturing'
          ),
          array (
            'name' => 'Pipe, concrete, manufacturing'
          ),
          array (
            'name' => 'Post or pole, concrete, manufacturing'
          ),
          array (
            'name' => 'Prefabricated concrete building manufacturing'
          ),
          array (
            'name' => 'Railway sleeper, concrete, manufacturing'
          ),
          array (
            'name' => 'Roof component, concrete, manufacturing'
          ),
          array (
            'name' => 'Roof tile, concrete, manufacturing'
          ),
          array (
            'name' => 'Sink or tub, concrete, manufacturing'
          ),
          array (
            'name' => 'Tank, concrete, manufacturing'
          ),
          array (
            'name' => 'Terrazzo product manufacturing'
          ),
          array (
            'name' => 'Tile, concrete, manufacturing'
          ),
          array (
            'name' => 'Wall fitting, terrazzo, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Non-metallic mineral product manufacturing (not elsewhere classified)',
        'code' => '26400',
        'cover_plus' => '1.26',
        'cover_plus_extra' => '1.68',
        'activities' => array (
          array (
            'name' => 'Abrasives manufacturing',
            'bic_code' => 'C209010'
          ),
          array (
            'name' => 'Acoustic tile, panel or board manufacturing (glass or mineral wool)',
            'bic_code' => 'C209020'
          ),
          array (
            'name' => 'Brick, silica lime, manufacturing',
            'bic_code' => 'C209030'
          ),
          array (
            'name' => 'Building board, imitation brick or stone, manufacturing)',
            'bic_code' => 'C209040'
          ),
          array (
            'name' => 'Carbon product manufacturing (except brushes, electrodes or bearings)',
            'bic_code' => 'C209050'
          ),
          array (
            'name' => 'Chalk product manufacturing (not elsewhere classified)',
            'bic_code' => 'C209060'
          ),
          array (
            'name' => 'Diamond powder manufacturing',
            'bic_code' => 'C209065'
          ),
          array (
            'name' => 'Expanded non-metallic mineral manufacturing',
            'bic_code' => 'C209070'
          ),
          array (
            'name' => 'Feldspar, ground, manufacturing',
            'bic_code' => 'C209080'
          ),
          array (
            'name' => 'Flooring material magnesite, manufacturing'
          ),
          array (
            'name' => 'Foundry core, sand, manufacturing'
          ),
          array (
            'name' => 'Fullers earth, ground, manufacturing'
          ),
          array (
            'name' => 'Granulated slag manufacturing'
          ),
          array (
            'name' => 'Graphite product manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Headstone manufacturing or engraving (except concrete)'
          ),
          array (
            'name' => 'Insulation, glass fibre or mineral wool, manufacturing'
          ),
          array (
            'name' => 'Mica product manufacturing'
          ),
          array (
            'name' => 'Mineral earths, ground, manufacturing'
          ),
          array (
            'name' => 'Mineral wool manufacturing'
          ),
          array (
            'name' => 'Mineral wool product manufacturing'
          ),
          array (
            'name' => 'Monument making (except concrete)'
          ),
          array (
            'name' => 'Perlite, expanded, manufacturing'
          ),
          array (
            'name' => 'Processed lightweight aggregate manufacturing'
          ),
          array (
            'name' => 'Resin coated sand manufacturing'
          ),
          array (
            'name' => 'Rockwool manufacturing'
          ),
          array (
            'name' => 'Silicon carbide abrasives manufacturing'
          ),
          array (
            'name' => 'Slag crushing'
          ),
          array (
            'name' => 'Stone cutting, dressing, polishing or shaping'
          ),
          array (
            'name' => 'Stone product manufacturing'
          ),
          array (
            'name' => 'Synthetic gemstone manufacturing'
          ),
          array (
            'name' => 'Talc, ground, manufacturing'
          ),
          array (
            'name' => 'Vermiculite, expanded, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Iron smelting and steel manufacturing',
        'code' => '27110',
        'cover_plus' => '1.18',
        'cover_plus_extra' => '1.57',
        'activities' => array (
          array (
            'name' => 'Band, steel, manufacturing',
            'bic_code' => 'C211010'
          ),
          array (
            'name' => 'Bar, iron or steel, manufacturing',
            'bic_code' => 'C211020'
          ),
          array (
            'name' => 'Blank, steel, manufacturing',
            'bic_code' => 'C211025'
          ),
          array (
            'name' => 'Direct reduction iron (DRI) manufacturing',
            'bic_code' => 'C211030'
          ),
          array (
            'name' => 'Ferro-alloy manufacturing (including, manganese, silicon or chrome)',
            'bic_code' => 'C211040'
          ),
          array (
            'name' => 'Flat-rolled product, iron or steel, manufacturing',
            'bic_code' => 'C211050'
          ),
          array (
            'name' => 'High carbon tool steel manufacturing',
            'bic_code' => 'C211060'
          ),
          array (
            'name' => 'High speed steel manufacturing',
            'bic_code' => 'C211070'
          ),
          array (
            'name' => 'Pig iron manufacturing',
            'bic_code' => 'C211080'
          ),
          array (
            'name' => 'Powder, iron or steel, manufacturing'
          ),
          array (
            'name' => 'Rail fastening or other rail accessory manufacturing'
          ),
          array (
            'name' => 'Rail, steel, manufacturing'
          ),
          array (
            'name' => 'Roof decking, steel, manufacturing'
          ),
          array (
            'name' => 'Section, steel, manufacturing'
          ),
          array (
            'name' => 'Semi-finished product, iron or steel, manufacturing'
          ),
          array (
            'name' => 'Skelp, steel, manufacturing'
          ),
          array (
            'name' => 'Spring steel manufacturing'
          ),
          array (
            'name' => 'Stainless steel manufacturing'
          ),
          array (
            'name' => 'Steel alloy manufacturing'
          ),
          array (
            'name' => 'Structural steel shape manufacturing (not fabricated)'
          ),
          array (
            'name' => 'Tinplate sheet or strip manufacturing'
          )
        )
      ),
      array (
        'name' => 'Iron and steel casting',
        'code' => '27120',
        'cover_plus' => '1.18',
        'cover_plus_extra' => '1.57',
        'activities' => array (
          array (
            'name' => 'Castings, iron, manufacturing',
            'bic_code' => 'C212110'
          ),
          array (
            'name' => 'Castings, steel, manufacturing',
            'bic_code' => 'C212120'
          ),
          array (
            'name' => 'Chain, cast steel, manufacturing'
          ),
          array (
            'name' => 'Die-casting, steel, manufacturing'
          ),
          array (
            'name' => 'Direct casting, iron, manufacturing'
          ),
          array (
            'name' => 'Direct casting, steel, manufacturing'
          ),
          array (
            'name' => 'Moulded cast iron pipe or tube manufacturing'
          ),
          array (
            'name' => 'Pipe fitting, cast, ferrous, manufacturing'
          ),
          array (
            'name' => 'Stainless steel cast, seamless pipe or tube manufacturing'
          ),
          array (
            'name' => 'Tube, spun-cast iron, manufacturing'
          ),
          array (
            'name' => 'Valve or valve parts, steam, gas or water, cast, manufacturing (ferrous metal)'
          )
        )
      ),
      array (
        'name' => 'Steel pipe and tube manufacturing',
        'code' => '27130',
        'cover_plus' => '1.18',
        'cover_plus_extra' => '1.57',
        'activities' => array (
          array (
            'name' => 'Cold drawn steel pipe or tube manufacturing',
            'bic_code' => 'C212210'
          ),
          array (
            'name' => 'Fittings, steam, gas or water, manufacturing (ferrous metal; except cast or forged)'
          ),
          array (
            'name' => 'Galvanised seamless or welded steel pipe or tube manufacturing'
          ),
          array (
            'name' => 'Hot rolling pipe and tube manufacturing'
          )
        )
      ),
      array (
        'name' => 'Alumina production',
        'code' => '27210',
        'cover_plus' => '1.32',
        'cover_plus_extra' => '1.75',
        'activities' => array (
          array (
            'name' => 'Alumina manufacturing',
            'bic_code' => 'C213110'
          ),
          array (
            'name' => 'Bauxite refining'
          ),
          array (
            'name' => 'Calcined alumina manufacturing'
          )
        )
      ),
      array (
        'name' => 'Aluminium smelting',
        'code' => '27220',
        'cover_plus' => '1.32',
        'cover_plus_extra' => '1.75',
        'activities' => array (
          array (
            'name' => 'Aluminium casting',
            'bic_code' => 'C213205'
          ),
          array (
            'name' => 'Aluminium from scrap recovery (except collection)',
            'bic_code' => 'C213210'
          ),
          array (
            'name' => 'Aluminium smelting (from alumina)'
          ),
          array (
            'name' => 'Electrolytic aluminium manufacturing'
          )
        )
      ),
      array (
        'name' => 'Copper, silver, lead, and zinc smelting and refining',
        'code' => '27230',
        'cover_plus' => '1.32',
        'cover_plus_extra' => '1.75',
        'activities' => array (
          array (
            'name' => 'Blister copper manufacturing',
            'bic_code' => 'C213305'
          ),
          array (
            'name' => 'Copper smelting, refining',
            'bic_code' => 'C213310'
          ),
          array (
            'name' => 'Copper, silver, lead or zinc from scrap or waste material recovering',
            'bic_code' => 'C213320'
          ),
          array (
            'name' => 'Electrolytic copper manufacturing',
            'bic_code' => 'C213330'
          ),
          array (
            'name' => 'Electrolytic zinc manufacturing',
            'bic_code' => 'C213340'
          ),
          array (
            'name' => 'Lead smelting or refining',
            'bic_code' => 'C213350'
          ),
          array (
            'name' => 'Silver smelting, refining'
          ),
          array (
            'name' => 'Spelter manufacturing'
          ),
          array (
            'name' => 'Wirebar, copper, manufacturing'
          ),
          array (
            'name' => 'Zinc smelting or refining'
          )
        )
      ),
      array (
        'name' => 'Basic non-ferrous metal manufacturing (not elsewhere classified)',
        'code' => '27290',
        'cover_plus' => '1.32',
        'cover_plus_extra' => '1.75',
        'activities' => array (
          array (
            'name' => 'Antimony, refined, manufacturing',
            'bic_code' => 'C213910'
          ),
          array (
            'name' => 'Bismuth smelting or refining',
            'bic_code' => 'C213920'
          ),
          array (
            'name' => 'Bronze manufacturing',
            'bic_code' => 'C213925'
          ),
          array (
            'name' => 'Can de-tinning',
            'bic_code' => 'C213930'
          ),
          array (
            'name' => 'Gold refining',
            'bic_code' => 'C213940'
          ),
          array (
            'name' => 'Molybdenum metal powder or flake manufacturing'
          ),
          array (
            'name' => 'Nickel oxide production in association with nickel smelting'
          ),
          array (
            'name' => 'Nickel smelting or refining'
          ),
          array (
            'name' => 'Non-ferrous alloy manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Non-ferrous metal (not elsewhere classified) from waste material recovering'
          ),
          array (
            'name' => 'Non-ferrous metal (not elsewhere classified) refining'
          ),
          array (
            'name' => 'Rare earth metal smelting'
          ),
          array (
            'name' => 'Silicon smelting'
          ),
          array (
            'name' => 'Solder manufacturing'
          ),
          array (
            'name' => 'Tantalum metal powder manufacturing'
          ),
          array (
            'name' => 'Tin smelting'
          ),
          array (
            'name' => 'Titanium smelting'
          ),
          array (
            'name' => 'Welding rod manufacturing'
          )
        )
      ),
      array (
        'name' => 'Non-ferrous metal casting and forging',
        'code' => '27330',
        'cover_plus' => '0.49',
        'cover_plus_extra' => '0.67',
        'activities' => array (
          array (
            'name' => 'Castings, non-ferrous metal, manufacturing (not elsewhere classified)',
            'bic_code' => 'C214110'
          ),
          array (
            'name' => 'Die casting, non-ferrous metal, manufacturing (not elsewhere classified)',
            'bic_code' => 'C214120'
          ),
          array (
            'name' => 'Forgings, non-ferrous metal, manufacturing (not elsewhere classified)',
            'bic_code' => 'C214130'
          )
        )
      ),
      array (
        'name' => 'Aluminium rolling, drawing, and extruding',
        'code' => '27310',
        'cover_plus' => '1.31',
        'cover_plus_extra' => '1.74',
        'activities' => array (
          array (
            'name' => 'Aluminium foil, household, manufacturing',
            'bic_code' => 'C214210'
          ),
          array (
            'name' => 'Aluminium rolling, drawing or extruding',
            'bic_code' => 'C214220'
          ),
          array (
            'name' => 'Bar, aluminium, manufacturing',
            'bic_code' => 'C214230'
          ),
          array (
            'name' => 'Foil, aluminium, manufacturing',
            'bic_code' => 'C214240'
          ),
          array (
            'name' => 'Paper-backed aluminium foil manufacturing',
            'bic_code' => 'C214250'
          ),
          array (
            'name' => 'Pipe, aluminium, manufacturing'
          ),
          array (
            'name' => 'Plastic-coated aluminium foil manufacturing'
          ),
          array (
            'name' => 'Plate, aluminium, manufacturing'
          ),
          array (
            'name' => 'Powder or flake, aluminium, manufacturing'
          ),
          array (
            'name' => 'Rod, aluminium, manufacturing'
          ),
          array (
            'name' => 'Section, aluminium, rolling, drawing or extruding'
          ),
          array (
            'name' => 'Sheet, aluminium, manufacturing'
          ),
          array (
            'name' => 'Strip, aluminium, manufacturing'
          ),
          array (
            'name' => 'Tube, aluminium, manufacturing'
          ),
          array (
            'name' => 'Wire, aluminium, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Basic non-ferrous metal product manufacturing (not elsewhere classified)',
        'code' => '27320',
        'cover_plus' => '0.49',
        'cover_plus_extra' => '0.67',
        'activities' => array (
          array (
            'name' => 'Bar, non-ferrous metal, (except aluminium) manufacturing',
            'bic_code' => 'C214910'
          ),
          array (
            'name' => 'Foil, non-ferrous metal, manufacturing (except aluminium)',
            'bic_code' => 'C214920'
          ),
          array (
            'name' => 'Non-ferrous metal rolling, drawing or extruding (except aluminium)',
            'bic_code' => 'C214930'
          ),
          array (
            'name' => 'Pipe, non-ferrous metal, manufacturing (except aluminium)',
            'bic_code' => 'C214940'
          ),
          array (
            'name' => 'Plate, non-ferrous metal, manufacturing (except aluminium)'
          ),
          array (
            'name' => 'Powder or flake, non-ferrous metal, manufacturing,(not elsewhere classified)'
          ),
          array (
            'name' => 'Rod, non-ferrous metal, manufacturing (except aluminium)'
          ),
          array (
            'name' => 'Section, non-ferrous metal, rolling, drawing or extruding (except aluminium)'
          ),
          array (
            'name' => 'Sheet, non-ferrous metal, manufacturing (except aluminium)'
          ),
          array (
            'name' => 'Strip, non-ferrous metal, manufacturing (except insulated or from aluminium)'
          ),
          array (
            'name' => 'Tube, non-ferrous metal, manufacturing (except aluminium)'
          ),
          array (
            'name' => 'Wire, non-ferrous metal, manufacturing (except stranded, braided or insulated or from aluminium)'
          )
        )
      ),
      array (
        'name' => 'Iron and steel forging',
        'code' => '27121',
        'cover_plus' => '1.18',
        'cover_plus_extra' => '1.57',
        'activities' => array (
          array (
            'name' => 'Chain, forged steel, manufacturing',
            'bic_code' => 'C221010'
          ),
          array (
            'name' => 'Fittings, steam, gas or water, forged iron or steel, manufacturing',
            'bic_code' => 'C221020'
          ),
          array (
            'name' => 'Forgings, iron or steel, manufacturing',
            'bic_code' => 'C221030'
          ),
          array (
            'name' => 'Horse shoe mass production',
            'bic_code' => 'C221040'
          ),
          array (
            'name' => 'Pipe fittings, forged iron or steel, manufacturing'
          ),
          array (
            'name' => 'Tube fittings, forged iron or steel, manufacturing'
          ),
          array (
            'name' => 'Valves or valve parts, steam, gas or water, forged iron or steel, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Structural steel fabricating',
        'code' => '27410',
        'cover_plus' => '1.96',
        'cover_plus_extra' => '2.59',
        'activities' => array (
          array (
            'name' => 'Fabricated structural steel manufacturing (readymade parts for structures)',
            'bic_code' => 'C222110'
          ),
          array (
            'name' => 'Girder, prefabricated steel, manufacturing',
            'bic_code' => 'C222120'
          ),
          array (
            'name' => 'Joist, prefabricated steel, manufacturing',
            'bic_code' => 'C222130'
          ),
          array (
            'name' => 'Prefabricated structural steel parts manufacturing',
            'bic_code' => 'C222140'
          ),
          array (
            'name' => 'Rafter, prefabricated steel, manufacturing',
            'bic_code' => 'C222150'
          ),
          array (
            'name' => 'Reinforcing mesh, welded steel, manufacturing',
            'bic_code' => 'C222160'
          ),
          array (
            'name' => 'Reinforcing steel rod, processed, manufacturing (from wire bar or merchant bar)'
          ),
          array (
            'name' => 'Roof truss, prefabricated steel, manufacturing'
          ),
          array (
            'name' => 'Scaffolding, prefabricated steel, manufacturing'
          ),
          array (
            'name' => 'Steel plate, perforated, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Prefabricated metal building manufacturing',
        'code' => '29110',
        'cover_plus' => '1.29',
        'cover_plus_extra' => '1.72',
        'activities' => array (
          array (
            'name' => 'Building, prefabricated metal, manufacturing',
            'bic_code' => 'C222210'
          ),
          array (
            'name' => 'Bus shelter, prefabricated metal, manufacturing',
            'bic_code' => 'C222220'
          ),
          array (
            'name' => 'Carport, prefabricated metal, manufacturing',
            'bic_code' => 'C222230'
          ),
          array (
            'name' => 'Garage, prefabricated metal, manufacturing'
          ),
          array (
            'name' => 'Kit set home, prefabricated metal, manufacturing'
          ),
          array (
            'name' => 'Shed, prefabricated metal, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Architectural aluminium product manufacturing',
        'code' => '27420',
        'cover_plus' => '1.31',
        'cover_plus_extra' => '1.74',
        'activities' => array (
          array (
            'name' => 'Aluminium framed door, glazed, manufacturing',
            'bic_code' => 'C222305'
          ),
          array (
            'name' => 'Aluminium roofing manufacturing',
            'bic_code' => 'C222307'
          ),
          array (
            'name' => 'Architectural aluminium product manufacturing',
            'bic_code' => 'C222310'
          ),
          array (
            'name' => 'Ceiling section, fabricated aluminium, manufacturing',
            'bic_code' => 'C222315'
          ),
          array (
            'name' => 'Chain curtain or screen, aluminium, manufacturing',
            'bic_code' => 'C222320'
          ),
          array (
            'name' => 'Curtain wall, aluminium, manufacturing',
            'bic_code' => 'C222325'
          ),
          array (
            'name' => 'Door or door frame, aluminium, manufacturing',
            'bic_code' => 'C222330'
          ),
          array (
            'name' => 'Downpipe, aluminium, manufacturing',
            'bic_code' => 'C222335'
          ),
          array (
            'name' => 'Fascia, aluminium, manufacturing',
            'bic_code' => 'C222340'
          ),
          array (
            'name' => 'Fly screen door, aluminium, manufacturing',
            'bic_code' => 'C222345'
          ),
          array (
            'name' => 'Garage door, aluminium, manufacturing',
            'bic_code' => 'C222350'
          ),
          array (
            'name' => 'Gate, aluminium, manufacturing',
            'bic_code' => 'C222355'
          ),
          array (
            'name' => 'Guttering, aluminium, manufacturing',
            'bic_code' => 'C222360'
          ),
          array (
            'name' => 'Ornamental architectural aluminium work manufacturing',
            'bic_code' => 'C222365'
          ),
          array (
            'name' => 'Partition, prefabricated aluminium, manufacturing'
          ),
          array (
            'name' => 'Railing, aluminium, manufacturing'
          ),
          array (
            'name' => 'Roller shutter, aluminium, manufacturing'
          ),
          array (
            'name' => 'Shop front, aluminium, manufacturing'
          ),
          array (
            'name' => 'Shower screen, aluminium framed, manufacturing'
          ),
          array (
            'name' => 'Skylight, aluminium, manufacturing'
          ),
          array (
            'name' => 'Window frame or sash, aluminium, manufacturing'
          ),
          array (
            'name' => 'Window screen, aluminium, manufacturing'
          ),
          array (
            'name' => 'Window, aluminium framed, manufacturing (complete with glass)'
          )
        )
      ),
      array (
        'name' => 'Metal roof and guttering manufacturing (except aluminium)',
        'code' => '27430',
        'cover_plus' => '1.35',
        'cover_plus_extra' => '1.79',
        'activities' => array (
          array (
            'name' => 'Guttering, metal, manufacturing (except aluminium)',
            'bic_code' => 'C222410'
          ),
          array (
            'name' => 'Roofing component, metal, manufacturing (except aluminium)',
            'bic_code' => 'C222420'
          ),
          array (
            'name' => 'Roofing, metal, manufacturing (except aluminium)'
          )
        )
      ),
      array (
        'name' => 'Structural metal product manufacturing (not elsewhere classified)',
        'code' => '27490',
        'cover_plus' => '1.96',
        'cover_plus_extra' => '2.59',
        'activities' => array (
          array (
            'name' => 'Architectural metal product manufacturing (exceptaluminium)',
            'bic_code' => 'C222905'
          ),
          array (
            'name' => 'Balcony, metal, manufacturing (except aluminium)',
            'bic_code' => 'C222910'
          ),
          array (
            'name' => 'Balustrade, metal, manufacturing (except aluminium)',
            'bic_code' => 'C222915'
          ),
          array (
            'name' => 'Curtain wall, metal, manufacturing (except aluminium)',
            'bic_code' => 'C222920'
          ),
          array (
            'name' => 'Door or door frame, metal, manufacturing (except aluminium)',
            'bic_code' => 'C222925'
          ),
          array (
            'name' => 'Door, fire resistant, manufacturing',
            'bic_code' => 'C222930'
          ),
          array (
            'name' => 'Fascia, metal, manufacturing (except aluminium)',
            'bic_code' => 'C222935'
          ),
          array (
            'name' => 'Fire escape, prefabricated metal, manufacturing (except aluminium)',
            'bic_code' => 'C222940'
          ),
          array (
            'name' => 'Fly screen door, metal, manufacturing (except aluminium)',
            'bic_code' => 'C222945'
          ),
          array (
            'name' => 'Garage door, metal, manufacturing (except aluminium)',
            'bic_code' => 'C222950'
          ),
          array (
            'name' => 'Gate, metal, manufacturing (except aluminium or wire)',
            'bic_code' => 'C222955'
          ),
          array (
            'name' => 'Ornamental architectural metalwork manufacturing (except aluminium)',
            'bic_code' => 'C222960'
          ),
          array (
            'name' => 'Partition, prefabricated metal, manufacturing (except aluminium)',
            'bic_code' => 'C222965'
          ),
          array (
            'name' => 'Railing, metal, manufacturing (except aluminium)',
            'bic_code' => 'C222970'
          ),
          array (
            'name' => 'Roller shutter, metal, manufacturing (except aluminium)'
          ),
          array (
            'name' => 'Shop front, metal, manufacturing (except aluminium)'
          ),
          array (
            'name' => 'Shutter, metal, manufacturing (except aluminium)'
          ),
          array (
            'name' => 'Skylight, metal, manufacturing (except aluminium)'
          ),
          array (
            'name' => 'Stair or staircase, prefabricated metal, manufacturing (except aluminium)'
          ),
          array (
            'name' => 'Window frame or sash, metal, manufacturing (except aluminium)'
          ),
          array (
            'name' => 'Window screen, metal, manufacturing (except aluminium)'
          )
        )
      ),
      array (
        'name' => 'Boiler, tank, and other heavy-gauge metal container manufacturing',
        'code' => '27692',
        'cover_plus' => '1.29',
        'cover_plus_extra' => '1.72',
        'activities' => array (
          array (
            'name' => 'Boiler, metal, manufacturing',
            'bic_code' => 'C223110'
          ),
          array (
            'name' => 'Farm storage tank, metal, manufacturing',
            'bic_code' => 'C223120'
          ),
          array (
            'name' => 'Gas cylinder manufacturing',
            'bic_code' => 'C223130'
          ),
          array (
            'name' => 'Metal vat or tank manufacturing',
            'bic_code' => 'C223140'
          ),
          array (
            'name' => 'Silo, metal, manufacturing'
          ),
          array (
            'name' => 'Steam generating boiler manufacturing'
          ),
          array (
            'name' => 'Steam superheater manufacturing'
          ),
          array (
            'name' => 'Storage tank, metal, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Metal container manufacturing (not elsewhere classified)',
        'code' => '27510',
        'cover_plus' => '0.98',
        'cover_plus_extra' => '1.31',
        'activities' => array (
          array (
            'name' => 'Bin, metal, manufacturing',
            'bic_code' => 'C223910'
          ),
          array (
            'name' => 'Can, metal, manufacturing',
            'bic_code' => 'C223920'
          ),
          array (
            'name' => 'Drum, metal, manufacturing',
            'bic_code' => 'C223930'
          ),
          array (
            'name' => 'Food and drink can manufacturing',
            'bic_code' => 'C223940'
          ),
          array (
            'name' => 'Garbage can, metal, manufacturing',
            'bic_code' => 'C223950'
          ),
          array (
            'name' => 'Letter box, metal, manufacturing',
            'bic_code' => 'C223960'
          ),
          array (
            'name' => 'Metal container, metal, manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Tool box, metal, manufacturing'
          ),
          array (
            'name' => 'Vacuum bottle and jug manufacturing'
          )
        )
      ),
      array (
        'name' => 'Sheet metal product manufacturing (except metal structural and container products)',
        'code' => '27590',
        'cover_plus' => '1.35',
        'cover_plus_extra' => '1.79',
        'activities' => array (
          array (
            'name' => 'Bottle closure, metal, manufacturing',
            'bic_code' => 'C224010'
          ),
          array (
            'name' => 'Chute, sheet metal, manufacturing',
            'bic_code' => 'C224020'
          ),
          array (
            'name' => 'Coppersmithing (except boiler making)',
            'bic_code' => 'C224030'
          ),
          array (
            'name' => 'Cornice, sheet metal, manufacturing',
            'bic_code' => 'C224040'
          ),
          array (
            'name' => 'Crown seal, metal, manufacturing',
            'bic_code' => 'C224050'
          ),
          array (
            'name' => 'Duct work, air conditioning, manufacturing'
          ),
          array (
            'name' => 'Duct, sheet metal, manufacturing'
          ),
          array (
            'name' => 'Eyelet, metal, manufacturing'
          ),
          array (
            'name' => 'Funnel, sheet metal, manufacturing'
          ),
          array (
            'name' => 'Holloware, pressed or spun metal, manufacturing'
          ),
          array (
            'name' => 'Hopper, sheet metal, manufacturing'
          ),
          array (
            'name' => 'Machine guard, sheet metal, manufacturing'
          ),
          array (
            'name' => 'Metal stampings manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Motor vehicle number plate manufacturing'
          ),
          array (
            'name' => 'Stainless steel pressed holloware manufacturing'
          ),
          array (
            'name' => 'Stove pipe, sheet metal, manufacturing'
          ),
          array (
            'name' => 'Tag, sheet metal, manufacturing'
          ),
          array (
            'name' => 'Ventilator, sheet metal, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Spring and wire product manufacturing',
        'code' => '27620',
        'cover_plus' => '0.98',
        'cover_plus_extra' => '1.31',
        'activities' => array (
          array (
            'name' => 'Barbed wire manufacturing',
            'bic_code' => 'C229110'
          ),
          array (
            'name' => 'Braided ferrous wire, cable or strip, manufacturing',
            'bic_code' => 'C229120'
          ),
          array (
            'name' => 'Chain manufacturing (except forged, cast or sprocket chain)',
            'bic_code' => 'C229130'
          ),
          array (
            'name' => 'Fence dropper, wire, manufacturing',
            'bic_code' => 'C229140'
          ),
          array (
            'name' => 'Fence post or dropper, rolled steel, manufacturing',
            'bic_code' => 'C229150'
          ),
          array (
            'name' => 'Gate, wire, manufacturing',
            'bic_code' => 'C229160'
          ),
          array (
            'name' => 'Guard, wire, manufacturing',
            'bic_code' => 'C229170'
          ),
          array (
            'name' => 'Hook, wire, manufacturing',
            'bic_code' => 'C229180'
          ),
          array (
            'name' => 'Household articles, wire, manufacturing'
          ),
          array (
            'name' => 'Nail manufacturing'
          ),
          array (
            'name' => 'Pin manufacturing (except metallic dowel pins)'
          ),
          array (
            'name' => 'Round wire manufacturing'
          ),
          array (
            'name' => 'Safety pin manufacturing'
          ),
          array (
            'name' => 'Screening, wire, manufacturing'
          ),
          array (
            'name' => 'Shopping trolley manufacturing'
          ),
          array (
            'name' => 'Skewer, metal, manufacturing'
          ),
          array (
            'name' => 'Sling, wire, manufacturing'
          ),
          array (
            'name' => 'Spike, wire, manufacturing'
          ),
          array (
            'name' => 'Spring manufacturing'
          ),
          array (
            'name' => 'Stranded ferrous wire, cable or strip, manufacturing'
          ),
          array (
            'name' => 'Welded link chain manufacturing'
          ),
          array (
            'name' => 'Wire mesh manufacturing (except reinforcing mesh)'
          ),
          array (
            'name' => 'Wire netting manufacturing'
          ),
          array (
            'name' => 'Wire product manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Woven wire product manufacturing (except mattress supports)'
          )
        )
      ),
      array (
        'name' => 'Nut, bolt, screw, and rivet manufacturing',
        'code' => '27630',
        'cover_plus' => '0.49',
        'cover_plus_extra' => '0.67',
        'activities' => array (
          array (
            'name' => 'Dowel pin, metal, manufacturing',
            'bic_code' => 'C229210'
          ),
          array (
            'name' => 'Expansion bolt, metal, manufacturing',
            'bic_code' => 'C229220'
          ),
          array (
            'name' => 'Machine screw, metal, manufacturing',
            'bic_code' => 'C229230'
          ),
          array (
            'name' => 'Masonry anchor, metal, manufacturing',
            'bic_code' => 'C229240'
          ),
          array (
            'name' => 'Nut or bolt, metal, manufacturing'
          ),
          array (
            'name' => 'Rivet, metal, manufacturing'
          ),
          array (
            'name' => 'Screw, metal, manufacturing'
          ),
          array (
            'name' => 'Set screw, metal, manufacturing'
          ),
          array (
            'name' => 'Turnbuckle, metal, manufacturing'
          ),
          array (
            'name' => 'Washer, metal, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Surface coating and finishing',
        'code' => '27640',
        'cover_plus' => '1.29',
        'cover_plus_extra' => '1.72',
        'activities' => array (
          array (
            'name' => 'Anodising',
            'bic_code' => 'C229305'
          ),
          array (
            'name' => 'Boron nitride coating of machine tool parts',
            'bic_code' => 'C229310'
          ),
          array (
            'name' => 'Brass finishing or plating',
            'bic_code' => 'C229315'
          ),
          array (
            'name' => 'Cadmium plating',
            'bic_code' => 'C229320'
          ),
          array (
            'name' => 'Chromium plating',
            'bic_code' => 'C229325'
          ),
          array (
            'name' => 'Copper plating',
            'bic_code' => 'C229330'
          ),
          array (
            'name' => 'Enamelling of metal',
            'bic_code' => 'C229335'
          ),
          array (
            'name' => 'Engraving on metal including laser (except printing, process or photographic)',
            'bic_code' => 'C229340'
          ),
          array (
            'name' => 'Galvanising of client supplied materials',
            'bic_code' => 'C229345'
          ),
          array (
            'name' => 'Gold plating',
            'bic_code' => 'C229355'
          ),
          array (
            'name' => 'Heat treating metal or metal products',
            'bic_code' => 'C229360'
          ),
          array (
            'name' => 'Metal coating (not elsewhere classified)',
            'bic_code' => 'C229365'
          ),
          array (
            'name' => 'Metal polishing or finishing',
            'bic_code' => 'C229370'
          ),
          array (
            'name' => 'Nickel plating',
            'bic_code' => 'C229375'
          ),
          array (
            'name' => 'Painting of manufactured metal products',
            'bic_code' => 'C229380'
          ),
          array (
            'name' => 'Painting of manufactured wooden products',
            'bic_code' => 'C259915'
          ),
          array (
            'name' => 'Plastic coating of metal'
          ),
          array (
            'name' => 'Powder coating of metal and metal products'
          ),
          array (
            'name' => 'Silver plating'
          ),
          array (
            'name' => 'Vitreous enamelling'
          )
        )
      ),
      array (
        'name' => 'Fabricated metal product manufacturing (not elsewhere classified)',
        'code' => '27690',
        'cover_plus' => '1.29',
        'cover_plus_extra' => '1.72',
        'activities' => array (
          array (
            'name' => 'Ammunition manufacturing',
            'bic_code' => 'C229902'
          ),
          array (
            'name' => 'Awning or blind, metal, manufacturing (including aluminium)',
            'bic_code' => 'C229905'
          ),
          array (
            'name' => 'Bathroom or toilet fittings, metal, manufacturing',
            'bic_code' => 'C229908'
          ),
          array (
            'name' => 'Blow torch manufacturing',
            'bic_code' => 'C229910'
          ),
          array (
            'name' => 'Bottle or can opener manufacturing (except power operated)',
            'bic_code' => 'C229912'
          ),
          array (
            'name' => 'Button, metal, manufacturing',
            'bic_code' => 'C229915'
          ),
          array (
            'name' => 'Clothes hoist manufacturing',
            'bic_code' => 'C229918'
          ),
          array (
            'name' => 'Coupling, metal, manufacturing',
            'bic_code' => 'C229920'
          ),
          array (
            'name' => 'Cutlery manufacturing (except of solid silver or gold)',
            'bic_code' => 'C229922'
          ),
          array (
            'name' => 'Door handle, metal, manufacturing',
            'bic_code' => 'C229925'
          ),
          array (
            'name' => 'Drilling bit manufacturing (except twist drills)',
            'bic_code' => 'C229928'
          ),
          array (
            'name' => 'Fabricated metal product manufacturing (not elsewhere classified)',
            'bic_code' => 'C229930'
          ),
          array (
            'name' => 'Fire sprinkler manufacturing',
            'bic_code' => 'C229932'
          ),
          array (
            'name' => 'Firearm manufacturing',
            'bic_code' => 'C229935'
          ),
          array (
            'name' => 'Fittings, steam, gas or water, manufacturing (nonferrous metal',
            'bic_code' => 'C229938'
          ),
          array (
            'name' => 'Garden tool manufacturing (except power operated)',
            'bic_code' => 'C229940'
          ),
          array (
            'name' => 'General engineering',
            'bic_code' => 'C229942'
          ),
          array (
            'name' => 'Grease gun manufacturing (except pneumatic or power operated)',
            'bic_code' => 'C229945'
          ),
          array (
            'name' => 'Hand tool manufacturing (except pneumatic or power operated)',
            'bic_code' => 'C229948'
          ),
          array (
            'name' => 'Handbag frame, metal, manufacturing',
            'bic_code' => 'C229950'
          ),
          array (
            'name' => 'Key manufacturing',
            'bic_code' => 'C229952'
          ),
          array (
            'name' => 'Knife blank manufacturing',
            'bic_code' => 'C229955'
          ),
          array (
            'name' => 'Knife, hand held, manufacturing (except power operated)',
            'bic_code' => 'C229958'
          ),
          array (
            'name' => 'Livestock yarding equipment, metal, manufacturing',
            'bic_code' => 'C229960'
          ),
          array (
            'name' => 'Lock manufacturing',
            'bic_code' => 'C229962'
          ),
          array (
            'name' => 'Machine knife or blades manufacturing',
            'bic_code' => 'C229965'
          ),
          array (
            'name' => 'Mast, aluminium, manufacturing',
            'bic_code' => 'C229968'
          ),
          array (
            'name' => 'Mattress support, woven wire, link mesh or wire spring, manufacturing (except upholstered)',
            'bic_code' => 'C229970'
          ),
          array (
            'name' => 'Pipe coil manufacturing',
            'bic_code' => 'C229972'
          ),
          array (
            'name' => 'Pipe fittings, non-ferrous metal, manufacturing',
            'bic_code' => 'C229975'
          ),
          array (
            'name' => 'Platework (not elsewhere classified)',
            'bic_code' => 'C229978'
          ),
          array (
            'name' => 'Razor or razor blade manufacturing (except power operated)',
            'bic_code' => 'C229980'
          ),
          array (
            'name' => 'Scissor manufacturing (except electric)',
            'bic_code' => 'C229985'
          ),
          array (
            'name' => 'Screwdriver manufacturing (including bits or blades;except pneumatic or power operated)',
            'bic_code' => 'C249910'
          ),
          array (
            'name' => 'Soldering iron manufacturing (except power operated)'
          ),
          array (
            'name' => 'Sprocket chain manufacturing'
          ),
          array (
            'name' => 'Steel wool manufacturing'
          ),
          array (
            'name' => 'Tackle block, metal, manufacturing'
          ),
          array (
            'name' => 'Tube fittings, non-ferrous metal, manufacturing'
          ),
          array (
            'name' => 'Tubing, flexible metal, manufacturing'
          ),
          array (
            'name' => 'Turnstile, metal, manufacturing'
          ),
          array (
            'name' => 'Valve or valve parts, steam, gas or water, manufacturing (non-ferrous metal)'
          ),
          array (
            'name' => 'Vice, bench, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Motor vehicle manufacturing',
        'code' => '28110',
        'cover_plus' => '1.04',
        'cover_plus_extra' => '1.39',
        'activities' => array (
          array (
            'name' => 'Bus manufacturing',
            'bic_code' => 'C231105'
          ),
          array (
            'name' => 'Hydrogen, fuelcell, hybrid or electric vehicle manufacturing',
            'bic_code' => 'C231110'
          ),
          array (
            'name' => 'Motor car manufacturing',
            'bic_code' => 'C231120'
          ),
          array (
            'name' => 'Motor vehicle assembling'
          ),
          array (
            'name' => 'Motor vehicle engine manufacturing'
          ),
          array (
            'name' => 'Truck manufacturing (except off-highway)'
          ),
          array (
            'name' => 'Van manufacturing'
          )
        )
      ),
      array (
        'name' => 'Motor vehicle body and trailer manufacturing',
        'code' => '28120',
        'cover_plus' => '1.36',
        'cover_plus_extra' => '1.81',
        'activities' => array (
          array (
            'name' => 'Ambulance converting',
            'bic_code' => 'C231210'
          ),
          array (
            'name' => 'Boat trailer manufacturing',
            'bic_code' => 'C231220'
          ),
          array (
            'name' => 'Bus vehicle body assembly on supplied motor and chassis',
            'bic_code' => 'C231230'
          ),
          array (
            'name' => 'Campervan manufacturing',
            'bic_code' => 'C231240'
          ),
          array (
            'name' => 'Caravan manufacturing',
            'bic_code' => 'C231250'
          ),
          array (
            'name' => 'Firetruck converting',
            'bic_code' => 'C231255'
          ),
          array (
            'name' => 'Horse float manufacturing',
            'bic_code' => 'C231260'
          ),
          array (
            'name' => 'Motor vehicle converting',
            'bic_code' => 'C231270'
          ),
          array (
            'name' => 'Stock crate manufacturing',
            'bic_code' => 'C231280'
          ),
          array (
            'name' => 'Trailer manufacturing'
          ),
          array (
            'name' => 'Truck body manufacturing'
          ),
          array (
            'name' => 'Truck tipper tray manufacturing'
          )
        )
      ),
      array (
        'name' => 'Automotive electrical components manufacturing',
        'code' => '28130',
        'cover_plus' => '0.53',
        'cover_plus_extra' => '0.73',
        'activities' => array (
          array (
            'name' => 'Air conditioner, automotive, manufacturing',
            'bic_code' => 'C231310'
          ),
          array (
            'name' => 'Alternator manufacturing'
          ),
          array (
            'name' => 'Automotive electrical component manufacturing (except batteries)'
          ),
          array (
            'name' => 'Automotive electrical component, factory reconditioning'
          ),
          array (
            'name' => 'Automotive wire manufacturing'
          ),
          array (
            'name' => 'Car horn, electric, manufacturing'
          ),
          array (
            'name' => 'Heater and demister, automotive, manufacturing'
          ),
          array (
            'name' => 'Ignition coil manufacturing'
          ),
          array (
            'name' => 'Light fittings, automotive, manufacturing'
          ),
          array (
            'name' => 'Spark plug manufacturing'
          ),
          array (
            'name' => 'Spotlight, automotive, manufacturing'
          ),
          array (
            'name' => 'Starter motor manufacturing'
          ),
          array (
            'name' => 'Windscreen wiper manufacturing'
          )
        )
      ),
      array (
        'name' => 'Motor vehicle parts manufacturing (not elsewhere classified)',
        'code' => '28190',
        'cover_plus' => '1.04',
        'cover_plus_extra' => '1.39',
        'activities' => array (
          array (
            'name' => 'Automotive parts manufacturing (not elsewhere classified)',
            'bic_code' => 'C231910'
          ),
          array (
            'name' => 'Car accessory manufacturing',
            'bic_code' => 'C231920'
          ),
          array (
            'name' => 'Child car restraint manufacturing',
            'bic_code' => 'C231930 '
          ),
          array (
            'name' => 'Clutch assembly manufacturing',
            'bic_code' => 'C231940'
          ),
          array (
            'name' => 'Factory reconditioning of changeover motors',
            'bic_code' => 'C231950'
          ),
          array (
            'name' => 'Gearbox manufacturing',
            'bic_code' => 'C231960'
          ),
          array (
            'name' => 'Marine conversion of automotive engines',
            'bic_code' => 'C231970'
          ),
          array (
            'name' => 'Muffler manufacturing'
          ),
          array (
            'name' => 'Radiator manufacturing'
          ),
          array (
            'name' => 'Roof rack manufacturing'
          ),
          array (
            'name' => 'Seat belt manufacturing'
          ),
          array (
            'name' => 'Shock absorber manufacturing'
          ),
          array (
            'name' => 'Suspension component manufacturing'
          ),
          array (
            'name' => 'Transmission manufacturing'
          ),
          array (
            'name' => 'Wheel manufacturing'
          )
        )
      ),
      array (
        'name' => 'Shipbuilding and ship repair services (any vessel 50 tonnes displacement or over)',
        'code' => '28210',
        'cover_plus' => '0.98',
        'cover_plus_extra' => '1.31',
        'activities' => array (
          array (
            'name' => 'Drydock operation',
            'bic_code' => 'C239110'
          ),
          array (
            'name' => 'Hull and deck coating',
            'bic_code' => 'C239120'
          ),
          array (
            'name' => 'Hull cleaning',
            'bic_code' => 'C239130'
          ),
          array (
            'name' => 'Ship painting',
            'bic_code' => 'C239140'
          ),
          array (
            'name' => 'Ship repairing'
          ),
          array (
            'name' => 'Ship wrecking'
          ),
          array (
            'name' => 'Shipbuilding'
          )
        )
      ),
      array (
        'name' => 'Boatbuilding and boat repair services (all vessels under 50 tonnes displacement)',
        'code' => '28220',
        'cover_plus' => '1.54',
        'cover_plus_extra' => '2.04',
        'activities' => array (
          array (
            'name' => 'Boat painting',
            'bic_code' => 'C239210'
          ),
          array (
            'name' => 'Boat repairing',
            'bic_code' => 'C239220'
          ),
          array (
            'name' => 'Boatbuilding',
            'bic_code' => 'C239230'
          ),
          array (
            'name' => 'Canoe manufacturing'
          ),
          array (
            'name' => 'Dinghy manufacturing'
          ),
          array (
            'name' => 'Inflatable boat manufacturing'
          ),
          array (
            'name' => 'Jet boat building'
          ),
          array (
            'name' => 'Motorboat, inboard and outboard, building'
          ),
          array (
            'name' => 'Powerboat building'
          ),
          array (
            'name' => 'Sailboat manufacturing'
          ),
          array (
            'name' => 'Yacht construction'
          )
        )
      ),
      array (
        'name' => 'Railway rolling stock manufacturing and repair services',
        'code' => '28230',
        'cover_plus' => '1.36',
        'cover_plus_extra' => '1.81',
        'activities' => array (
          array (
            'name' => 'Cable car manufacturing',
            'bic_code' => 'C239310'
          ),
          array (
            'name' => 'Locomotive manufacturing',
            'bic_code' => 'C239320'
          ),
          array (
            'name' => 'Rail carriage manufacturing',
            'bic_code' => 'C239330'
          ),
          array (
            'name' => 'Railway rolling stock manufacturing'
          ),
          array (
            'name' => 'Repair of locomotives and rolling stock'
          ),
          array (
            'name' => 'Tram manufacturing'
          )
        )
      ),
      array (
        'name' => 'Aircraft manufacturing and repair services',
        'code' => '28240',
        'cover_plus' => '0.36',
        'cover_plus_extra' => '0.50',
        'activities' => array (
          array (
            'name' => 'Aircraft engine building or repairing',
            'bic_code' => 'C239410'
          ),
          array (
            'name' => 'Aircraft manufacturing',
            'bic_code' => 'C239420'
          ),
          array (
            'name' => 'Airframe building and repair',
            'bic_code' => 'C239430'
          ),
          array (
            'name' => 'Avionics equipment repairing (not elsewhere classified)',
            'bic_code' => 'C239440'
          ),
          array (
            'name' => 'Glider manufacturing and repair (except hang glider)'
          ),
          array (
            'name' => 'Guided missile manufacturing'
          ),
          array (
            'name' => 'Helicopter manufacturing or repairing'
          )
        )
      ),
      array (
        'name' => 'Transport equipment manufacturing (not elsewhere classified)',
        'code' => '28290',
        'cover_plus' => '1.04',
        'cover_plus_extra' => '1.39',
        'activities' => array (
          array (
            'name' => 'Baby stroller manufacturing',
            'bic_code' => 'C239910'
          ),
          array (
            'name' => 'Bicycle manufacturing',
            'bic_code' => 'C239920'
          ),
          array (
            'name' => 'Golf buggy manufacturing',
            'bic_code' => 'C239930'
          ),
          array (
            'name' => 'Horse drawn vehicle manufacturing',
            'bic_code' => 'C239940'
          ),
          array (
            'name' => 'Hovercraft manufacturing',
            'bic_code' => 'C239950'
          ),
          array (
            'name' => 'Motor cycle manufacturing'
          ),
          array (
            'name' => 'Scooter, electric, manufacturing'
          ),
          array (
            'name' => 'Trotting gig manufacturing'
          ),
          array (
            'name' => 'Unusual terrain vehicle manufacturing'
          ),
          array (
            'name' => 'Wheelbarrow manufacturing'
          ),
          array (
            'name' => 'Wheelchair manufacturing'
          ),
          array (
            'name' => 'Wheelchair, electric, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Photographic, optical, and ophthalmic equipment manufacturing',
        'code' => '28310',
        'cover_plus' => '0.20',
        'cover_plus_extra' => '0.29',
        'activities' => array (
          array (
            'name' => 'Binocular manufacturing',
            'bic_code' => 'C241110'
          ),
          array (
            'name' => 'Camera manufacturing',
            'bic_code' => 'C241120'
          ),
          array (
            'name' => 'Contact lens manufacturing',
            'bic_code' => 'C241130'
          ),
          array (
            'name' => 'Microscope manufacturing',
            'bic_code' => 'C241140'
          ),
          array (
            'name' => 'Ophthalmic article manufacturing',
            'bic_code' => 'C241150'
          ),
          array (
            'name' => 'Optical instrument or equipment manufacturing',
            'bic_code' => 'C241160'
          ),
          array (
            'name' => 'Optical lens grinding'
          ),
          array (
            'name' => 'Spectacle frame manufacturing'
          ),
          array (
            'name' => 'Spectacle lens grinding'
          ),
          array (
            'name' => 'Sunglasses manufacturing'
          ),
          array (
            'name' => 'Telescope manufacturing'
          )
        )
      ),
      array (
        'name' => 'Medical and surgical equipment and prosthetics manufacturing',
        'code' => '28320',
        'cover_plus' => '0.20',
        'cover_plus_extra' => '0.29',
        'activities' => array (
          array (
            'name' => 'Artificial eye manufacturing',
            'bic_code' => 'C241205'
          ),
          array (
            'name' => 'Artificial joint manufacturing',
            'bic_code' => 'C241210'
          ),
          array (
            'name' => 'Artificial limb manufacturing',
            'bic_code' => 'C241215'
          ),
          array (
            'name' => 'Dental amalgam manufacturing',
            'bic_code' => 'C241220'
          ),
          array (
            'name' => 'Dental chair manufacturing (fitted with mechanical device)',
            'bic_code' => 'C241225'
          ),
          array (
            'name' => 'Dental instrument or equipment manufacturing',
            'bic_code' => 'C241230'
          ),
          array (
            'name' => 'Dental plaster or cement manufacturing',
            'bic_code' => 'C241235'
          ),
          array (
            'name' => 'Denture manufacturing',
            'bic_code' => 'C241240'
          ),
          array (
            'name' => 'Electromedical equipment manufacturing',
            'bic_code' => 'C241245'
          ),
          array (
            'name' => 'First aid equipment manufacturing',
            'bic_code' => 'C241250'
          ),
          array (
            'name' => 'Hearing aid manufacturing'
          ),
          array (
            'name' => 'Hypodermic needle or syringe manufacturing'
          ),
          array (
            'name' => 'Magnetic resonance imaging (medical) equipment manufacturing'
          ),
          array (
            'name' => 'Medical diagnostic apparatus manufacturing'
          ),
          array (
            'name' => 'Medical equipment manufacturing'
          ),
          array (
            'name' => 'Medical ultrasound equipment manufacturing'
          ),
          array (
            'name' => 'Orthotics (arch support) manufacturing'
          ),
          array (
            'name' => 'Pacemaker manufacturing'
          ),
          array (
            'name' => 'Respirator manufacturing'
          ),
          array (
            'name' => 'Surgical equipment manufacturing'
          ),
          array (
            'name' => 'Thermometer, medical, manufacturing'
          ),
          array (
            'name' => 'Veterinary instrument manufacturing'
          )
        )
      ),
      array (
        'name' => 'Metal and mineral wholesaling',
        'code' => '45220',
        'cover_plus' => '1.31',
        'cover_plus_extra' => '1.74',
        'activities' => array (
          array (
            'name' => 'Aluminium or aluminium alloy wholesaling',
            'bic_code' => 'F332210'
          ),
          array (
            'name' => 'Bearing metal wholesaling',
            'bic_code' => 'F332220'
          ),
          array (
            'name' => 'Briquette wholesaling',
            'bic_code' => 'F332230'
          ),
          array (
            'name' => 'Charcoal wholesaling',
            'bic_code' => 'F332240'
          ),
          array (
            'name' => 'Coal wholesaling',
            'bic_code' => 'F332250'
          ),
          array (
            'name' => 'Coke wholesaling',
            'bic_code' => 'F332260'
          ),
          array (
            'name' => 'Copper or copper alloy wholesaling',
            'bic_code' => 'F332270'
          ),
          array (
            'name' => 'Metal scrap wholesaling',
            'bic_code' => 'F332275'
          ),
          array (
            'name' => 'Metallic ore wholesaling'
          ),
          array (
            'name' => 'Mineral earths wholesaling'
          ),
          array (
            'name' => 'Mineral wholesaling (not elsewhere classified)'
          ),
          array (
            'name' => 'Precious metal scrap dealing (wholesaling)'
          ),
          array (
            'name' => 'Steel bar, plate, rod, sheet or strip wholesaling'
          ),
          array (
            'name' => 'Steel wholesaling'
          ),
          array (
            'name' => 'Zinc wholesaling'
          )
        )
      ),
      array (
        'name' => 'Professional and scientific goods wholesaling',
        'code' => '46120',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Aeronautical equipment wholesaling',
            'bic_code' => 'F349110'
          ),
          array (
            'name' => 'Dental instrument or equipment wholesaling',
            'bic_code' => 'F349120'
          ),
          array (
            'name' => 'Draughting instrument wholesaling',
            'bic_code' => 'F349130'
          ),
          array (
            'name' => 'Mathematical instrument wholesaling'
          ),
          array (
            'name' => 'Medical equipment wholesaling'
          ),
          array (
            'name' => 'Navigation equipment wholesaling'
          ),
          array (
            'name' => 'Optical instrument wholesaling'
          ),
          array (
            'name' => 'Professional equipment wholesaling (not elsewhere classified)'
          ),
          array (
            'name' => 'Scientific equipment wholesaling'
          ),
          array (
            'name' => 'Surgical equipment wholesaling'
          ),
          array (
            'name' => 'X-ray equipment or film wholesaling'
          )
        )
      ),
      array (
        'name' => 'Professional and scientific equipment manufacturing (not elsewhere classified)',
        'code' => '28390',
        'cover_plus' => '0.20',
        'cover_plus_extra' => '0.29',
        'activities' => array (
          array (
            'name' => 'Clock manufacturing',
            'bic_code' => 'C241905'
          ),
          array (
            'name' => 'Control equipment, electrical, manufacturing',
            'bic_code' => 'C241910'
          ),
          array (
            'name' => 'Electricity and electric signal testing equipment manufacturing',
            'bic_code' => 'C241915'
          ),
          array (
            'name' => 'Gas meter manufacturing',
            'bic_code' => 'C241920'
          ),
          array (
            'name' => 'Global positioning system (GPS) equipment manufacturing',
            'bic_code' => 'C241925'
          ),
          array (
            'name' => 'Laboratory analytic instrument manufacturing',
            'bic_code' => 'C241930'
          ),
          array (
            'name' => 'Magnetic resonance imaging (except medical) equipment manufacturing',
            'bic_code' => 'C241935'
          ),
          array (
            'name' => 'Measuring instrument manufacturing',
            'bic_code' => 'C241940'
          ),
          array (
            'name' => 'Meteorological instrument manufacturing (not elsewhere classified)',
            'bic_code' => 'C241943'
          ),
          array (
            'name' => 'Nautical instrument manufacturing',
            'bic_code' => 'C241945'
          ),
          array (
            'name' => 'Navigational systems and equipment manufacturing',
            'bic_code' => 'C241950'
          ),
          array (
            'name' => 'Optical fibre cable, uninsulated, manufacturing',
            'bic_code' => 'C241955'
          ),
          array (
            'name' => 'Parking meter manufacturing',
            'bic_code' => 'C241960 '
          ),
          array (
            'name' => 'Professional and scientific equipment manufacturing (not elsewhere classified)',
            'bic_code' => 'C241965'
          ),
          array (
            'name' => 'Radar systems and equipment manufacturing',
            'bic_code' => 'C241970'
          ),
          array (
            'name' => 'Radio remote control equipment manufacturing (not elsewhere classified)',
            'bic_code' => 'C241975'
          ),
          array (
            'name' => 'Signalling equipment, electrical, manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Sonar systems and equipment manufacturing'
          ),
          array (
            'name' => 'Surveying instrument manufacturing'
          ),
          array (
            'name' => 'Taxi meter manufacturing'
          ),
          array (
            'name' => 'Technology education equipment manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Traffic signal, electrical, manufacturing'
          ),
          array (
            'name' => 'Watch manufacturing'
          ),
          array (
            'name' => 'Water meter manufacturing'
          )
        )
      ),
      array (
        'name' => 'Computer and electronic office equipment manufacturing',
        'code' => '28410',
        'cover_plus' => '0.20',
        'cover_plus_extra' => '0.29',
        'activities' => array (
          array (
            'name' => 'Calculator manufacturing',
            'bic_code' => 'C242110'
          ),
          array (
            'name' => 'Cash register manufacturing',
            'bic_code' => 'C242120'
          ),
          array (
            'name' => 'Computer manufacturing'
          ),
          array (
            'name' => 'Computer monitor manufacturing'
          ),
          array (
            'name' => 'Computer peripheral manufacturing'
          ),
          array (
            'name' => 'Computer printer manufacturing'
          ),
          array (
            'name' => 'Joystick manufacturing'
          ),
          array (
            'name' => 'Keyboard manufacturing'
          ),
          array (
            'name' => 'Laptop manufacturing'
          ),
          array (
            'name' => 'Office machine (electronic) manufacturing'
          ),
          array (
            'name' => 'Photocopying machine manufacturing'
          ),
          array (
            'name' => 'Typewriter (electronic) manufacturing'
          )
        )
      ),
      array (
        'name' => 'Communications equipment manufacturing',
        'code' => '28420',
        'cover_plus' => '0.20',
        'cover_plus_extra' => '0.29',
        'activities' => array (
          array (
            'name' => 'Cable television equipment manufacturing',
            'bic_code' => 'C242210'
          ),
          array (
            'name' => 'Data transmission equipment (bridges, gateways, routers etc..) manufacturing',
            'bic_code' => 'C242220'
          ),
          array (
            'name' => 'Intercom equipment manufacturing',
            'bic_code' => 'C242230'
          ),
          array (
            'name' => 'Modem manufacturing',
            'bic_code' => 'C242240'
          ),
          array (
            'name' => 'Pager manufacturing',
            'bic_code' => 'C242250'
          ),
          array (
            'name' => 'Radio broadcast studio equipment manufacturing'
          ),
          array (
            'name' => 'Radio transceiver manufacturing'
          ),
          array (
            'name' => 'Radio transmitter manufacturing'
          ),
          array (
            'name' => 'Remote monitoring alarm system equipment manufacturing'
          ),
          array (
            'name' => 'Telecommunication equipment manufacturing'
          ),
          array (
            'name' => 'Telephone equipment manufacturing'
          ),
          array (
            'name' => 'Telephone switching equipment manufacturing'
          ),
          array (
            'name' => 'Telephone, cellular, manufacturing'
          ),
          array (
            'name' => 'Television antenna or parts manufacturing'
          ),
          array (
            'name' => 'Television studio equipment manufacturing'
          )
        )
      ),
      array (
        'name' => 'Electronic equipment manufacturing (not elsewhere classified)',
        'code' => '28490',
        'cover_plus' => '0.36',
        'cover_plus_extra' => '0.50',
        'activities' => array (
          array (
            'name' => 'Amplifier, audio-frequency, manufacturing',
            'bic_code' => 'C242910'
          ),
          array (
            'name' => 'Blank computer disc manufacturing',
            'bic_code' => 'C242920'
          ),
          array (
            'name' => 'Blank video cassette manufacturing',
            'bic_code' => 'C242930'
          ),
          array (
            'name' => 'Circuit board, printed (bare or loaded),manufacturing',
            'bic_code' => 'C242940'
          ),
          array (
            'name' => 'Compact disc player manufacturing',
            'bic_code' => 'C242950'
          ),
          array (
            'name' => 'Earphone manufacturing'
          ),
          array (
            'name' => 'Electronic circuit component manufacturing'
          ),
          array (
            'name' => 'Fire alarm apparatus manufacturing'
          ),
          array (
            'name' => 'Headphone manufacturing'
          ),
          array (
            'name' => 'Integrated circuit manufacturing'
          ),
          array (
            'name' => 'Loudspeaker manufacturing'
          ),
          array (
            'name' => 'Microphone manufacturing'
          ),
          array (
            'name' => 'Radio receiving set manufacturing'
          ),
          array (
            'name' => 'Record player manufacturing'
          ),
          array (
            'name' => 'Semi-conductor manufacturing'
          ),
          array (
            'name' => 'Sound recording equipment manufacturing'
          ),
          array (
            'name' => 'Sound reproducing equipment manufacturing'
          ),
          array (
            'name' => 'Tape recorder manufacturing'
          ),
          array (
            'name' => 'Television receiving set manufacturing'
          ),
          array (
            'name' => 'Transistor manufacturing'
          )
        )
      ),
      array (
        'name' => 'Electric cable and wire manufacturing',
        'code' => '28520',
        'cover_plus' => '0.36',
        'cover_plus_extra' => '0.50',
        'activities' => array (
          array (
            'name' => 'Co-axial cable manufacturing',
            'bic_code' => 'C243105'
          ),
          array (
            'name' => 'Fuse wire manufacturing',
            'bic_code' => 'C243110'
          ),
          array (
            'name' => 'Non-ferrous cable, wire or strip manufacturing',
            'bic_code' => 'C243120'
          ),
          array (
            'name' => 'Optical fibre cable, insulated, manufacturing'
          ),
          array (
            'name' => 'Telecommunications cable manufacturing'
          ),
          array (
            'name' => 'Wire or cable, electric, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Electric lighting equipment manufacturing',
        'code' => '28540',
        'cover_plus' => '0.36',
        'cover_plus_extra' => '0.50',
        'activities' => array (
          array (
            'name' => 'Bulb or tube, electric light, manufacturing',
            'bic_code' => 'C243210'
          ),
          array (
            'name' => 'Element, electrical, manufacturing',
            'bic_code' => 'C243220'
          ),
          array (
            'name' => 'Fittings, electric light, manufacturing (except automotive)',
            'bic_code' => 'C243230'
          ),
          array (
            'name' => 'Flashlight bulb manufacturing',
            'bic_code' => 'C243240'
          ),
          array (
            'name' => 'Lamp, infra-red or ultra-violet, manufacturing'
          ),
          array (
            'name' => 'Neon sign manufacturing'
          ),
          array (
            'name' => 'Sign, electric, manufacturing'
          ),
          array (
            'name' => 'Spotlight manufacturing (except automotive)'
          )
        )
      ),
      array (
        'name' => 'Electrical equipment manufacturing (not elsewhere classified)',
        'code' => '28590',
        'cover_plus' => '0.36',
        'cover_plus_extra' => '0.50',
        'activities' => array (
          array (
            'name' => 'Battery manufacturing (including motor vehicles)',
            'bic_code' => 'C243910'
          ),
          array (
            'name' => 'Brush, carbon, manufacturing',
            'bic_code' => 'C243920'
          ),
          array (
            'name' => 'Distribution box or board, electricity, manufacturing',
            'bic_code' => 'C243930'
          ),
          array (
            'name' => 'Dry cell battery manufacturing',
            'bic_code' => 'C243940'
          ),
          array (
            'name' => 'Dynamo manufacturing (not elsewhere classified)',
            'bic_code' => 'C243960'
          ),
          array (
            'name' => 'Electric motor manufacturing (not elsewhere classified)',
            'bic_code' => 'C243970'
          ),
          array (
            'name' => 'Electric motor rewinding'
          ),
          array (
            'name' => 'Electrical equipment or machinery manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Furnace, electric, manufacturing (except space heaters)'
          ),
          array (
            'name' => 'Fuse or cutout manufacturing'
          ),
          array (
            'name' => 'Generator manufacturing (except wind powered)'
          ),
          array (
            'name' => 'Magnet manufacturing'
          ),
          array (
            'name' => 'Soldering iron, electrical, manufacturing'
          ),
          array (
            'name' => 'Transformer manufacturing'
          ),
          array (
            'name' => 'Transmission equipment, electrical, manufacturing'
          ),
          array (
            'name' => 'Welding equipment, electrical, manufacturing'
          ),
          array (
            'name' => 'Wet cell battery manufacturing'
          )
        )
      ),
      array (
        'name' => 'Whiteware appliance manufacturing',
        'code' => '28511',
        'cover_plus' => '0.36',
        'cover_plus_extra' => '0.50',
        'activities' => array (
          array (
            'name' => 'Barbecue, solid fuel or gas, manufacturing',
            'bic_code' => 'C244110'
          ),
          array (
            'name' => 'Clothes drier, domestic, manufacturing',
            'bic_code' => 'C244120'
          ),
          array (
            'name' => 'Dishwasher, domestic, manufacturing'
          ),
          array (
            'name' => 'Food waste disposal unit, domestic, manufacturing'
          ),
          array (
            'name' => 'Freezer, domestic, manufacturing'
          ),
          array (
            'name' => 'Microwave oven, domestic, manufacturing'
          ),
          array (
            'name' => 'Ovens, domestic, manufacturing'
          ),
          array (
            'name' => 'Rangehood, domestic, manufacturing'
          ),
          array (
            'name' => 'Refrigerator compressor, domestic, manufacturing'
          ),
          array (
            'name' => 'Refrigerator, domestic, manufacturing'
          ),
          array (
            'name' => 'Stove, domestic, manufacturing'
          ),
          array (
            'name' => 'Washing machine, domestic, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Domestic appliance manufacturing (not elsewhere classified)',
        'code' => '28510',
        'cover_plus' => '0.36',
        'cover_plus_extra' => '0.50',
        'activities' => array (
          array (
            'name' => 'Air conditioner compressor, domestic, manufacturing',
            'bic_code' => 'C244910'
          ),
          array (
            'name' => 'Air conditioner, domestic, manufacturing',
            'bic_code' => 'C244920'
          ),
          array (
            'name' => 'Blanket, electric, manufacturing',
            'bic_code' => 'C244930'
          ),
          array (
            'name' => 'Domestic appliance manufacturing (not elsewhere classified)',
            'bic_code' => 'C244940'
          ),
          array (
            'name' => 'Electric toothbrush manufacturing',
            'bic_code' => 'C244950'
          ),
          array (
            'name' => 'Fan, domestic electric, manufacturing',
            'bic_code' => 'C244960'
          ),
          array (
            'name' => 'Gas heating appliance, domestic, manufacturing',
            'bic_code' => 'C244970'
          ),
          array (
            'name' => 'Hair drier, domestic electric, manufacturing'
          ),
          array (
            'name' => 'Hair drier, domestic electric, manufacturing'
          ),
          array (
            'name' => 'Heater, domestic, manufacturing'
          ),
          array (
            'name' => 'Hot water system, domestic, manufacturing'
          ),
          array (
            'name' => 'Hot water urn, domestic, manufacturing'
          ),
          array (
            'name' => 'Kerosene heater, domestic, manufacturing'
          ),
          array (
            'name' => 'Kettle or jug, electric, manufacturing'
          ),
          array (
            'name' => 'Oil heater, domestic, manufacturing'
          ),
          array (
            'name' => 'Sewing machine, domestic, manufacturing'
          ),
          array (
            'name' => 'Space heater, domestic, manufacturing'
          ),
          array (
            'name' => 'Toaster, domestic electric, manufacturing'
          ),
          array (
            'name' => 'Vacuum cleaner, domestic, manufacturing'
          ),
          array (
            'name' => 'Water treatment equipment, domestic, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Pump and compressor manufacturing',
        'code' => '28660',
        'cover_plus' => '0.58',
        'cover_plus_extra' => '0.79',
        'activities' => array (
          array (
            'name' => 'Air compressor manufacturing',
            'bic_code' => 'C245110'
          ),
          array (
            'name' => 'Gas compressor manufacturing (except refrigeration or air conditioning compressors)',
            'bic_code' => 'C245120'
          ),
          array (
            'name' => 'Hydraulic pump, fluid power, manufacturing'
          ),
          array (
            'name' => 'Petrol pump manufacturing'
          ),
          array (
            'name' => 'Pump manufacturing'
          ),
          array (
            'name' => 'Pumping equipment manufacturing'
          )
        )
      ),
      array (
        'name' => 'Agricultural product wholesaling (not elsewhere classified)',
        'code' => '45190',
        'cover_plus' => '0.48',
        'cover_plus_extra' => '0.66',
        'activities' => array (
          array (
            'name' => 'Farm produce wholesaling (not elsewhere classified)',
            'bic_code' => 'F331905'
          ),
          array (
            'name' => 'Feed wholesaling',
            'bic_code' => 'F331910'
          ),
          array (
            'name' => 'Flower, cut, wholesaling',
            'bic_code' => 'F331915 '
          ),
          array (
            'name' => 'Hide wholesaling',
            'bic_code' => 'F331920'
          ),
          array (
            'name' => 'Leather wholesaling',
            'bic_code' => 'F331925'
          ),
          array (
            'name' => 'Livestock wholesaling',
            'bic_code' => 'F331930'
          ),
          array (
            'name' => 'Meat meal wholesaling',
            'bic_code' => 'F331935'
          ),
          array (
            'name' => 'Nursery stock, horticultural, wholesaling',
            'bic_code' => 'F331940'
          ),
          array (
            'name' => 'Seed, farm or garden, wholesaling',
            'bic_code' => 'F331945'
          ),
          array (
            'name' => 'Sugar, raw, wholesaling',
            'bic_code' => 'F331950'
          ),
          array (
            'name' => 'Tallow wholesaling',
            'bic_code' => 'F331955'
          ),
          array (
            'name' => 'Tobacco leaf wholesaling',
            'bic_code' => 'F331960'
          ),
          array (
            'name' => 'Tree or shrub, potted, wholesaling'
          ),
          array (
            'name' => 'Vegetable oil meal wholesaling'
          )
        )
      ),
      array (
        'name' => 'Fixed space heating, cooling, and ventilation equipment manufacturing',
        'code' => '28670',
        'cover_plus' => '0.58',
        'cover_plus_extra' => '0.79',
        'activities' => array (
          array (
            'name' => 'Air conditioning compressor or parts, commercial or industrial, manufacturing',
            'bic_code' => 'C245210'
          ),
          array (
            'name' => 'Air conditioning equipment, commercial or industrial, manufacturing (except motor vehicles)',
            'bic_code' => 'C245220'
          ),
          array (
            'name' => 'Beverage dispensing equipment (cooling) manufacturing',
            'bic_code' => 'C245225'
          ),
          array (
            'name' => 'Cool room refrigeration plant manufacturing',
            'bic_code' => 'C245230'
          ),
          array (
            'name' => 'Refrigeration equipment, commercial or industrial, manufacturing'
          ),
          array (
            'name' => 'Solar water heating system, commercial or industrial, manufacturing'
          ),
          array (
            'name' => 'Space heating system, commercial or industrial, manufacturing'
          ),
          array (
            'name' => 'Vending machine, refrigerated, manufacturing'
          ),
          array (
            'name' => 'Water cooler, commercial or industrial, manufacturing'
          ),
          array (
            'name' => 'Water heater, commercial or industrial, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Agricultural machinery and equipment manufacturing',
        'code' => '28610',
        'cover_plus' => '1.36',
        'cover_plus_extra' => '1.81',
        'activities' => array (
          array (
            'name' => 'Agricultural harvesting equipment manufacturing',
            'bic_code' => 'C246110'
          ),
          array (
            'name' => 'Agricultural implement manufacturing (except garden tools)',
            'bic_code' => 'C246120'
          ),
          array (
            'name' => 'Agricultural machinery or equipment manufacturing (not elsewhere classified)',
            'bic_code' => 'C246130'
          ),
          array (
            'name' => 'Agricultural planting equipment manufacturing',
            'bic_code' => 'C246140'
          ),
          array (
            'name' => 'Irrigation equipment manufacturing'
          ),
          array (
            'name' => 'Lawn mower manufacturing'
          ),
          array (
            'name' => 'Mowing equipment manufacturing'
          ),
          array (
            'name' => 'Tractor attachment, agricultural, manufacturing'
          ),
          array (
            'name' => 'Tractor, agricultural, manufacturing (except crawlertractors)'
          ),
          array (
            'name' => 'Windmill, agricultural, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Mining and construction machinery manufacturing',
        'code' => '28620',
        'cover_plus' => '1.36',
        'cover_plus_extra' => '1.81',
        'activities' => array (
          array (
            'name' => 'Back hoe manufacturing',
            'bic_code' => 'C246210'
          ),
          array (
            'name' => 'Concrete mixer manufacturing',
            'bic_code' => 'C246220'
          ),
          array (
            'name' => 'Crawler tractor manufacturing',
            'bic_code' => 'C246230'
          ),
          array (
            'name' => 'Crushing machinery manufacturing (not elsewhere classified)',
            'bic_code' => 'C246240'
          ),
          array (
            'name' => 'Dozer, angle dozer, bulldozer manufacturing',
            'bic_code' => 'C246250'
          ),
          array (
            'name' => 'Drilling machinery manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Earthmoving machinery manufacturing'
          ),
          array (
            'name' => 'Front-end loader manufacturing'
          ),
          array (
            'name' => 'Grader, road, manufacturing'
          ),
          array (
            'name' => 'Jack hammer manufacturing'
          ),
          array (
            'name' => 'Mining machinery manufacturing'
          ),
          array (
            'name' => 'Off-highway truck manufacturing'
          ),
          array (
            'name' => 'Pneumatic drill manufacturing (for construction work)'
          ),
          array (
            'name' => 'Roller, road, manufacturing'
          ),
          array (
            'name' => 'Tractor, construction or earthmoving, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Machine tool and parts manufacturing',
        'code' => '28640',
        'cover_plus' => '0.58',
        'cover_plus_extra' => '0.79',
        'activities' => array (
          array (
            'name' => 'Die, metalworking, manufacturing (hand or machine tool)',
            'bic_code' => 'C246310'
          ),
          array (
            'name' => 'Drill, portable electric, manufacturing',
            'bic_code' => 'C246320'
          ),
          array (
            'name' => 'Drilling machinery, woodworking or metalworking, manufacturing',
            'bic_code' => 'C246330'
          ),
          array (
            'name' => 'Explosive powered tool manufacturing (except for construction work)',
            'bic_code' => 'C246340'
          ),
          array (
            'name' => 'Forging machinery manufacturing',
            'bic_code' => 'C246350'
          ),
          array (
            'name' => 'Foundry machinery manufacturing (except furnaces)',
            'bic_code' => 'C246360'
          ),
          array (
            'name' => 'Hand tool, pneumatic or power operated, manufacturing',
            'bic_code' => 'C246370'
          ),
          array (
            'name' => 'Lathe, woodworking or metal working, manufacturing'
          ),
          array (
            'name' => 'Machine tool attachment or parts manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Metal moulding machinery manufacturing'
          ),
          array (
            'name' => 'Metalworking machinery manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Milling machine, metalworking, manufacturing'
          ),
          array (
            'name' => 'Mould making machinery manufacturing'
          ),
          array (
            'name' => 'Pneumatic tool manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Power tool, pneumatic or power operated, manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Press, woodworking or metal working, manufacturing'
          ),
          array (
            'name' => 'Saw, power operated, manufacturing'
          ),
          array (
            'name' => 'Tyre matrix manufacturing'
          ),
          array (
            'name' => 'Welding or cutting equipment, gas, manufacturing'
          ),
          array (
            'name' => 'Woodworking machinery manufacturing (not elsewhere classified)'
          )
        )
      ),
      array (
        'name' => 'Food and other industry-specific machinery and equipment manufacturing (not elsewhere classified)',
        'code' => '28630',
        'cover_plus' => '0.58',
        'cover_plus_extra' => '0.79',
        'activities' => array (
          array (
            'name' => 'Amusement machine manufacturing',
            'bic_code' => 'C246910'
          ),
          array (
            'name' => 'Bakery machinery manufacturing',
            'bic_code' => 'C246920'
          ),
          array (
            'name' => 'Bottling machine, food or drink, manufacturing',
            'bic_code' => 'C246930'
          ),
          array (
            'name' => 'Can making or sealing machinery manufacturing (food or drink processing)',
            'bic_code' => 'C246940'
          ),
          array (
            'name' => 'Canning machinery, food or drink, manufacturing',
            'bic_code' => 'C246950'
          ),
          array (
            'name' => 'Carnival or fairground equipment, mechanical, manufacturing',
            'bic_code' => 'C246960'
          ),
          array (
            'name' => 'Cement making machinery manufacturing',
            'bic_code' => 'C246970'
          ),
          array (
            'name' => 'Chemical processing machinery manufacturing'
          ),
          array (
            'name' => 'Crushing machinery manufacturing (food processing)'
          ),
          array (
            'name' => 'Distilling equipment, beverage, manufacturing'
          ),
          array (
            'name' => 'Dry-cleaning machinery manufacturing'
          ),
          array (
            'name' => 'Filter manufacturing (food processing machinery)'
          ),
          array (
            'name' => 'Flour milling machinery manufacturing'
          ),
          array (
            'name' => 'Food packing machinery manufacturing'
          ),
          array (
            'name' => 'Food processing machinery, commercial, manufacturing'
          ),
          array (
            'name' => 'Ironing or pressing machinery, industrial, manufacturing'
          ),
          array (
            'name' => 'Juice extractor, fruit or vegetable, commercial, manufacturing'
          ),
          array (
            'name' => 'Knitting machine, industrial, manufacturing'
          ),
          array (
            'name' => 'Laundry machinery, industrial, manufacturing'
          ),
          array (
            'name' => 'Moulding machine manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Paper making machinery or equipment manufacturing'
          ),
          array (
            'name' => 'Printing machinery or equipment manufacturing'
          ),
          array (
            'name' => 'Sewing machine, industrial, manufacturing'
          ),
          array (
            'name' => 'Slicing machinery, food, manufacturing'
          ),
          array (
            'name' => 'Specialised machinery and equipment manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Textile working machinery manufacturing'
          ),
          array (
            'name' => 'Toaster, commercial electric, manufacturing'
          ),
          array (
            'name' => 'Tyre retreading or repairing machinery manufacturing'
          ),
          array (
            'name' => 'Vacuum cleaner, commercial, manufacturing'
          )
        )
      ),
      array (
        'name' => 'Food and other specialised industrial machinery and equipment wholesaling',
        'code' => '46190',
        'cover_plus' => '0.42',
        'cover_plus_extra' => '0.58',
        'activities' => array (
          array (
            'name' => 'Air conditioning equipment, non-electric, wholesaling',
            'bic_code' => 'F341910'
          ),
          array (
            'name' => 'Bearing wholesaling',
            'bic_code' => 'F341920'
          ),
          array (
            'name' => 'Binocular wholesaling',
            'bic_code' => 'F341930'
          ),
          array (
            'name' => 'Blank cassette wholesaling',
            'bic_code' => 'F341940'
          ),
          array (
            'name' => 'Boat wholesaling',
            'bic_code' => 'F341950'
          ),
          array (
            'name' => 'Compressor, air or gas, wholesaling',
            'bic_code' => 'F341960'
          ),
          array (
            'name' => 'Display or notice board wholesaling',
            'bic_code' => 'F341970'
          ),
          array (
            'name' => 'Distilling equipment wholesaling',
            'bic_code' => 'F341980'
          ),
          array (
            'name' => 'Dry-cleaning machinery or equipment wholesaling',
            'bic_code' => 'F341990'
          ),
          array (
            'name' => 'Fire protection equipment, wholesaling',
            'bic_code' => 'F349910'
          ),
          array (
            'name' => 'Food processing machinery or equipment wholesaling',
            'bic_code' => 'F349920'
          ),
          array (
            'name' => 'Forging machinery or equipment wholesaling',
            'bic_code' => 'F349930'
          ),
          array (
            'name' => 'Foundry machinery or equipment wholesaling',
            'bic_code' => 'F349940'
          ),
          array (
            'name' => 'Furnace or furnace equipment, industrial, wholesaling (except electrical)',
            'bic_code' => 'F349945'
          ),
          array (
            'name' => 'Gas generator or equipment wholesaling',
            'bic_code' => 'F349950'
          ),
          array (
            'name' => 'Gas heater wholesaling'
          ),
          array (
            'name' => 'Hairdressing equipment wholesaling'
          ),
          array (
            'name' => 'Industrial brush wholesaling'
          ),
          array (
            'name' => 'Internal combustion engine wholesaling (except motor vehicle engines)'
          ),
          array (
            'name' => 'Jetski part wholesaling'
          ),
          array (
            'name' => 'Jetski wholesaling'
          ),
          array (
            'name' => 'Kerosene heater wholesaling'
          ),
          array (
            'name' => 'Leather working machinery or equipment wholesaling'
          ),
          array (
            'name' => 'Lubricating machinery or equipment wholesaling'
          ),
          array (
            'name' => 'Machine attachment, part or accessory wholesaling'
          ),
          array (
            'name' => 'Machine tool wholesaling'
          ),
          array (
            'name' => 'Machinery and equipment wholesaling (not elsewhere classified)'
          ),
          array (
            'name' => 'Marine engine wholesaling'
          ),
          array (
            'name' => 'Marine equipment wholesaling (not elsewhere classified)'
          ),
          array (
            'name' => 'Material handling equipment wholesaling'
          ),
          array (
            'name' => 'Metalworking machinery wholesaling'
          ),
          array (
            'name' => 'Milk processing machinery or equipment wholesaling'
          ),
          array (
            'name' => 'Mining machinery or equipment wholesaling'
          ),
          array (
            'name' => 'Oil heater wholesaling'
          ),
          array (
            'name' => 'Paper making machinery or equipment wholesaling'
          ),
          array (
            'name' => 'Photographic film wholesaling'
          ),
          array (
            'name' => 'Photographic supply wholesaling (not elsewhere classified)'
          ),
          array (
            'name' => 'Portable fire extinguisher wholesaling'
          ),
          array (
            'name' => 'Printing machinery or equipment wholesaling'
          ),
          array (
            'name' => 'Pumping machinery or equipment wholesaling'
          ),
          array (
            'name' => 'Rubber making or working machinery or equipment wholesaling'
          ),
          array (
            'name' => 'Scale, non-electrical or non-electronic, wholesaling'
          ),
          array (
            'name' => 'Tanning machinery or equipment wholesaling'
          ),
          array (
            'name' => 'Textile working machinery or equipment wholesaling'
          ),
          array (
            'name' => 'Weighing machinery wholesaling (not elsewhere classified)'
          ),
          array (
            'name' => 'Welding machinery or equipment, non-electric, wholesaling'
          ),
          array (
            'name' => 'Wire working machinery or equipment wholesaling'
          ),
          array (
            'name' => 'Woodworking machinery or equipment wholesaling'
          )
        )
      ),
      array (
        'name' => 'Lifting and material-handling equipment manufacturing',
        'code' => '28650',
        'cover_plus' => '0.58',
        'cover_plus_extra' => '0.79',
        'activities' => array (
          array (
            'name' => 'Capstan manufacturing (except for lathes)',
            'bic_code' => 'C249110'
          ),
          array (
            'name' => 'Conveyor or conveying system manufacturing',
            'bic_code' => 'C249120'
          ),
          array (
            'name' => 'Crane manufacturing',
            'bic_code' => 'C249130'
          ),
          array (
            'name' => 'Derrick manufacturing',
            'bic_code' => 'C249140'
          ),
          array (
            'name' => 'Elevator manufacturing'
          ),
          array (
            'name' => 'Escalator or escalator parts manufacturing'
          ),
          array (
            'name' => 'Forklift truck manufacturing'
          ),
          array (
            'name' => 'Hoist or hoisting equipment manufacturing (except clothes hoists)'
          ),
          array (
            'name' => 'Hydraulic lifting equipment and parts manufacturing'
          ),
          array (
            'name' => 'Jacking equipment manufacturing'
          ),
          array (
            'name' => 'Pneumatic conveyor system manufacturing'
          ),
          array (
            'name' => 'Robotic material handling equipment manufacturing'
          ),
          array (
            'name' => 'Staking machinery manufacturing'
          ),
          array (
            'name' => 'Tractor manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Winch manufacturing'
          )
        )
      ),
      array (
        'name' => 'Machinery and equipment manufacturing (not elsewhere classified)',
        'code' => '28690',
        'cover_plus' => '1.36',
        'cover_plus_extra' => '1.81',
        'activities' => array (
          array (
            'name' => 'Bearing manufacturing',
            'bic_code' => 'C243950'
          ),
          array (
            'name' => 'Diesel engine manufacturing (not elsewhere classified)',
            'bic_code' => 'C249920'
          ),
          array (
            'name' => 'Engine, internal combustion, manufacturing (not elsewhere classified)',
            'bic_code' => 'C249930'
          ),
          array (
            'name' => 'Fan, industrial, manufacturing',
            'bic_code' => 'C249940'
          ),
          array (
            'name' => 'Filter, internal combustion engine, manufacturing',
            'bic_code' => 'C249950'
          ),
          array (
            'name' => 'Furnace, industrial, manufacturing (except electric furnaces or space heaters)'
          ),
          array (
            'name' => 'Gas burner, industrial, manufacturing'
          ),
          array (
            'name' => 'Generator, gas, manufacturing'
          ),
          array (
            'name' => 'Hydraulic cylinder manufacturing'
          ),
          array (
            'name' => 'Machinery or equipment manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Marine engine manufacturing (except diesel inboard engines 37kW brake power and over)'
          ),
          array (
            'name' => 'Marine jet unit manufacturing'
          ),
          array (
            'name' => 'Oil burner, industrial, manufacturing'
          ),
          array (
            'name' => 'Outboard motor manufacturing'
          ),
          array (
            'name' => 'Oven, industrial, manufacturing (except electric)'
          ),
          array (
            'name' => 'Press, mechanical, manual or hydraulic, manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Pressure gauge manufacturing'
          ),
          array (
            'name' => 'Sporting machinery manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Vending machine manufacturing (except refrigerated)'
          ),
          array (
            'name' => 'Water treatment equipment, commercial, manufacturing'
          ),
          array (
            'name' => 'Weighing machinery manufacturing (except electronic)'
          ),
          array (
            'name' => 'Wire working machinery manufacturing'
          )
        )
      ),
      array (
        'name' => 'Wooden furniture and upholstered seat manufacturing',
        'code' => '29210',
        'cover_plus' => '0.75',
        'cover_plus_extra' => '1.01',
        'activities' => array (
          array (
            'name' => 'Bedroom suite, wooden, manufacturing',
            'bic_code' => 'C251110'
          ),
          array (
            'name' => 'Chair manufacturing (except dental chairs fitted with mechanical devices)',
            'bic_code' => 'C251120'
          ),
          array (
            'name' => 'Dining room furniture, wooden, manufacturing',
            'bic_code' => 'C251130'
          ),
          array (
            'name' => 'Disassembled furniture, wooden, manufacturing',
            'bic_code' => 'C251140'
          ),
          array (
            'name' => 'Disassembled kitchen furniture, wooden, manufacturing',
            'bic_code' => 'C251150'
          ),
          array (
            'name' => 'Furniture part, wooden, manufacturing',
            'bic_code' => 'C251155'
          ),
          array (
            'name' => 'Furniture reupholstering',
            'bic_code' => 'C251160'
          ),
          array (
            'name' => 'Lounge suite manufacturing',
            'bic_code' => 'C251170'
          ),
          array (
            'name' => 'Office furniture, wooden, manufacturing'
          ),
          array (
            'name' => 'Outdoor furniture, wooden, manufacturing'
          ),
          array (
            'name' => 'Seat, upholstered, manufacturing'
          ),
          array (
            'name' => 'Stage scenery, propos and furniture mfg - wooden'
          ),
          array (
            'name' => 'Table, wooden, manufacturing'
          ),
          array (
            'name' => 'Upholstered furniture manufacturing'
          )
        )
      ),
      array (
        'name' => 'Metal furniture manufacturing',
        'code' => '29220',
        'cover_plus' => '0.58',
        'cover_plus_extra' => '0.79',
        'activities' => array (
          array (
            'name' => 'Cabinet, metal, manufacturing',
            'bic_code' => 'C251210'
          ),
          array (
            'name' => 'Cabinet, radio, radiogram or television, manufacturing (metal framed)',
            'bic_code' => 'C251215'
          ),
          array (
            'name' => 'Disassembled furniture, metal, manufacturing',
            'bic_code' => 'C251220'
          ),
          array (
            'name' => 'Filing cabinet, metal, manufacturing',
            'bic_code' => 'C251225'
          ),
          array (
            'name' => 'Furniture fittings, metal, manufacturing'
          ),
          array (
            'name' => 'Furniture part, metal, manufacturing'
          ),
          array (
            'name' => 'Furniture, metal, manufacturing'
          ),
          array (
            'name' => 'Metal furniture manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Screen partition, metal, manufacturing'
          ),
          array (
            'name' => 'Shelving, metal, manufacturing'
          ),
          array (
            'name' => 'Stage scenery, props and furniture manufacturing sheet metal'
          )
        )
      ),
      array (
        'name' => 'Mattress manufacturing',
        'code' => '29230',
        'cover_plus' => '0.75',
        'cover_plus_extra' => '1.01',
        'activities' => array (
          array (
            'name' => 'Bed base, upholstered, manufacturing',
            'bic_code' => 'C251310'
          ),
          array (
            'name' => 'Inner spring mattress manufacturing',
            'bic_code' => 'C251320'
          ),
          array (
            'name' => 'Mattress support manufacturing'
          ),
          array (
            'name' => 'Mattress, plastic, rubber, latex or sponge,manufacturing'
          ),
          array (
            'name' => 'Mattress, upholstered, manufacturing'
          ),
          array (
            'name' => 'Water mattress manufacturing'
          )
        )
      ),
      array (
        'name' => 'Furniture manufacturing (not elsewhere classified)',
        'code' => '29290',
        'cover_plus' => '0.75',
        'cover_plus_extra' => '1.01',
        'activities' => array (
          array (
            'name' => 'Bamboo furniture manufacturing',
            'bic_code' => 'C251920'
          ),
          array (
            'name' => 'Cane furniture manufacturing',
            'bic_code' => 'C251930'
          ),
          array (
            'name' => 'Fibreglass furniture manufacturing',
            'bic_code' => 'E324250'
          ),
          array (
            'name' => 'Furniture manufacturing (not elsewhere classified)',
            'bic_code' => 'C251940'
          ),
          array (
            'name' => 'Furniture part manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Rattan furniture manufacturing'
          ),
          array (
            'name' => 'Stage scenery, props and furniture, manufacturing (not elsewhere classified)'
          )
        )
      ),
      array (
        'name' => 'Jewellery and silverware manufacturing',
        'code' => '29410',
        'cover_plus' => '0.20',
        'cover_plus_extra' => '0.29',
        'activities' => array (
          array (
            'name' => 'Badge manufacturing (not elsewhere classified)',
            'bic_code' => 'C259110'
          ),
          array (
            'name' => 'Coin minting',
            'bic_code' => 'C259120'
          ),
          array (
            'name' => 'Costume jewellery manufacturing',
            'bic_code' => 'C259130'
          ),
          array (
            'name' => 'Custom-made jewellery manufacturing',
            'bic_code' => 'C259140'
          ),
          array (
            'name' => 'Diamond cutting',
            'bic_code' => 'C259150'
          ),
          array (
            'name' => 'Gem cutting',
            'bic_code' => 'C259160'
          ),
          array (
            'name' => 'Goldsmithing',
            'bic_code' => 'C259170'
          ),
          array (
            'name' => 'Lapidary work'
          ),
          array (
            'name' => 'Medal manufacturing'
          ),
          array (
            'name' => 'Silverware manufacturing'
          ),
          array (
            'name' => 'Trophy manufacturing'
          )
        )
      ),
      array (
        'name' => 'Toy, sporting, and recreational product manufacturing',
        'code' => '29420',
        'cover_plus' => '0.75',
        'cover_plus_extra' => '1.01',
        'activities' => array (
          array (
            'name' => 'Archery equipment manufacturing',
            'bic_code' => 'C259210'
          ),
          array (
            'name' => 'Billiard, snooker or pool table and equipment manufacturing',
            'bic_code' => 'C259220'
          ),
          array (
            'name' => 'Cricket set manufacturing',
            'bic_code' => 'C259225'
          ),
          array (
            'name' => 'Fishing tackle manufacturing',
            'bic_code' => 'C259230'
          ),
          array (
            'name' => 'Hang glider manufacturing',
            'bic_code' => 'C259240'
          ),
          array (
            'name' => 'Lawn bowls equipment manufacturing',
            'bic_code' => 'C259250'
          ),
          array (
            'name' => 'Playground equipment manufacturing',
            'bic_code' => 'C259260'
          ),
          array (
            'name' => 'Sailboard manufacturing',
            'bic_code' => 'C259270'
          ),
          array (
            'name' => 'Skateboard manufacturing',
            'bic_code' => 'C259280'
          ),
          array (
            'name' => 'Sports equipment manufacturing (not elsewhere classified)'
          ),
          array (
            'name' => 'Surfboard manufacturing'
          ),
          array (
            'name' => 'Toy manufacturing (except fur or leather)'
          ),
          array (
            'name' => 'Tricycle manufacturing'
          ),
          array (
            'name' => 'Weight training equipment manufacturing'
          )
        )
      ),
      array (
        'name' => 'Manufacturing (not elsewhere classified)',
        'code' => '29490',
        'cover_plus' => '0.75',
        'cover_plus_extra' => '1.01',
        'activities' => array (
          array (
            'name' => 'Ball point pen manufacturing',
            'bic_code' => 'C259905'
          ),
          array (
            'name' => 'Broom manufacturing',
            'bic_code' => 'C259907'
          ),
          array (
            'name' => 'Brush manufacturing',
            'bic_code' => 'C259910'
          ),
          array (
            'name' => 'Floor mop manufacturing',
            'bic_code' => 'C259920'
          ),
          array (
            'name' => 'Hair brush manufacturing',
            'bic_code' => 'C259925'
          ),
          array (
            'name' => 'Manufacturing (not elsewhere classified)',
            'bic_code' => 'C259935'
          ),
          array (
            'name' => 'Musical instrument manufacturing',
            'bic_code' => 'C259940'
          ),
          array (
            'name' => 'Paint brush manufacturing',
            'bic_code' => 'C259945'
          ),
          array (
            'name' => 'Pen manufacturing',
            'bic_code' => 'C259950'
          ),
          array (
            'name' => 'Pencil manufacturing',
            'bic_code' => 'C259955'
          ),
          array (
            'name' => 'Sign manufacturing (except electrical)'
          ),
          array (
            'name' => 'Stamp pad manufacturing'
          ),
          array (
            'name' => 'Stapler manufacturing'
          ),
          array (
            'name' => 'Tooth brush manufacturing (except electrical)'
          ),
          array (
            'name' => 'Umbrella manufacturing'
          ),
          array (
            'name' => 'Vacuum flask manufacturing'
          ),
          array (
            'name' => 'Wig manufacturing'
          ),
          array (
            'name' => 'Zipper manufacturing'
          )
        )
      ),
      array (
        'name' => 'Fossil fuel electricity generation',
        'code' => '36110',
        'cover_plus' => '0.34',
        'cover_plus_extra' => '0.48',
        'activities' => array (
          array (
            'name' => 'Electricity generation using coal or coal derived products',
            'bic_code' => 'D261110'
          ),
          array (
            'name' => 'Electricity generation using mineral gas including coal gas'
          ),
          array (
            'name' => 'Electricity generation using mineral oil or mineral oil derived products'
          ),
          array (
            'name' => 'Electricity generation using other mineral fuels'
          )
        )
      ),
      array (
        'name' => 'Hydroelectricity generation',
        'code' => '36120',
        'cover_plus' => '0.34',
        'cover_plus_extra' => '0.48',
        'activities' => array (
          array (
            'name' => 'Hydro-electricity generation',
            'bic_code' => 'D261210'
          )
        )
      ),
      array (
        'name' => 'Electricity generation (not elsewhere classified)',
        'code' => '36130',
        'cover_plus' => '0.34',
        'cover_plus_extra' => '0.48',
        'activities' => array (
          array (
            'name' => 'Biomass electricity generation (not elsewhere classified)',
            'bic_code' => 'D261910'
          ),
          array (
            'name' => 'Electricity generation (not elsewhere classified)'
          ),
          array (
            'name' => 'Geothermal electricity generation'
          ),
          array (
            'name' => 'Solar electricity generation'
          ),
          array (
            'name' => 'Tidal electricity generation'
          ),
          array (
            'name' => 'Wind electricity generation'
          )
        )
      ),
      array (
        'name' => 'Electricity line-system operation',
        'code' => '36101',
        'cover_plus' => '0.97',
        'cover_plus_extra' => '1.29',
        'activities' => array (
          array (
            'name' => 'Electricity sub-station operation',
            'bic_code' => 'D262010'
          ),
          array (
            'name' => 'Electricity transmission',
            'bic_code' => 'D263010'
          ),
          array (
            'name' => 'Electricity distribution'
          )
        )
      ),
      array (
        'name' => 'Energy and services utilities operation (excluding construction, maintenance, and plant operation)',
        'code' => '36103',
        'cover_plus' => '0.34',
        'cover_plus_extra' => '0.48',
        'activities' => array (
          array (
            'name' => 'Operation of electricity line system - all construction and maintenance sub-contracted',
            'bic_code' => array (
              'D263020',
              'D281130'
            )
          )
        )
      ),
      array (
        'name' => 'On-selling electricity and electricity market operation',
        'code' => '36102',
        'cover_plus' => '0.34',
        'cover_plus_extra' => '0.48',
        'activities' => array (
          array (
            'name' => 'Electricity market operation',
            'bic_code' => 'D264010'
          ),
          array (
            'name' => 'Electricity retailing',
            'bic_code' => 'D264020'
          ),
          array (
            'name' => 'Electricity wholesaling'
          )
        )
      ),
      array (
        'name' => 'Gas supply',
        'code' => '36200',
        'cover_plus' => '0.97',
        'cover_plus_extra' => '1.29',
        'activities' => array (
          array (
            'name' => 'Coal gas distribution through mains system',
            'bic_code' => 'D270010'
          ),
          array (
            'name' => 'Fuel gas distribution through mains system',
            'bic_code' => 'D270020'
          ),
          array (
            'name' => 'Liquefied petroleum gas distribution through main system'
          ),
          array (
            'name' => 'Liquefied petroleum gas reforming for distribution through mains system'
          ),
          array (
            'name' => 'Natural gas distribution through mains system'
          )
        )
      ),
      array (
        'name' => 'Water supply',
        'code' => '37010',
        'cover_plus' => '0.97',
        'cover_plus_extra' => '1.29',
        'activities' => array (
          array (
            'name' => 'Dam operation (water supply)',
            'bic_code' => 'D281110'
          ),
          array (
            'name' => 'Desalination plant operation',
            'bic_code' => 'D281120'
          ),
          array (
            'name' => 'Mineral water supply from the ground'
          ),
          array (
            'name' => 'Water distribution by pipelines'
          ),
          array (
            'name' => 'Water filtration plant operation'
          ),
          array (
            'name' => 'Water reservoir operation'
          ),
          array (
            'name' => 'Water supply for irrigation'
          ),
          array (
            'name' => 'Water supply pumping station operation'
          ),
          array (
            'name' => 'Water supply system operation'
          )
        )
      ),
      array (
        'name' => 'Sewerage and drainage services',
        'code' => '37020',
        'cover_plus' => '0.97',
        'cover_plus_extra' => '1.29',
        'activities' => array (
          array (
            'name' => 'Sewage pumping station operation',
            'bic_code' => 'D281210'
          ),
          array (
            'name' => 'Sewage treatment plant operation',
            'bic_code' => 'D281220'
          ),
          array (
            'name' => 'Sewerage system operation',
            'bic_code' => 'D281230'
          ),
          array (
            'name' => 'Stormwater drainage system operation'
          ),
          array (
            'name' => 'Town drainage system operation'
          )
        )
      ),
      array (
        'name' => 'Solid waste collection services',
        'code' => '96350',
        'cover_plus' => '1.53',
        'cover_plus_extra' => '2.03',
        'activities' => array (
          array (
            'name' => 'Bin hiring and waste collection service',
            'bic_code' => 'D291105'
          ),
          array (
            'name' => 'Garbage collection service',
            'bic_code' => 'D291110'
          ),
          array (
            'name' => 'Hazardous waste, solid, collection service',
            'bic_code' => 'D291120'
          ),
          array (
            'name' => 'Industrial waste, solid, collection service',
            'bic_code' => 'D291130'
          ),
          array (
            'name' => 'Metal barrel/skip hiring and waste collection service',
            'bic_code' => 'D291135'
          ),
          array (
            'name' => 'Night soil collection service',
            'bic_code' => 'D291140'
          ),
          array (
            'name' => 'Portable toilet hiring and waste collection service'
          ),
          array (
            'name' => 'Rubbish collection service'
          ),
          array (
            'name' => 'Solid waste collection service'
          ),
          array (
            'name' => 'Solid waste haulage service (local)'
          ),
          array (
            'name' => 'Waste, solid, collection service'
          )
        )
      ),
      array (
        'name' => 'Waste collection services (not elsewhere classified)',
        'code' => '96380',
        'cover_plus' => '1.53',
        'cover_plus_extra' => '2.03',
        'activities' => array (
          array (
            'name' => 'Hazardous waste (except solid) collection service',
            'bic_code' => 'D291903'
          ),
          array (
            'name' => 'Industrial waste (except solid) collection service',
            'bic_code' => 'D291907'
          ),
          array (
            'name' => 'Liquid waste collection service',
            'bic_code' => 'D291910'
          ),
          array (
            'name' => 'Liquid waste haulage service (local)',
            'bic_code' => 'D291920'
          ),
          array (
            'name' => 'Oil collection service',
            'bic_code' => 'D291930'
          ),
          array (
            'name' => 'Septic tank waste collection service (except repairs and maintenance)'
          ),
          array (
            'name' => 'Waste collection service (not elsewhere classified)'
          )
        )
      ),
      array (
        'name' => 'Waste treatment and disposal services',
        'code' => '96340',
        'cover_plus' => '1.53',
        'cover_plus_extra' => '2.03',
        'activities' => array (
          array (
            'name' => 'Garbage disposal service',
            'bic_code' => 'D292110'
          ),
          array (
            'name' => 'Operating landfills',
            'bic_code' => 'D292120'
          ),
          array (
            'name' => 'Operating other waste treatment facilities',
            'bic_code' => 'D292130'
          ),
          array (
            'name' => 'Rubbish dump or tip operation',
            'bic_code' => 'D292140'
          ),
          array (
            'name' => 'Sanitary disposal service'
          )
        )
      ),
      array (
        'name' => 'Waste remediation and materials recovery services',
        'code' => '96370',
        'cover_plus' => '1.53',
        'cover_plus_extra' => '2.03',
        'activities' => array (
          array (
            'name' => 'Asbestos removal service',
            'bic_code' => 'D292115'
          ),
          array (
            'name' => 'Hazardous material removal',
            'bic_code' => 'D292210'
          ),
          array (
            'name' => 'Hazardous waste treatment or disposal service',
            'bic_code' => 'D292213'
          ),
          array (
            'name' => 'Lead paint abatement service',
            'bic_code' => 'D292215'
          ),
          array (
            'name' => 'Materials recovery station operation',
            'bic_code' => 'D292217'
          ),
          array (
            'name' => 'Materials separating and sorting operation',
            'bic_code' => 'D292240'
          ),
          array (
            'name' => 'Mine reclamation activities',
            'bic_code' => 'D292250'
          ),
          array (
            'name' => 'Remediation service, environmental'
          ),
          array (
            'name' => 'Toxic material abatement'
          ),
          array (
            'name' => 'Waste remediation'
          )
        )
      ),
      array (
        'name' => 'House construction',
        'code' => '41110',
        'cover_plus' => '2.13',
        'cover_plus_extra' => '2.81',
        'activities' => array (
          array (
            'name' => 'Garage construction',
            'bic_code' => 'E301110'
          ),
          array (
            'name' => 'Garage, shed or carport, prefabricated, assembly, erection or installation (on site)',
            'bic_code' => 'E301120'
          ),
          array (
            'name' => 'House construction, alteration or renovation',
            'bic_code' => 'E301130'
          ),
          array (
            'name' => 'House, prefabricated, assembly, erection or installation (on site)',
            'bic_code' => 'E301140'
          )
        )
      ),
      array (
        'name' => 'Residential building construction (not elsewhere classified)',
        'code' => '41120',
        'cover_plus' => '2.01',
        'cover_plus_extra' => '2.66',
        'activities' => array (
          array (
            'name' => 'Apartment construction',
            'bic_code' => 'E301910'
          ),
          array (
            'name' => 'Duplex house construction',
            'bic_code' => 'E301920'
          ),
          array (
            'name' => 'Flat construction'
          ),
          array (
            'name' => 'High-rise flat construction'
          ),
          array (
            'name' => 'Renovation or alteration of residential building (not elsewhere classified)'
          ),
          array (
            'name' => 'Semi-detached house construction'
          )
        )
      ),
      array (
        'name' => 'Non-residential building construction',
        'code' => '41130',
        'cover_plus' => '2.00',
        'cover_plus_extra' => '2.64',
        'activities' => array (
          array (
            'name' => 'Commercial building construction',
            'bic_code' => 'E302010'
          ),
          array (
            'name' => 'Industrial building construction',
            'bic_code' => 'E302020'
          ),
          array (
            'name' => 'Office building construction'
          ),
          array (
            'name' => 'Prefabricated non-residential building assembly, erection or installation on-site (except sheds, garages or carports)'
          ),
          array (
            'name' => 'Prefabricated temperature controlled structures installation'
          ),
          array (
            'name' => 'Renovation or alteration of non-residential buildings'
          )
        )
      ),
      array (
        'name' => 'Road and bridge construction',
        'code' => '41210',
        'cover_plus' => '1.17',
        'cover_plus_extra' => '1.56',
        'activities' => array (
          array (
            'name' => 'Aerodrome runway construction',
            'bic_code' => 'E310110'
          ),
          array (
            'name' => 'Asphalt surfacing',
            'bic_code' => 'E310120'
          ),
          array (
            'name' => 'Bridge construction (including construction from prefabricated components)',
            'bic_code' => 'E310130'
          ),
          array (
            'name' => 'Elevated highway construction',
            'bic_code' => 'E310140'
          ),
          array (
            'name' => 'Overpass construction',
            'bic_code' => 'E310150'
          ),
          array (
            'name' => 'Parking lot construction (except buildings)',
            'bic_code' => 'E310160'
          ),
          array (
            'name' => 'Repair or maintenance of roads or bridges',
            'bic_code' => 'E310170'
          ),
          array (
            'name' => 'Road construction or sealing'
          )
        )
      ),
      array (
        'name' => 'Heavy and civil engineering construction (not elsewhere classified)',
        'code' => '41220',
        'cover_plus' => '1.53',
        'cover_plus_extra' => '2.03',
        'activities' => array (
          array (
            'name' => 'Breakwater construction',
            'bic_code' => 'E310907'
          ),
          array (
            'name' => 'Canal construction',
            'bic_code' => 'E310910'
          ),
          array (
            'name' => 'Dam construction',
            'bic_code' => 'E310913'
          ),
          array (
            'name' => 'Dredging (harbours or rivers)',
            'bic_code' => 'E310917'
          ),
          array (
            'name' => 'Electrical machinery, heavy, installation (on-site assembly)',
            'bic_code' => 'E310920'
          ),
          array (
            'name' => 'Electricity power plant construction (except buildings)',
            'bic_code' => 'E310923'
          ),
          array (
            'name' => 'Flood control system construction',
            'bic_code' => 'E310927'
          ),
          array (
            'name' => 'Furnace construction (for industrial plants from prefabricated components)',
            'bic_code' => 'E310930'
          ),
          array (
            'name' => 'Golf course construction',
            'bic_code' => 'E310933'
          ),
          array (
            'name' => 'Harbour work construction (except buildings)',
            'bic_code' => 'E310937'
          ),
          array (
            'name' => 'Irrigation system construction',
            'bic_code' => 'E310940'
          ),
          array (
            'name' => 'Jetty construction',
            'bic_code' => 'E310943'
          ),
          array (
            'name' => 'Lake construction',
            'bic_code' => 'E310947'
          ),
          array (
            'name' => 'Mine site construction (not elsewhere classified)',
            'bic_code' => 'E310957'
          ),
          array (
            'name' => 'Oil refinery construction (except buildings)',
            'bic_code' => 'E310960'
          ),
          array (
            'name' => 'Pipeline construction (non-utility)',
            'bic_code' => 'E310967'
          ),
          array (
            'name' => 'Pile driving',
            'bic_code' => 'E310973'
          ),
          array (
            'name' => 'Railway permanent way construction'
          ),
          array (
            'name' => 'River work construction'
          ),
          array (
            'name' => 'Sewerage treatment plant construction'
          ),
          array (
            'name' => 'Sports field construction'
          ),
          array (
            'name' => 'Swimming pool, below ground concrete or fibreglass, construction'
          ),
          array (
            'name' => 'Tunnel construction'
          ),
          array (
            'name' => 'Water tank construction (except for structural steel)'
          )
        )
      ),
      array (
        'name' => 'Utility and communications network construction and maintenance services',
        'code' => '41221',
        'cover_plus' => '0.97',
        'cover_plus_extra' => '1.29',
        'activities' => array (
          array (
            'name' => 'Cable laying (except communication cables within buildings)',
            'bic_code' => 'E310970'
          ),
          array (
            'name' => 'Communications network construction and maintenance services',
            'bic_code' => 'E310980'
          ),
          array (
            'name' => 'Distribution line, electricity or communication, construction and maintenance',
            'bic_code' => 'E310903'
          ),
          array (
            'name' => 'Repair and maintenance to utility networks',
            'bic_code' => 'E310904'
          ),
          array (
            'name' => 'Sewerage or stormwater drainage network construction and maintenance',
            'bic_code' => 'E310953'
          ),
          array (
            'name' => 'Transmission tower construction and maintenance',
            'bic_code' => 'E310963'
          ),
          array (
            'name' => 'Utility pipeline construction and maintenance -except special trade repair service',
            'bic_code' => 'E329982'
          ),
          array (
            'name' => 'Utility network drilling service',
            'bic_code' => 'E310950'
          ),
          array (
            'name' => 'Water supply pipeline construction and maintenance',
            'bic_code' => 'E310908'
          ),
          array (
            'name' => 'Water reticulation construction and maintenance'
          )
        )
      ),
      array (
        'name' => 'Land development and subdivision',
        'code' => '41222',
        'cover_plus' => '1.59',
        'cover_plus_extra' => '2.11',
        'activities' => array (
          array (
            'name' => 'Land subdivision or development',
            'bic_code' => 'E321110'
          )
        )
      ),
      array (
        'name' => 'Landscape construction services',
        'code' => '42510',
        'cover_plus' => '2.27',
        'cover_plus_extra' => '2.99',
        'activities' => array (
          array (
            'name' => 'Brick paving',
            'bic_code' => 'E329110'
          ),
          array (
            'name' => 'Fence construction',
            'bic_code' => 'E329120'
          ),
          array (
            'name' => 'Landscape construction',
            'bic_code' => 'E329130'
          ),
          array (
            'name' => 'Lawn construction',
            'bic_code' => 'E329140'
          ),
          array (
            'name' => 'Pond construction',
            'bic_code' => 'E329150'
          ),
          array (
            'name' => 'Retaining wall construction',
            'bic_code' => 'E329160'
          ),
          array (
            'name' => 'Rockery work',
            'bic_code' => 'E329170'
          ),
          array (
            'name' => 'Streetscape planting'
          )
        )
      ),
      array (
        'name' => 'Site preparation services',
        'code' => '42100',
        'cover_plus' => '1.59',
        'cover_plus_extra' => '2.11',
        'activities' => array (
          array (
            'name' => 'Blasting services (except sandblasting or blasting of building exteriors)',
            'bic_code' => 'E321210'
          ),
          array (
            'name' => 'Bulldozing of construction sites',
            'bic_code' => 'E321220'
          ),
          array (
            'name' => 'Demolition of buildings or other structures',
            'bic_code' => 'E321225'
          ),
          array (
            'name' => 'Earthmoving',
            'bic_code' => 'E321230'
          ),
          array (
            'name' => 'Earthmoving plant and equipment hiring with operator',
            'bic_code' => 'E321240'
          ),
          array (
            'name' => 'Excavation',
            'bic_code' => 'E321250'
          ),
          array (
            'name' => 'Explosives laying',
            'bic_code' => 'E321260'
          ),
          array (
            'name' => 'Ground de-watering',
            'bic_code' => 'E321270'
          ),
          array (
            'name' => 'Land clearing (except bush or rural)',
            'bic_code' => 'E321280'
          ),
          array (
            'name' => 'Levelling (construction sites)'
          ),
          array (
            'name' => 'Removal of overburden'
          ),
          array (
            'name' => 'Trench digging'
          )
        )
      ),
      array (
        'name' => 'Concreting services',
        'code' => '42210',
        'cover_plus' => '2.27',
        'cover_plus_extra' => '2.99',
        'activities' => array (
          array (
            'name' => 'Concrete cutting',
            'bic_code' => 'E322110'
          ),
          array (
            'name' => 'Concrete footpath construction',
            'bic_code' => 'E322120'
          ),
          array (
            'name' => 'Concrete foundation construction',
            'bic_code' => 'E322130'
          ),
          array (
            'name' => 'Concrete kerb and guttering construction',
            'bic_code' => 'E322140'
          ),
          array (
            'name' => 'Concrete pumping',
            'bic_code' => 'E322150'
          ),
          array (
            'name' => 'Concrete work on construction projects',
            'bic_code' => 'E322160'
          ),
          array (
            'name' => 'Repair of kerbs, gutters or other concrete structural products'
          )
        )
      ),
      array (
        'name' => 'Bricklaying services',
        'code' => '42220',
        'cover_plus' => '2.58',
        'cover_plus_extra' => '3.39',
        'activities' => array (
          array (
            'name' => 'Bricklaying',
            'bic_code' => 'E322210'
          ),
          array (
            'name' => 'Concrete block laying',
            'bic_code' => 'E322215'
          ),
          array (
            'name' => 'Repair of brickwork',
            'bic_code' => 'E322220'
          ),
          array (
            'name' => 'Stonework on construction projects'
          )
        )
      ),
      array (
        'name' => 'Roofing services',
        'code' => '42230',
        'cover_plus' => '2.58',
        'cover_plus_extra' => '3.39',
        'activities' => array (
          array (
            'name' => 'Metal roof fixing',
            'bic_code' => 'E322310'
          ),
          array (
            'name' => 'Roof painting, spraying or coating',
            'bic_code' => 'E322315'
          ),
          array (
            'name' => 'Roof tiling',
            'bic_code' => array (
              'E322320',
              'E322330'
            )
          )
        )
      ),
      array (
        'name' => 'Structural steel erection services',
        'code' => '42240',
        'cover_plus' => '2.57',
        'cover_plus_extra' => '3.38',
        'activities' => array (
          array (
            'name' => 'Metal silo erection',
            'bic_code' => 'E322410'
          ),
          array (
            'name' => 'Metal storage tank erection',
            'bic_code' => 'E322420'
          ),
          array (
            'name' => 'Reinforcing steel erection',
            'bic_code' => 'E322430'
          ),
          array (
            'name' => 'Structural steel erection'
          ),
          array (
            'name' => 'Truss or joist, steel, erection'
          ),
          array (
            'name' => 'Welding work on construction projects'
          )
        )
      ),
      array (
        'name' => 'Plumbing services',
        'code' => '42310',
        'cover_plus' => '1.46',
        'cover_plus_extra' => '1.94',
        'activities' => array (
          array (
            'name' => 'Drainlaying construction or repairing (except sewerage or stormwater drainage networks)',
            'bic_code' => 'E323110'
          ),
          array (
            'name' => 'Gas plumbing',
            'bic_code' => 'E323120'
          ),
          array (
            'name' => 'Guttering, roof, installation or repair',
            'bic_code' => 'E323130'
          ),
          array (
            'name' => 'Hot water system installation',
            'bic_code' => 'E323140'
          ),
          array (
            'name' => 'Plumbing (except marine)',
            'bic_code' => 'E323150'
          ),
          array (
            'name' => 'Repair of installed plumbing',
            'bic_code' => 'E323160'
          ),
          array (
            'name' => 'Septic tank installation (including repair)',
            'bic_code' => 'E323170'
          ),
          array (
            'name' => 'Solar hot water system installation'
          )
        )
      ),
      array (
        'name' => 'Electrical services (including telecommunication services within buildings)',
        'code' => '42320',
        'cover_plus' => '0.86',
        'cover_plus_extra' => '1.15',
        'activities' => array (
          array (
            'name' => 'Electric light installation',
            'bic_code' => 'E323210'
          ),
          array (
            'name' => 'Electric wiring installation',
            'bic_code' => 'E323220'
          ),
          array (
            'name' => 'Electrical installation work (e.g. switchboards, circuit breakers, etc..)',
            'bic_code' => 'E323222'
          ),
          array (
            'name' => 'Electrical traffic signal installation or maintenance',
            'bic_code' => 'E323223'
          ),
          array (
            'name' => 'Installation of television antennae or cable',
            'bic_code' => 'E323225'
          ),
          array (
            'name' => 'Installation of television satellite dish',
            'bic_code' => 'E323230'
          ),
          array (
            'name' => 'Repair or maintenance of electrical wiring (except of electricity transmission or distribution lines)',
            'bic_code' => 'E323240'
          ),
          array (
            'name' => 'Telecommunication cable or wire installation (except transmission lines)',
            'bic_code' => 'E323250'
          )
        )
      ),
      array (
        'name' => 'Air conditioning and heating services',
        'code' => '42330',
        'cover_plus' => '1.03',
        'cover_plus_extra' => '1.38',
        'activities' => array (
          array (
            'name' => 'Air conditioning duct work installation',
            'bic_code' => 'E323310'
          ),
          array (
            'name' => 'Air conditioning equipment installation (except motor vehicle air conditioning equipment)',
            'bic_code' => 'E323315'
          ),
          array (
            'name' => 'Cool room refrigerator installation',
            'bic_code' => 'E323320'
          ),
          array (
            'name' => 'Freezer room construction',
            'bic_code' => 'E323330'
          ),
          array (
            'name' => 'Heating equipment installation (except industrial furnaces)',
            'bic_code' => 'E323340'
          ),
          array (
            'name' => 'Oil heater installation'
          ),
          array (
            'name' => 'Refrigeration equipment installation'
          ),
          array (
            'name' => 'Ventilation equipment installation (not elsewhere classified)'
          )
        )
      ),
      array (
        'name' => 'Fire and security alarm installation services',
        'code' => '42341',
        'cover_plus' => '1.03',
        'cover_plus_extra' => '1.38',
        'activities' => array (
          array (
            'name' => 'Closed circuit video surveillance system installation',
            'bic_code' => 'E323410'
          ),
          array (
            'name' => 'Fire alarm system installation',
            'bic_code' => 'E323420'
          ),
          array (
            'name' => 'Fire sprinkler installation',
            'bic_code' => 'E323430'
          ),
          array (
            'name' => 'Repair of installed fire or burglar security alarm systems',
            'bic_code' => 'E323440'
          ),
          array (
            'name' => 'Security system installation'
          ),
          array (
            'name' => 'Smoke detector installation'
          )
        )
      ),
      array (
        'name' => 'Building installation services (not elsewhere classified)',
        'code' => '42342',
        'cover_plus' => '2.26',
        'cover_plus_extra' => '2.98',
        'activities' => array (
          array (
            'name' => 'Awning, blind or shutter installation',
            'bic_code' => 'E323910'
          ),
          array (
            'name' => 'Curtain installation',
            'bic_code' => 'E323920'
          ),
          array (
            'name' => 'Door installation - except on-site joinery',
            'bic_code' => 'E323925'
          ),
          array (
            'name' => 'Elevator, escalator or lift installation',
            'bic_code' => 'E323930'
          ),
          array (
            'name' => 'Flywire screen installation',
            'bic_code' => 'E323932'
          ),
          array (
            'name' => 'Fireplace construction, installation and restoration',
            'bic_code' => 'E323935'
          ),
          array (
            'name' => 'Insulation material installation',
            'bic_code' => 'E323940'
          ),
          array (
            'name' => 'Security door installation',
            'bic_code' => 'E323950'
          ),
          array (
            'name' => 'Roller doors, shutter or grilles installation',
            'bic_code' => 'E323955'
          ),
          array (
            'name' => 'Window tinting or coating service',
            'bic_code' => 'E323960'
          )
        )
      ),
      array (
        'name' => 'Plastering and ceiling services',
        'code' => '42410',
        'cover_plus' => '2.26',
        'cover_plus_extra' => '2.98',
        'activities' => array (
          array (
            'name' => 'Cement rendering of buildings',
            'bic_code' => 'E324110'
          ),
          array (
            'name' => 'Decorative plaster fixing',
            'bic_code' => 'E324120'
          ),
          array (
            'name' => 'Fibrous plaster fixing or finishing',
            'bic_code' => 'E324130'
          ),
          array (
            'name' => 'Plaster work on construction projects'
          ),
          array (
            'name' => 'Plasterboard fixing or finishing'
          )
        )
      ),
      array (
        'name' => 'Plumbing goods wholesaling',
        'code' => '45391',
        'cover_plus' => '0.48',
        'cover_plus_extra' => '0.66',
        'activities' => array (
          array (
            'name' => 'Bath wholesaling',
            'bic_code' => 'F333210'
          ),
          array (
            'name' => 'Bathroom or toilet fitting wholesaling',
            'bic_code' => 'F333215'
          ),
          array (
            'name' => 'Downpipe or guttering wholesaling',
            'bic_code' => 'F333220'
          ),
          array (
            'name' => 'Gas fitting wholesaling'
          ),
          array (
            'name' => 'Hot water system wholesaling'
          ),
          array (
            'name' => 'Pipes and pipe fitting wholesaling'
          ),
          array (
            'name' => 'Plumber’s fitting wholesaling'
          ),
          array (
            'name' => 'Plumbing tool wholesaling'
          ),
          array (
            'name' => 'Sink and basin wholesaling'
          ),
          array (
            'name' => 'Tap wholesaling'
          ),
          array (
            'name' => 'Toilet wholesaling'
          ),
          array (
            'name' => 'Wash basin wholesaling'
          )
        )
      ),
      array (
        'name' => 'Carpentry services',
        'code' => '42420',
        'cover_plus' => '2.27',
        'cover_plus_extra' => '2.99',
        'activities' => array (
          array (
            'name' => 'Carpentry work on construction projects',
            'bic_code' => 'E324210'
          ),
          array (
            'name' => 'Joinery work on construction projects (on-site fabrication only)',
            'bic_code' => 'E324220'
          ),
          array (
            'name' => 'Roof truss, wooden, fixing',
            'bic_code' => 'E324230'
          ),
          array (
            'name' => 'Wooden flooring installation',
            'bic_code' => 'E324240'
          ),
          array (
            'name' => 'Wooden formwork erection'
          ),
          array (
            'name' => 'Wooden kitchen cabinet installation'
          ),
          array (
            'name' => 'Wooden roof truss installation'
          )
        )
      ),
      array (
        'name' => 'Tiling and carpeting services',
        'code' => '42430',
        'cover_plus' => '2.26',
        'cover_plus_extra' => '2.98',
        'activities' => array (
          array (
            'name' => 'Carpet or carpet tile laying',
            'bic_code' => 'E324310'
          ),
          array (
            'name' => 'Floor covering laying (not elsewhere classified)',
            'bic_code' => 'E324320'
          ),
          array (
            'name' => 'Floor sanding',
            'bic_code' => 'E324330'
          ),
          array (
            'name' => 'Floor tiling (using ceramic, concrete or cut stone tiles)',
            'bic_code' => 'E324340'
          ),
          array (
            'name' => 'Linoleum or linotile fixing'
          ),
          array (
            'name' => 'Mosaic work on construction projects'
          ),
          array (
            'name' => 'Slate flooring installation'
          ),
          array (
            'name' => 'Terrazzo laying'
          ),
          array (
            'name' => 'Wall tiling (using ceramic, concrete or cut stone tiles)'
          )
        )
      ),
      array (
        'name' => 'Painting and decorating services',
        'code' => '42440',
        'cover_plus' => '1.87',
        'cover_plus_extra' => '2.47',
        'activities' => array (
          array (
            'name' => 'House painting',
            'bic_code' => 'E324410'
          ),
          array (
            'name' => 'Interior painting and decorating',
            'bic_code' => 'E324415'
          ),
          array (
            'name' => 'Painting of buildings or other structures',
            'bic_code' => 'E324420'
          ),
          array (
            'name' => 'Spray painting of buildings or other structures',
            'bic_code' => 'E324430'
          ),
          array (
            'name' => 'Wallpapering',
            'bic_code' => array (
              'E324440',
              'E324450'
            )
          )
        )
      ),
      array (
        'name' => 'Glazing services',
        'code' => '42450',
        'cover_plus' => '2.26',
        'cover_plus_extra' => '2.98',
        'activities' => array (
          array (
            'name' => 'Glazing',
            'bic_code' => 'E324510'
          ),
          array (
            'name' => 'Window frame installation',
            'bic_code' => 'E324520'
          ),
          array (
            'name' => 'Window installation',
            'bic_code' => 'E324530'
          ),
          array (
            'name' => 'Window insulation fixing',
            'bic_code' => 'E324540'
          )
        )
      ),
      array (
        'name' => 'Construction services and property developers – all trades subcontracted',
        'code' => '42595',
        'cover_plus' => '2.26',
        'cover_plus_extra' => '2.98',
        'activities' => array (
          array (
            'name' => 'Construction services where trades work is
            subcontracted',
            'bic_code' => 'E321120'
          ),
          array (
            'name' => 'Residential property development (excluding
            construction)',
            'bic_code' => 'E329920'
          ),
          array (
            'name' => 'Commercial property development (excluding
            construction)',
            'bic_code' => 'L671180'
          )
        )
      ),
      array (
        'name' => 'Motor vehicle new-part wholesaling',
        'code' => '46230',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Automotive air conditioning wholesaling',
            'bic_code' => 'F350410'
          ),
          array (
            'name' => 'Automotive battery wholesaling',
            'bic_code' => 'F350420'
          ),
          array (
            'name' => 'Car radio or CD-player wholesaling',
            'bic_code' => 'F350430'
          ),
          array (
            'name' => 'Motor cycle accessory, new, wholesaling',
            'bic_code' => 'F350440'
          ),
          array (
            'name' => 'Motor cycle part, new, wholesaling'
          ),
          array (
            'name' => 'Motor vehicle accessory, new, wholesaling'
          ),
          array (
            'name' => 'Motor vehicle part, new, wholesaling'
          ),
          array (
            'name' => 'Tyre wholesaling'
          )
        )
      ),
      array (
        'name' => 'Motor vehicle dismantling and used-part wholesaling',
        'code' => '46240',
        'cover_plus' => '1.40',
        'cover_plus_extra' => '1.86',
        'activities' => array (
          array (
            'name' => 'Motor cycle dismantling',
            'bic_code' => 'F350510'
          ),
          array (
            'name' => 'Motor vehicle dismantling',
            'bic_code' => 'F350520'
          ),
          array (
            'name' => 'Second hand motor cycle parts wholesaling',
            'bic_code' => 'F350530'
          ),
          array (
            'name' => 'Second hand motor vehicle parts wholesaling'
          )
        )
      ),
      array (
        'name' => 'Grocery wholesaling multiple product ranges',
        'code' => '47191',
        'cover_plus' => '0.69',
        'cover_plus_extra' => '0.93',
        'activities' => array (
          array (
            'name' => 'General line grocery wholesaling',
            'bic_code' => 'F360110'
          ),
          array (
            'name' => 'Grocery wholesaling with multiple product ranges'
          )
        )
      ),
      array (
        'name' => 'Meat, poultry, and smallgoods wholesaling',
        'code' => '47110',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Bacon wholesaling',
            'bic_code' => 'F360210'
          ),
          array (
            'name' => 'Frozen meat wholesaling',
            'bic_code' => 'F360220'
          ),
          array (
            'name' => 'Ham wholesaling',
            'bic_code' => 'F360230'
          ),
          array (
            'name' => 'Meat wholesaling (except canned)',
            'bic_code' => 'F360240'
          ),
          array (
            'name' => 'Poultry wholesaling (except canned)'
          ),
          array (
            'name' => 'Rabbit meat wholesaling'
          ),
          array (
            'name' => 'Sausage wholesaling'
          ),
          array (
            'name' => 'Smallgoods wholesaling'
          )
        )
      ),
      array (
        'name' => 'Dairy produce wholesaling',
        'code' => '47130',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Butter wholesaling',
            'bic_code' => 'F360310'
          ),
          array (
            'name' => 'Cheese wholesaling',
            'bic_code' => 'F360320'
          ),
          array (
            'name' => 'Cream wholesaling',
            'bic_code' => 'F360330'
          ),
          array (
            'name' => 'Dairy product wholesaling'
          ),
          array (
            'name' => 'Frozen dessert wholesaling'
          ),
          array (
            'name' => 'Ice cream wholesaling'
          ),
          array (
            'name' => 'Milk wholesaling (except canned)'
          )
        )
      ),
      array (
        'name' => 'Fish and seafood wholesaling',
        'code' => '47140',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Crustacean wholesaling (including processed, except canned)',
            'bic_code' => 'F360410'
          ),
          array (
            'name' => 'Fish wholesaling',
            'bic_code' => 'F360420'
          ),
          array (
            'name' => 'Mollusc wholesaling (including processed, except canned)'
          ),
          array (
            'name' => 'Seafood, fresh or frozen, wholesaling'
          )
        )
      ),
      array (
        'name' => 'Fruit and vegetable wholesaling',
        'code' => '47150',
        'cover_plus' => '0.69',
        'cover_plus_extra' => '0.93',
        'activities' => array (
          array (
            'name' => 'Fruit, fresh, wholesaling'
          ),
          array (
            'name' => 'Packing fresh fruit or vegetables',
            'bic_code' => 'F360510'
          ),
          array (
            'name' => 'Vegetable, fresh, wholesaling',
            'bic_code' => 'F360520'
          )
        )
      ),
      array (
        'name' => 'Liquor and tobacco product wholesaling',
        'code' => '47170',
        'cover_plus' => '0.27',
        'cover_plus_extra' => '0.38',
        'activities' => array (
          array (
            'name' => 'Alcoholic beverage wholesaling',
            'bic_code' => 'F360610'
          ),
          array (
            'name' => 'Tobacco product wholesaling',
            'bic_code' => 'F360620'
          )
        )
      ),
      array (
        'name' => 'Grocery wholesaling (not elsewhere classified)',
        'code' => '47190',
        'cover_plus' => '0.69',
        'cover_plus_extra' => '0.93',
        'activities' => array (
          array (
            'name' => 'Aerated water wholesaling',
            'bic_code' => 'F360905'
          ),
          array (
            'name' => 'Biscuits wholesaling',
            'bic_code' => 'F360910'
          ),
          array (
            'name' => 'Bottled water wholesaling',
            'bic_code' => 'F360915'
          ),
          array (
            'name' => 'Canned foods wholesaling',
            'bic_code' => 'F360920'
          ),
          array (
            'name' => 'Cereal foods wholesaling',
            'bic_code' => 'F360925'
          ),
          array (
            'name' => 'Coffee wholesaling',
            'bic_code' => 'F360930'
          ),
          array (
            'name' => 'Condiments wholesaling',
            'bic_code' => 'F360935'
          ),
          array (
            'name' => 'Confectionery wholesaling',
            'bic_code' => 'F360940'
          ),
          array (
            'name' => 'Cooking oils or fats wholesaling',
            'bic_code' => 'F360945'
          ),
          array (
            'name' => 'Cordial, aerated or carbonated, wholesaling',
            'bic_code' => 'F360950'
          ),
          array (
            'name' => 'Egg and egg products wholesaling'
          ),
          array (
            'name' => 'Ethnic groceries wholesaling'
          ),
          array (
            'name' => 'Food wholesaling (not elsewhere classified)'
          ),
          array (
            'name' => 'Fruit juice wholesaling'
          ),
          array (
            'name' => 'Health foods wholesaling'
          ),
          array (
            'name' => 'Honey wholesaling'
          ),
          array (
            'name' => 'Margarine wholesaling'
          ),
          array (
            'name' => 'Milk, dried, condensed or concentrated, wholesaling'
          ),
          array (
            'name' => 'Nuts wholesaling (roasted, salted or coated)'
          ),
          array (
            'name' => 'Potato crisp wholesaling'
          ),
          array (
            'name' => 'Preserved fruits or vegetables wholesaling'
          ),
          array (
            'name' => 'Rice, milled or polished, wholesaling'
          ),
          array (
            'name' => 'Salt, household, wholesaling'
          ),
          array (
            'name' => 'Seafood’s, canned, wholesaling'
          ),
          array (
            'name' => 'Soft drink wholesaling'
          ),
          array (
            'name' => 'Specialised food wholesaling (not elsewhere classified)'
          ),
          array (
            'name' => 'Specific cultural grocery wholesaling'
          ),
          array (
            'name' => 'Tea wholesaling'
          ),
          array (
            'name' => 'Vinegar wholesaling or bottling'
          ),
          array (
            'name' => 'Yeast wholesaling'
          )
        )
      ),
      array (
        'name' => 'Textile product wholesaling',
        'code' => '47210',
        'cover_plus' => '0.27',
        'cover_plus_extra' => '0.38',
        'activities' => array (
          array (
            'name' => 'Awning, textile, wholesaling',
            'bic_code' => 'F371105'
          ),
          array (
            'name' => 'Bag or sack, textile, wholesaling',
            'bic_code' => 'F371110'
          ),
          array (
            'name' => 'Blanket wholesaling',
            'bic_code' => 'F371115'
          ),
          array (
            'name' => 'Blind, textile, wholesaling',
            'bic_code' => 'F371120'
          ),
          array (
            'name' => 'Canvas goods wholesaling (not elsewhere classified)',
            'bic_code' => 'F371125'
          ),
          array (
            'name' => 'Cordage wholesaling',
            'bic_code' => 'F371130'
          ),
          array (
            'name' => 'Elasticised fabrics wholesaling',
            'bic_code' => 'F371135'
          ),
          array (
            'name' => 'Fabric, textile, wholesaling',
            'bic_code' => 'F371140'
          ),
          array (
            'name' => 'Felt wholesaling (except floor coverings)',
            'bic_code' => 'F371145'
          ),
          array (
            'name' => 'Glass fibre fabric wholesaling',
            'bic_code' => 'F371150'
          ),
          array (
            'name' => 'Lace wholesaling'
          ),
          array (
            'name' => 'Linen wholesaling'
          ),
          array (
            'name' => 'Narrow fabrics wholesaling'
          ),
          array (
            'name' => 'Netting, textile wholesaling'
          ),
          array (
            'name' => 'Piece-goods wholesaling'
          ),
          array (
            'name' => 'Rope wholesaling (except wire rope)'
          ),
          array (
            'name' => 'Sail cloth wholesaling'
          ),
          array (
            'name' => 'Sewing thread wholesaling'
          ),
          array (
            'name' => 'Soft furnishings wholesaling'
          ),
          array (
            'name' => 'String wholesaling'
          ),
          array (
            'name' => 'Tarpaulins wholesaling'
          ),
          array (
            'name' => 'Tents wholesaling'
          ),
          array (
            'name' => 'Textiles wholesaling (not elsewhere classified)'
          ),
          array (
            'name' => 'Thread wholesaling'
          ),
          array (
            'name' => 'Towels wholesaling'
          ),
          array (
            'name' => 'Trimmings, textile, wholesaling'
          ),
          array (
            'name' => 'Yarns wholesaling'
          )
        )
      ),
      array (
        'name' => 'Clothing and footwear wholesaling',
        'code' => '47220',
        'cover_plus' => '0.27',
        'cover_plus_extra' => '0.38',
        'activities' => array (
          array (
            'name' => 'Clothing wholesaling',
            'bic_code' => 'F371210'
          ),
          array (
            'name' => 'Footwear wholesaling',
            'bic_code' => 'F371220'
          ),
          array (
            'name' => 'Hosiery wholesaling'
          ),
          array (
            'name' => 'Leather clothing wholesaling'
          ),
          array (
            'name' => 'Millinery wholesaling'
          ),
          array (
            'name' => 'Sports clothing wholesaling'
          ),
          array (
            'name' => 'Sports footwear wholesaling'
          )
        )
      ),
      array (
        'name' => 'Pharmaceutical and toiletry goods wholesaling',
        'code' => '47960',
        'cover_plus' => '0.27',
        'cover_plus_extra' => '0.38',
        'activities' => array (
          array (
            'name' => 'Cosmetic wholesaling',
            'bic_code' => 'F372010'
          ),
          array (
            'name' => 'Drug wholesaling',
            'bic_code' => 'F372020'
          ),
          array (
            'name' => 'Medicine wholesaling',
            'bic_code' => 'F372030'
          ),
          array (
            'name' => 'Perfume wholesaling',
            'bic_code' => 'F372040'
          ),
          array (
            'name' => 'Toiletry wholesaling',
            'bic_code' => 'F372050'
          ),
          array (
            'name' => 'Veterinary drug wholesaling',
            'bic_code' => 'F372060'
          ),
          array (
            'name' => 'Veterinary medicine wholesaling',
            'bic_code' => 'F372070'
          )
        )
      ),
      array (
        'name' => 'Furniture and floor-coverings wholesaling',
        'code' => '47320',
        'cover_plus' => '0.48',
        'cover_plus_extra' => '0.66',
        'activities' => array (
          array (
            'name' => 'Blind wholesaling (except textile)',
            'bic_code' => 'F373110'
          ),
          array (
            'name' => 'Floor covering wholesaling (except ceramic floor tiles)',
            'bic_code' => 'F373120'
          ),
          array (
            'name' => 'Furniture wholesaling',
            'bic_code' => 'F373130'
          ),
          array (
            'name' => 'Mattress wholesaling',
            'bic_code' => array (
              'F373140',
              'F373150'
            )
          )
        )
      ),
      array (
        'name' => 'Jewellery and watch wholesaling',
        'code' => '47920',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Clock wholesaling',
            'bic_code' => 'F373210'
          ),
          array (
            'name' => 'Costume jewellery wholesaling',
            'bic_code' => 'F373220'
          ),
          array (
            'name' => 'Gemstone wholesaling',
            'bic_code' => 'F373230'
          ),
          array (
            'name' => 'Jewellery wholesaling',
            'bic_code' => 'F373240'
          ),
          array (
            'name' => 'Precious metal wholesaling',
            'bic_code' => 'F373250'
          ),
          array (
            'name' => 'Watch or clock part wholesaling',
            'bic_code' => 'F373260'
          ),
          array (
            'name' => 'Watch wholesaling'
          )
        )
      ),
      array (
        'name' => 'Kitchenware and diningware wholesaling',
        'code' => '47390',
        'cover_plus' => '0.48',
        'cover_plus_extra' => '0.66',
        'activities' => array (
          array (
            'name' => 'Brushware wholesaling',
            'bic_code' => 'F373310'
          ),
          array (
            'name' => 'Chinaware wholesaling',
            'bic_code' => 'F373315'
          ),
          array (
            'name' => 'Cooking utensil wholesaling (except electric appliances)',
            'bic_code' => 'F373320'
          ),
          array (
            'name' => 'Crockery wholesaling'
          ),
          array (
            'name' => 'Cutlery wholesaling'
          ),
          array (
            'name' => 'Enamelware wholesaling'
          ),
          array (
            'name' => 'Glassware wholesaling'
          ),
          array (
            'name' => 'Kitchenware wholesaling'
          ),
          array (
            'name' => 'Tableware wholesaling'
          )
        )
      ),
      array (
        'name' => 'Toy and sporting goods wholesaling',
        'code' => '47930',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Ammunition wholesaling',
            'bic_code' => 'F373410'
          ),
          array (
            'name' => 'Bicycle parts wholesaling',
            'bic_code' => 'F373420'
          ),
          array (
            'name' => 'Bicycle wholesaling',
            'bic_code' => 'F373430'
          ),
          array (
            'name' => 'Playground equipment wholesaling',
            'bic_code' => 'F373440'
          ),
          array (
            'name' => 'Sporting goods wholesaling (except clothing or footwear)',
            'bic_code' => 'F373450'
          ),
          array (
            'name' => 'Toy wholesaling',
            'bic_code' => 'F373460'
          )
        )
      ),
      array (
        'name' => 'Book and magazine wholesaling',
        'code' => '47940',
        'cover_plus' => '0.27',
        'cover_plus_extra' => '0.38',
        'activities' => array (
          array (
            'name' => 'Book wholesaling',
            'bic_code' => 'F373510'
          ),
          array (
            'name' => 'Magazine wholesaling',
            'bic_code' => 'F373520'
          ),
          array (
            'name' => 'Newspaper wholesaling',
            'bic_code' => 'F373530'
          ),
          array (
            'name' => 'Periodical wholesaling',
            'bic_code' => 'F373540'
          )
        )
      ),
      array (
        'name' => 'Paper product wholesaling',
        'code' => '47950',
        'cover_plus' => '0.27',
        'cover_plus_extra' => '0.38',
        'activities' => array (
          array (
            'name' => 'Greeting card wholesaling',
            'bic_code' => 'F373610'
          ),
          array (
            'name' => 'Paper or paper board container wholesaling',
            'bic_code' => 'F373620'
          ),
          array (
            'name' => 'Paper product wholesaling (not elsewhere classified)',
            'bic_code' => 'F373630'
          ),
          array (
            'name' => 'Paper stationery wholesaling',
            'bic_code' => 'F373640'
          ),
          array (
            'name' => 'Paper wholesaling'
          )
        )
      ),
      array (
        'name' => 'Wholesaling (not elsewhere classified)',
        'code' => '47990',
        'cover_plus' => '0.52',
        'cover_plus_extra' => '0.72',
        'activities' => array (
          array (
            'name' => 'Artists’ supplies wholesaling',
            'bic_code' => 'D292220'
          ),
          array (
            'name' => 'New can, metal, wholesaling',
            'bic_code' => 'D292230'
          ),
          array (
            'name' => 'Cask wholesaling',
            'bic_code' => 'F373910'
          ),
          array (
            'name' => 'Coffin wholesaling',
            'bic_code' => 'F373920'
          ),
          array (
            'name' => 'Container wholesaling (except of paper or paper board)',
            'bic_code' => 'F373930'
          ),
          array (
            'name' => 'Crate, wooden, wholesaling',
            'bic_code' => 'F373940'
          ),
          array (
            'name' => 'Firewood wholesaling',
            'bic_code' => 'F373950'
          ),
          array (
            'name' => 'Fur, dyed or dressed, wholesaling',
            'bic_code' => 'F373960'
          ),
          array (
            'name' => 'Glass container wholesaling',
            'bic_code' => 'F373970'
          ),
          array (
            'name' => 'Leather good wholesaling (except clothing or footwear)'
          ),
          array (
            'name' => 'Luggage wholesaling'
          ),
          array (
            'name' => 'Musical instrument wholesaling'
          ),
          array (
            'name' => 'Pet food wholesaling'
          ),
          array (
            'name' => 'Pre-recorded audio media wholesaling'
          ),
          array (
            'name' => 'Pre-recorded entertainment media wholesaling'
          ),
          array (
            'name' => 'Pre-recorded video media wholesaling'
          ),
          array (
            'name' => 'Second-hand bottle dealing (wholesaling)'
          ),
          array (
            'name' => 'Second-hand goods wholesaling (not elsewhere classified)'
          ),
          array (
            'name' => 'Sheet music wholesaling'
          ),
          array (
            'name' => 'Wholesale trade (not elsewhere classified)'
          )
        )
      ),
      array (
        'name' => 'Wholesaling - commission-based or excluding storage and handling of goods',
        'code' => '47991',
        'cover_plus' => '0.22',
        'cover_plus_extra' => '0.32',
        'activities' => array (
          array (
            'name' => 'Auction house or rooms',
            'bic_code' => 'F373980'
          ),
          array (
            'name' => 'Auction service (except real estate)',
            'bic_code' => 'F380005'
          ),
          array (
            'name' => 'Export agent, wholesaler',
            'bic_code' => 'F380010'
          ),
          array (
            'name' => 'Goods wholesale broking (not elsewhere classified)',
            'bic_code' => 'F380020'
          ),
          array (
            'name' => 'Import agent, wholesaler',
            'bic_code' => 'F380060'
          ),
          array (
            'name' => 'Livestock agent',
            'bic_code' => 'F380030'
          ),
          array (
            'name' => 'Manufacturer’s sales agent',
            'bic_code' => 'F380050'
          ),
          array (
            'name' => 'Stock and station agent',
            'bic_code' => 'F380040'
          ),
          array (
            'name' => 'Stockyard operation'
          ),
          array (
            'name' => 'Wholesaler’s sales agent'
          ),
          array (
            'name' => 'Wholesaling, all products (excluding storage and handling of goods)'
          ),
          array (
            'name' => 'Wool broking'
          )
        )
      ),
      array (
        'name' => 'Car retailing (including associated vehicle servicing)',
        'code' => '53110',
        'cover_plus' => '0.39',
        'cover_plus_extra' => '0.54',
        'activities' => array (
          array (
            'name' => 'New car retailing',
            'bic_code' => 'G391110'
          ),
          array (
            'name' => 'Used car retailing'
          )
        )
      ),
      array (
        'name' => 'Car wholesaling',
        'code' => '46210',
        'cover_plus' => '0.27',
        'cover_plus_extra' => '0.38',
        'activities' => array (
          array (
            'name' => 'New car wholesaling',
            'bic_code' => 'F350110'
          ),
          array (
            'name' => 'Used car wholesaling'
          )
        )
      ),
      array (
        'name' => 'Motor cycle retailing (including associated vehicle servicing)',
        'code' => '53120',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'All terrain vehicle retailing',
            'bic_code' => 'G391210'
          ),
          array (
            'name' => 'Go-kart, motorised, retailing',
            'bic_code' => 'G391220'
          ),
          array (
            'name' => 'Motor cycle or scooter retailing'
          ),
          array (
            'name' => 'Motorised minibike retailing'
          )
        )
      ),
      array (
        'name' => 'Trailer and motor vehicle retailing (not elsewhere classified)',
        'code' => '53130',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Box trailer retailing',
            'bic_code' => 'G391310'
          ),
          array (
            'name' => 'Caravan retailing',
            'bic_code' => 'G391320'
          ),
          array (
            'name' => 'Horse float retailing',
            'bic_code' => 'G391330'
          ),
          array (
            'name' => 'Mobile home retailing',
            'bic_code' => 'G391340'
          ),
          array (
            'name' => 'Trailer retailing (except boat trailers)'
          )
        )
      ),
      array (
        'name' => 'Trailer and motor vehicle wholesaling (not elsewhere classified)',
        'code' => '46221',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Box trailer wholesaling',
            'bic_code' => 'F350305'
          ),
          array (
            'name' => 'Caravan wholesaling',
            'bic_code' => 'F350310'
          ),
          array (
            'name' => 'Four-wheeled motor cycle wholesaling',
            'bic_code' => 'F350320'
          ),
          array (
            'name' => 'Motor cycle wholesaling'
          ),
          array (
            'name' => 'Motorhome wholesaling'
          ),
          array (
            'name' => 'Scooter wholesaling'
          ),
          array (
            'name' => 'Trailer wholesaling (not elsewhere classified)'
          )
        )
      ),
      array (
        'name' => 'Motor vehicle parts retailing',
        'code' => '53140',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Automotive air conditioning retailing',
            'bic_code' => 'G392110'
          ),
          array (
            'name' => 'Automotive battery retailing',
            'bic_code' => 'G392120'
          ),
          array (
            'name' => 'Car radio or CD-player retailing',
            'bic_code' => 'G392130'
          ),
          array (
            'name' => 'Motor cycle or scooter parts or accessory retailing',
            'bic_code' => 'G392140'
          ),
          array (
            'name' => 'Motor vehicle accessory retailing'
          ),
          array (
            'name' => 'Motor vehicle parts retailing'
          )
        )
      ),
      array (
        'name' => 'Tyre retailing',
        'code' => '53240',
        'cover_plus' => '1.40',
        'cover_plus_extra' => '1.86',
        'activities' => array (
          array (
            'name' => 'Motor cycle or scooter tyre and tube retailing',
            'bic_code' => 'G392210'
          ),
          array (
            'name' => 'Tyre or tube, motor vehicle, retailing',
            'bic_code' => 'G392220'
          )
        )
      ),
      array (
        'name' => 'Petroleum fuel retailing (including associated vehicle servicing)',
        'code' => '53210',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Automotive CNG retailing',
            'bic_code' => 'G400010'
          ),
          array (
            'name' => 'Diesel oil retailing',
            'bic_code' => 'G400020'
          ),
          array (
            'name' => 'Distillate retailing',
            'bic_code' => 'G400030'
          ),
          array (
            'name' => 'Engine oil retailing'
          ),
          array (
            'name' => 'Kerosene retailing'
          ),
          array (
            'name' => 'LPG, automotive, retailing'
          ),
          array (
            'name' => 'Lubricating oil or grease retailing'
          ),
          array (
            'name' => 'Petrol retailing'
          ),
          array (
            'name' => 'Service station operation (mainly petrol retailing)'
          )
        )
      ),
      array (
        'name' => 'Petroleum product wholesaling (including product ownership to retail point-of-sale)',
        'code' => '45210',
        'cover_plus' => '0.27',
        'cover_plus_extra' => '0.38',
        'activities' => array (
          array (
            'name' => 'Bitumen wholesaling',
            'bic_code' => 'F332110'
          ),
          array (
            'name' => 'Crude oil wholesaling',
            'bic_code' => 'F332120'
          ),
          array (
            'name' => 'Crude petroleum wholesaling',
            'bic_code' => 'F332130'
          ),
          array (
            'name' => 'Diesel oil wholesaling'
          ),
          array (
            'name' => 'Distillate wholesaling'
          ),
          array (
            'name' => 'Fuel oil wholesaling'
          ),
          array (
            'name' => 'Heating oil dealing (wholesaling)'
          ),
          array (
            'name' => 'Kerosene wholesaling'
          ),
          array (
            'name' => 'Liquefied petroleum gas (LPG) wholesaling (in bulk or in containers)'
          ),
          array (
            'name' => 'Oil or grease, industrial or lubricating, wholesaling'
          ),
          array (
            'name' => 'Paraffin wholesaling'
          ),
          array (
            'name' => 'Petroleum product wholesaling'
          )
        )
      ),
      array (
        'name' => 'Supermarket and grocery stores',
        'code' => '51100',
        'cover_plus' => '0.81',
        'cover_plus_extra' => '1.09',
        'activities' => array (
          array (
            'name' => 'Convenience store operation',
            'bic_code' => 'G411010'
          ),
          array (
            'name' => 'Grocery retailing',
            'bic_code' => 'G411020'
          ),
          array (
            'name' => 'Grocery supermarket operation',
            'bic_code' => array (
              'G411030',
              'G411040'
            )
          )
        )
      ),
      array (
        'name' => 'In-store retail support services',
        'code' => '51110',
        'cover_plus' => '0.52',
        'cover_plus_extra' => '0.72',
        'activities' => array (
          array (
            'name' => 'In-store demonstration',
            'bic_code' => 'N729935'
          ),
          array (
            'name' => 'In-store retail support',
            'bic_code' => 'N729945'
          ),
          array (
            'name' => 'Merchandising services',
            'bic_code' => 'N729955'
          ),
          array (
            'name' => 'Plan-o-gram service',
            'bic_code' => 'M694050'
          ),
          array (
            'name' => 'Product display assembly',
            'bic_code' => 'N729960'
          ),
          array (
            'name' => 'Shelf restocking',
            'bic_code' => 'N729970'
          )
        )
      ),
      array (
        'name' => 'Industrial and agricultural chemical product wholesaling',
        'code' => '45230',
        'cover_plus' => '0.27',
        'cover_plus_extra' => '0.38',
        'activities' => array (
          array (
            'name' => 'Acid wholesaling',
            'bic_code' => 'F332305'
          ),
          array (
            'name' => 'Adhesive wholesaling',
            'bic_code' => 'F332310'
          ),
          array (
            'name' => 'Agricultural chemical wholesaling',
            'bic_code' => 'F332315'
          ),
          array (
            'name' => 'Alcohol, industrial, wholesaling',
            'bic_code' => 'F332320'
          ),
          array (
            'name' => 'Bleaching compound wholesaling',
            'bic_code' => 'F332325'
          ),
          array (
            'name' => 'Chemical colour wholesaling',
            'bic_code' => 'F332330'
          ),
          array (
            'name' => 'Chemical wholesaling (not elsewhere classified)',
            'bic_code' => 'F332335'
          ),
          array (
            'name' => 'Cleanser, abrasive, wholesaling',
            'bic_code' => 'F332340'
          ),
          array (
            'name' => 'Detergent, industrial, wholesaling',
            'bic_code' => 'F332345 '
          ),
          array (
            'name' => 'Dry-cleaning preparation wholesaling',
            'bic_code' => 'F332350'
          ),
          array (
            'name' => 'Dyestuff wholesaling',
            'bic_code' => 'F332355'
          ),
          array (
            'name' => 'Explosive wholesaling (except ammunition)',
            'bic_code' => 'F332360'
          ),
          array (
            'name' => 'Fertiliser wholesaling',
            'bic_code' => 'F332365'
          ),
          array (
            'name' => 'Gas, industrial, wholesaling (except liquefied petroleum gas)'
          ),
          array (
            'name' => 'Gelatine wholesaling'
          ),
          array (
            'name' => 'Herbicide wholesaling'
          ),
          array (
            'name' => 'Industrial oil or fat wholesaling'
          ),
          array (
            'name' => 'Insecticide wholesaling'
          ),
          array (
            'name' => 'Liquefied gas wholesaling (except liquefied petroleum gas'
          ),
          array (
            'name' => 'Marine oil wholesaling'
          ),
          array (
            'name' => 'Match wholesaling'
          ),
          array (
            'name' => 'Methylated spirit wholesaling'
          ),
          array (
            'name' => 'Oil treating compound wholesaling'
          ),
          array (
            'name' => 'Pesticide wholesaling'
          ),
          array (
            'name' => 'Photographic chemical wholesaling'
          ),
          array (
            'name' => 'Pigment wholesaling'
          ),
          array (
            'name' => 'Plastic block, rod, plate or other unfinished form wholesaling'
          ),
          array (
            'name' => 'Plastic film sheeting wholesaling'
          ),
          array (
            'name' => 'Plasticiser wholesaling'
          ),
          array (
            'name' => 'Polish wholesaling'
          ),
          array (
            'name' => 'Sheep dip wholesaling'
          ),
          array (
            'name' => 'Sheep lick wholesaling'
          ),
          array (
            'name' => 'Soap, industrial, wholesaling'
          ),
          array (
            'name' => 'Synthetic rubber wholesaling'
          ),
          array (
            'name' => 'Tanning requisite wholesaling'
          ),
          array (
            'name' => 'Water proofing compound wholesaling'
          ),
          array (
            'name' => 'Weedkiller wholesaling'
          )
        )
      ),
      array (
        'name' => 'Fresh meat, fish, and poultry retailing',
        'code' => '51210',
        'cover_plus' => '0.81',
        'cover_plus_extra' => '1.09',
        'activities' => array (
          array (
            'name' => 'Butcher’s shop operation (retail)',
            'bic_code' => 'G412110'
          ),
          array (
            'name' => 'Fish, fresh, retailing',
            'bic_code' => 'G412120'
          ),
          array (
            'name' => 'Meat, fresh, retailing',
            'bic_code' => 'G412130'
          ),
          array (
            'name' => 'Poultry, fresh, retailing',
            'bic_code' => 'G412140'
          ),
          array (
            'name' => 'Seafood, fresh, retailing'
          )
        )
      ),
      array (
        'name' => 'Fruit and vegetable retailing',
        'code' => '51220',
        'cover_plus' => '0.81',
        'cover_plus_extra' => '1.09',
        'activities' => array (
          array (
            'name' => 'Fruit, fresh, retailing',
            'bic_code' => 'G412210'
          ),
          array (
            'name' => 'Greengrocery operation (retail)',
            'bic_code' => 'G412220'
          ),
          array (
            'name' => 'Vegetable, fresh, retailing',
            'bic_code' => 'G412230'
          )
        )
      ),
      array (
        'name' => 'Liquor retailing',
        'code' => '51230',
        'cover_plus' => '0.81',
        'cover_plus_extra' => '1.09',
        'activities' => array (
          array (
            'name' => 'Alcoholic beverage retailing (for consumption off the premises only)',
            'bic_code' => array (
              'G412310',
              'G412320'
            )
          )
        )
      ),
      array (
        'name' => 'Specialised food retailing (not elsewhere classified)',
        'code' => '51290',
        'cover_plus' => '0.81',
        'cover_plus_extra' => '1.09',
        'activities' => array (
          array (
            'name' => 'Biscuit retailing (not manufactured on the same premises)',
            'bic_code' => 'G412905'
          ),
          array (
            'name' => 'Bread retailing (not manufactured on the same premises)',
            'bic_code' => 'G412910'
          ),
          array (
            'name' => 'Bread vendor (not manufactured on the same premises)',
            'bic_code' => 'G412915'
          ),
          array (
            'name' => 'Cake retailing (not manufactured on the same premises)',
            'bic_code' => 'G412920'
          ),
          array (
            'name' => 'Confectionery retailing',
            'bic_code' => 'G412925'
          ),
          array (
            'name' => 'Non-alcoholic drinks retailing',
            'bic_code' => 'G412930'
          ),
          array (
            'name' => 'Pastry retailing (not manufactured on the same premises)',
            'bic_code' => 'G412935'
          ),
          array (
            'name' => 'Smallgoods retailing',
            'bic_code' => 'G412940'
          ),
          array (
            'name' => 'Specialised food retailing (not elsewhere classified)',
            'bic_code' => array (
              'G412945',
              'G412950'
            )
          )
        )
      ),
      array (
        'name' => 'Furniture retailing',
        'code' => '52310',
        'cover_plus' => '0.79',
        'cover_plus_extra' => '1.07',
        'activities' => array (
          array (
            'name' => 'Antique reproduction furniture retailing',
            'bic_code' => 'G421110'
          ),
          array (
            'name' => 'Awning retailing',
            'bic_code' => 'G421120'
          ),
          array (
            'name' => 'Blind retailing',
            'bic_code' => 'G421130'
          ),
          array (
            'name' => 'Furniture retailing',
            'bic_code' => 'G421140'
          ),
          array (
            'name' => 'Mattress retailing',
            'bic_code' => 'G421150'
          )
        )
      ),
      array (
        'name' => 'Floor-covering retailing',
        'code' => '52320',
        'cover_plus' => '0.79',
        'cover_plus_extra' => '1.07',
        'activities' => array (
          array (
            'name' => 'Carpet retailing',
            'bic_code' => 'G421210'
          ),
          array (
            'name' => 'Floor coverings retailing (except ceramic floor tiles)',
            'bic_code' => 'G421220'
          ),
          array (
            'name' => 'Floor rug retailing',
            'bic_code' => 'G421230'
          ),
          array (
            'name' => 'Floor tile retailing (lino, vinyl, cork, carpet or rubber)'
          ),
          array (
            'name' => 'Parquetry retailing'
          )
        )
      ),
      array (
        'name' => 'Houseware retailing',
        'code' => '52331',
        'cover_plus' => '0.41',
        'cover_plus_extra' => '0.57',
        'activities' => array (
          array (
            'name' => 'Brushware retailing',
            'bic_code' => 'G421310'
          ),
          array (
            'name' => 'Chinaware retailing',
            'bic_code' => 'G421315'
          ),
          array (
            'name' => 'Cooking utensil retailing (except electric)',
            'bic_code' => 'G421320'
          ),
          array (
            'name' => 'Crockery retailing',
            'bic_code' => 'G421330'
          ),
          array (
            'name' => 'Cutlery retailing',
            'bic_code' => 'G421340'
          ),
          array (
            'name' => 'Enamelware retailing'
          ),
          array (
            'name' => 'Glassware retailing'
          ),
          array (
            'name' => 'Kitchenware retailing'
          ),
          array (
            'name' => 'Picnicware retailing'
          ),
          array (
            'name' => 'Plastic container retailing'
          ),
          array (
            'name' => 'Silverware retailing'
          )
        )
      ),
      array (
        'name' => 'Manchester and textile goods retailing (not elsewhere classified)',
        'code' => '52230',
        'cover_plus' => '0.79',
        'cover_plus_extra' => '1.07',
        'activities' => array (
          array (
            'name' => 'Blanket retailing ',
            'bic_code' => 'G421410'
          ),
          array (
            'name' => 'Curtain retailing',
            'bic_code' => 'G421420'
          ),
          array (
            'name' => 'Dressmaking requisites retailing',
            'bic_code' => 'G421430'
          ),
          array (
            'name' => 'Fabric, textile, retailing',
            'bic_code' => 'G421440'
          ),
          array (
            'name' => 'Household textile retailing',
            'bic_code' => 'G421450'
          ),
          array (
            'name' => 'Linen retailing',
            'bic_code' => 'G421460'
          ),
          array (
            'name' => 'Piece-goods retailing',
            'bic_code' => 'G421470'
          ),
          array (
            'name' => 'Soft furnishing retailing',
            'bic_code' => 'G421480'
          ),
          array (
            'name' => 'Yarn retailing'
          )
        )
      ),
      array (
        'name' => 'Electrical, electronic, and gas appliance retailing',
        'code' => '52340',
        'cover_plus' => '0.41',
        'cover_plus_extra' => '0.57',
        'activities' => array (
          array (
            'name' => 'Air conditioner retailing',
            'bic_code' => 'G422105'
          ),
          array (
            'name' => 'Appliance, electric, retailing',
            'bic_code' => 'G422110'
          ),
          array (
            'name' => 'Barbecue retailing',
            'bic_code' => 'G422115'
          ),
          array (
            'name' => 'Camera retailing',
            'bic_code' => 'G422120'
          ),
          array (
            'name' => 'Compact disc player retailing',
            'bic_code' => 'G422125'
          ),
          array (
            'name' => 'Cooking utensil, electric, retailing',
            'bic_code' => 'G422130'
          ),
          array (
            'name' => 'Digital versatile disc (DVD) player retailing',
            'bic_code' => 'G422135'
          ),
          array (
            'name' => 'Electronic beeper retailing',
            'bic_code' => 'G422140'
          ),
          array (
            'name' => 'Fan, electric, retailing',
            'bic_code' => 'G422145'
          ),
          array (
            'name' => 'Floor polisher, electric, retailing',
            'bic_code' => 'G422150'
          ),
          array (
            'name' => 'Gas appliance retailing',
            'bic_code' => 'G422155'
          ),
          array (
            'name' => 'Heating equipment, electric or gas, retailing',
            'bic_code' => 'G422160'
          ),
          array (
            'name' => 'Mobile phone retailing',
            'bic_code' => 'G422165'
          ),
          array (
            'name' => 'Modem retailing'
          ),
          array (
            'name' => 'Pager retailing'
          ),
          array (
            'name' => 'Pocket calculator, electronic, retailing'
          ),
          array (
            'name' => 'Projector retailing'
          ),
          array (
            'name' => 'Radio receiving set retailing (except car radios)'
          ),
          array (
            'name' => 'Refrigerator, retailing'
          ),
          array (
            'name' => 'Shaver, electric, retailing'
          ),
          array (
            'name' => 'Sound reproducing equipment retailing'
          ),
          array (
            'name' => 'Stereo retailing'
          ),
          array (
            'name' => 'Stove, retailing'
          ),
          array (
            'name' => 'Television antennae retailing'
          ),
          array (
            'name' => 'Television set retailing'
          ),
          array (
            'name' => 'Two-way radio equipment retailing'
          ),
          array (
            'name' => 'Vacuum cleaner retailing'
          ),
          array (
            'name' => 'Video cassette recorder (VCR) retailing'
          ),
          array (
            'name' => 'Washing machine retailing'
          )
        )
      ),
      array (
        'name' => 'Computer and computer peripherals retailing',
        'code' => '52341',
        'cover_plus' => '0.41',
        'cover_plus_extra' => '0.57',
        'activities' => array (
          array (
            'name' => 'Compact disc burner retailing',
            'bic_code' => 'G422203'
          ),
          array (
            'name' => 'Computer equipment retailing',
            'bic_code' => 'G422207'
          ),
          array (
            'name' => 'Computer game console retailing',
            'bic_code' => 'G422210'
          ),
          array (
            'name' => 'Computer hardware retailing',
            'bic_code' => 'G422220'
          ),
          array (
            'name' => 'Computer software retailing (except computer games)'
          ),
          array (
            'name' => 'Printer retailing'
          ),
          array (
            'name' => 'Visual display unit (VDU) retailing'
          )
        )
      ),
      array (
        'name' => 'Computer and computer peripherals wholesaling',
        'code' => '46130',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Blank compact disc (CD) wholesaling',
            'bic_code' => 'F349203'
          ),
          array (
            'name' => 'Blank computer disc wholesaling',
            'bic_code' => 'F349207'
          ),
          array (
            'name' => 'Blank digital versatile disc (DVD) wholesaling',
            'bic_code' => 'F349210'
          ),
          array (
            'name' => 'Computer game wholesaling'
          ),
          array (
            'name' => 'Computer peripheral wholesaling (not elsewhere classified)'
          ),
          array (
            'name' => 'Computer software wholesaling'
          ),
          array (
            'name' => 'Computer wholesaling'
          ),
          array (
            'name' => 'Inkjet printer wholesaling'
          ),
          array (
            'name' => 'Keyboard wholesaling'
          ),
          array (
            'name' => 'Laser printer wholesaling'
          )
        )
      ),
      array (
        'name' => 'Electrical and electronic goods retailing (not elsewhere classified)',
        'code' => '52342',
        'cover_plus' => '0.41',
        'cover_plus_extra' => '0.57',
        'activities' => array (
          array (
            'name' => 'Dry cell battery retailing',
            'bic_code' => 'G422910'
          ),
          array (
            'name' => 'Electric light fittings retailing',
            'bic_code' => 'G422920'
          ),
          array (
            'name' => 'Electrical goods retailing (not elsewhere classified)',
            'bic_code' => 'G422930'
          ),
          array (
            'name' => 'Electronic goods retailing (not elsewhere classified)'
          )
        )
      ),
      array (
        'name' => 'Electrical and electronic goods wholesaling (not elsewhere classified)',
        'code' => '46150',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Air conditioning equipment, electric, wholesaling',
            'bic_code' => 'F349405'
          ),
          array (
            'name' => 'Alarm system, electric or electronic, wholesaling',
            'bic_code' => 'F349410'
          ),
          array (
            'name' => 'Battery, dry cell, wholesaling',
            'bic_code' => 'F349415'
          ),
          array (
            'name' => 'Bulb or tube, electric light, wholesaling',
            'bic_code' => 'F349420'
          ),
          array (
            'name' => 'Cable or wire, electric, wholesaling',
            'bic_code' => 'F349425'
          ),
          array (
            'name' => 'Calculator wholesaling',
            'bic_code' => 'F349430'
          ),
          array (
            'name' => 'Camera wholesaling',
            'bic_code' => 'F349435'
          ),
          array (
            'name' => 'Cash register wholesaling',
            'bic_code' => 'F349440'
          ),
          array (
            'name' => 'Digital versatile disc (DVD) players wholesaling',
            'bic_code' => 'F349445'
          ),
          array (
            'name' => 'Electric fitting dealing (wholesaling) (not elsewhere classified)',
            'bic_code' => 'F349450'
          ),
          array (
            'name' => 'Electrical distribution equipment wholesaling',
            'bic_code' => 'F349455 '
          ),
          array (
            'name' => 'Electrical goods wholesaling (not elsewhere classified)',
            'bic_code' => 'F349460'
          ),
          array (
            'name' => 'Electrical measuring or testing instrument wholesaling',
            'bic_code' => 'F349465'
          ),
          array (
            'name' => 'Electronic good wholesaling (not elsewhere classified)',
            'bic_code' => 'F349470'
          ),
          array (
            'name' => 'Fan, electric, wholesaling',
            'bic_code' => 'F349475'
          ),
          array (
            'name' => 'Floor polisher wholesaling',
            'bic_code' => 'F349480'
          ),
          array (
            'name' => 'Generator, electricity, wholesaling',
            'bic_code' => 'F349485'
          ),
          array (
            'name' => 'Kitchen appliance, electric, wholesaling'
          ),
          array (
            'name' => 'Light fitting, electric, wholesaling'
          ),
          array (
            'name' => 'Motor, electric, wholesaling'
          ),
          array (
            'name' => 'Photocopier wholesaling'
          ),
          array (
            'name' => 'Photographic equipment, electrical or electronic, wholesaling'
          ),
          array (
            'name' => 'Radio or television part wholesaling'
          ),
          array (
            'name' => 'Radio receiving set wholesaling'
          ),
          array (
            'name' => 'Refrigeration equipment, wholesaling'
          ),
          array (
            'name' => 'Scale, electrical or electronic, wholesaling'
          ),
          array (
            'name' => 'Sewing machine wholesaling'
          ),
          array (
            'name' => 'Shaver, electric, wholesaling'
          ),
          array (
            'name' => 'Shop or office scale wholesaling'
          ),
          array (
            'name' => 'Sound recording or reproducing equipment, wholesaling'
          ),
          array (
            'name' => 'Stove or heater wholesaling'
          ),
          array (
            'name' => 'Switchgear, electrical, wholesaling'
          ),
          array (
            'name' => 'Television set wholesaling'
          ),
          array (
            'name' => 'Vacuum cleaner wholesaling'
          ),
          array (
            'name' => 'Video cassette recorder (VCR) wholesaling'
          ),
          array (
            'name' => 'Washing machine wholesaling'
          ),
          array (
            'name' => 'Welding equipment, electrical, wholesaling'
          )
        )
      ),
      array (
        'name' => 'Hardware and building supplies retailing',
        'code' => '52330',
        'cover_plus' => '0.79',
        'cover_plus_extra' => '1.07',
        'activities' => array (
          array (
            'name' => 'Carpenters’ tools retailing',
            'bic_code' => 'G423110'
          ),
          array (
            'name' => 'Cement retailing',
            'bic_code' => 'G423115'
          ),
          array (
            'name' => 'Ceramic floor tile retailing',
            'bic_code' => 'G423120'
          ),
          array (
            'name' => 'Garden tool retailing',
            'bic_code' => 'G423130'
          ),
          array (
            'name' => 'Hardware retailing',
            'bic_code' => 'G423140'
          ),
          array (
            'name' => 'Lacquer retailing',
            'bic_code' => 'G423150'
          ),
          array (
            'name' => 'Lawn mower retailing',
            'bic_code' => 'G423160'
          ),
          array (
            'name' => 'Lock retailing',
            'bic_code' => 'G423170'
          ),
          array (
            'name' => 'Mineral turpentine retailing'
          ),
          array (
            'name' => 'Nail retailing'
          ),
          array (
            'name' => 'Paint retailing'
          ),
          array (
            'name' => 'Plumbers’ fittings retailing'
          ),
          array (
            'name' => 'Plumbers’ tools retailing'
          ),
          array (
            'name' => 'Timber retailing'
          ),
          array (
            'name' => 'Tool retailing'
          ),
          array (
            'name' => 'Wallpaper retailing'
          ),
          array (
            'name' => 'Woodworking tools retailing'
          )
        )
      ),
      array (
        'name' => 'Garden supplies retailing',
        'code' => '52530',
        'cover_plus' => '0.79',
        'cover_plus_extra' => '1.07',
        'activities' => array (
          array (
            'name' => 'Bulb, flower, retailing',
            'bic_code' => 'G423210'
          ),
          array (
            'name' => 'Fertiliser retailing',
            'bic_code' => 'G423220'
          ),
          array (
            'name' => 'Garden ornament retailing',
            'bic_code' => 'G423230'
          ),
          array (
            'name' => 'Garden supplies retailing (not elsewhere classified)',
            'bic_code' => 'G423240'
          ),
          array (
            'name' => 'Nursery stock retailing',
            'bic_code' => 'G423250'
          ),
          array (
            'name' => 'Pesticide retailing',
            'bic_code' => 'G423260'
          ),
          array (
            'name' => 'Plant, garden, retailing',
            'bic_code' => 'G423270'
          ),
          array (
            'name' => 'Pot plant retailing',
            'bic_code' => 'G423280'
          ),
          array (
            'name' => 'Seed, garden, retailing',
            'bic_code' => 'G423290'
          ),
          array (
            'name' => 'Seedlings retailing'
          ),
          array (
            'name' => 'Shrub or tree retailing'
          ),
          array (
            'name' => 'Tuber, flower, retailing'
          )
        )
      ),
      array (
        'name' => 'Hardware goods wholesaling (not elsewhere classified)',
        'code' => '45390',
        'cover_plus' => '0.62',
        'cover_plus_extra' => '0.84',
        'activities' => array (
          array (
            'name' => 'Abrasive wholesaling (except abrasive cleansers)',
            'bic_code' => 'F333905 '
          ),
          array (
            'name' => 'Awning wholesaling (except textile)',
            'bic_code' => 'F333910'
          ),
          array (
            'name' => 'Brick wholesaling',
            'bic_code' => 'F333915'
          ),
          array (
            'name' => 'Builders’ hardware dealing (wholesaling) (not elsewhere classified)',
            'bic_code' => 'F333920'
          ),
          array (
            'name' => 'Building material dealing (wholesaling) (not elsewhere classified)',
            'bic_code' => 'F333925'
          ),
          array (
            'name' => 'Building paper and paper board wholesaling',
            'bic_code' => 'F333930'
          ),
          array (
            'name' => 'Cement wholesaling',
            'bic_code' => 'F333935'
          ),
          array (
            'name' => 'Clothes hoist wholesaling',
            'bic_code' => 'F333940'
          ),
          array (
            'name' => 'Door or window wholesaling',
            'bic_code' => 'F333945'
          ),
          array (
            'name' => 'Earthenware good wholesaling',
            'bic_code' => 'F333950'
          ),
          array (
            'name' => 'Fence post wholesaling (except timber',
            'bic_code' => 'F333955'
          ),
          array (
            'name' => 'Fencing wire wholesaling',
            'bic_code' => 'F333960'
          ),
          array (
            'name' => 'Galvanised iron product wholesaling'
          ),
          array (
            'name' => 'Garden tool wholesaling'
          ),
          array (
            'name' => 'Hand tool wholesaling (including power operated)'
          ),
          array (
            'name' => 'Insulating material wholesaling'
          ),
          array (
            'name' => 'Kitchen and bathroom cabinet and wardrobe dealing'
          ),
          array (
            'name' => 'Lock wholesaling'
          ),
          array (
            'name' => 'Mineral turpentine wholesaling'
          ),
          array (
            'name' => 'Paint wholesaling'
          ),
          array (
            'name' => 'Plaster wholesaling'
          ),
          array (
            'name' => 'Plastic decorative laminated sheet wholesaling'
          ),
          array (
            'name' => 'Plastic wood wholesaling'
          ),
          array (
            'name' => 'Reinforcing wire wholesaling'
          ),
          array (
            'name' => 'Roller shutter wholesaling'
          ),
          array (
            'name' => 'Roofing material wholesaling'
          ),
          array (
            'name' => 'Sand wholesaling'
          ),
          array (
            'name' => 'Screening wire wholesaling'
          ),
          array (
            'name' => 'Screens, window, wholesaling'
          ),
          array (
            'name' => 'Stain wholesaling'
          ),
          array (
            'name' => 'Stonecutter’s tool wholesaling'
          ),
          array (
            'name' => 'Swimming pool, below ground fibreglass, wholesaling'
          ),
          array (
            'name' => 'Tile, ceramic, wholesaling'
          ),
          array (
            'name' => 'Wall or ceiling board wholesaling'
          ),
          array (
            'name' => 'Wallpaper wholesaling'
          ),
          array (
            'name' => 'Wire netting wholesaling'
          ),
          array (
            'name' => 'Wire or cable wholesaling (except electric cable)'
          ),
          array (
            'name' => 'Woodworking tool wholesaling'
          )
        )
      ),
      array (
        'name' => 'Sport and camping equipment retailing',
        'code' => '52410',
        'cover_plus' => '0.34',
        'cover_plus_extra' => '0.48',
        'activities' => array (
          array (
            'name' => 'Ammunition retailing',
            'bic_code' => 'G424110'
          ),
          array (
            'name' => 'Bicycle retailing',
            'bic_code' => 'G424120'
          ),
          array (
            'name' => 'Camping equipment retailing',
            'bic_code' => 'G424130'
          ),
          array (
            'name' => 'Canoe retailing',
            'bic_code' => 'G424140'
          ),
          array (
            'name' => 'Equestrian equipment retailing',
            'bic_code' => 'G424150'
          ),
          array (
            'name' => 'Fishing tackle retailing',
            'bic_code' => 'G424160'
          ),
          array (
            'name' => 'Fitness equipment retailing'
          ),
          array (
            'name' => 'Golfing equipment retailing'
          ),
          array (
            'name' => 'Gun or rifle retailing'
          ),
          array (
            'name' => 'Gymnasium equipment retailing'
          ),
          array (
            'name' => 'Sailboard retailing'
          ),
          array (
            'name' => 'Snow ski retailing'
          ),
          array (
            'name' => 'Sporting equipment retailing (except clothing or footwear)'
          ),
          array (
            'name' => 'Wetsuit retailing'
          )
        )
      ),
      array (
        'name' => 'Entertainment media retailing',
        'code' => '52350',
        'cover_plus' => '0.34',
        'cover_plus_extra' => '0.48',
        'activities' => array (
          array (
            'name' => 'Audio cassette retailing',
            'bic_code' => 'G424210'
          ),
          array (
            'name' => 'Compact disc retailing',
            'bic_code' => 'G424220'
          ),
          array (
            'name' => 'Computer game retailing',
            'bic_code' => 'G424230'
          ),
          array (
            'name' => 'Digital versatile disc (DVD) retailing'
          ),
          array (
            'name' => 'Video cassette retailing'
          )
        )
      ),
      array (
        'name' => 'Toy and game retailing',
        'code' => '52420',
        'cover_plus' => '0.41',
        'cover_plus_extra' => '0.57',
        'activities' => array (
          array (
            'name' => 'Doll retailing',
            'bic_code' => 'G424310'
          ),
          array (
            'name' => 'Game retailing',
            'bic_code' => 'G424320'
          ),
          array (
            'name' => 'Toy retailing',
            'bic_code' => array (
              'G424330',
              'G424340'
            )
          )
        )
      ),
      array (
        'name' => 'Newspaper and new and used book retailing',
        'code' => '52430',
        'cover_plus' => '0.34',
        'cover_plus_extra' => '0.48',
        'activities' => array (
          array (
            'name' => 'Book retailing',
            'bic_code' => 'G424410'
          ),
          array (
            'name' => 'Magazine retailing',
            'bic_code' => 'G424420'
          ),
          array (
            'name' => 'Newspaper retailing',
            'bic_code' => 'G424430'
          ),
          array (
            'name' => 'Periodical retailing',
            'bic_code' => 'G427340'
          ),
          array (
            'name' => 'Religious book retailing',
            'bic_code' => 'G427957'
          ),
          array (
            'name' => 'Second-hand book retailing'
          )
        )
      ),
      array (
        'name' => 'Marine equipment retailing',
        'code' => '52450',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Boat retailing (including used)',
            'bic_code' => 'G424510'
          ),
          array (
            'name' => 'Boat trailer retailing',
            'bic_code' => 'G424520'
          ),
          array (
            'name' => 'Marine accessory retailing (not elsewhere classified)',
            'bic_code' => 'G424530'
          ),
          array (
            'name' => 'Outboard motor retailing',
            'bic_code' => 'G424540'
          ),
          array (
            'name' => 'Sailing or nautical accessory retailing',
            'bic_code' => 'G424550'
          ),
          array (
            'name' => 'Yacht retailing',
            'bic_code' => array (
              'G424560',
              'G424570'
            )
          )
        )
      ),
      array (
        'name' => 'Clothing retailing',
        'code' => '52210',
        'cover_plus' => '0.33',
        'cover_plus_extra' => '0.47',
        'activities' => array (
          array (
            'name' => 'Clothing accessory retailing',
            'bic_code' => 'G425105'
          ),
          array (
            'name' => 'Clothing retailing',
            'bic_code' => 'G425110'
          ),
          array (
            'name' => 'Foundation garment retailing',
            'bic_code' => 'G425115'
          ),
          array (
            'name' => 'Fur clothing retailing',
            'bic_code' => 'G425120'
          ),
          array (
            'name' => 'Glove retailing',
            'bic_code' => 'G425125'
          ),
          array (
            'name' => 'Hosiery retailing',
            'bic_code' => 'G425130'
          ),
          array (
            'name' => 'Leather clothing retailing',
            'bic_code' => 'G425135'
          ),
          array (
            'name' => 'Millinery retailing',
            'bic_code' => 'G425140'
          ),
          array (
            'name' => 'Sports clothing retailing',
            'bic_code' => 'G425145'
          ),
          array (
            'name' => 'Work clothing retailing',
            'bic_code' => 'G425150'
          )
        )
      ),
      array (
        'name' => 'Footwear retailing',
        'code' => '52220',
        'cover_plus' => '0.33',
        'cover_plus_extra' => '0.47',
        'activities' => array (
          array (
            'name' => 'Boot retailing',
            'bic_code' => 'G425210'
          ),
          array (
            'name' => 'Footwear retailing',
            'bic_code' => 'G425220'
          ),
          array (
            'name' => 'Shoe retailing'
          ),
          array (
            'name' => 'Sports footwear retailing'
          )
        )
      ),
      array (
        'name' => 'Watch and jewellery retailing',
        'code' => '52550',
        'cover_plus' => '0.34',
        'cover_plus_extra' => '0.48',
        'activities' => array (
          array (
            'name' => 'Jewellery retailing',
            'bic_code' => 'G425310'
          ),
          array (
            'name' => 'Watch retailing',
            'bic_code' => 'G425320'
          )
        )
      ),
      array (
        'name' => 'Personal accessories retailing (not elsewhere classified)',
        'code' => '52560',
        'cover_plus' => '0.34',
        'cover_plus_extra' => '0.48',
        'activities' => array (
          array (
            'name' => 'Briefcase retailing',
            'bic_code' => 'G425910'
          ),
          array (
            'name' => 'Handbag retailing',
            'bic_code' => 'G425920'
          ),
          array (
            'name' => 'Leather goods retailing (except clothing and footwear)',
            'bic_code' => 'G425923'
          ),
          array (
            'name' => 'Luggage retailing',
            'bic_code' => 'G425927'
          ),
          array (
            'name' => 'Personal accessory retailing (not elsewhere classified)',
            'bic_code' => 'G425930'
          ),
          array (
            'name' => 'Sunglass retailing',
            'bic_code' => 'G425940'
          ),
          array (
            'name' => 'Umbrella retailing'
          ),
          array (
            'name' => 'Wig retailing'
          )
        )
      ),
      array (
        'name' => 'Department stores',
        'code' => '52100',
        'cover_plus' => '0.41',
        'cover_plus_extra' => '0.57',
        'activities' => array (
          array (
            'name' => 'Department store operation',
            'bic_code' => 'G426010'
          )
        )
      ),
      array (
        'name' => 'Pharmaceutical, cosmetic, and toiletry goods retailing',
        'code' => '52510',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Cosmetic retailing',
            'bic_code' => 'G427110'
          ),
          array (
            'name' => 'Drug retailing',
            'bic_code' => 'G427120'
          ),
          array (
            'name' => 'Health supplement retailing',
            'bic_code' => 'G427125'
          ),
          array (
            'name' => 'Patent medicine retailing',
            'bic_code' => 'G427130'
          ),
          array (
            'name' => 'Perfume retailing',
            'bic_code' => 'G427140'
          ),
          array (
            'name' => 'Pharmacy, retail, operation',
            'bic_code' => 'G427150'
          ),
          array (
            'name' => 'Prescription, medicine, dispensing'
          ),
          array (
            'name' => 'Toiletry retailing'
          ),
          array (
            'name' => 'Vitamin product retailing'
          )
        )
      ),
      array (
        'name' => 'Stationery goods retailing',
        'code' => '52460',
        'cover_plus' => '0.34',
        'cover_plus_extra' => '0.48',
        'activities' => array (
          array (
            'name' => 'Artists’ supplies retailing',
            'bic_code' => 'G427210'
          ),
          array (
            'name' => 'Ink retailing',
            'bic_code' => 'G427220'
          ),
          array (
            'name' => 'Note book retailing'
          ),
          array (
            'name' => 'Pen or pencil retailing'
          ),
          array (
            'name' => 'Stationery retailing'
          ),
          array (
            'name' => 'Writing material retailing'
          )
        )
      ),
      array (
        'name' => 'Antique and used goods retailing',
        'code' => '52520',
        'cover_plus' => '0.79',
        'cover_plus_extra' => '1.07',
        'activities' => array (
          array (
            'name' => 'Antique retailing',
            'bic_code' => 'G427310'
          ),
          array (
            'name' => 'Coin dealing (retailing)',
            'bic_code' => 'G427320'
          ),
          array (
            'name' => 'Disposals retailing',
            'bic_code' => 'G427330'
          ),
          array (
            'name' => 'Pawnbroking',
            'bic_code' => 'G427350'
          ),
          array (
            'name' => 'Second-hand clothing retailing',
            'bic_code' => 'G427360'
          ),
          array (
            'name' => 'Second-hand electrical, electronic or computer equipment retailing',
            'bic_code' => 'G427370'
          ),
          array (
            'name' => 'Second-hand furniture retailing',
            'bic_code' => 'G427380'
          ),
          array (
            'name' => 'Second-hand goods retailing (not elsewhere classified)',
            'bic_code' => 'G427390'
          ),
          array (
            'name' => 'Second-hand jewellery retailing'
          ),
          array (
            'name' => 'Second-hand record, tape, CD, DVD or videos retailing'
          ),
          array (
            'name' => 'Second-hand sports card retailing'
          ),
          array (
            'name' => 'Stamp, collectible, dealing (retailing)'
          )
        )
      ),
      array (
        'name' => 'Flower retailing',
        'code' => '52540',
        'cover_plus' => '0.34',
        'cover_plus_extra' => '0.48',
        'activities' => array (
          array (
            'name' => 'Cut flower retailing',
            'bic_code' => 'G427410'
          ),
          array (
            'name' => 'Display foliage retailing',
            'bic_code' => 'G427420'
          ),
          array (
            'name' => 'Dried flower retailing',
            'bic_code' => 'G427430'
          ),
          array (
            'name' => 'Florist, retail, operation'
          )
        )
      ),
      array (
        'name' => 'Store-based retailing (not elsewhere classified)',
        'code' => '52590',
        'cover_plus' => '0.41',
        'cover_plus_extra' => '0.57',
        'activities' => array (
          array (
            'name' => 'Art gallery operation (retail)',
            'bic_code' => 'C149950'
          ),
          array (
            'name' => 'Binocular retailing',
            'bic_code' => 'G427903'
          ),
          array (
            'name' => 'Bottled liquefied petroleum gas (LPG) retailing',
            'bic_code' => 'G427907'
          ),
          array (
            'name' => 'Clock retailing',
            'bic_code' => 'G427910'
          ),
          array (
            'name' => 'Computer consumables (toners, inks) retailing',
            'bic_code' => 'G427913'
          ),
          array (
            'name' => 'Duty free store operation',
            'bic_code' => 'G427917'
          ),
          array (
            'name' => 'Firework retailing',
            'bic_code' => 'G427927'
          ),
          array (
            'name' => 'Greeting card retailing',
            'bic_code' => 'G427930'
          ),
          array (
            'name' => 'Ice retailing',
            'bic_code' => 'G427937'
          ),
          array (
            'name' => 'Map retailing',
            'bic_code' => 'G427943'
          ),
          array (
            'name' => 'Musical instrument retailing',
            'bic_code' => 'G427947'
          ),
          array (
            'name' => 'Pet and pet accessory retailing',
            'bic_code' => 'G427950'
          ),
          array (
            'name' => 'Photographic chemical retailing',
            'bic_code' => 'G427953'
          ),
          array (
            'name' => 'Photographic film or paper retailing',
            'bic_code' => 'G427960'
          ),
          array (
            'name' => 'Picture frame retailing',
            'bic_code' => 'G427963'
          ),
          array (
            'name' => 'Picture framing service',
            'bic_code' => 'G427967'
          ),
          array (
            'name' => 'Pram retailing',
            'bic_code' => 'G427970'
          ),
          array (
            'name' => 'Religious goods (except books) retailing',
            'bic_code' => 'G427973'
          ),
          array (
            'name' => 'Specialty stores (not elsewhere classified)',
            'bic_code' => 'G427977'
          ),
          array (
            'name' => 'Store-based retailing (not elsewhere classified)',
            'bic_code' => 'G427980'
          ),
          array (
            'name' => 'Swimming pool retailing'
          ),
          array (
            'name' => 'Tobacco product retailing'
          ),
          array (
            'name' => 'Variety store operation'
          )
        )
      ),
      array (
        'name' => 'Craft and gift retailing (not elsewhere classified)',
        'code' => '52591',
        'cover_plus' => '0.34',
        'cover_plus_extra' => '0.48',
        'activities' => array (
          array (
            'name' => 'Craft goods retailing',
            'bic_code' => array (
              'G427923',
              'G427940'
            )
          )
        )
      ),
      array (
        'name' => 'Firewood, coal, and coke retailing',
        'code' => '52592',
        'cover_plus' => '1.55',
        'cover_plus_extra' => '2.05',
        'activities' => array (
          array (
            'name' => 'Briquette retailing',
            'bic_code' => 'G427920'
          ),
          array (
            'name' => 'Coal retailing',
            'bic_code' => 'G427933'
          ),
          array (
            'name' => 'Coke retailing',
            'bic_code' => 'G427962'
          ),
          array (
            'name' => 'Firewood retailing'
          )
        )
      ),
      array (
        'name' => 'Non-store retailing',
        'code' => '52595',
        'cover_plus' => '0.41',
        'cover_plus_extra' => '0.57',
        'activities' => array (
          array (
            'name' => 'Direct mail retailing',
            'bic_code' => 'G431010'
          ),
          array (
            'name' => 'Direct selling of books',
            'bic_code' => 'G431020'
          ),
          array (
            'name' => 'Direct selling of cosmetics',
            'bic_code' => 'G431030'
          ),
          array (
            'name' => 'Direct selling of goods (not elsewhere classified)',
            'bic_code' => 'G431040'
          ),
          array (
            'name' => 'Internet only retailing',
            'bic_code' => 'G431050'
          ),
          array (
            'name' => 'Milk vending',
            'bic_code' => 'G431060'
          ),
          array (
            'name' => 'Mobile food retailing (except takeaway food)',
            'bic_code' => 'G431070'
          ),
          array (
            'name' => 'Non-store-based retailing (not elsewhere classified)',
            'bic_code' => 'G431075'
          ),
          array (
            'name' => 'Vending machine operation (except leasing)',
            'bic_code' => 'G431090'
          )
        )
      ),
      array (
        'name' => 'Retail commission-based buying or selling (or both)',
        'code' => '52597',
        'cover_plus' => '0.41',
        'cover_plus_extra' => '0.57',
        'activities' => array (
          array (
            'name' => 'Commission buying service',
            'bic_code' => 'G432010'
          ),
          array (
            'name' => 'Commission retailing of books',
            'bic_code' => 'G432020'
          ),
          array (
            'name' => 'Commission retailing of cosmetics'
          ),
          array (
            'name' => 'Commission retailing of health foods'
          ),
          array (
            'name' => 'Commission selling service'
          ),
          array (
            'name' => 'Commission-based milk vending'
          )
        )
      ),
      array (
        'name' => 'Accommodation',
        'code' => '57100',
        'cover_plus' => '0.7',
        'cover_plus_extra' => '0.94',
        'activities' => array (
          array (
            'name' => 'Backpacker accommodation',
            'bic_code' => 'H440005'
          ),
          array (
            'name' => 'Camping ground operation',
            'bic_code' => 'H440010'
          ),
          array (
            'name' => 'Caravan park operation (visitor)',
            'bic_code' => 'H440015'
          ),
          array (
            'name' => 'Holiday house and flat operation',
            'bic_code' => 'H440020'
          ),
          array (
            'name' => 'Hotel operation',
            'bic_code' => 'H440025'
          ),
          array (
            'name' => 'Motel operation',
            'bic_code' => 'H440030'
          ),
          array (
            'name' => 'Resort operation',
            'bic_code' => 'H440035'
          ),
          array (
            'name' => 'Serviced apartments',
            'bic_code' => 'H440040'
          ),
          array (
            'name' => 'Ski-lodge operation',
            'bic_code' => 'H440045'
          ),
          array (
            'name' => 'Student residences operation (except boarding schools)',
            'bic_code' => 'H440050'
          ),
          array (
            'name' => 'Youth hostel operation',
            'bic_code' => array (
              'H440055',
              'H440060',
              'H440065',
              'H440070'
            )
          )
        )
      ),
      array (
        'name' => 'Cafes and restaurants',
        'code' => '57300',
        'cover_plus' => '0.56',
        'cover_plus_extra' => '0.77',
        'activities' => array (
          array (
            'name' => 'Cafe operation',
            'bic_code' => 'H451110'
          ),
          array (
            'name' => 'Restaurant operation',
            'bic_code' => array (
              'H451120',
              'H451130'
            )
          )
        )
      ),
      array (
        'name' => 'Takeaway food services',
        'code' => '51250',
        'cover_plus' => '0.56',
        'cover_plus_extra' => '0.77',
        'activities' => array (
          array (
            'name' => 'Food outlet in food hall or food court',
            'bic_code' => 'H451205'
          ),
          array (
            'name' => 'Juice bar operation',
            'bic_code' => 'H451210'
          ),
          array (
            'name' => 'Mobile food van operation',
            'bic_code' => 'H451215'
          ),
          array (
            'name' => 'Takeaway food operation',
            'bic_code' => 'H451220'
          ),
          array (
            'name' => 'Mobile café - serving coffee',
            'bic_code' => array (
              'H451225',
              'H451230',
              'H451235',
              'H451270',
              'H451240',
              'H451250',
              'H451260'
            )
          )
        )
      ),
      array (
        'name' => 'Catering services (including on-hired hospitality staff)',
        'code' => '51270',
        'cover_plus' => '0.56',
        'cover_plus_extra' => '0.77',
        'activities' => array (
          array (
            'name' => 'Airline food catering service',
            'bic_code' => 'H451310'
          ),
          array (
            'name' => 'Catering service operation',
            'bic_code' => 'H451320'
          ),
          array (
            'name' => 'Labour supply for hospitality',
            'bic_code' => 'N721270'
          )
        )
      ),
      array (
        'name' => 'Pubs, taverns, and bars',
        'code' => '57200',
        'cover_plus' => '0.70',
        'cover_plus_extra' => '0.94',
        'activities' => array (
          array (
            'name' => 'Bar operation',
            'bic_code' => 'H452010'
          ),
          array (
            'name' => 'Hotel bar operation',
            'bic_code' => 'H452020'
          ),
          array (
            'name' => 'Night club operation',
            'bic_code' => 'H452030'
          ),
          array (
            'name' => 'Pub operation',
            'bic_code' => 'H452040'
          ),
          array (
            'name' => 'Tavern operation',
            'bic_code' => 'H452050'
          ),
          array (
            'name' => 'Wine bar operation',
            'bic_code' => 'H452060'
          )
        )
      ),
      array (
        'name' => 'Clubs (hospitality)',
        'code' => '57400',
        'cover_plus' => '0.70',
        'cover_plus_extra' => '0.94',
        'activities' => array (
          array (
            'name' => 'Hospitality club operation',
            'bic_code' => 'H453010'
          ),
          array (
            'name' => 'Licensed club operation',
            'bic_code' => array (
              'H453020',
              'H453030'
            )
          )
        )
      ),
      array (
        'name' => 'Road freight transport',
        'code' => '61100',
        'cover_plus' => '2.34',
        'cover_plus_extra' => '3.08',
        'activities' => array (
          array (
            'name' => 'Delivery service (road)',
            'bic_code' => 'I461010'
          ),
          array (
            'name' => 'Furniture removal service',
            'bic_code' => 'I461020'
          ),
          array (
            'name' => 'Log haulage service (road)',
            'bic_code' => 'I461030'
          ),
          array (
            'name' => 'Road freight transport service',
            'bic_code' => 'I461040'
          ),
          array (
            'name' => 'Road vehicle towing',
            'bic_code' => 'I461050'
          ),
          array (
            'name' => 'Taxi truck service (with driver)',
            'bic_code' => 'I461060'
          ),
          array (
            'name' => 'Truck hire service (with driver)',
            'bic_code' => 'I461070'
          )
        )
      ),
      array (
        'name' => 'Interurban and rural bus transport',
        'code' => '61210',
        'cover_plus' => '1.01',
        'cover_plus_extra' => '1.35',
        'activities' => array (
          array (
            'name' => 'Bus transport service, outside metropolitan area',
            'bic_code' => 'I462110'
          ),
          array (
            'name' => 'Charter bus service, outside metropolitan area',
            'bic_code' => 'I462120'
          ),
          array (
            'name' => 'Interurban bus service',
            'bic_code' => 'I462130'
          ),
          array (
            'name' => 'Rural bus service'
          )
        )
      ),
      array (
        'name' => 'Urban bus transport',
        'code' => '61220',
        'cover_plus' => '1.01',
        'cover_plus_extra' => '1.35',
        'activities' => array (
          array (
            'name' => 'Airport bus service',
            'bic_code' => 'I462210'
          ),
          array (
            'name' => 'Metropolitan bus service',
            'bic_code' => 'I462220'
          ),
          array (
            'name' => 'Metropolitan charter bus service',
            'bic_code' => 'I462230'
          ),
          array (
            'name' => 'School bus service',
            'bic_code' => 'I462240'
          ),
          array (
            'name' => 'Tramway passenger transport service',
            'bic_code' => 'I462250'
          ),
          array (
            'name' => 'Urban bus service'
          )
        )
      ),
      array (
        'name' => 'Taxi and road transport (not elsewhere classified)',
        'code' => '61230',
        'cover_plus' => '1.01',
        'cover_plus_extra' => '1.35',
        'activities' => array (
          array (
            'name' => 'Hire car service (with driver)',
            'bic_code' => 'I462310'
          ),
          array (
            'name' => 'Road passenger transport (not elsewhere classified)',
            'bic_code' => 'I462320'
          ),
          array (
            'name' => 'Shuttle van service operation',
            'bic_code' => 'I462330'
          ),
          array (
            'name' => 'Taxi cab management service (i.e. operation on behalf of owner)'
          ),
          array (
            'name' => 'Taxi service'
          )
        )
      ),
      array (
        'name' => 'Rail freight transport',
        'code' => '62000',
        'cover_plus' => '1.01',
        'cover_plus_extra' => '1.35',
        'activities' => array (
          array (
            'name' => 'Rail freight transport service',
            'bic_code' => 'I471010'
          ),
          array (
            'name' => 'Suburban rail freight service'
          )
        )
      ),
      array (
        'name' => 'Rail passenger transport',
        'code' => '62100',
        'cover_plus' => '1.01',
        'cover_plus_extra' => '1.35',
        'activities' => array (
          array (
            'name' => 'Commuter rail passenger service',
            'bic_code' => 'I472010'
          ),
          array (
            'name' => 'Metropolitan rail passenger service',
            'bic_code' => 'I472020'
          ),
          array (
            'name' => 'Monorail operation'
          ),
          array (
            'name' => 'Rail passenger transport service'
          )
        )
      ),
      array (
        'name' => 'Coastal or international water transport (vessels over 45 metres length and over 500 tonnes displacement)',
        'code' => '63010',
        'cover_plus' => '1.23',
        'cover_plus_extra' => '1.64',
        'activities' => array (
          array (
            'name' => 'Ferry operation - international or coastal (vessels over 45 metres length and 500 tonnes displacement)',
            'bic_code' => 'I481040'
          ),
          array (
            'name' => 'Ocean cruise services',
            'bic_code' => 'I481050'
          ),
          array (
            'name' => 'Ship charter, lease or rental (with crew; vessels over 45 metres length and 500 tonnes displacement)',
            'bic_code' => 'I481070'
          ),
          array (
            'name' => 'Ship management service (i.e. operation of ships on behalf of owners) (vessels over 45 metres length and 500 tonnes displacement)',
            'bic_code' => array (
              'I482010',
              'I482040'
            )
          )
        )
      ),
      array (
        'name' => 'Coastal or international water transport (vessels 45 metres length and under, or 500 tonnes displacement and under)',
        'code' => '63020',
        'cover_plus' => '1.23',
        'cover_plus_extra' => '1.64',
        'activities' => array (
          array (
            'name' => 'Boat charter, lease or rental (with crew; for any period) (vessels 45 metres length and under or 500 tonnes displacement and under)',
            'bic_code' => 'I481030'
          ),
          array (
            'name' => 'Ferry operation - international or coastal (vessels 45 metres length and under or 500 tonnes displacement and under)',
            'bic_code' => 'I481060'
          ),
          array (
            'name' => 'Ship management service (i.e.operation of ships on behalf of owners) (vessels 45 metres length and under or 500 tonnes displacement and under)',
            'bic_code' => array (
              'I481080',
              'I482020'
            )
          )
        )
      ),
      array (
        'name' => 'Inland water transport (except passenger-only)',
        'code' => '63030',
        'cover_plus' => '1.23',
        'cover_plus_extra' => '1.64',
        'activities' => array (
          array (
            'name' => 'Freight transport service (river, harbour or lake)',
            'bic_code' => array (
              'I481010',
              'I481020',
              'I482060'
            )
          )
        )
      ),
      array (
        'name' => 'Water passenger transport (river, lake, or harbour)',
        'code' => '63031',
        'cover_plus' => '1.23',
        'cover_plus_extra' => '1.64',
        'activities' => array (
          array (
            'name' => 'Passenger ferry operation (river, harbour or lake)',
            'bic_code' => 'I482030'
          ),
          array (
            'name' => 'Passenger transport service (river, harbour or lake)',
            'bic_code' => 'I482050'
          ),
          array (
            'name' => 'Water taxi service (river, harbour or lake)',
            'bic_code' => 'I482070'
          )
        )
      ),
      array (
        'name' => 'Air transport under Civil Aviation Rules Part 121, 125, or 129',
        'code' => '64010',
        'cover_plus' => '0.46',
        'cover_plus_extra' => '0.63',
        'activities' => array (
          array (
            'name' => 'Operation of medium and large aeroplanes',
            'bic_code' => 'I490008'
          ),
          array (
            'name' => 'Foreign airlines operation',
            'bic_code' => array (
              'I490010',
              'I490020',
              'I490030',
              'I490040',
              'I490050',
              'I490060'
            )
          )
        )
      ),
      array (
        'name' => 'Air operations under Civil Aviation Rules Part 137',
        'code' => '02130',
        'cover_plus' => '2.07',
        'cover_plus_extra' => '2.73',
        'activities' => array (
          array (
            'name' => 'Agricultural aircraft operations',
            'bic_code' => array (
              'A052903',
              'A052907',
              'A052910',
              'A052930',
              'A052950',
              'A052973',
              'A052977'
            )
          )
        )
      ),
      array (
        'name' => 'Air operations under Civil Aviation Rules Part 133 or 135',
        'code' => '64040',
        'cover_plus' => '2.07',
        'cover_plus_extra' => '2.73',
        'activities' => array (
          array (
            'name' => 'Flying school operation',
            'bic_code' => 'I490004'
          ),
          array (
            'name' => 'Helicopter operations',
            'bic_code' => 'I490006'
          ),
          array (
            'name' => 'Small aeroplane operation',
            'bic_code' => array (
              'I501030',
              'P821920'
            )
          )
        )
      ),
      array (
        'name' => 'Air operations under Civil Aviation Rules Part 101, 103, 104, 105, 106, or 115',
        'code' => '64050',
        'cover_plus' => '2.07',
        'cover_plus_extra' => '2.73',
        'activities' => array (
          array (
            'name' => 'Gyrogliders and parasails operation',
            'bic_code' => 'I490002'
          ),
          array (
            'name' => 'Microlight aircraft operation',
            'bic_code' => 'I501055 '
          ),
          array (
            'name' => 'Glider operation',
            'bic_code' => 'I501035'
          ),
          array (
            'name' => 'Parachuting and sky-diving operations',
            'bic_code' => 'I501017'
          ),
          array (
            'name' => 'Hang glider operation',
            'bic_code' => 'I501043'
          )
        )
      ),
      array (
        'name' => 'Scenic and sightseeing transport (excluding aviation)',
        'code' => '66500',
        'cover_plus' => '1.23',
        'cover_plus_extra' => '1.64',
        'activities' => array (
          array (
            'name' => 'Aerial cable car operation',
            'bic_code' => 'I501001'
          ),
          array (
            'name' => 'Airboat operation',
            'bic_code' => 'I501005'
          ),
          array (
            'name' => 'Boat hiring (with crew)'
          ),
          array (
            'name' => 'Chair lift operation (except ski lift operation)',
            'bic_code' => 'I501010'
          ),
          array (
            'name' => 'Charter fishing boat operation',
            'bic_code' => 'I501013'
          ),
          array (
            'name' => 'Cruise operation (river, harbour or lake; with or without restaurant facilities)',
            'bic_code' => 'I501015'
          ),
          array (
            'name' => 'Harbour sightseeing tour operation',
            'bic_code' => 'I501020'
          ),
          array (
            'name' => 'Hovercraft operation',
            'bic_code' => 'I501023'
          ),
          array (
            'name' => 'Jet boat operation',
            'bic_code' => 'I501025'
          ),
          array (
            'name' => 'Scenic railway operation',
            'bic_code' => 'I501040'
          ),
          array (
            'name' => 'Sightseeing bus, coach or tramway operation',
            'bic_code' => 'I501045'
          ),
          array (
            'name' => 'Steam train operation',
            'bic_code' => 'I501050'
          ),
          array (
            'name' => 'Whale watching cruise operation',
            'bic_code' => array (
              'I501060',
              'R913955'
            )
          )
        )
      ),
      array (
        'name' => 'Pipeline transport',
        'code' => '65010',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Pipeline operation for the transport of gas, oil and other material',
            'bic_code' => 'I502110'
          )
        )
      ),
      array (
        'name' => 'Transport (not elsewhere classified)',
        'code' => '65090',
        'cover_plus' => '0.99',
        'cover_plus_extra' => '1.33',
        'activities' => array (
          array (
            'name' => 'Over snow transport operation',
            'bic_code' => 'I502910'
          ),
          array (
            'name' => 'Ski lift operation',
            'bic_code' => 'I502920'
          ),
          array (
            'name' => 'Transport operation (not elsewhere classified)',
            'bic_code' => 'I502930'
          )
        )
      ),
      array (
        'name' => 'Postal services',
        'code' => '71110',
        'cover_plus' => '1.21',
        'cover_plus_extra' => '1.61',
        'activities' => array (
          array (
            'name' => 'Mail network operation',
            'bic_code' => 'I510110'
          ),
          array (
            'name' => 'Mailbox rental service',
            'bic_code' => 'I510120'
          ),
          array (
            'name' => 'Post office operation',
            'bic_code' => 'I510140'
          )
        )
      ),
      array (
        'name' => 'Retail postal services',
        'code' => '71111',
        'cover_plus' => '0.34',
        'cover_plus_extra' => '0.48',
        'activities' => array (
          array (
            'name' => 'Postshop operation',
            'bic_code' => 'I510145'
          ),
          array (
            'name' => 'Postal agency operation',
            'bic_code' => 'I510150'
          )
        )
      ),
      array (
        'name' => 'Courier pick-up and delivery services',
        'code' => '71120',
        'cover_plus' => '1.21',
        'cover_plus_extra' => '1.61',
        'activities' => array (
          array (
            'name' => 'Circular home delivery service',
            'bic_code' => 'I510210'
          ),
          array (
            'name' => 'Customised express pick-up and delivery service',
            'bic_code' => 'I510213'
          ),
          array (
            'name' => 'Grocery home delivery service',
            'bic_code' => 'I510105'
          ),
          array (
            'name' => 'Home delivery service',
            'bic_code' => 'I510108'
          ),
          array (
            'name' => 'Junk mail home delivery service',
            'bic_code' => 'I510240'
          ),
          array (
            'name' => 'Letterbox delivery services - addressed mail',
            'bic_code' => 'I510220'
          ),
          array (
            'name' => 'Letterbox delivery services - advertising material',
            'bic_code' => 'I510130'
          ),
          array (
            'name' => 'Letterbox delivery services (non-postal)',
            'bic_code' => 'I510230'
          ),
          array (
            'name' => 'Messenger service',
            'bic_code' => 'I510235'
          ),
          array (
            'name' => 'Newspaper home delivery'
          ),
          array (
            'name' => 'Pick-up and delivery service (not elsewhere classified)'
          ),
          array (
            'name' => 'Rural post pick-up and delivery service'
          )
        )
      ),
      array (
        'name' => 'Stevedoring services',
        'code' => '66210',
        'cover_plus' => '2.40',
        'cover_plus_extra' => '3.17',
        'activities' => array (
          array (
            'name' => 'Ship loading or unloading service (provision of labour)',
            'bic_code' => 'I521110'
          ),
          array (
            'name' => 'Stevedoring service',
            'bic_code' => 'I521120'
          )
        )
      ),
      array (
        'name' => 'Port and water transport terminal operations',
        'code' => '66230',
        'cover_plus' => '1.23',
        'cover_plus_extra' => '1.64',
        'activities' => array (
          array (
            'name' => 'Coal loader operation (water transport)',
            'bic_code' => 'I521205'
          ),
          array (
            'name' => 'Container terminal operation (water transport)',
            'bic_code' => 'I521210'
          ),
          array (
            'name' => 'Grain loader operation (water transport)',
            'bic_code' => 'I521215'
          ),
          array (
            'name' => 'Port operation',
            'bic_code' => 'I521220'
          ),
          array (
            'name' => 'Ship mooring service',
            'bic_code' => 'I521235'
          ),
          array (
            'name' => 'Water freight terminal operation',
            'bic_code' => 'I521240'
          ),
          array (
            'name' => 'Water passenger terminal operation',
            'bic_code' => 'I521245'
          ),
          array (
            'name' => 'Wharf operation',
            'bic_code' => array (
              'I521250',
              'I521255'
            )
          )
        )
      ),
      array (
        'name' => 'Water transport support services (not elsewhere classified)',
        'code' => '66290',
        'cover_plus' => '1.23',
        'cover_plus_extra' => '1.64',
        'activities' => array (
          array (
            'name' => 'Diving (including salvage and underwater inspection; excluding fishing)',
            'bic_code' => 'I521225'
          ),
          array (
            'name' => 'Lighterage service',
            'bic_code' => 'I521910'
          ),
          array (
            'name' => 'Navigation service (water transport)',
            'bic_code' => 'I521920'
          ),
          array (
            'name' => 'Pilotage service',
            'bic_code' => 'I521930'
          ),
          array (
            'name' => 'Salvage service, marine',
            'bic_code' => 'I521940'
          ),
          array (
            'name' => 'Towboat and tugboat operation',
            'bic_code' => 'I521950'
          ),
          array (
            'name' => 'Water vessel towing service',
            'bic_code' => array (
              'I521960',
              'I521970'
            )
          )
        )
      ),
      array (
        'name' => 'Airport operations and air transport support services (not elsewhere classified)',
        'code' => '66300',
        'cover_plus' => '0.46',
        'cover_plus_extra' => '0.63',
        'activities' => array (
          array (
            'name' => 'Air traffic control service',
            'bic_code' => 'I522010'
          ),
          array (
            'name' => 'Air transport navigation service',
            'bic_code' => 'I522020'
          ),
          array (
            'name' => 'Aircraft support service (not elsewhere classified)',
            'bic_code' => 'I522030'
          ),
          array (
            'name' => 'Airport baggage handling service'
          ),
          array (
            'name' => 'Airport operation'
          ),
          array (
            'name' => 'Airport terminal operation'
          )
        )
      ),
      array (
        'name' => 'Customs and shipping agents and freight-forwarding services (no handling of goods)',
        'code' => '66440',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Customs agency service (no goods handling)',
            'bic_code' => 'I529110'
          ),
          array (
            'name' => 'Customs clearance service (no goods handling)',
            'bic_code' => 'I529120'
          ),
          array (
            'name' => 'Export documentation preparating service (no goods handling)',
            'bic_code' => 'I529130'
          ),
          array (
            'name' => 'Freight brokerage service (no goods handling)',
            'bic_code' => 'I529140'
          ),
          array (
            'name' => 'Freight forwarding (no goods handling) nec',
            'bic_code' => 'I529211'
          ),
          array (
            'name' => 'Import documentation preparation service (no goods handling)',
            'bic_code' => 'I529221'
          ),
          array (
            'name' => 'Shipping agency service (no goods handling)',
            'bic_code' => 'I529231'
          ),
          array (
            'name' => 'Air freight forwarding service (no goods handling)',
            'bic_code' => 'I529241'
          ),
          array (
            'name' => 'Rail freight forwarding service (no goods handling)',
            'bic_code' => 'I529930'
          ),
          array (
            'name' => 'Road freight forwarding service (no goods handling)'
          ),
          array (
            'name' => 'Water freight forwarding service (no goods handling)'
          )
        )
      ),
      array (
        'name' => 'Freight-forwarding services and customs and shipping agents (including handling of goods)',
        'code' => '66420',
        'cover_plus' => '0.99',
        'cover_plus_extra' => '1.33',
        'activities' => array (
          array (
            'name' => 'Customs agency service (including goods handling)',
            'bic_code' => 'I529111'
          ),
          array (
            'name' => 'Customs clearance service (including goods handling)',
            'bic_code' => 'I529121'
          ),
          array (
            'name' => 'Export documentation preparating service (including goods handling)',
            'bic_code' => 'I529131'
          ),
          array (
            'name' => 'Freight brokerage service (including goods handling)',
            'bic_code' => 'I521980'
          ),
          array (
            'name' => 'Freight forwarding (including goods handling) nec',
            'bic_code' => 'I529210'
          ),
          array (
            'name' => 'Import documentation preparation service (including goods handling)',
            'bic_code' => 'I529220'
          ),
          array (
            'name' => 'Shipping agency service (including goods handling)',
            'bic_code' => 'I529230'
          ),
          array (
            'name' => 'Air freight forwarding service (including goods handling)',
            'bic_code' => 'I529240'
          ),
          array (
            'name' => 'Rail freight forwarding service (including goods handling)',
            'bic_code' => 'I529931'
          ),
          array (
            'name' => 'Road freight forwarding service (including goods handling)'
          ),
          array (
            'name' => 'Water freight forwarding service (including goods handling)'
          )
        )
      ),
      array (
        'name' => 'Transport support services (not elsewhere classified)',
        'code' => '66190',
        'cover_plus' => '0.99',
        'cover_plus_extra' => '1.33',
        'activities' => array (
          array (
            'name' => 'Container terminal operation (road and rail)',
            'bic_code' => 'C141120'
          ),
          array (
            'name' => 'Freight or goods terminal (not elsewhere classified)',
            'bic_code' => 'I529910'
          ),
          array (
            'name' => 'Logyard operation in conjunction with a processing plant',
            'bic_code' => 'I529920'
          ),
          array (
            'name' => 'Railway station or terminal operation',
            'bic_code' => 'I529940'
          ),
          array (
            'name' => 'Road freight terminal operation',
            'bic_code' => 'I529950'
          ),
          array (
            'name' => 'Road passenger terminal operation',
            'bic_code' => 'I529960'
          ),
          array (
            'name' => 'Road vehicle driving service (except owner/operator)',
            'bic_code' => 'I529970'
          ),
          array (
            'name' => 'Toll bridge operation',
            'bic_code' => 'I529980'
          ),
          array (
            'name' => 'Toll road operation',
            'bic_code' => 'I529990'
          ),
          array (
            'name' => 'Weighbridge operation',
            'bic_code' => 'N732040'
          )
        )
      ),
      array (
        'name' => 'Taxi and Other Vehicle Scheduling Operations',
        'code' => '61231',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Taxi radio base operation',
            'bic_code' => array (
              'I462340',
              'I529985'
            )
          )
        )
      ),
      array (
        'name' => 'Grain storage services',
        'code' => '67010',
        'cover_plus' => '1.13',
        'cover_plus_extra' => '1.51',
        'activities' => array (
          array (
            'name' => 'Grain elevator operation',
            'bic_code' => 'I530110'
          ),
          array (
            'name' => 'Grain silo operation',
            'bic_code' => 'I530120'
          ),
          array (
            'name' => 'Grain storage service',
            'bic_code' => 'I530130'
          )
        )
      ),
      array (
        'name' => 'Warehousing and storage services (not elsewhere classified)',
        'code' => '67090',
        'cover_plus' => '1.13',
        'cover_plus_extra' => '1.51',
        'activities' => array (
          array (
            'name' => 'Bond store operation',
            'bic_code' => 'F331120'
          ),
          array (
            'name' => 'Bulk petroleum storage service',
            'bic_code' => 'I530910'
          ),
          array (
            'name' => 'Controlled atmosphere store operation',
            'bic_code' => 'I530920'
          ),
          array (
            'name' => 'Cool room storage service',
            'bic_code' => 'I530930'
          ),
          array (
            'name' => 'Free store operation (storage of goods not under bond)',
            'bic_code' => 'I530940'
          ),
          array (
            'name' => 'Furniture storage service',
            'bic_code' => 'I530950'
          ),
          array (
            'name' => 'Refrigerated storage service',
            'bic_code' => 'I530960'
          ),
          array (
            'name' => 'Storage (not elsewhere classified)',
            'bic_code' => 'I530970'
          ),
          array (
            'name' => 'Warehousing (not elsewhere classified)'
          ),
          array (
            'name' => 'Wool storage service'
          )
        )
      ),
      array (
        'name' => 'Newspaper publishing',
        'code' => '24211',
        'cover_plus' => '0.07',
        'cover_plus_extra' => '0.13',
        'activities' => array (
          array (
            'name' => 'Newspaper publishing (except internet)',
            'bic_code' => 'J541110'
          ),
          array (
            'name' => 'Newspaper publishing (excluding printing)'
          )
        )
      ),
      array (
        'name' => 'Magazine and other periodical publishing',
        'code' => '24221',
        'cover_plus' => '0.07',
        'cover_plus_extra' => '0.13',
        'activities' => array (
          array (
            'name' => 'Comic book publishing (except internet)',
            'bic_code' => 'J541205'
          ),
          array (
            'name' => 'Journal (including trade journal) publishing (except internet)',
            'bic_code' => 'J541210'
          ),
          array (
            'name' => 'Magazine publishing (except internet)'
          ),
          array (
            'name' => 'Newsletter publishing (except internet)'
          ),
          array (
            'name' => 'Periodical publishing (except internet)'
          ),
          array (
            'name' => 'Racing form publishing (except internet)'
          ),
          array (
            'name' => 'Radio and television guide publishing (except internet)'
          )
        )
      ),
      array (
        'name' => 'Book publishing',
        'code' => '24230',
        'cover_plus' => '0.07',
        'cover_plus_extra' => '0.13',
        'activities' => array (
          array (
            'name' => 'Atlas publishing (except internet)',
            'bic_code' => 'J541310'
          ),
          array (
            'name' => 'Book publishing (except internet)'
          ),
          array (
            'name' => 'Encyclopaedia publishing (except internet)'
          ),
          array (
            'name' => 'Technical manual publishing (except internet)'
          ),
          array (
            'name' => 'Textbook publishing (except internet)'
          ),
          array (
            'name' => 'Travel guide book publishing (except internet)'
          )
        )
      ),
      array (
        'name' => 'Directory and mailing-list publishing',
        'code' => '24233',
        'cover_plus' => '0.07',
        'cover_plus_extra' => '0.13',
        'activities' => array (
          array (
            'name' => 'Address list publishing (except internet)',
            'bic_code' => 'J541410'
          ),
          array (
            'name' => 'Business directory publishing (except internet)'
          ),
          array (
            'name' => 'Directory publishing (except internet)'
          ),
          array (
            'name' => 'Mailing list publishing (except internet)'
          ),
          array (
            'name' => 'Telephone directory publishing (except internet)'
          )
        )
      ),
      array (
        'name' => 'Publishing (not elsewhere classified) (except software, music, and Internet)',
        'code' => '24231',
        'cover_plus' => '0.07',
        'cover_plus_extra' => '0.13',
        'activities' => array (
          array (
            'name' => 'Art print publishing (except internet)',
            'bic_code' => 'J541910'
          ),
          array (
            'name' => 'Calendar publishing (except internet)'
          ),
          array (
            'name' => 'Diary publishing (except internet)'
          ),
          array (
            'name' => 'Greeting card publishing (except internet)'
          ),
          array (
            'name' => 'Postcard publishing (except internet)'
          )
        )
      ),
      array (
        'name' => 'Software publishing',
        'code' => '24235',
        'cover_plus' => '0.09',
        'cover_plus_extra' => '0.15',
        'activities' => array (
          array (
            'name' => 'Software publishing (non-customised)',
            'bic_code' => 'J542010'
          )
        )
      ),
      array (
        'name' => 'Motion picture and video production and other motion picture and video activities (not elsewhere classified)',
        'code' => '91110',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Motion picture and video activities (not elsewhere classified)',
            'bic_code' => 'J551110'
          ),
          array (
            'name' => 'Motion picture production',
            'bic_code' => 'J551120'
          ),
          array (
            'name' => 'Television commercial production',
            'bic_code' => 'J551407'
          ),
          array (
            'name' => 'Television program production',
            'bic_code' => 'J551130'
          ),
          array (
            'name' => 'Video filming for commercial usage',
            'bic_code' => 'J551140'
          ),
          array (
            'name' => 'Video production'
          )
        )
      ),
      array (
        'name' => 'Motion picture and video distribution',
        'code' => '91120',
        'cover_plus' => '0.13',
        'cover_plus_extra' => '0.20',
        'activities' => array (
          array (
            'name' => 'Film distribution',
            'bic_code' => 'J551210'
          ),
          array (
            'name' => 'Motion picture and video distribution',
            'bic_code' => 'J551220'
          ),
          array (
            'name' => 'Motion picture leasing',
            'bic_code' => 'J551230'
          ),
          array (
            'name' => 'Motion picture library operation (stock footage)'
          ),
          array (
            'name' => 'Television program distribution'
          )
        )
      ),
      array (
        'name' => 'Motion picture exhibition',
        'code' => '91130',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Cinema operation',
            'bic_code' => 'J551310'
          ),
          array (
            'name' => 'Drive-in theatre operation',
            'bic_code' => 'J551320'
          ),
          array (
            'name' => 'Festival operation (exhibition of motion pictures)'
          ),
          array (
            'name' => 'Motion picture screening'
          ),
          array (
            'name' => 'Motion picture theatre operation'
          )
        )
      ),
      array (
        'name' => 'Post-production and digital visual effects services',
        'code' => '91111',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Computer graphic, animation service',
            'bic_code' => 'J551403'
          ),
          array (
            'name' => 'Special effect post-production service',
            'bic_code' => 'J551404'
          ),
          array (
            'name' => 'Developing and printing motion picture film',
            'bic_code' => 'J551405'
          ),
          array (
            'name' => 'Digital visual effect services',
            'bic_code' => 'J551410'
          ),
          array (
            'name' => 'Film or tape closed captioning'
          ),
          array (
            'name' => 'Film or video post-production service'
          ),
          array (
            'name' => 'Film or video transfer service'
          ),
          array (
            'name' => 'Motion picture film reproducing'
          ),
          array (
            'name' => 'Motion picture or video editing service'
          ),
          array (
            'name' => 'Post synchronisation sound dubbing'
          ),
          array (
            'name' => 'Post-production facility, motion picture or video'
          ),
          array (
            'name' => 'Sound dubbing service, motion picture'
          ),
          array (
            'name' => 'Subtitling of motion picture, film or video'
          ),
          array (
            'name' => 'Teleproduction service'
          ),
          array (
            'name' => 'Titling of motion picture film or video'
          ),
          array (
            'name' => 'Video conversion service (between audio and visual media formats)'
          )
        )
      ),
      array (
        'name' => 'Music publishing',
        'code' => '24234',
        'cover_plus' => '0.07',
        'cover_plus_extra' => '0.13',
        'activities' => array (
          array (
            'name' => 'Authorising use of copyrighted musical composition',
            'bic_code' => 'J552110'
          ),
          array (
            'name' => 'Music book (bound sheet music) publishing'
          ),
          array (
            'name' => 'Music book publishing'
          ),
          array (
            'name' => 'Music copyright buying and selling'
          ),
          array (
            'name' => 'Music publishing'
          ),
          array (
            'name' => 'Sheet music publishing'
          ),
          array (
            'name' => 'Song publishing'
          )
        )
      ),
      array (
        'name' => 'Music and other sound recording activities (not elsewhere classified)',
        'code' => '92510',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Audio service (including for meetings and conferences)',
            'bic_code' => 'J552210'
          ),
          array (
            'name' => 'Producing pre-recorded radio programming',
            'bic_code' => 'J552220'
          ),
          array (
            'name' => 'Record distribution'
          ),
          array (
            'name' => 'Record production'
          ),
          array (
            'name' => 'Record production and distribution'
          ),
          array (
            'name' => 'Sound recording post-production service'
          ),
          array (
            'name' => 'Sound recording studio operation'
          )
        )
      ),
      array (
        'name' => 'Radio broadcasting',
        'code' => '91210',
        'cover_plus' => '0.13',
        'cover_plus_extra' => '0.20',
        'activities' => array (
          array (
            'name' => 'Radio (including satellite radio) network operation',
            'bic_code' => 'J561010'
          ),
          array (
            'name' => 'Radio broadcasting service',
            'bic_code' => 'J561020'
          ),
          array (
            'name' => 'Radio network operation'
          ),
          array (
            'name' => 'Radio station operation'
          )
        )
      ),
      array (
        'name' => 'Free-to-air television broadcasting',
        'code' => '91220',
        'cover_plus' => '0.13',
        'cover_plus_extra' => '0.20',
        'activities' => array (
          array (
            'name' => 'Free-to-air television service',
            'bic_code' => 'J562110'
          ),
          array (
            'name' => 'Television broadcasting network operation',
            'bic_code' => 'J562120'
          ),
          array (
            'name' => 'Television broadcasting station operation'
          )
        )
      ),
      array (
        'name' => 'Cable and other subscription programming',
        'code' => '91230',
        'cover_plus' => '0.13',
        'cover_plus_extra' => '0.20',
        'activities' => array (
          array (
            'name' => 'Cable broadcasting network operation',
            'bic_code' => 'J562205'
          ),
          array (
            'name' => 'Cable broadcasting station operation',
            'bic_code' => 'J562210'
          ),
          array (
            'name' => 'Pay television, broadcasting network operation',
            'bic_code' => 'J562220'
          ),
          array (
            'name' => 'Pay television, broadcasting service'
          ),
          array (
            'name' => 'Pay television, broadcasting station operation'
          ),
          array (
            'name' => 'Satellite broadcasting network operation'
          ),
          array (
            'name' => 'Satellite broadcasting station operation'
          ),
          array (
            'name' => 'Subscription television broadcasting service'
          )
        )
      ),
      array (
        'name' => 'Internet publishing and broadcasting',
        'code' => '24232',
        'cover_plus' => '0.07',
        'cover_plus_extra' => '0.13',
        'activities' => array (
          array (
            'name' => 'Internet art print publishing',
            'bic_code' => 'J570010'
          ),
          array (
            'name' => 'Internet atlas publishing'
          ),
          array (
            'name' => 'Internet audio broadcasting'
          ),
          array (
            'name' => 'Internet book publishing'
          ),
          array (
            'name' => 'Internet directory publishing'
          ),
          array (
            'name' => 'Internet encyclopaedia and dictionary publishing'
          ),
          array (
            'name' => 'Internet greeting card publishing'
          ),
          array (
            'name' => 'Internet journal publishing'
          ),
          array (
            'name' => 'Internet magazine publishing'
          ),
          array (
            'name' => 'Internet news publishing'
          ),
          array (
            'name' => 'Internet periodical publishing'
          ),
          array (
            'name' => 'Internet video broadcasting'
          )
        )
      ),
      array (
        'name' => 'Wired telecommunications network operation',
        'code' => '71200',
        'cover_plus' => '0.19',
        'cover_plus_extra' => '0.28',
        'activities' => array (
          array (
            'name' => 'International telephone network operation (wired)',
            'bic_code' => 'J580110'
          ),
          array (
            'name' => 'Local telephone network operation (wired)'
          ),
          array (
            'name' => 'Long distance telephone network operation (wired)'
          ),
          array (
            'name' => 'Telecommunications network operation (wired)'
          )
        )
      ),
      array (
        'name' => 'Wireless telecommunications network operation (not elsewhere classified)',
        'code' => '71210',
        'cover_plus' => '0.19',
        'cover_plus_extra' => '0.28',
        'activities' => array (
          array (
            'name' => 'Mobile telecommunications network operation',
            'bic_code' => 'J580210'
          ),
          array (
            'name' => 'Satellite telecommunications network operation',
            'bic_code' => 'J580220'
          ),
          array (
            'name' => 'Wireless telecommunications network operation'
          )
        )
      ),
      array (
        'name' => 'Telecommunications services (not elsewhere classified)',
        'code' => '71230',
        'cover_plus' => '0.19',
        'cover_plus_extra' => '0.28',
        'activities' => array (
          array (
            'name' => 'Paging service',
            'bic_code' => 'J580910'
          ),
          array (
            'name' => 'Satellite earth station operation'
          ),
          array (
            'name' => 'Telecommunications reselling (including satellite systems)'
          )
        )
      ),
      array (
        'name' => 'Internet service providers and web-search portals',
        'code' => '71240',
        'cover_plus' => '0.19',
        'cover_plus_extra' => '0.28',
        'activities' => array (
          array (
            'name' => 'Internet access provision',
            'bic_code' => 'J591010'
          ),
          array (
            'name' => 'Internet access service, on-line',
            'bic_code' => 'J591020'
          ),
          array (
            'name' => 'Internet search portal operation'
          ),
          array (
            'name' => 'Internet search web site operation'
          ),
          array (
            'name' => 'Internet service provision (ISP)'
          ),
          array (
            'name' => 'Portal web search operation'
          ),
          array (
            'name' => 'Web search portal operation'
          )
        )
      ),
      array (
        'name' => 'Data processing and web-hosting services',
        'code' => '78310',
        'cover_plus' => '0.08',
        'cover_plus_extra' => '0.14',
        'activities' => array (
          array (
            'name' => 'Application hosting',
            'bic_code' => 'J592110'
          ),
          array (
            'name' => 'Application service provision',
            'bic_code' => 'J592120'
          ),
          array (
            'name' => 'Audio and visual media streaming service',
            'bic_code' => 'J592130'
          ),
          array (
            'name' => 'Automated data processing service',
            'bic_code' => 'J592135'
          ),
          array (
            'name' => 'Computer input preparation service',
            'bic_code' => 'J592140'
          ),
          array (
            'name' => 'Computer time leasing or renting',
            'bic_code' => 'J592150'
          ),
          array (
            'name' => 'Computer time sharing service'
          ),
          array (
            'name' => 'Data capture imaging service'
          ),
          array (
            'name' => 'Data entry service (electronic'
          ),
          array (
            'name' => 'Data processing computer service'
          ),
          array (
            'name' => 'Disk and diskette conversion and recertification service'
          ),
          array (
            'name' => 'Electronic data processing service'
          ),
          array (
            'name' => 'Microfiche or microfilm recording and imaging service'
          ),
          array (
            'name' => 'Optical scanning service'
          ),
          array (
            'name' => 'Web hosting'
          )
        )
      ),
      array (
        'name' => 'Electronic information storage services',
        'code' => '78320',
        'cover_plus' => '0.08',
        'cover_plus_extra' => '0.14',
        'activities' => array (
          array (
            'name' => 'Computer data storage and retrieval service (except library service)',
            'bic_code' => 'J592210'
          ),
          array (
            'name' => 'Electronic information storage and retrieval service (except library service)'
          )
        )
      ),
      array (
        'name' => 'Libraries and archives',
        'code' => '92100',
        'cover_plus' => '0.13',
        'cover_plus_extra' => '0.20',
        'activities' => array (
          array (
            'name' => 'Archive operation',
            'bic_code' => 'J601010'
          ),
          array (
            'name' => 'Film archive operation',
            'bic_code' => 'J601020'
          ),
          array (
            'name' => 'Lending library operation',
            'bic_code' => 'J601030'
          ),
          array (
            'name' => 'Library operation (except motion picture stock footage and distribution)',
            'bic_code' => 'J601040'
          ),
          array (
            'name' => 'Mobile library operation',
            'bic_code' => 'J601050'
          ),
          array (
            'name' => 'Motion picture film archive operation'
          ),
          array (
            'name' => 'Music archive operation'
          ),
          array (
            'name' => 'Reference library operation'
          )
        )
      ),
      array (
        'name' => 'Information services (not elsewhere classified)',
        'code' => '92110',
        'cover_plus' => '0.12',
        'cover_plus_extra' => '0.19',
        'activities' => array (
          array (
            'name' => 'News collection service',
            'bic_code' => 'J602010'
          ),
          array (
            'name' => 'Telephone-based recorded information service',
            'bic_code' => array (
              'J602020',
              'J602030',
              'J602040'
            )
          )
        )
      ),
      array (
        'name' => 'Central banking',
        'code' => '73100',
        'cover_plus' => '0.10',
        'cover_plus_extra' => '0.17',
        'activities' => array (
          array (
            'name' => 'Central banking',
            'bic_code' => 'K621010'
          ),
          array (
            'name' => 'Financial regulatory service',
            'bic_code' => 'K621020'
          )
        )
      ),
      array (
        'name' => 'Banking',
        'code' => '73210',
        'cover_plus' => '0.10',
        'cover_plus_extra' => '0.17',
        'activities' => array (
          array (
            'name' => 'Development bank operation',
            'bic_code' => 'K622110'
          ),
          array (
            'name' => 'Savings bank operation',
            'bic_code' => 'K622120'
          ),
          array (
            'name' => 'Trading bank operation'
          )
        )
      ),
      array (
        'name' => 'Building society operation',
        'code' => '73220',
        'cover_plus' => '0.10',
        'cover_plus_extra' => '0.17',
        'activities' => array (
          array (
            'name' => 'Building society operation',
            'bic_code' => array (
              'K622210',
              'K622220'
            )
          )
        )
      ),
      array (
        'name' => 'Credit union operation',
        'code' => '73230',
        'cover_plus' => '0.10',
        'cover_plus_extra' => '0.17',
        'activities' => array (
          array (
            'name' => 'Credit union operation',
            'bic_code' => 'K622310'
          )
        )
      ),
      array (
        'name' => 'Depository financial intermediation (not elsewhere classified)',
        'code' => '73290',
        'cover_plus' => '0.10',
        'cover_plus_extra' => '0.17',
        'activities' => array (
          array (
            'name' => 'Bill of exchange discounting or financing (except by banks)',
            'bic_code' => 'K622905'
          ),
          array (
            'name' => 'Commercial finance company operation',
            'bic_code' => 'K622910'
          ),
          array (
            'name' => 'Depository financial intermediation (not elsewhere classified)',
            'bic_code' => 'K623010'
          ),
          array (
            'name' => 'Merchant banking operation'
          ),
          array (
            'name' => 'Money market dealing'
          )
        )
      ),
      array (
        'name' => 'Non-depository financing',
        'code' => '73300',
        'cover_plus' => '0.10',
        'cover_plus_extra' => '0.17',
        'activities' => array (
          array (
            'name' => 'Co-operative housing society operation (except cooperative housing society management services on a commission or fee basis)',
            'bic_code' => 'K623020'
          ),
          array (
            'name' => 'Credit card issuing operation',
            'bic_code' => 'K623030'
          ),
          array (
            'name' => 'Non-depository financing',
            'bic_code' => 'K641960'
          ),
          array (
            'name' => 'Non-depository home lending operation'
          ),
          array (
            'name' => 'Non-depository money lending operation'
          )
        )
      ),
      array (
        'name' => 'Financial asset investing',
        'code' => '73400',
        'cover_plus' => '0.10',
        'cover_plus_extra' => '0.17',
        'activities' => array (
          array (
            'name' => 'Charitable/educational trust or foundation operation (investment type; in predominantly financial assets, except trust management services on a commission or fee basis)',
            'bic_code' => 'K624010'
          ),
          array (
            'name' => 'Friendly society operation (investment type; in predominantly financial assets)',
            'bic_code' => 'K624020'
          ),
          array (
            'name' => 'Holding company operation (viz. holding shares in subsidiary companies)',
            'bic_code' => 'K624030'
          ),
          array (
            'name' => 'Investment operation (own account; in predominantly financial assets; except superannuation funds)',
            'bic_code' => 'K624040'
          ),
          array (
            'name' => 'Mutual fund operation (except fund management on a commission or fee basis)',
            'bic_code' => 'K624050'
          ),
          array (
            'name' => 'Unit trust operation (investment type; in predominantly financial assets; except trust management on a commission or fee basis)',
            'bic_code' => array (
              'K624060',
              'K624070'
            )
          )
        )
      ),
      array (
        'name' => 'Life insurance',
        'code' => '74110',
        'cover_plus' => '0.10',
        'cover_plus_extra' => '0.17',
        'activities' => array (
          array (
            'name' => 'Life insurance provision',
            'bic_code' => 'K631010'
          ),
          array (
            'name' => 'Life reinsurance provision'
          )
        )
      ),
      array (
        'name' => 'Health insurance',
        'code' => '74210',
        'cover_plus' => '0.10',
        'cover_plus_extra' => '0.17',
        'activities' => array (
          array (
            'name' => 'Dental insurance provision',
            'bic_code' => 'K632110'
          ),
          array (
            'name' => 'Funeral benefit provision',
            'bic_code' => 'K632120'
          ),
          array (
            'name' => 'Health insurance provision',
            'bic_code' => 'K632130'
          )
        )
      ),
      array (
        'name' => 'General insurance',
        'code' => '74220',
        'cover_plus' => '0.10',
        'cover_plus_extra' => '0.17',
        'activities' => array (
          array (
            'name' => 'Accident insurance provision',
            'bic_code' => 'K632210'
          ),
          array (
            'name' => 'All risks insurance provision',
            'bic_code' => 'K632220'
          ),
          array (
            'name' => 'Fire insurance provision',
            'bic_code' => 'K632230'
          ),
          array (
            'name' => 'General insurance provision',
            'bic_code' => 'K632240'
          ),
          array (
            'name' => 'Household insurance provision',
            'bic_code' => 'K632250'
          ),
          array (
            'name' => 'Insurance provision (not elsewhere classified)',
            'bic_code' => 'K632260'
          ),
          array (
            'name' => 'Mortgage insurance provision',
            'bic_code' => 'K632270'
          ),
          array (
            'name' => 'Motor vehicle insurance provision',
            'bic_code' => 'K632280'
          ),
          array (
            'name' => 'Owner’s liability insurance provision',
            'bic_code' => 'K632290'
          ),
          array (
            'name' => 'Reinsurance (except life) provision'
          ),
          array (
            'name' => 'Third party insurance provision'
          ),
          array (
            'name' => 'Travel insurance provision'
          ),
          array (
            'name' => 'Worker’s compensation insurance provision'
          )
        )
      ),
      array (
        'name' => 'Superannuation funds',
        'code' => '74120',
        'cover_plus' => '0.10',
        'cover_plus_extra' => '0.17',
        'activities' => array (
          array (
            'name' => 'Approved deposit fund (superannuation) operation',
            'bic_code' => 'K633010'
          ),
          array (
            'name' => 'Pension fund, separately constituted, operation',
            'bic_code' => 'K633020'
          ),
          array (
            'name' => 'Superannuation fund, separately constituted, operation'
          )
        )
      ),
      array (
        'name' => 'Financial asset broking services',
        'code' => '75110',
        'cover_plus' => '0.10',
        'cover_plus_extra' => '0.17',
        'activities' => array (
          array (
            'name' => 'Commodity futures broking or dealing (on a commission or transaction fee basis)',
            'bic_code' => 'K641105'
          ),
          array (
            'name' => 'Financial asset broking service (on a commission or transaction fee basis)',
            'bic_code' => 'K641110'
          ),
          array (
            'name' => 'Stock broking or trading (on a commission or transaction fee basis)',
            'bic_code' => 'K641120'
          ),
          array (
            'name' => 'Trading of mortgages operation (on a commission or transaction fee basis)',
            'bic_code' => 'K641130'
          )
        )
      ),
      array (
        'name' => 'Auxiliary finance and investment services (not elsewhere classified)',
        'code' => '75190',
        'cover_plus' => '0.10',
        'cover_plus_extra' => '0.17',
        'activities' => array (
          array (
            'name' => 'Arranging home loans for others (on a commission or fee basis)',
            'bic_code' => 'K641905'
          ),
          array (
            'name' => 'Auxiliary finance service (not elsewhere classified)',
            'bic_code' => 'K641910'
          ),
          array (
            'name' => 'Clearing house operation',
            'bic_code' => 'K641915'
          ),
          array (
            'name' => 'Co-operative housing society management service (on a commission or fee basis)',
            'bic_code' => 'K641920'
          ),
          array (
            'name' => 'Credit card administration service',
            'bic_code' => 'K641925'
          ),
          array (
            'name' => 'Executor service',
            'bic_code' => 'K641930'
          ),
          array (
            'name' => 'Finance consultant service',
            'bic_code' => 'K641935'
          ),
          array (
            'name' => 'Financial asset investment consultant service',
            'bic_code' => 'K641940'
          ),
          array (
            'name' => 'Insurance fund management service (on a commission or fee basis)',
            'bic_code' => 'K641945'
          ),
          array (
            'name' => 'Money changing service (non-bank)',
            'bic_code' => 'K641950'
          ),
          array (
            'name' => 'Nominee service',
            'bic_code' => 'K641955'
          ),
          array (
            'name' => 'Portfolio, investment, management service (on a commission or fee basis)',
            'bic_code' => 'K641965'
          ),
          array (
            'name' => 'Security valuation service',
            'bic_code' => 'K641970'
          ),
          array (
            'name' => 'Share registry operation'
          ),
          array (
            'name' => 'Stock exchange operation'
          ),
          array (
            'name' => 'Superannuation fund management service (on a commission or fee basis)'
          ),
          array (
            'name' => 'Trustee service'
          )
        )
      ),
      array (
        'name' => 'Auxiliary insurance services',
        'code' => '75200',
        'cover_plus' => '0.10',
        'cover_plus_extra' => '0.17',
        'activities' => array (
          array (
            'name' => 'Actuarial service',
            'bic_code' => 'K642010'
          ),
          array (
            'name' => 'Claim adjustment service',
            'bic_code' => 'K642020'
          ),
          array (
            'name' => 'Claim assessment service',
            'bic_code' => 'K642030'
          ),
          array (
            'name' => 'Insurance agency service',
            'bic_code' => 'K642040'
          ),
          array (
            'name' => 'Insurance broking service',
            'bic_code' => 'K642050'
          ),
          array (
            'name' => 'Insurance consultant service',
            'bic_code' => 'K642060'
          )
        )
      ),
      array (
        'name' => 'Passenger car and minibus rental and hiring',
        'code' => '77410',
        'cover_plus' => '0.75',
        'cover_plus_extra' => '1.01',
        'activities' => array (
          array (
            'name' => 'Car rental (without driver)',
            'bic_code' => array (
              'L661110',
              'L661120'
            )
          )
        )
      ),
      array (
        'name' => 'Motor vehicle and transport equipment rental and hiring (not elsewhere classified)',
        'code' => '77420',
        'cover_plus' => '1.17',
        'cover_plus_extra' => '1.56',
        'activities' => array (
          array (
            'name' => 'Aeroplane rental (without pilot)',
            'bic_code' => 'L661905'
          ),
          array (
            'name' => 'Boat hiring (without crew)',
            'bic_code' => 'L661910'
          ),
          array (
            'name' => 'Bus (except minibus) rental (without driver)',
            'bic_code' => 'L661915'
          ),
          array (
            'name' => 'Caravan rental',
            'bic_code' => 'L661920'
          ),
          array (
            'name' => 'Houseboat rental',
            'bic_code' => 'L661925'
          ),
          array (
            'name' => 'Motor cycle rental',
            'bic_code' => 'L661930'
          ),
          array (
            'name' => 'Motor home rental',
            'bic_code' => 'L661935'
          ),
          array (
            'name' => 'Railway stock leasing',
            'bic_code' => 'L661940'
          ),
          array (
            'name' => 'Ship rental (without crew)',
            'bic_code' => 'L661945'
          ),
          array (
            'name' => 'Shipping container rental',
            'bic_code' => 'L661950'
          ),
          array (
            'name' => 'Trailer rental',
            'bic_code' => 'L661955'
          ),
          array (
            'name' => 'Truck rental (without driver)',
            'bic_code' => 'L661960'
          )
        )
      ),
      array (
        'name' => 'Holder investor farms and livestock',
        'code' => '77301',
        'cover_plus' => '0.75',
        'cover_plus_extra' => '1.01',
        'activities' => array (
          array (
            'name' => 'Agricultural land renting or leasing',
            'bic_code' => 'L662065'
          ),
          array (
            'name' => 'Bloodstock leasing',
            'bic_code' => 'L662070'
          ),
          array (
            'name' => 'Dairy cattle leasing (for sharemilking purposes)',
            'bic_code' => 'L662075'
          ),
          array (
            'name' => 'Farm animal leasing',
            'bic_code' => 'L662080'
          ),
          array (
            'name' => 'Racehorse leasing',
            'bic_code' => 'L662085'
          ),
          array (
            'name' => 'Sharemilking (non-active participant)'
          )
        )
      ),
      array (
        'name' => 'Heavy machinery and scaffolding rental and hiring',
        'code' => '77430',
        'cover_plus' => '1.17',
        'cover_plus_extra' => '1.56',
        'activities' => array (
          array (
            'name' => 'Agricultural machinery rental (without operator)',
            'bic_code' => 'L663110'
          ),
          array (
            'name' => 'Construction machinery rental (without operator)',
            'bic_code' => 'L663120'
          ),
          array (
            'name' => 'Forklift hiring or leasing (without operator)',
            'bic_code' => 'L663130'
          ),
          array (
            'name' => 'Mining machinery rental (without operator',
            'bic_code' => 'L663947'
          ),
          array (
            'name' => 'Mobile platform rental'
          ),
          array (
            'name' => 'Scaffolding rental (except erection of scaffolding)'
          )
        )
      ),
      array (
        'name' => 'Video and other electronic media rental and hiring',
        'code' => '95110',
        'cover_plus' => '0.34',
        'cover_plus_extra' => '0.48',
        'activities' => array (
          array (
            'name' => 'Computer game rental',
            'bic_code' => 'L663210'
          ),
          array (
            'name' => 'Pre-recorded electronic media rental',
            'bic_code' => 'L663220'
          ),
          array (
            'name' => 'Pre-recorded video cassette rental'
          ),
          array (
            'name' => 'Pre-recorded video disc rental'
          ),
          array (
            'name' => 'Video game rental'
          )
        )
      ),
      array (
        'name' => 'Goods and equipment rental and hiring (not elsewhere classified)',
        'code' => '95190',
        'cover_plus' => '0.75',
        'cover_plus_extra' => '1.01',
        'activities' => array (
          array (
            'name' => 'Amusement machine, coin operated, rental and hiring',
            'bic_code' => 'L663903'
          ),
          array (
            'name' => 'Art work rental',
            'bic_code' => 'L663907'
          ),
          array (
            'name' => 'Bicycle rental',
            'bic_code' => 'L663910'
          ),
          array (
            'name' => 'Camping equipment rental',
            'bic_code' => 'L663913'
          ),
          array (
            'name' => 'Costume hire',
            'bic_code' => 'L663917'
          ),
          array (
            'name' => 'DIY equipment rental',
            'bic_code' => 'L663920'
          ),
          array (
            'name' => 'Electric and electronic appliance rental',
            'bic_code' => 'L663922'
          ),
          array (
            'name' => 'Function equipment renting, leasing or hiring',
            'bic_code' => 'L663923'
          ),
          array (
            'name' => 'Furniture rental',
            'bic_code' => 'L663925'
          ),
          array (
            'name' => 'Hand tool rental',
            'bic_code' => 'L663927'
          ),
          array (
            'name' => 'Office machinery rental',
            'bic_code' => 'L663930'
          ),
          array (
            'name' => 'Pot plant hire',
            'bic_code' => 'L663933'
          ),
          array (
            'name' => 'Sound reproducing equipment rental',
            'bic_code' => 'L663937'
          ),
          array (
            'name' => 'Sports and recreation equipment rental',
            'bic_code' => 'L663940'
          ),
          array (
            'name' => 'Suit hire',
            'bic_code' => 'L663943'
          ),
          array (
            'name' => 'Video recorder or player rental',
            'bic_code' => array (
              'L663950',
              'L663953',
              'L663957',
              'L663960',
              'L663963',
              'L663967',
              'L663970',
              'L663973'
            )
          )
        )
      ),
      array (
        'name' => 'Non-financial assets leasing and investment (including franchisors)',
        'code' => '77300',
        'cover_plus' => '0.24',
        'cover_plus_extra' => '0.34',
        'activities' => array (
          array (
            'name' => 'Brand-name leasing',
            'bic_code' => 'L664070'
          ),
          array (
            'name' => 'Fishing quota leasing',
            'bic_code' => 'L664075'
          ),
          array (
            'name' => 'Franchise agreement leasing',
            'bic_code' => 'L664010'
          ),
          array (
            'name' => 'Horticultural plant variety right leasing',
            'bic_code' => 'L664020'
          ),
          array (
            'name' => 'Mineral exploration right on-leasing',
            'bic_code' => 'L664030'
          ),
          array (
            'name' => 'Patent leasing',
            'bic_code' => 'L664040'
          ),
          array (
            'name' => 'Radio spectrum right leasing',
            'bic_code' => 'L664050'
          ),
          array (
            'name' => 'Taxi cab plate leasing',
            'bic_code' => 'L664060'
          ),
          array (
            'name' => 'Trademark leasing'
          )
        ),
      ),
      array (
        'name' => 'Residential property operators and developers (excluding construction)',
        'code' => '77110',
        'cover_plus' => '0.53',
        'cover_plus_extra' => '0.73',
        'activities' => array (
          array (
            'name' => 'Apartment (except holiday apartment) renting or leasing',
            'bic_code' => 'L671110'
          ),
          array (
            'name' => 'Caravan park, residential (other than holiday), operation',
            'bic_code' => 'L671120'
          ),
          array (
            'name' => 'Flats (except holiday flats) renting or leasing',
            'bic_code' => 'L671130'
          ),
          array (
            'name' => 'House (except holiday house) renting or leasing',
            'bic_code' => 'L671140'
          ),
          array (
            'name' => 'Residential property body corporate operation',
            'bic_code' => 'L671150'
          ),
          array (
            'name' => 'Residential property development (excluding construction)',
            'bic_code' => 'L671160'
          ),
          array (
            'name' => 'Residential property strata corporation operation',
            'bic_code' => array (
              'L671170',
              'L671180'
            )
          )
        )
      ),
      array (
        'name' => 'Non-residential property operators and developers (excluding construction)',
        'code' => '77120',
        'cover_plus' => '0.53',
        'cover_plus_extra' => '0.73',
        'activities' => array (
          array (
            'name' => 'Commercial or industrial property renting or leasing',
            'bic_code' => 'E321120'
          ),
          array (
            'name' => 'Commercial property body corporation',
            'bic_code' => 'L671205'
          ),
          array (
            'name' => 'Commercial property development (excluding construction)',
            'bic_code' => 'L671210'
          ),
          array (
            'name' => 'Commercial property strata corporation',
            'bic_code' => 'L671220'
          ),
          array (
            'name' => 'Factory renting or leasing',
            'bic_code' => 'L671230'
          ),
          array (
            'name' => 'Office space renting or leasing',
            'bic_code' => 'L671233'
          ),
          array (
            'name' => 'Self-storage renting or leasing',
            'bic_code' => 'L671237'
          ),
          array (
            'name' => 'Shopping centre renting or leasing',
            'bic_code' => 'L671240'
          ),
          array (
            'name' => 'Warehouse renting or leasing',
            'bic_code' => array (
              'L671250',
              'L671260'
            )
          )
        )
      ),
      array (
        'name' => 'Real estate services',
        'code' => '77200',
        'cover_plus' => '0.24',
        'cover_plus_extra' => '0.34',
        'activities' => array (
          array (
            'name' => 'Broking service (real estate)',
            'bic_code' => 'L672010'
          ),
          array (
            'name' => 'Real estate agency service',
            'bic_code' => 'L672020'
          ),
          array (
            'name' => 'Real estate auctioning service',
            'bic_code' => 'L672030'
          ),
          array (
            'name' => 'Real estate management service',
            'bic_code' => 'L672040'
          ),
          array (
            'name' => 'Real estate rental agency service',
            'bic_code' => 'L672050'
          ),
          array (
            'name' => 'Time share apartment management service',
            'bic_code' => 'L672060'
          ),
          array (
            'name' => 'Valuing service (real estate)',
            'bic_code' => array (
              'L672070',
              'L672080'
            )
          )
        )
      ),
      array (
        'name' => 'Scientific research services',
        'code' => '78100',
        'cover_plus' => '0.15',
        'cover_plus_extra' => '0.23',
        'activities' => array (
          array (
            'name' => 'Aeronautical research service',
            'bic_code' => 'M691005'
          ),
          array (
            'name' => 'Agricultural research service',
            'bic_code' => 'M691010'
          ),
          array (
            'name' => 'Biological research service',
            'bic_code' => 'M691015'
          ),
          array (
            'name' => 'Biotechnology research service',
            'bic_code' => 'M691020'
          ),
          array (
            'name' => 'Economic research service',
            'bic_code' => 'M691025'
          ),
          array (
            'name' => 'Food research service',
            'bic_code' => 'M691030'
          ),
          array (
            'name' => 'Industrial research service',
            'bic_code' => 'M691035'
          ),
          array (
            'name' => 'Medical research service',
            'bic_code' => 'M691040'
          ),
          array (
            'name' => 'Observatory research service',
            'bic_code' => 'M691045'
          ),
          array (
            'name' => 'Research farm operation',
            'bic_code' => 'M691050'
          ),
          array (
            'name' => 'Scientific research service',
            'bic_code' => 'M691055'
          ),
          array (
            'name' => 'Social science research service'
          ),
          array (
            'name' => 'Space tracking research station operation'
          )
        )
      ),
      array (
        'name' => 'Architectural services',
        'code' => '78210',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Architectural service',
            'bic_code' => 'M692110'
          ),
          array (
            'name' => 'Drafting service, architectural',
            'bic_code' => 'M692120'
          ),
          array (
            'name' => 'Landscape architectural service',
            'bic_code' => 'M692130'
          ),
          array (
            'name' => 'Town planning service',
            'bic_code' => array (
              'M692140',
              'M692150'
            )
          )
        )
      ),
      array (
        'name' => 'Surveying and mapping services',
        'code' => '78220',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Aerial surveying service',
            'bic_code' => 'M692210'
          ),
          array (
            'name' => 'Cadastral surveying service',
            'bic_code' => 'M692220'
          ),
          array (
            'name' => 'Engineering surveying service',
            'bic_code' => 'M692230'
          ),
          array (
            'name' => 'Geodetic surveying service',
            'bic_code' => 'M692240'
          ),
          array (
            'name' => 'Gravimetric surveying service',
            'bic_code' => 'M692250'
          ),
          array (
            'name' => 'Hydrographic surveying service',
            'bic_code' => 'M692260'
          ),
          array (
            'name' => 'Land surveying service',
            'bic_code' => 'M692270'
          ),
          array (
            'name' => 'Map preparation service',
            'bic_code' => 'M692280'
          ),
          array (
            'name' => 'Mining surveying service',
            'bic_code' => 'M692290'
          ),
          array (
            'name' => 'Oceanographic surveying service'
          ),
          array (
            'name' => 'Photogrammetry'
          ),
          array (
            'name' => 'Seismic surveying service'
          )
        )
      ),
      array (
        'name' => 'Engineering design and engineering consulting services',
        'code' => '78230',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Boat designing service ',
            'bic_code' => 'M692305'
          ),
          array (
            'name' => 'Building consulting service',
            'bic_code' => 'M692310'
          ),
          array (
            'name' => 'Building inspection service',
            'bic_code' => 'M692315'
          ),
          array (
            'name' => 'Chemical engineering consulting service',
            'bic_code' => 'M692320'
          ),
          array (
            'name' => 'Civil engineering consulting service',
            'bic_code' => 'M692325'
          ),
          array (
            'name' => 'Construction consulting service',
            'bic_code' => 'M692330'
          ),
          array (
            'name' => 'Construction project management service (on a fee or contract basis - except prime contractor)',
            'bic_code' => 'M692335'
          ),
          array (
            'name' => 'Drafting service, engineering',
            'bic_code' => 'M692340'
          ),
          array (
            'name' => 'Electrical engineering consulting service',
            'bic_code' => 'M692343'
          ),
          array (
            'name' => 'Electronic engineering consulting service',
            'bic_code' => 'M692345'
          ),
          array (
            'name' => 'Engineering consulting service (not elsewhere classified)',
            'bic_code' => 'M692347'
          ),
          array (
            'name' => 'Geotechnical engineering consulting service',
            'bic_code' => 'M692350'
          ),
          array (
            'name' => 'Hydraulic engineering consulting service',
            'bic_code' => 'M692355'
          ),
          array (
            'name' => 'Industrial design service',
            'bic_code' => 'M692360'
          ),
          array (
            'name' => 'Irrigation system design service',
            'bic_code' => 'M692365'
          ),
          array (
            'name' => 'Marine engineering consulting service',
            'bic_code' => 'M692370'
          ),
          array (
            'name' => 'Materials handling engineering consulting service',
            'bic_code' => 'M692375'
          ),
          array (
            'name' => 'Mechanical engineering consulting service'
          ),
          array (
            'name' => 'Mining engineering consulting service'
          ),
          array (
            'name' => 'Naval architecture service'
          ),
          array (
            'name' => 'Pipeline engineering consulting service'
          ),
          array (
            'name' => 'Process engineering consulting service'
          ),
          array (
            'name' => 'Product design service (for furniture, fittings, machinery or equipment)'
          ),
          array (
            'name' => 'Quantity surveying service'
          ),
          array (
            'name' => 'Sanitary engineering consulting service'
          ),
          array (
            'name' => 'Traffic engineering consulting service'
          )
        )
      ),
      array (
        'name' => 'Specialised design services (not elsewhere classified)',
        'code' => '78520',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Commercial art service',
            'bic_code' => 'C259930'
          ),
          array (
            'name' => 'Fashion design service',
            'bic_code' => 'M692410'
          ),
          array (
            'name' => 'Graphic design service',
            'bic_code' => 'M692420'
          ),
          array (
            'name' => 'Interior design service',
            'bic_code' => 'M692430'
          ),
          array (
            'name' => 'Jewellery design service',
            'bic_code' => 'M692435'
          ),
          array (
            'name' => 'Rubber stamp manufacturing',
            'bic_code' => 'M692440'
          ),
          array (
            'name' => 'Signwriting',
            'bic_code' => 'M692450'
          ),
          array (
            'name' => 'Textile design service',
            'bic_code' => 'M692460'
          ),
          array (
            'name' => 'Ticket writing',
            'bic_code' => 'M692470'
          ),
          array (
            'name' => 'Window dressing service',
            'bic_code' => 'M692480'
          )
        )
      ),
      array (
        'name' => 'Scientific testing and analysis services',
        'code' => '78290',
        'cover_plus' => '0.42',
        'cover_plus_extra' => '0.58',
        'activities' => array (
          array (
            'name' => 'Chemical analysis service (not elsewhere classified)',
            'bic_code' => 'M692505'
          ),
          array (
            'name' => 'Forensic science service (except pathology service)',
            'bic_code' => 'M692510'
          ),
          array (
            'name' => 'Geology and geophysical testing service',
            'bic_code' => 'M692515'
          ),
          array (
            'name' => 'Laboratory operation (providing chemical, food, electrical engineering or other technical services)',
            'bic_code' => 'M692520'
          ),
          array (
            'name' => 'Materials strength testing service',
            'bic_code' => 'M692525'
          ),
          array (
            'name' => 'Non-destructive testing service',
            'bic_code' => 'M692530'
          ),
          array (
            'name' => 'Pollution monitoring service',
            'bic_code' => 'M692535'
          ),
          array (
            'name' => 'Seismic survey data analysis service',
            'bic_code' => 'M692540'
          ),
          array (
            'name' => 'Testing or assay service on fee or contract',
            'bic_code' => 'M692545'
          ),
          array (
            'name' => 'Wine testing',
            'bic_code' => 'M692550'
          ),
          array (
            'name' => 'Wool testing service',
            'bic_code' => 'M692555'
          )
        )
      ),
      array (
        'name' => 'Legal services',
        'code' => '78410',
        'cover_plus' => '0.07',
        'cover_plus_extra' => '0.13',
        'activities' => array (
          array (
            'name' => 'Advocate service',
            'bic_code' => 'M693105'
          ),
          array (
            'name' => 'Barrister service',
            'bic_code' => 'M693110'
          ),
          array (
            'name' => 'Conveyancing service',
            'bic_code' => 'M693115'
          ),
          array (
            'name' => 'Legal aid service',
            'bic_code' => 'M693120'
          ),
          array (
            'name' => 'Legal service',
            'bic_code' => 'M693125'
          ),
          array (
            'name' => 'Notary service',
            'bic_code' => 'M693130'
          ),
          array (
            'name' => 'Patent attorney service',
            'bic_code' => 'M693135'
          ),
          array (
            'name' => 'Solicitor service',
            'bic_code' => 'M693140'
          ),
          array (
            'name' => 'Title-searching service',
            'bic_code' => array (
              'M693145',
              'M693150'
            )
          )
        )
      ),
      array (
        'name' => 'Accounting services',
        'code' => '78420',
        'cover_plus' => '0.07',
        'cover_plus_extra' => '0.13',
        'activities' => array (
          array (
            'name' => 'Accounting service',
            'bic_code' => 'M693210'
          ),
          array (
            'name' => 'Auditing service',
            'bic_code' => 'M693220'
          ),
          array (
            'name' => 'Bookkeeping service',
            'bic_code' => 'M693230'
          ),
          array (
            'name' => 'Tax agent service',
            'bic_code' => array (
              'M693240',
              'M693250'
            )
          )
        )
      ),
      array (
        'name' => 'Advertising services',
        'code' => '78510',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Advertising agency service',
            'bic_code' => 'M694010'
          ),
          array (
            'name' => 'Advertising material preparation service',
            'bic_code' => 'M694020'
          ),
          array (
            'name' => 'Advertising placement service',
            'bic_code' => 'M694030'
          ),
          array (
            'name' => 'Advertising service (except the sale of advertising space in own publication or broadcast)',
            'bic_code' => 'M694040'
          ),
          array (
            'name' => 'Advertising space selling (on a commission or fee basis)',
            'bic_code' => 'M694045'
          ),
          array (
            'name' => 'Aerial advertising service',
            'bic_code' => 'M694060'
          ),
          array (
            'name' => 'Direct mail advertising service'
          ),
          array (
            'name' => 'Sample distribution service'
          )
        )
      ),
      array (
        'name' => 'Market research and statistical services',
        'code' => '78530',
        'cover_plus' => '0.08',
        'cover_plus_extra' => '0.14',
        'activities' => array (
          array (
            'name' => 'Market research service',
            'bic_code' => 'M695010'
          ),
          array (
            'name' => 'Public opinion research service',
            'bic_code' => 'M695020'
          ),
          array (
            'name' => 'Statistical bureau operation'
          ),
          array (
            'name' => 'Statistical consulting service'
          )
        )
      ),
      array (
        'name' => 'Corporate head office management services',
        'code' => '78560',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Corporate head office management',
            'bic_code' => 'M696110'
          )
        )
      ),
      array (
        'name' => 'Management services and related consulting services',
        'code' => '78550',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Agricultural management consulting service',
            'bic_code' => 'M696205'
          ),
          array (
            'name' => 'Business management service',
            'bic_code' => 'M696210'
          ),
          array (
            'name' => 'Directorships in companies',
            'bic_code' => 'M696220'
          ),
          array (
            'name' => 'Efficiency advisory service',
            'bic_code' => 'M696225'
          ),
          array (
            'name' => 'Elected or appointed representatives on statutory bodies',
            'bic_code' => 'M696230'
          ),
          array (
            'name' => 'Environmental consulting service',
            'bic_code' => 'M696235'
          ),
          array (
            'name' => 'Forestry management consulting service',
            'bic_code' => 'M696240'
          ),
          array (
            'name' => 'Management consulting service',
            'bic_code' => 'M696245'
          ),
          array (
            'name' => 'Merchandising consulting service',
            'bic_code' => 'M696250'
          ),
          array (
            'name' => 'Operations research service (commercial)',
            'bic_code' => 'M696252'
          ),
          array (
            'name' => 'Personnel management consulting service',
            'bic_code' => 'M696255'
          ),
          array (
            'name' => 'Sales advisory service',
            'bic_code' => 'M696260'
          ),
          array (
            'name' => 'Tariff consulting service',
            'bic_code' => 'M696265'
          ),
          array (
            'name' => 'Tourism development consulting service',
            'bic_code' => array (
              'M696270',
              'M696272',
              'M696275',
              'M696280',
              'M696285',
              'M696290',
              'M696295',
              'M696296',
              'M696297',
              'M699940'
            )
          )
        )
      ),
      array (
        'name' => 'Veterinary services',
        'code' => '86400',
        'cover_plus' => '0.42',
        'cover_plus_extra' => '0.58',
        'activities' => array (
          array (
            'name' => 'Animal clinic operation',
            'bic_code' => 'M697010'
          ),
          array (
            'name' => 'Animal hospital operation',
            'bic_code' => 'M697020'
          ),
          array (
            'name' => 'Animal pathology service',
            'bic_code' => 'M697030'
          ),
          array (
            'name' => 'Animal quarantine station operation'
          ),
          array (
            'name' => 'Spaying service'
          ),
          array (
            'name' => 'Veterinary clinic operation'
          ),
          array (
            'name' => 'Veterinary service'
          )
        )
      ),
      array (
        'name' => 'Professional photographic services',
        'code' => '95230',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Portrait photography service',
            'bic_code' => 'M699110'
          ),
          array (
            'name' => 'Professional photography service',
            'bic_code' => 'M699120'
          ),
          array (
            'name' => 'Street photography service',
            'bic_code' => 'M699130'
          ),
          array (
            'name' => 'Studio photography service',
            'bic_code' => 'M699140'
          ),
          array (
            'name' => 'Video filming of special events (e.g. birthdays,weddings)',
            'bic_code' => 'M699150'
          ),
          array (
            'name' => 'Wedding photography service',
            'bic_code' => 'M699160'
          )
        )
      ),
      array (
        'name' => 'Professional, scientific, and technical services (not elsewhere classified)',
        'code' => '78291',
        'cover_plus' => '0.42',
        'cover_plus_extra' => '0.58',
        'activities' => array (
          array (
            'name' => 'Interpretation service',
            'bic_code' => 'M699910'
          ),
          array (
            'name' => 'Mediation service - except court or personal relationship',
            'bic_code' => 'M699920'
          ),
          array (
            'name' => 'Meteorological service',
            'bic_code' => 'M699930'
          ),
          array (
            'name' => 'Professional, scientific and technical services (not elsewhere classified)',
            'bic_code' => 'M699945'
          ),
          array (
            'name' => 'Weather station operation',
            'bic_code' => 'M699960'
          ),
          array (
            'name' => 'Valuation service - except for real estate or securities',
            'bic_code' => 'M699970'
          ),
          array (
            'name' => 'Wool or livestock stocktaking service',
            'bic_code' => 'M699975'
          )
        )
      ),
      array (
        'name' => 'Computer systems design and related services',
        'code' => '78340',
        'cover_plus' => '0.06',
        'cover_plus_extra' => '0.12',
        'activities' => array (
          array (
            'name' => 'Computer hardware consulting service',
            'bic_code' => 'M700010'
          ),
          array (
            'name' => 'Computer programming service',
            'bic_code' => 'M700020'
          ),
          array (
            'name' => 'Computer software consulting service',
            'bic_code' => 'M700030'
          ),
          array (
            'name' => 'Internet and web design consulting service',
            'bic_code' => 'M700040'
          ),
          array (
            'name' => 'Software development (customised) service (except publishing)',
            'bic_code' => 'M700050'
          ),
          array (
            'name' => 'Software installation service',
            'bic_code' => 'M700060'
          ),
          array (
            'name' => 'Systems analysis service'
          )
        )
      ),
      array (
        'name' => 'Employment placement and recruitment services (no on-hired staff)',
        'code' => '78610',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Artist, entertainer or other public figures management service',
            'bic_code' => 'M696215'
          ),
          array (
            'name' => 'Casting agency operation',
            'bic_code' => 'N721110'
          ),
          array (
            'name' => 'Employment agency operation',
            'bic_code' => 'N721120'
          ),
          array (
            'name' => 'Employment recruitment service',
            'bic_code' => 'N721130'
          ),
          array (
            'name' => 'Employment registry operation',
            'bic_code' => 'N721140'
          ),
          array (
            'name' => 'Executive placement service',
            'bic_code' => 'N721150'
          ),
          array (
            'name' => 'Outplacement service',
            'bic_code' => 'N721160'
          )
        )
      ),
      array (
        'name' => 'Labour supply services (on-hired staff office workers only)',
        'code' => '78620',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Temporary staff – office workers only',
            'bic_code' => array (
              'N721230',
              'N721260'
            )
          )
        )
      ),
      array (
        'name' => 'Labour supply services (on-hired staff non-office work including up to 30% office work) (not elsewhere classified)',
        'code' => '78621',
        'cover_plus' => '1.36',
        'cover_plus_extra' => '1.81',
        'activities' => array (
          array (
            'name' => 'Temporary labour hire – non-office work (including up to 30% office work)',
            'bic_code' => array (
              'N721220',
              'N721250'
            )
          )
        )
      ),
      array (
        'name' => 'Labour supply services (on-hired staff both office and non-office work minimum 30% office work)',
        'code' => '78622',
        'cover_plus' => '0.80',
        'cover_plus_extra' => '1.08',
        'bic_code' => 'N721210'
      ),
      array (
        'name' => 'Labour supply services (nursing, medical, and dental)',
        'code' => '86131',
        'cover_plus' => '1.00',
        'cover_plus_extra' => '1.34',
        'activities' => array (
          array (
            'name' => 'Nursing bureau',
            'bic_code' => 'N721240'
          ),
          array (
            'name' => 'Nursing service (own account)',
            'bic_code' => 'N721245'
          ),
          array (
            'name' => 'On-hired staff - nursing, medical, or dental',
            'bic_code' => array (
              'Q853960',
              'Q853965'
            )
          )
        )
      ),
      array (
        'name' => 'Travel agency and tour arrangement services',
        'code' => '66410',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Arranging and assembling tours',
            'bic_code' => 'N722010'
          ),
          array (
            'name' => 'Booking service (accommodation)',
            'bic_code' => 'N722020'
          ),
          array (
            'name' => 'Booking service (travel)',
            'bic_code' => 'N722030'
          ),
          array (
            'name' => 'Inbound tour operator service',
            'bic_code' => 'N722035'
          ),
          array (
            'name' => 'Tour arrangement service',
            'bic_code' => 'N722040'
          ),
          array (
            'name' => 'Tour guide (excluding outdoor pursuits)',
            'bic_code' => 'N722050'
          ),
          array (
            'name' => 'Tour operator service (arranging and assembling tours)',
            'bic_code' => 'N729990'
          ),
          array (
            'name' => 'Tour retailing service',
            'bic_code' => 'N722060'
          ),
          array (
            'name' => 'Tour wholesaling service'
          ),
          array (
            'name' => 'Travel agency operation'
          ),
          array (
            'name' => 'Travel ticket agency operation'
          )
        )
      ),
      array (
        'name' => 'Office administrative services',
        'code' => '78540',
        'cover_plus' => '0.28',
        'cover_plus_extra' => '0.39',
        'activities' => array (
          array (
            'name' => 'Billing and record-keeping service',
            'bic_code' => 'N729110'
          ),
          array (
            'name' => 'Business administrative service',
            'bic_code' => 'N729115'
          ),
          array (
            'name' => 'Clerical service',
            'bic_code' => 'N729120'
          ),
          array (
            'name' => 'Manual data entry service'
          ),
          array (
            'name' => 'Office administrative service (not elsewhere classified)'
          ),
          array (
            'name' => 'Payroll processing'
          ),
          array (
            'name' => 'Reception service'
          )
        )
      ),
      array (
        'name' => 'Document preparation services',
        'code' => '78630',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Desktop publishing (document preparation service)',
            'bic_code' => 'N729210'
          ),
          array (
            'name' => 'Document editing or proofreading service',
            'bic_code' => 'N729215'
          ),
          array (
            'name' => 'Letter writing service',
            'bic_code' => 'N729220'
          ),
          array (
            'name' => 'Resume writing service',
            'bic_code' => 'N729230'
          ),
          array (
            'name' => 'Stenographic service',
            'bic_code' => 'M699950'
          ),
          array (
            'name' => 'Transcription service'
          ),
          array (
            'name' => 'Translation service'
          ),
          array (
            'name' => 'Typing service'
          ),
          array (
            'name' => 'Word processing service'
          )
        )
      ),
      array (
        'name' => 'Credit reporting and debt collection services',
        'code' => '78691',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Account collection service',
            'bic_code' => 'N729310'
          ),
          array (
            'name' => 'Bill collection service',
            'bic_code' => 'N729320'
          ),
          array (
            'name' => 'Collection agency operation'
          ),
          array (
            'name' => 'Consumer credit reporting service'
          ),
          array (
            'name' => 'Credit bureau or agency operation'
          ),
          array (
            'name' => 'Credit investigation service'
          ),
          array (
            'name' => 'Credit rating service'
          ),
          array (
            'name' => 'Debt collection service'
          ),
          array (
            'name' => 'Mercantile credit reporting service'
          ),
          array (
            'name' => 'Repossession service'
          )
        )
      ),
      array (
        'name' => 'Call centre operation',
        'code' => '78692',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Alarm monitoring service',
            'bic_code' => 'G431080'
          ),
          array (
            'name' => 'Security alarm monitoring service',
            'bic_code' => 'N729410'
          ),
          array (
            'name' => 'Telemarketing service',
            'bic_code' => 'N729420'
          ),
          array (
            'name' => 'Telephone answering service',
            'bic_code' => 'O771205'
          ),
          array (
            'name' => 'Telephone call centre operation',
            'bic_code' => 'O771250'
          ),
          array (
            'name' => 'Voice mailbox service'
          )
        )
      ),
      array (
        'name' => 'Administrative services (not elsewhere classified)',
        'code' => '78693',
        'cover_plus' => '0.28',
        'cover_plus_extra' => '0.39',
        'activities' => array (
          array (
            'name' => 'Administrative service (not elsewhere classified)',
            'bic_code' => 'N729905'
          ),
          array (
            'name' => 'Event management service',
            'bic_code' => 'N729910'
          ),
          array (
            'name' => 'Meter reading service (electricity, gas, or water)',
            'bic_code' => 'N729920'
          ),
          array (
            'name' => 'Sport, art and similar event promotion service (without facilities)',
            'bic_code' => 'N729930'
          ),
          array (
            'name' => 'Sports ticketing service',
            'bic_code' => 'N729940'
          ),
          array (
            'name' => 'Theatre and concert booking service',
            'bic_code' => array (
              'N729950',
              'N72998'
            )
          )
        )
      ),
      array (
        'name' => 'Cleaning services and facilities management (not elsewhere classified)',
        'code' => '78660',
        'cover_plus' => '1.32',
        'cover_plus_extra' => '1.75',
        'activities' => array (
          array (
            'name' => 'Bathroom/toilet cleaning',
            'bic_code' => 'N731105'
          ),
          array (
            'name' => 'Building exterior cleaning (except sand blasting or steam cleaning)',
            'bic_code' => 'N731110'
          ),
          array (
            'name' => 'Building interior cleaning',
            'bic_code' => 'N731115'
          ),
          array (
            'name' => 'Caretaker (mainly cleaning and maintenance)',
            'bic_code' => 'N731120'
          ),
          array (
            'name' => 'Chimney cleaning',
            'bic_code' => 'N731123'
          ),
          array (
            'name' => 'Duct cleaning',
            'bic_code' => 'N731125'
          ),
          array (
            'name' => 'Facilities management on a contract or fee basis (not elsewhere classified)',
            'bic_code' => 'N731130'
          ),
          array (
            'name' => 'Gutter cleaning',
            'bic_code' => 'N731135'
          ),
          array (
            'name' => 'House cleaning service',
            'bic_code' => 'N731140'
          ),
          array (
            'name' => 'Janitorial service (including transport equipment)',
            'bic_code' => 'N731145'
          ),
          array (
            'name' => 'Multi-trade inspection and maintenance services (excluding construction)',
            'bic_code' => 'N731150'
          ),
          array (
            'name' => 'Office cleaning service'
          ),
          array (
            'name' => 'Residential building cleaning'
          ),
          array (
            'name' => 'Restaurant vat and cooker cleaning service'
          ),
          array (
            'name' => 'Road sweeping'
          ),
          array (
            'name' => 'Street cleaning'
          ),
          array (
            'name' => 'Swimming pool cleaning'
          ),
          array (
            'name' => 'Telephone cleaning service'
          ),
          array (
            'name' => 'Transport equipment cleaning'
          ),
          array (
            'name' => 'Window cleaning'
          )
        )
      ),
      array (
        'name' => 'Pest control services (except agricultural and forestry)',
        'code' => '78650',
        'cover_plus' => '1.32',
        'cover_plus_extra' => '1.75',
        'activities' => array (
          array (
            'name' => 'Exterminating service (except agricultural and forestry)',
            'bic_code' => 'N731210'
          ),
          array (
            'name' => 'Fumigating service (except crop fumigating)',
            'bic_code' => 'N731220'
          ),
          array (
            'name' => 'Insect control service (except agricultural and forestry)'
          ),
          array (
            'name' => 'Pest control service (except agricultural and forestry)'
          ),
          array (
            'name' => 'Termite control service (except agricultural and forestry)'
          ),
          array (
            'name' => 'Weed control service (except agricultural and forestry)'
          )
        )
      ),
      array (
        'name' => 'Gardening and turf management services',
        'code' => '95250',
        'cover_plus' => '2.26',
        'cover_plus_extra' => '2.98',
        'activities' => array (
          array (
            'name' => 'Arboricultural service (tree surgeon)',
            'bic_code' => 'N731310'
          ),
          array (
            'name' => 'Garden maintenance service',
            'bic_code' => 'N731320'
          ),
          array (
            'name' => 'Gardening service',
            'bic_code' => 'N731330'
          ),
          array (
            'name' => 'Lawn care service (e.g. fertilising, seeding, spraying)',
            'bic_code' => 'N731340 '
          ),
          array (
            'name' => 'Lawn mowing service',
            'bic_code' => 'N731350'
          ),
          array (
            'name' => 'Maintenance of plants and shrubs in buildings',
            'bic_code' => 'N731360'
          ),
          array (
            'name' => 'Roadside and nature strip grass mowing/slashing service'
          )
        )
      ),
      array (
        'name' => 'Packaging services',
        'code' => '78670',
        'cover_plus' => '0.70',
        'cover_plus_extra' => '0.94',
        'activities' => array (
          array (
            'name' => 'Apparel and textile folding and packaging service',
            'bic_code' => 'N732010'
          ),
          array (
            'name' => 'Bottling drinking liquid',
            'bic_code' => 'N732020'
          ),
          array (
            'name' => 'Bottling or rebottling wine or spirits',
            'bic_code' => 'N732030'
          ),
          array (
            'name' => 'Bottling other liquid'
          ),
          array (
            'name' => 'Contract packing or filling (except fresh fruit, fresh vegetables, meat, poultry, fish or peat)'
          ),
          array (
            'name' => 'Crating service'
          ),
          array (
            'name' => 'Kit assembling and packaging service'
          ),
          array (
            'name' => 'Packing pharmaceutical and medical products'
          ),
          array (
            'name' => 'Shrink wrapping service'
          )
        )
      ),
      array (
        'name' => 'Central government administration (not elsewhere classified)',
        'code' => '81110',
        'cover_plus' => '0.12',
        'cover_plus_extra' => '0.19',
        'activities' => array (
          array (
            'name' => 'Administration, except justice and defence (central government)',
            'bic_code' => 'O751010'
          ),
          array (
            'name' => 'Crown entities and commissions operation (not elsewhere classified)',
            'bic_code' => 'O751020'
          ),
          array (
            'name' => 'Financial and economic management except banking (central government)',
            'bic_code' => 'O751030'
          ),
          array (
            'name' => 'Government funding agency',
            'bic_code' => 'O751040'
          ),
          array (
            'name' => 'Governor-General’s unit operation'
          ),
          array (
            'name' => 'Legislation enactment (central government)'
          ),
          array (
            'name' => 'Parliament operation (central government)'
          ),
          array (
            'name' => 'Policy formulation and administration (central government)'
          ),
          array (
            'name' => 'Statutory review authority'
          )
        )
      ),
      array (
        'name' => 'Local government administration (not elsewhere classified)',
        'code' => '81130',
        'cover_plus' => '0.21',
        'cover_plus_extra' => '0.30',
        'activities' => array (
          array (
            'name' => 'Animal control (local government)',
            'bic_code' => 'O753010'
          ),
          array (
            'name' => 'Beach inspection (local government)',
            'bic_code' => 'O753020'
          ),
          array (
            'name' => 'Building inspection (local government)'
          ),
          array (
            'name' => 'Environmental standards control (local government)'
          ),
          array (
            'name' => 'Financial and economic management except banking (local government)'
          ),
          array (
            'name' => 'Food and water standards control (local government)'
          ),
          array (
            'name' => 'Health inspection (local government)'
          ),
          array (
            'name' => 'Land transport planning (local government)'
          ),
          array (
            'name' => 'Local government administration (except justice)'
          ),
          array (
            'name' => 'Parking and traffic control (local government)'
          ),
          array (
            'name' => 'Policy formulation and administration (local government)'
          ),
          array (
            'name' => 'Regulation enactment and enforcement (local government)'
          ),
          array (
            'name' => 'Territorial strategic planning (local government)'
          )
        )
      ),
      array (
        'name' => 'Justice',
        'code' => '81200',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Arbitration court operation',
            'bic_code' => 'O754010'
          ),
          array (
            'name' => 'Bankruptcy court operation',
            'bic_code' => 'O754020'
          ),
          array (
            'name' => 'Children’s court operation',
            'bic_code' => 'O754030'
          ),
          array (
            'name' => 'Conciliation and arbitration commission operation'
          ),
          array (
            'name' => 'Employment court or tribunal operation'
          ),
          array (
            'name' => 'Industrial relations court operation'
          ),
          array (
            'name' => 'Judicial authority operation'
          ),
          array (
            'name' => 'Law court operation'
          ),
          array (
            'name' => 'Royal commission operation'
          ),
          array (
            'name' => 'Tribunal operation'
          )
        )
      ),
      array (
        'name' => 'Foreign government representation',
        'code' => '81300',
        'cover_plus' => '0.12',
        'cover_plus_extra' => '0.19',
        'activities' => array (
          array (
            'name' => 'Consulate operation (foreign government)',
            'bic_code' => 'O755210'
          ),
          array (
            'name' => 'Embassy operation (foreign government)',
            'bic_code' => 'O755220'
          ),
          array (
            'name' => 'High commission operation (foreign government)',
            'bic_code' => 'O755230'
          ),
          array (
            'name' => 'International government organisation (United Nations, World Trade Organisation, etc.) administration',
            'bic_code' => 'O755240'
          ),
          array (
            'name' => 'Legation operation (foreign government)'
          ),
          array (
            'name' => 'Trade commission operation (foreign government)'
          )
        )
      ),
      array (
        'name' => 'Defence',
        'code' => '82000',
        'cover_plus' => '0.97',
        'cover_plus_extra' => '1.29',
        'activities' => array (
          array (
            'name' => 'Armed forces unit operation (except manufacturing or educational)',
            'bic_code' => 'O760010'
          ),
          array (
            'name' => 'Civil defence operation (military)'
          ),
          array (
            'name' => 'Government administration (defence)'
          )
        )
      ),
      array (
        'name' => 'Police services',
        'code' => '96310',
        'cover_plus' => '0.60',
        'cover_plus_extra' => '0.82',
        'activities' => array (
          array (
            'name' => 'Police service operation',
            'bic_code' => 'O771110'
          ),
          array (
            'name' => 'Police station operation'
          ),
          array (
            'name' => 'Traffic policing activity'
          )
        )
      ),
      array (
        'name' => 'Investigation and security services',
        'code' => '78640',
        'cover_plus' => '0.63',
        'cover_plus_extra' => '0.85',
        'activities' => array (
          array (
            'name' => 'Armoured car service',
            'bic_code' => 'O771210'
          ),
          array (
            'name' => 'Body guard service',
            'bic_code' => 'O771215'
          ),
          array (
            'name' => 'Burglary protection service',
            'bic_code' => 'O771220'
          ),
          array (
            'name' => 'Caretaker (security, excluding cleaning and maintenance)',
            'bic_code' => 'O771225'
          ),
          array (
            'name' => 'Detective agency service',
            'bic_code' => 'O771230'
          ),
          array (
            'name' => 'Enquiry agency service',
            'bic_code' => 'O771235'
          ),
          array (
            'name' => 'Locksmith service',
            'bic_code' => 'O771240'
          ),
          array (
            'name' => 'Night watchman service',
            'bic_code' => 'O771245'
          ),
          array (
            'name' => 'Protection service'
          ),
          array (
            'name' => 'Security guard service'
          )
        )
      ),
      array (
        'name' => 'Fire protection and other emergency services (except police and ambulance services)',
        'code' => '96330',
        'cover_plus' => '1.03',
        'cover_plus_extra' => '1.38',
        'activities' => array (
          array (
            'name' => 'Airport fire service',
            'bic_code' => 'O771310'
          ),
          array (
            'name' => 'Civil defence operation (non-military)',
            'bic_code' => 'O771320'
          ),
          array (
            'name' => 'Emergency service (other than defence and police)',
            'bic_code' => 'O771330'
          ),
          array (
            'name' => 'Fire brigade service',
            'bic_code' => 'O771340'
          ),
          array (
            'name' => 'Fire fighting service',
            'bic_code' => 'O771350'
          ),
          array (
            'name' => 'Fire prevention service',
            'bic_code' => 'O771360'
          ),
          array (
            'name' => 'Forest fire fighting service'
          ),
          array (
            'name' => 'Rescue service'
          )
        )
      ),
      array (
        'name' => 'Correctional and detention services',
        'code' => '96320',
        'cover_plus' => '1.13',
        'cover_plus_extra' => '1.51',
        'activities' => array (
          array (
            'name' => 'Correctional centre operation',
            'bic_code' => 'O771410'
          ),
          array (
            'name' => 'Detention centre operation',
            'bic_code' => 'O771420'
          ),
          array (
            'name' => 'Gaol operation',
            'bic_code' => 'O771440'
          ),
          array (
            'name' => 'Juvenile detention centre operation',
            'bic_code' => 'O771450'
          ),
          array (
            'name' => 'Operating correctional facility on a contract or fee basis'
          ),
          array (
            'name' => 'Prison farm operation'
          ),
          array (
            'name' => 'Prison operation'
          ),
          array (
            'name' => 'Remand centre operation'
          )
        )
      ),
      array (
        'name' => 'Public order and safety services (not elsewhere classified)',
        'code' => '96360',
        'cover_plus' => '0.21',
        'cover_plus_extra' => '0.3',
        'activities' => array (
          array (
            'name' => 'Agencies empowered to preserve social and economic safety',
            'bic_code' => 'O771910'
          ),
          array (
            'name' => 'Coastwatch service',
            'bic_code' => 'O771920'
          ),
          array (
            'name' => 'Intelligence service operation',
            'bic_code' => 'O771120'
          ),
          array (
            'name' => 'Public order and safety service (not elsewhere classified)',
            'bic_code' => 'O771930'
          ),
          array (
            'name' => 'Surveillance of country borders (by land, sea and air)'
          ),
          array (
            'name' => 'Traffic management or control service (except police)'
          )
        )
      ),
      array (
        'name' => 'Regulatory services (licensing and inspection) (not elsewhere classified)',
        'code' => '96400',
        'cover_plus' => '0.21',
        'cover_plus_extra' => '0.30',
        'activities' => array (
          array (
            'name' => 'Consumer protection service',
            'bic_code' => 'O772007'
          ),
          array (
            'name' => 'Licensing and permit issuance',
            'bic_code' => 'O772012'
          ),
          array (
            'name' => 'Motor vehicle testing',
            'bic_code' => 'O772013'
          ),
          array (
            'name' => 'Regulating casino and other gambling',
            'bic_code' => 'O772017'
          ),
          array (
            'name' => 'Regulating food and agricultural standards',
            'bic_code' => 'O772020'
          ),
          array (
            'name' => 'Regulating qualification standards',
            'bic_code' => 'O772030'
          ),
          array (
            'name' => 'Weights and measures regulation'
          )
        )
      ),
      array (
        'name' => 'Meat and food inspection services',
        'code' => '21111',
        'cover_plus' => '0.63',
        'cover_plus_extra' => '0.85',
        'activities' => array (
          array (
            'name' => 'Meat inspection service',
            'bic_code' => 'O772003'
          ),
          array (
            'name' => 'Food inspection service',
            'bic_code' => 'O772010'
          )
        )
      ),
      array (
        'name' => 'Preschool education',
        'code' => '84100',
        'cover_plus' => '0.53',
        'cover_plus_extra' => '0.73',
        'activities' => array (
          array (
            'name' => 'Kindergarten, preschool, operation (except child minding centre)',
            'bic_code' => 'P801010'
          ),
          array (
            'name' => 'Playcentre operation (NZ only)',
            'bic_code' => 'P801020'
          ),
          array (
            'name' => 'Pre-school operation (except child minding centre)'
          )
        )
      ),
      array (
        'name' => 'Primary education',
        'code' => '84210',
        'cover_plus' => '0.25',
        'cover_plus_extra' => '0.36',
        'activities' => array (
          array (
            'name' => 'Boarding school operation (primary school; except combined primary/secondary',
            'bic_code' => 'P802105'
          ),
          array (
            'name' => 'Intermediate school operation (NZ only)',
            'bic_code' => 'P802110'
          ),
          array (
            'name' => 'Primary school operation (except combined primary/secondary school)',
            'bic_code' => 'P802120'
          )
        )
      ),
      array (
        'name' => 'Secondary education',
        'code' => '84220',
        'cover_plus' => '0.25',
        'cover_plus_extra' => '0.36',
        'activities' => array (
          array (
            'name' => 'Agricultural high school operation (except combined primary/secondary school',
            'bic_code' => 'P802210'
          ),
          array (
            'name' => 'Boarding school operation (secondary school education; except combined primary/secondary school',
            'bic_code' => 'P802220'
          ),
          array (
            'name' => 'Secondary college operation (except combined primary/secondary school)'
          ),
          array (
            'name' => 'Secondary school operation (except combined primary/ secondary school)'
          )
        )
      ),
      array (
        'name' => 'Commercial vehicle wholesaling',
        'code' => '46220',
        'cover_plus' => '0.83',
        'cover_plus_extra' => '1.12',
        'activities' => array (
          array (
            'name' => 'Bus, wholesaling',
            'bic_code' => 'F350210'
          ),
          array (
            'name' => 'Commercial vehicle wholesaling (not elsewhere classified)',
            'bic_code' => 'F350220'
          ),
          array (
            'name' => 'Truck wholesaling',
            'bic_code' => 'F350230'
          )
        )
      ),
      array (
        'name' => 'Combined primary and secondary education',
        'code' => '84230',
        'cover_plus' => '0.25',
        'cover_plus_extra' => '0.36',
        'activities' => array (
          array (
            'name' => 'Agricultural high school operation (combined primary/secondary school)',
            'bic_code' => 'P802310'
          ),
          array (
            'name' => 'Boarding school operation (combined primary/secondary school)',
            'bic_code' => 'P802320'
          ),
          array (
            'name' => 'District school operation (combined primary/secondary school)'
          ),
          array (
            'name' => 'Secondary college operation (combined primary/secondary school)'
          ),
          array (
            'name' => 'Secondary school operation (combined primary/secondary school)'
          )
        )
      ),
      array (
        'name' => 'Special-school education',
        'code' => '84240',
        'cover_plus' => '0.25',
        'cover_plus_extra' => '0.36',
        'activities' => array (
          array (
            'name' => 'Special school operation (for children with disabilities and special needs; not providing mainstream primary or secondary school education)',
            'bic_code' => array (
              'P802410',
              'P802420'
            )
          )
        )
      ),
      array (
        'name' => 'Technical and vocational education and training',
        'code' => '84320',
        'cover_plus' => '0.12',
        'cover_plus_extra' => '0.19',
        'activities' => array (
          array (
            'name' => 'Business college and school operation',
            'bic_code' => 'P810110'
          ),
          array (
            'name' => 'Information technology training centre operation',
            'bic_code' => 'P810120'
          ),
          array (
            'name' => 'Institute of technology operation',
            'bic_code' => 'P810140'
          ),
          array (
            'name' => 'Polytechnic operation',
            'bic_code' => 'P810150'
          ),
          array (
            'name' => 'Professional and management development training',
            'bic_code' => 'P810160'
          ),
          array (
            'name' => 'Secretarial training',
            'bic_code' => 'P810170'
          ),
          array (
            'name' => 'Technical and further education college operation'
          ),
          array (
            'name' => 'Technical college operation'
          ),
          array (
            'name' => 'Vocational computer training'
          ),
          array (
            'name' => 'Workplace training'
          )
        )
      ),
      array (
        'name' => 'Telecommunication goods wholesaling',
        'code' => '46160',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Cellular telephone wholesaling',
            'bic_code' => 'F349310'
          ),
          array (
            'name' => 'Communication equipment, wholesaling',
            'bic_code' => 'F349320'
          ),
          array (
            'name' => 'Facsimile (fax) machine wholesaling'
          ),
          array (
            'name' => 'Mobile phone accessory wholesaling'
          ),
          array (
            'name' => 'Mobile phone battery wholesaling'
          ),
          array (
            'name' => 'Mobile phone wholesaling'
          ),
          array (
            'name' => 'Modem wholesaling'
          ),
          array (
            'name' => 'Telecommunication equipment parts wholesaling'
          ),
          array (
            'name' => 'Telephone and electric cable ducting system wholesaling'
          ),
          array (
            'name' => 'Telephone or telegraph equipment wholesaling'
          ),
          array (
            'name' => 'Two-way radio equipment wholesaling'
          )
        )
      ),
      array (
        'name' => 'Higher education (undergraduate and postgraduate courses)',
        'code' => '84310',
        'cover_plus' => '0.12',
        'cover_plus_extra' => '0.19',
        'activities' => array (
          array (
            'name' => 'Colleges of education operation',
            'bic_code' => 'P810130'
          ),
          array (
            'name' => 'Postgraduate school, university operation',
            'bic_code' => 'P810210'
          ),
          array (
            'name' => 'Research school, university operation',
            'bic_code' => 'P810220'
          ),
          array (
            'name' => 'Specialist institute or college'
          ),
          array (
            'name' => 'Teachers’ college operation'
          ),
          array (
            'name' => 'Undergraduate school, university operation'
          ),
          array (
            'name' => 'University operation'
          )
        )
      ),
      array (
        'name' => 'Hire of construction machinery and cranes with operator',
        'code' => '42101',
        'cover_plus' => '2.26',
        'cover_plus_extra' => '2.98',
        'activities' => array (
          array (
            'name' => 'Construction machinery hiring with operator (except earthmoving equipment)',
            'bic_code' => 'E329205'
          ),
          array (
            'name' => 'Crane hiring with operator',
            'bic_code' => array (
              'E329210',
              'E329220'
            )
          )
        )
      ),
      array (
        'name' => 'Modern Apprenticeship Co-ordinators employing apprentices',
        'code' => '84330',
        'cover_plus' => '0.98',
        'cover_plus_extra' => '1.31',
        'activities' => array (
          array (
            'name' => 'Apprenticeship Co-ordinators within the Modern Apprenticeship scheme who employ apprentices',
            'bic_code' => 'P810180'
          )
        )
      ),
      array (
        'name' => 'Sports and physical recreation instruction',
        'code' => '84500',
        'cover_plus' => '0.98',
        'cover_plus_extra' => '1.31',
        'activities' => array (
          array (
            'name' => 'Aerobics instruction',
            'bic_code' => 'P821103'
          ),
          array (
            'name' => 'Cricket coaching',
            'bic_code' => 'P821109'
          ),
          array (
            'name' => 'Diving instruction',
            'bic_code' => 'P821110'
          ),
          array (
            'name' => 'Fishing guide service',
            'bic_code' => 'P821113'
          ),
          array (
            'name' => 'Fishing instruction service',
            'bic_code' => 'P821117'
          ),
          array (
            'name' => 'Football instruction',
            'bic_code' => 'P821120'
          ),
          array (
            'name' => 'Golf instruction',
            'bic_code' => 'P821123'
          ),
          array (
            'name' => 'Martial arts school operation',
            'bic_code' => 'P821127'
          ),
          array (
            'name' => 'Sailing school operation',
            'bic_code' => 'P821130'
          ),
          array (
            'name' => 'Ski and snowboarding instruction',
            'bic_code' => 'P821137'
          ),
          array (
            'name' => 'Sports coaching (not elsewhere classified)',
            'bic_code' => 'P821140'
          ),
          array (
            'name' => 'Swimming instruction',
            'bic_code' => 'P821143'
          ),
          array (
            'name' => 'Tennis coaching',
            'bic_code' => 'P821147'
          ),
          array (
            'name' => 'Yoga instruction service',
            'bic_code' => array (
              'P821150',
              'P821153',
              'P821157',
              'P821160',
              'P821163',
              'P821167',
              'P821175',
              'R913940',
              'R913945'
            )
          )
        )
      ),
      array (
        'name' => 'Arts education',
        'code' => '84600',
        'cover_plus' => '0.28',
        'cover_plus_extra' => '0.39',
        'activities' => array (
          array (
            'name' => 'Acting and drama school operation',
            'bic_code' => 'P821210'
          ),
          array (
            'name' => 'Circus school operation',
            'bic_code' => 'P821215'
          ),
          array (
            'name' => 'Dance and ballet school operation',
            'bic_code' => 'P821220'
          ),
          array (
            'name' => 'Dance teaching',
            'bic_code' => 'P821225'
          ),
          array (
            'name' => 'Music school operation',
            'bic_code' => 'P821230'
          ),
          array (
            'name' => 'Painting instruction',
            'bic_code' => 'P821240'
          ),
          array (
            'name' => 'Performing arts school operation',
            'bic_code' => 'P821250'
          ),
          array (
            'name' => 'Photography school operation'
          ),
          array (
            'name' => 'Sculpture instruction'
          )
        )
      ),
      array (
        'name' => 'Adult, community, and other education (not elsewhere classified)',
        'code' => '84400',
        'cover_plus' => '0.28',
        'cover_plus_extra' => '0.39',
        'activities' => array (
          array (
            'name' => 'Driving school operation',
            'bic_code' => 'P821905'
          ),
          array (
            'name' => 'Home computing and keyboard skill instruction',
            'bic_code' => 'P821910'
          ),
          array (
            'name' => 'Home economics and personal management instruction',
            'bic_code' => 'P821925'
          ),
          array (
            'name' => 'Instruction in diet, exercise and lifestyle factors',
            'bic_code' => 'P821930'
          ),
          array (
            'name' => 'Instruction through Universities of the Third Age and Schools for Seniors',
            'bic_code' => 'P821940'
          ),
          array (
            'name' => 'Parental education program operation',
            'bic_code' => 'P821950'
          ),
          array (
            'name' => 'Public speaking training'
          ),
          array (
            'name' => 'Social and interpersonal skill training'
          ),
          array (
            'name' => 'Study skill, career development and job search training'
          ),
          array (
            'name' => 'Survival skill training'
          ),
          array (
            'name' => 'Tutoring service'
          )
        )
      ),
      array (
        'name' => 'Educational support services',
        'code' => '84700',
        'cover_plus' => '0.17',
        'cover_plus_extra' => '0.25',
        'activities' => array (
          array (
            'name' => 'Curriculum development, educational',
            'bic_code' => 'P822010'
          ),
          array (
            'name' => 'Educational support services (not elsewhere classified)',
            'bic_code' => 'P822020'
          ),
          array (
            'name' => 'Test and exam development and evaluation,educational',
            'bic_code' => 'P822030'
          ),
          array (
            'name' => 'Test and exam service, educational',
            'bic_code' => 'P822040'
          )
        )
      ),
      array (
        'name' => 'Hospitals (except psychiatric hospitals)',
        'code' => '86110',
        'cover_plus' => '0.52',
        'cover_plus_extra' => '0.72',
        'activities' => array (
          array (
            'name' => 'Children’s hospital',
            'bic_code' => 'Q840120'
          ),
          array (
            'name' => 'Day hospital',
            'bic_code' => 'Q840130'
          ),
          array (
            'name' => 'Ear, nose and throat hospital',
            'bic_code' => 'Q860940'
          ),
          array (
            'name' => 'Eye hospital'
          ),
          array (
            'name' => 'General hospital'
          ),
          array (
            'name' => 'Hospice operation'
          ),
          array (
            'name' => 'Hospital (except psychiatric or veterinary hospitals)'
          ),
          array (
            'name' => 'Infectious diseases hospital (including human quarantine stations)'
          ),
          array (
            'name' => 'Maternity hospital'
          ),
          array (
            'name' => 'Obstetric hospital'
          ),
          array (
            'name' => 'Women’s hospital'
          )
        )
      ),
      array (
        'name' => 'Psychiatric hospitals and psychiatric services (not elsewhere classified)',
        'code' => '86120',
        'cover_plus' => '0.52',
        'cover_plus_extra' => '0.72',
        'activities' => array (
          array (
            'name' => 'Community mental health hostel',
            'bic_code' => 'Q840210'
          ),
          array (
            'name' => 'Psychiatric community rehabilitation services'
          ),
          array (
            'name' => 'Psycho-social rehabilitation services'
          ),
          array (
            'name' => 'Psychiatric support for independence services'
          ),
          array (
            'name' => 'Psychiatric hospital'
          ),
          array (
            'name' => 'Psychiatric services (not elsewhere classified)'
          )
        )
      ),
      array (
        'name' => 'General practice medical services',
        'code' => '86210',
        'cover_plus' => '0.08',
        'cover_plus_extra' => '0.14',
        'activities' => array (
          array (
            'name' => 'General medical practitioner service',
            'bic_code' => 'Q851110'
          ),
          array (
            'name' => 'General practice medical clinic service',
            'bic_code' => 'Q851120'
          )
        )
      ),
      array (
        'name' => 'Specialist medical services',
        'code' => '86220',
        'cover_plus' => '0.08',
        'cover_plus_extra' => '0.14',
        'activities' => array (
          array (
            'name' => 'Allergy specialist service',
            'bic_code' => 'Q851205'
          ),
          array (
            'name' => 'Anaesthetist service',
            'bic_code' => 'Q851210'
          ),
          array (
            'name' => 'Dermatology service',
            'bic_code' => 'Q851215'
          ),
          array (
            'name' => 'Ear, nose and throat specialist service',
            'bic_code' => 'Q851220'
          ),
          array (
            'name' => 'Gynaecology service',
            'bic_code' => 'Q851225'
          ),
          array (
            'name' => 'Hair transplant service (by registered medical practitioner)',
            'bic_code' => 'Q851230'
          ),
          array (
            'name' => 'Neurology service',
            'bic_code' => 'Q851235'
          ),
          array (
            'name' => 'Obstetrics service',
            'bic_code' => 'Q851240'
          ),
          array (
            'name' => 'Ophthalmology service',
            'bic_code' => 'Q851245'
          ),
          array (
            'name' => 'Orthopaedic specialist service',
            'bic_code' => 'Q851250'
          ),
          array (
            'name' => 'Paediatric service',
            'bic_code' => 'Q851255'
          ),
          array (
            'name' => 'Psychiatry service',
            'bic_code' => 'Q851260'
          ),
          array (
            'name' => 'Rheumatology service',
            'bic_code' => 'Q851265'
          ),
          array (
            'name' => 'Specialist medical clinic service',
            'bic_code' => 'Q851270'
          ),
          array (
            'name' => 'Specialist medical practitioner service (not elsewhere classified)',
            'bic_code' => 'Q851275'
          ),
          array (
            'name' => 'Specialist surgical service',
            'bic_code' => 'Q851280'
          ),
          array (
            'name' => 'Thoracic specialist service',
            'bic_code' => 'Q851285'
          ),
          array (
            'name' => 'Urology service',
            'bic_code' => array (
              'Q851290',
              'Q851295'
            )
          )
        )
      ),
      array (
        'name' => 'Pathology and diagnostic imaging services',
        'code' => '86310',
        'cover_plus' => '0.23',
        'cover_plus_extra' => '0.33',
        'activities' => array (
          array (
            'name' => 'Diagnostic imaging service',
            'bic_code' => 'Q852005'
          ),
          array (
            'name' => 'Medical laboratory service',
            'bic_code' => 'Q852010'
          ),
          array (
            'name' => 'Pathology laboratory service',
            'bic_code' => 'Q852020'
          ),
          array (
            'name' => 'Radiology clinic operation',
            'bic_code' => 'Q852030'
          ),
          array (
            'name' => 'X-ray clinic service',
            'bic_code' => array (
              'Q852040',
              'Q852050',
              'Q852060',
              'Q852070'
            )
          )
        )
      ),
      array (
        'name' => 'Dental services',
        'code' => '86230',
        'cover_plus' => '0.9',
        'cover_plus_extra' => '0.15',
        'activities' => array (
          array (
            'name' => 'Conservative dental service',
            'bic_code' => 'Q853110'
          ),
          array (
            'name' => 'Dental hospital (out-patient)',
            'bic_code' => 'Q853120'
          ),
          array (
            'name' => 'Dental practice service',
            'bic_code' => 'Q853130'
          ),
          array (
            'name' => 'Dental practitioner service',
            'bic_code' => 'Q853140'
          ),
          array (
            'name' => 'Dental surgery service',
            'bic_code' => 'Q853150'
          ),
          array (
            'name' => 'Endodontic service',
            'bic_code' => 'Q853160'
          ),
          array (
            'name' => 'Oral pathology service',
            'bic_code' => 'Q853170'
          ),
          array (
            'name' => 'Oral surgery service',
            'bic_code' => 'Q853180'
          ),
          array (
            'name' => 'Orthodontic service',
            'bic_code' => 'Q853190'
          ),
          array (
            'name' => 'Paedodontic service'
          ),
          array (
            'name' => 'Periodontic service'
          ),
          array (
            'name' => 'Prosthodontic service'
          )
        )
      ),
      array (
        'name' => 'Optometry and optical dispensing',
        'code' => '86320',
        'cover_plus' => '0.08',
        'cover_plus_extra' => '0.14',
        'activities' => array (
          array (
            'name' => 'Contact lens dispensing',
            'bic_code' => 'Q853210'
          ),
          array (
            'name' => 'Eye testing (optometrist)',
            'bic_code' => 'Q853220'
          ),
          array (
            'name' => 'Optical dispensing',
            'bic_code' => 'Q853230'
          ),
          array (
            'name' => 'Orthoptic service',
            'bic_code' => 'Q853240'
          ),
          array (
            'name' => 'Spectacles dispensing',
            'bic_code' => array (
              'Q853250',
              'Q853260'
            )
          )
        )
      ),
      array (
        'name' => 'Physiotherapy services',
        'code' => '86350',
        'cover_plus' => '0.23',
        'cover_plus_extra' => '0.33',
        'activities' => array (
          array (
            'name' => 'Physiotherapy service',
            'bic_code' => 'Q853310'
          )
        )
      ),
      array (
        'name' => 'Chiropractic and osteopathic services',
        'code' => '86360',
        'cover_plus' => '0.23',
        'cover_plus_extra' => '0.33',
        'activities' => array (
          array (
            'name' => 'Chiropractic service',
            'bic_code' => 'Q853410'
          ),
          array (
            'name' => 'Osteopathic service',
            'bic_code' => 'Q853420'
          )
        )
      ),
      array (
        'name' => 'Allied health services (not elsewhere classified)',
        'code' => '86390',
        'cover_plus' => '0.23',
        'cover_plus_extra' => '0.33',
        'activities' => array (
          array (
            'name' => 'Acupuncture service',
            'bic_code' => 'Q853905'
          ),
          array (
            'name' => 'Adoption service',
            'bic_code' => 'Q853906'
          ),
          array (
            'name' => 'Alcohol counselling service',
            'bic_code' => 'Q853907'
          ),
          array (
            'name' => 'Allied health service (not elsewhere classified)',
            'bic_code' => 'Q853910'
          ),
          array (
            'name' => 'Aromatherapy service',
            'bic_code' => 'Q853915'
          ),
          array (
            'name' => 'Audiology service',
            'bic_code' => 'Q853920'
          ),
          array (
            'name' => 'Clinical psychology service',
            'bic_code' => 'Q853925'
          ),
          array (
            'name' => 'Dental hygiene service',
            'bic_code' => 'Q853930'
          ),
          array (
            'name' => 'Dietitian service',
            'bic_code' => 'Q853935'
          ),
          array (
            'name' => 'Hearing aid dispensing',
            'bic_code' => 'Q853940'
          ),
          array (
            'name' => 'Herbalist service',
            'bic_code' => 'Q853945'
          ),
          array (
            'name' => 'Homoeopathic service',
            'bic_code' => 'Q853950'
          ),
          array (
            'name' => 'Hydropathic service',
            'bic_code' => 'Q853957'
          ),
          array (
            'name' => 'Marriage guidance service',
            'bic_code' => 'Q853967'
          ),
          array (
            'name' => 'Naturopathic service',
            'bic_code' => 'Q853970'
          ),
          array (
            'name' => 'Occupational therapy service',
            'bic_code' => 'Q853975'
          ),
          array (
            'name' => 'Podiatry service',
            'bic_code' => 'Q853980'
          ),
          array (
            'name' => 'Speech pathology service',
            'bic_code' => 'Q853985'
          ),
          array (
            'name' => 'Therapeutic massage service',
            'bic_code' => 'Q853990'
          ),
          array (
            'name' => 'Welfare counselling service',
            'bic_code' => array (
              'Q879005',
              'Q879015',
              'Q879025',
              'Q879030',
              'Q879055',
              'Q879075',
              'S951120',
              'S953430'
            )
          )
        )
      ),
      array (
        'name' => 'Midwifery services',
        'code' => '86132',
        'cover_plus' => '0.52',
        'cover_plus_extra' => '0.72',
        'activities' => array (
          array (
            'name' => 'Midwifery service',
            'bic_code' => 'Q853955'
          )
        )
      ),
      array (
        'name' => 'Ambulance services',
        'code' => '86330',
        'cover_plus' => '1.21',
        'cover_plus_extra' => '1.61',
        'activities' => array (
          array (
            'name' => 'Aerial ambulance service',
            'bic_code' => 'Q859110'
          ),
          array (
            'name' => 'Ambulance service',
            'bic_code' => 'Q859120'
          )
        )
      ),
      array (
        'name' => 'Health care services (not elsewhere classified)',
        'code' => '86391',
        'cover_plus' => '0.23',
        'cover_plus_extra' => '0.33',
        'activities' => array (
          array (
            'name' => 'Blood bank operation',
            'bic_code' => 'Q859910'
          ),
          array (
            'name' => 'Health assessment service',
            'bic_code' => 'Q859940'
          ),
          array (
            'name' => 'Health care service (not elsewhere classified)',
            'bic_code' => 'Q859950'
          ),
          array (
            'name' => 'Porter service (hospital)'
          )
        )
      ),
      array (
        'name' => 'Community health centre operation',
        'code' => '86340',
        'cover_plus' => '0.23',
        'cover_plus_extra' => '0.33',
        'activities' => array (
          array (
            'name' => 'Child health centre operation (non-residential)',
            'bic_code' => 'Q859920'
          ),
          array (
            'name' => 'Drug referral centre operation (non-residential)',
            'bic_code' => 'Q859930'
          )
        )
      ),
      array (
        'name' => 'Agricultural and construction machinery wholesaling',
        'code' => '46110',
        'cover_plus' => '0.83',
        'cover_plus_extra' => '1.12',
        'activities' => array (
          array (
            'name' => 'Agricultural implement wholesaling',
            'bic_code' => 'F341110'
          ),
          array (
            'name' => 'Agricultural machinery wholesaling',
            'bic_code' => 'F341120'
          ),
          array (
            'name' => 'Construction machinery or equipment wholesaling',
            'bic_code' => 'F341130'
          ),
          array (
            'name' => 'Earthmoving machinery wholesaling'
          ),
          array (
            'name' => 'Excavation machinery wholesaling'
          ),
          array (
            'name' => 'Grader wholesaling'
          ),
          array (
            'name' => 'Harvester wholesaling'
          ),
          array (
            'name' => 'Lawn mower wholesaling'
          ),
          array (
            'name' => 'Parts, agricultural or construction machinery, wholesaling'
          ),
          array (
            'name' => 'Tractor wholesaling'
          ),
          array (
            'name' => 'Windmill wholesaling'
          )
        )
      ),
      array (
        'name' => 'Aged care residential services',
        'code' => '86130',
        'cover_plus' => '1.21',
        'cover_plus_extra' => '1.61',
        'activities' => array (
          array (
            'name' => 'Accommodation for the aged operation',
            'bic_code' => 'Q840110'
          ),
          array (
            'name' => 'Aged care hostel operation',
            'bic_code' => 'Q860110'
          ),
          array (
            'name' => 'Nursing home operation',
            'bic_code' => 'Q860120'
          ),
          array (
            'name' => 'Residential care for the aged operation',
            'bic_code' => 'Q860130'
          )
        )
      ),
      array (
        'name' => 'Retirement village operation (without rest home or hospital facilities)',
        'code' => '87210',
        'cover_plus' => '1.21',
        'cover_plus_extra' => '1.61',
        'bic_code' => 'Q879070'
      ),
      array (
        'name' => 'Retirement village operation (with rest home or hospital facilities)',
        'code' => '87211',
        'cover_plus' => '1.21',
        'cover_plus_extra' => '1.61',
        'bic_code' => 'Q860140'
      ),
      array (
        'name' => 'Residential care services (not elsewhere classified)',
        'code' => '87220',
        'cover_plus' => '1.21',
        'cover_plus_extra' => '1.61',
        'activities' => array (
          array (
            'name' => 'Children’s home operation',
            'bic_code' => 'Q860910'
          ),
          array (
            'name' => 'Crisis care accommodation operation',
            'bic_code' => 'Q860920'
          ),
          array (
            'name' => 'Home for the disadvantaged operation (not elsewhere classified)',
            'bic_code' => 'Q860930'
          ),
          array (
            'name' => 'Respite residential care operation',
            'bic_code' => 'Q860950'
          )
        )
      ),
      array (
        'name' => 'Residential refuge operation',
        'code' => '87222',
        'cover_plus' => '0.23',
        'cover_plus_extra' => '0.33',
        'bic_code' => 'Q860960'
      ),
      array (
        'name' => 'Childcare services',
        'code' => '87100',
        'cover_plus' => '0.53',
        'cover_plus_extra' => '0.73',
        'activities' => array (
          array (
            'name' => 'Before and/or after school care service',
            'bic_code' => 'Q871010'
          ),
          array (
            'name' => 'Child care service',
            'bic_code' => 'Q871020'
          ),
          array (
            'name' => 'Childminding service'
          ),
          array (
            'name' => 'Children’s nursery operation (except preschool education)'
          ),
          array (
            'name' => 'Family day care service'
          )
        )
      ),
      array (
        'name' => 'Social assistance services (not elsewhere classified)',
        'code' => '87290',
        'cover_plus' => '1.00',
        'cover_plus_extra' => '1.34',
        'activities' => array (
          array (
            'name' => 'Adult day care centre operation',
            'bic_code' => 'Q879010'
          ),
          array (
            'name' => 'Aged care assistance service',
            'bic_code' => 'Q879020'
          ),
          array (
            'name' => 'Disabilities assistance service',
            'bic_code' => 'Q879035'
          ),
          array (
            'name' => 'Mediation for marriage or relationships',
            'bic_code' => 'Q879040'
          ),
          array (
            'name' => 'Operation of soup kitchen (including mobile)',
            'bic_code' => 'Q879043'
          ),
          array (
            'name' => 'Youth welfare service',
            'bic_code' => array (
              'Q879045',
              'Q879050',
              'Q879060',
              'Q879073',
              'Q879080'
            )
          )
        )
      ),
      array (
        'name' => 'Parole or probationary services',
        'code' => '87292',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Periodic detention centre operation',
            'bic_code' => array (
              'O771430',
              'Q879065'
            )
          )
        )
      ),
      array (
        'name' => 'Museum operation',
        'code' => '92200',
        'cover_plus' => '0.30',
        'cover_plus_extra' => '0.43',
        'activities' => array (
          array (
            'name' => 'Art gallery operation (except retail)',
            'bic_code' => 'R891010'
          ),
          array (
            'name' => 'Art museum operation (except retail)',
            'bic_code' => 'R891020'
          ),
          array (
            'name' => 'Historic or heritage place, site or house operation',
            'bic_code' => 'R891030'
          ),
          array (
            'name' => 'Museum operation (not elsewhere classified)'
          ),
          array (
            'name' => 'Natural history and science museum operation'
          ),
          array (
            'name' => 'Social history museum'
          ),
          array (
            'name' => 'Transport and maritime museum operation'
          ),
          array (
            'name' => 'War memorial museum operation'
          )
        )
      ),
      array (
        'name' => 'Zoological and botanic gardens operation',
        'code' => '92310',
        'cover_plus' => '1.12',
        'cover_plus_extra' => '1.49',
        'activities' => array (
          array (
            'name' => 'Aquarium operation',
            'bic_code' => 'R892110'
          ),
          array (
            'name' => 'Arboretum operation',
            'bic_code' => 'R892120'
          ),
          array (
            'name' => 'Aviary operation',
            'bic_code' => 'R892130'
          ),
          array (
            'name' => 'Botanical garden operation',
            'bic_code' => 'R892140'
          ),
          array (
            'name' => 'Herbarium operation',
            'bic_code' => 'R892220'
          ),
          array (
            'name' => 'Reptile park operation'
          ),
          array (
            'name' => 'Wildlife park or reserve operation (wildlife actively managed)'
          ),
          array (
            'name' => 'Zoological park or garden operation'
          )
        )
      ),
      array (
        'name' => 'Nature reserve and conservation park operation',
        'code' => '92390',
        'cover_plus' => '1.12',
        'cover_plus_extra' => '1.49',
        'activities' => array (
          array (
            'name' => 'Bird reserve operation',
            'bic_code' => 'R892210'
          ),
          array (
            'name' => 'Conservation park operation',
            'bic_code' => 'R892215'
          ),
          array (
            'name' => 'Fauna reserve operation (fauna not actively managed)',
            'bic_code' => 'R892230'
          ),
          array (
            'name' => 'Flora reserve operation',
            'bic_code' => 'R892240'
          ),
          array (
            'name' => 'National or regional/district park or reserve operation',
            'bic_code' => 'R892250'
          ),
          array (
            'name' => 'Tourist caves operation',
            'bic_code' => 'R892260'
          ),
          array (
            'name' => 'Wildlife park or reserve operation (wildlife not actively managed)'
          )
        )
      ),
      array (
        'name' => 'Performing arts operation',
        'code' => '92410',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Circus operation',
            'bic_code' => 'R900110'
          ),
          array (
            'name' => 'Dance and ballet company operation',
            'bic_code' => 'R900120'
          ),
          array (
            'name' => 'Musical comedy company operation',
            'bic_code' => 'R900130'
          ),
          array (
            'name' => 'Musical productions',
            'bic_code' => 'R900140'
          ),
          array (
            'name' => 'Opera company operation',
            'bic_code' => 'R900150'
          ),
          array (
            'name' => 'Orchestra operation',
            'bic_code' => 'R900165'
          ),
          array (
            'name' => 'Performing arts operation (not elsewhere classified)',
            'bic_code' => 'R900170'
          ),
          array (
            'name' => 'Theatre restaurant operation (mainly entertainment facility)'
          ),
          array (
            'name' => 'Theatrical company operation'
          )
        )
      ),
      array (
        'name' => 'Creative artists, musicians, writers, and performers',
        'code' => '92420',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Actor',
            'bic_code' => 'R900160'
          ),
          array (
            'name' => 'Artists',
            'bic_code' => 'R900203'
          ),
          array (
            'name' => 'Cartooning',
            'bic_code' => 'R900207'
          ),
          array (
            'name' => 'Choreography service',
            'bic_code' => 'R900210'
          ),
          array (
            'name' => 'Composing (including musical composition)',
            'bic_code' => 'R900213'
          ),
          array (
            'name' => 'Costume designing',
            'bic_code' => 'R900217'
          ),
          array (
            'name' => 'Creative arts service',
            'bic_code' => 'R900220'
          ),
          array (
            'name' => 'Entertainer',
            'bic_code' => 'R900223'
          ),
          array (
            'name' => 'Journalistic service',
            'bic_code' => 'R900227'
          ),
          array (
            'name' => 'Model (fashion)',
            'bic_code' => 'R900230'
          ),
          array (
            'name' => 'Musicians',
            'bic_code' => 'R900233'
          ),
          array (
            'name' => 'Playwriting or screenwriting',
            'bic_code' => 'R900235'
          ),
          array (
            'name' => 'Producing or directing original artistic or cultural work',
            'bic_code' => 'R900236'
          ),
          array (
            'name' => 'Sculpting',
            'bic_code' => 'R900237'
          ),
          array (
            'name' => 'Set designing service',
            'bic_code' => 'R900240'
          ),
          array (
            'name' => 'Speaking service (including radio and television announcing)',
            'bic_code' => 'R900243'
          ),
          array (
            'name' => 'Theatre lighting design service',
            'bic_code' => 'R900247'
          ),
          array (
            'name' => 'Writing (including poetry and comedy)',
            'bic_code' => array (
              'R900248',
              'R900250',
              'R900253',
              'R900257',
              'R900260',
              'R900263',
              'R900267'
            )
          )
        )
      ),
      array (
        'name' => 'Performing arts venue operation',
        'code' => '92520',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Concert hall operation',
            'bic_code' => 'R900310'
          ),
          array (
            'name' => 'Entertainment centre operation',
            'bic_code' => 'R900320'
          ),
          array (
            'name' => 'Music bowl operation',
            'bic_code' => 'R900330'
          ),
          array (
            'name' => 'Opera house operation',
            'bic_code' => 'R900340'
          ),
          array (
            'name' => 'Performing arts venue operation (not elsewhere classified)',
            'bic_code' => 'R900350'
          ),
          array (
            'name' => 'Playhouse operation',
            'bic_code' => 'R900360'
          ),
          array (
            'name' => 'Theatre operation (except motion picture theatre)',
            'bic_code' => 'R900370'
          )
        )
      ),
      array (
        'name' => 'Health and fitness centres and gymnasia operation',
        'code' => '93130',
        'cover_plus' => '0.76',
        'cover_plus_extra' => '1.03',
        'activities' => array (
          array (
            'name' => 'Fitness centre operation',
            'bic_code' => 'R911110'
          ),
          array (
            'name' => 'Gymnasia operation',
            'bic_code' => 'R911120'
          ),
          array (
            'name' => 'Health club operation'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation community rugby',
        'code' => '93170',
        'cover_plus' => '1.88',
        'cover_plus_extra' => '2.48',
        'activities' => array (
          array (
            'name' => 'Sports administration service – community rugby',
            'bic_code' => 'R911252'
          ),
          array (
            'name' => 'Sporting club or association – community rugby',
            'bic_code' => 'R911415'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation community rugby league',
        'code' => '93171',
        'cover_plus' => '0.76',
        'cover_plus_extra' => '1.03',
        'activities' => array (
          array (
            'name' => 'Sports administration service – community rugby league',
            'bic_code' => 'R911255'
          ),
          array (
            'name' => 'Sporting club or association – community rugby league',
            'bic_code' => 'R911420'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation community cricket',
        'code' => '93174',
        'cover_plus' => '2.58',
        'cover_plus_extra' => '3.39',
        'activities' => array (
          array (
            'name' => 'Sports administration service – community cricket',
            'bic_code' => 'R911250'
          ),
          array (
            'name' => 'Sporting club or association – community cricket',
            'bic_code' => 'R911410'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation professional sport (not elsewhere classified)',
        'code' => '93175',
        'cover_plus' => '5.58',
        'cover_plus_extra' => '7.31',
        'activities' => array (
          array (
            'name' => 'Professional sports playing (not elsewhere classified)',
            'bic_code' => 'R911232'
          ),
          array (
            'name' => 'Sports administration service - professional sport (not elsewhere classified)',
            'bic_code' => array (
              'R911275',
              'R911460'
            )
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation professional rugby',
        'code' => '93180',
        'cover_plus' => '5.58',
        'cover_plus_extra' => '7.31',
        'activities' => array (
          array (
            'name' => 'Professional rugby playing',
            'bic_code' => 'R911222'
          ),
          array (
            'name' => 'Sports administration service – professional rugby'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation professional rugby league',
        'code' => '93181',
        'cover_plus' => '5.58',
        'cover_plus_extra' => '7.31',
        'activities' => array (
          array (
            'name' => 'Professional rugby league playing',
            'bic_code' => 'R911225'
          ),
          array (
            'name' => 'Sports administration service – professional rugby league'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation snow skiing',
        'code' => '93182',
        'cover_plus' => '1.88',
        'cover_plus_extra' => '2.48',
        'activities' => array (
          array (
            'name' => 'Professional snow skiing or snowboarding',
            'bic_code' => 'R911228'
          ),
          array (
            'name' => 'Ski field operation',
            'bic_code' => 'R911340'
          ),
          array (
            'name' => 'Sporting club or association – snow skiing or snowboarding',
            'bic_code' => 'R911278'
          ),
          array (
            'name' => 'Sports administration service – snow skiing or snowboarding',
            'bic_code' => 'R911465'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation softball or baseball',
        'code' => '93184',
        'cover_plus' => '0.76',
        'cover_plus_extra' => '1.03',
        'activities' => array (
          array (
            'name' => 'Professional softball or baseball playing',
            'bic_code' => 'R911230'
          ),
          array (
            'name' => 'Sporting club or association – softball or baseball',
            'bic_code' => 'R911280'
          ),
          array (
            'name' => 'Sports administration service – softball or basebal',
            'bic_code' => 'R911470'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation squash or badminton',
        'code' => '93185',
        'cover_plus' => '0.76',
        'cover_plus_extra' => '1.02',
        'activities' => array (
          array (
            'name' => 'Professional squash or badminton playing',
            'bic_code' => 'R911235'
          ),
          array (
            'name' => 'Sporting club or association – squash or badminton',
            'bic_code' => 'R911282'
          ),
          array (
            'name' => 'Sports administration service – squash or badminton',
            'bic_code' => 'R911475'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation swimming',
        'code' => '93186',
        'cover_plus' => '0.76',
        'cover_plus_extra' => '1.03',
        'activities' => array (
          array (
            'name' => 'Professional swimming',
            'bic_code' => 'R911238'
          ),
          array (
            'name' => 'Sporting club or association – swimming',
            'bic_code' => 'R911285'
          ),
          array (
            'name' => 'Sports administration service – swimming',
            'bic_code' => 'R911480'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation tennis',
        'code' => '93187',
        'cover_plus' => '0.76',
        'cover_plus_extra' => '1.03',
        'activities' => array (
          array (
            'name' => 'Professional tennis playing',
            'bic_code' => 'R911240'
          ),
          array (
            'name' => 'Sporting club or association – tennis',
            'bic_code' => 'R911288'
          ),
          array (
            'name' => 'Sports administration service – tennis',
            'bic_code' => 'R911485'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation water skiing',
        'code' => '93188',
        'cover_plus' => '0.76',
        'cover_plus_extra' => '1.03',
        'activities' => array (
          array (
            'name' => 'Professional water skiing',
            'bic_code' => 'R911242'
          ),
          array (
            'name' => 'Sporting club or association – water skiing',
            'bic_code' => 'R911290'
          ),
          array (
            'name' => 'Sports administration service – water skiing',
            'bic_code' => 'R911490'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation community (not elsewhere classified)',
        'code' => '93190',
        'cover_plus' => '0.76',
        'cover_plus_extra' => '1.03',
        'activities' => array (
          array (
            'name' => 'Sporting club or association - community sport (not elsewhere classified)',
            'bic_code' => 'R911258'
          ),
          array (
            'name' => 'Sports administration service - community sport (not elsewhere classified)',
            'bic_code' => array (
              'R911425',
              'R911495'
            )
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation boating or yachting',
        'code' => '93192',
        'cover_plus' => '0.76',
        'cover_plus_extra' => '1.03',
        'activities' => array (
          array (
            'name' => 'Professional boating or yachting',
            'bic_code' => 'R911202'
          ),
          array (
            'name' => 'Sporting club or association – boating or yachting',
            'bic_code' => 'R911248'
          ),
          array (
            'name' => 'Sports administration service – boating or yachting',
            'bic_code' => 'R911405'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation cycling',
        'code' => '93193',
        'cover_plus' => '0.76',
        'cover_plus_extra' => '1.03',
        'activities' => array (
          array (
            'name' => 'Professional cycling',
            'bic_code' => 'R911208'
          ),
          array (
            'name' => 'Sporting club or association – cycling',
            'bic_code' => 'R911260'
          ),
          array (
            'name' => 'Sports administration service – cycling',
            'bic_code' => 'R911430'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation - professional cricket',
        'code' => '93194',
        'cover_plus' => '2.58',
        'cover_plus_extra' => '3.39',
        'activities' => array (
          array (
            'name' => 'Professional cricket playing',
            'bic_code' => 'R911205'
          ),
          array (
            'name' => 'Sports administration service – professional cricket'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation - golf',
        'code' => '93195',
        'cover_plus' => '0.76',
        'cover_plus_extra' => '1.03',
        'activities' => array (
          array (
            'name' => 'Professional golf playing',
            'bic_code' => 'R911210'
          ),
          array (
            'name' => 'Sporting club or association – golf',
            'bic_code' => 'R911262'
          ),
          array (
            'name' => 'Sports administration service – golf',
            'bic_code' => 'R911435'
          )
        )
      ),
      array (
        'name' => 'Sporting and recreational equine activities (not elsewhere classified)',
        'code' => '93196',
        'cover_plus' => '5.58',
        'cover_plus_extra' => '7.31',
        'activities' => array (
          array (
            'name' => 'Horse trekking operation',
            'bic_code' => 'P821107'
          ),
          array (
            'name' => 'Professional horse riding',
            'bic_code' => 'P821133'
          ),
          array (
            'name' => 'Riding school operation',
            'bic_code' => 'R913960'
          ),
          array (
            'name' => 'Sporting club or association – horse riding',
            'bic_code' => 'R911212'
          ),
          array (
            'name' => 'Sports administration service – horse riding',
            'bic_code' => 'R911265'
          ),
          array (
            'name' => 'Sports coaching service – horse riding',
            'bic_code' => 'R911440'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation - motor cycling',
        'code' => '93197',
        'cover_plus' => '5.58',
        'cover_plus_extra' => '7.31',
        'activities' => array (
          array (
            'name' => 'Professional motor cycling',
            'bic_code' => 'R911215'
          ),
          array (
            'name' => 'Sporting club or association – motor cycling',
            'bic_code' => 'R911268'
          ),
          array (
            'name' => 'Sports administration service – motor cycling',
            'bic_code' => 'R911445'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation - motor racing',
        'code' => '93198',
        'cover_plus' => '2.58',
        'cover_plus_extra' => '3.39',
        'activities' => array (
          array (
            'name' => 'Professional motor racing',
            'bic_code' => 'R911218'
          ),
          array (
            'name' => 'Sporting club or association – motor racing',
            'bic_code' => 'R911270'
          ),
          array (
            'name' => 'Sports administration service – motor racing',
            'bic_code' => 'R911450'
          )
        )
      ),
      array (
        'name' => 'Sport and physical recreation - netball',
        'code' => '93199',
        'cover_plus' => '1.88',
        'cover_plus_extra' => '2.48',
        'activities' => array (
          array (
            'name' => 'Professional netball playing',
            'bic_code' => 'R911220'

          ),
          array (
            'name' => 'Sporting club or association – netball',
            'bic_code' => 'R911272'
          ),
          array (
            'name' => 'Sports administration service – netball',
            'bic_code' => 'R911455'
          )
        )
      ),
      array (
        'name' => 'Sports and physical recreation venues, grounds, and facilities operation',
        'code' => '93120',
        'cover_plus' => '0.76',
        'cover_plus_extra' => '1.03',
        'activities' => array (
          array (
            'name' => 'Athletics field or stadium operation',
            'bic_code' => 'I521230'
          ),
          array (
            'name' => 'Basketball court or stadium operation',
            'bic_code' => 'R911305'
          ),
          array (
            'name' => 'Billiard, snooker or pool hall operation',
            'bic_code' => 'R911310'
          ),
          array (
            'name' => 'Bowling alley, tenpin, operation',
            'bic_code' => 'R911315'
          ),
          array (
            'name' => 'Bowling green operation',
            'bic_code' => 'R911320'
          ),
          array (
            'name' => 'Boxing stadium operation',
            'bic_code' => 'R911325'
          ),
          array (
            'name' => 'Cricket ground operation',
            'bic_code' => 'R911330'
          ),
          array (
            'name' => 'Football field or stadium operation',
            'bic_code' => 'R911335'
          ),
          array (
            'name' => 'Golf course or practice range operation',
            'bic_code' => 'R911345'
          ),
          array (
            'name' => 'Ice or roller skating rink operation',
            'bic_code' => 'R911350'
          ),
          array (
            'name' => 'Marina operation',
            'bic_code' => 'R911353'
          ),
          array (
            'name' => 'Motor racing track or speedway operation',
            'bic_code' => 'R911355'
          ),
          array (
            'name' => 'Netball court or stadium operation',
            'bic_code' => 'R911360'
          ),
          array (
            'name' => 'Sports ground, stadium or venue operation (not elsewhere classified)',
            'bic_code' => 'R911365'
          ),
          array (
            'name' => 'Squash court operation'
          ),
          array (
            'name' => 'Swimming pool operation'
          ),
          array (
            'name' => 'Tennis court operation'
          )
        )
      ),
      array (
        'name' => 'Horse and dog racing administration and track operation',
        'code' => '93111',
        'cover_plus' => '0.76',
        'cover_plus_extra' => '1.03',
        'activities' => array (
          array (
            'name' => 'Dog race course or track operation',
            'bic_code' => 'R912110'
          ),
          array (
            'name' => 'Horse race course or track operation',
            'bic_code' => 'R912120'
          ),
          array (
            'name' => 'Racing authority or board',
            'bic_code' => array (
              'R912130',
              'R912140'
            )
          )
        )
      ),
      array (
        'name' => 'Horse racing activities thoroughbred and other (not elsewhere classified)',
        'code' => '93110',
        'cover_plus' => '5.58',
        'cover_plus_extra' => '7.31',
        'activities' => array (
          array (
            'name' => 'Race horse training (not elsewhere classified)',
            'bic_code' => 'R912940'
          ),
          array (
            'name' => 'Racing stables operation (not elsewhere classified)',
            'bic_code' => array (
              'R912970',
              'R912980'
            )
          )
        )
      ),
      array (
        'name' => 'Horse racing activities thoroughbred racing jockeys',
        'code' => '93114',
        'cover_plus' => '5.58',
        'cover_plus_extra' => '7.31',
        'bic_code' => 'R911245'
      ),
      array (
        'name' => 'Dog racing activities',
        'code' => '93112',
        'cover_plus' => '1.88',
        'cover_plus_extra' => '2.48',
        'activities' => array (
          array (
            'name' => 'Dog training (for racing)',
            'bic_code' => 'R912910'
          ),
          array (
            'name' => 'Greyhound training',
            'bic_code' => 'R912920'
          ),
          array (
            'name' => 'Racing kennels operation'
          )
        )
      ),
      array (
        'name' => 'Horse racing activities - harness racing',
        'code' => '93113',
        'cover_plus' => '2.58',
        'cover_plus_extra' => '3.39',
        'activities' => array (
          array (
            'name' => 'Race horse training (harness racing)',
            'bic_code' => 'R912930'
          ),
          array (
            'name' => 'Racing stables operation (harness racing)',
            'bic_code' => 'R912960'
          )
        )
      ),
      array (
        'name' => 'Horticultural contracting and labour supply services',
        'code' => '02195',
        'cover_plus' => '1.06',
        'cover_plus_extra' => '1.42',
        'activities' => array (
          array (
            'name' => 'Fruit or vegetable picking',
            'bic_code' => 'A052957'
          ),
          array (
            'name' => 'Horticultural contracting',
            'bic_code' => 'A052963'
          ),
          array (
            'name' => 'Labour supply for horticulture',
            'bic_code' => 'A052965'
          )
        )
      ),
      array (
        'name' => 'Horse racing activities - harness racing drivers',
        'code' => '93115',
        'cover_plus' => '2.58',
        'cover_plus_extra' => '3.39',
        'bic_code' => 'R912950'
      ),
      array (
        'name' => 'Amusement parks and centres operation',
        'code' => '93300',
        'cover_plus' => '0.76',
        'cover_plus_extra' => '1.03',
        'activities' => array (
          array (
            'name' => 'Amusement arcade or centre operation',
            'bic_code' => 'R913105'
          ),
          array (
            'name' => 'Amusement machine or ride operation (including concession operators)',
            'bic_code' => 'R913110'
          ),
          array (
            'name' => 'Amusement park operation',
            'bic_code' => 'R913120'
          ),
          array (
            'name' => 'Flight or driving simulator operation - mainly amusement',
            'bic_code' => 'R913125'
          ),
          array (
            'name' => 'Go-kart venue operation',
            'bic_code' => 'R913130'
          ),
          array (
            'name' => 'Indoor climbing operation'
          ),
          array (
            'name' => 'Merry-go-round operation'
          ),
          array (
            'name' => 'Mini-golf centre operation'
          ),
          array (
            'name' => 'Theme park operation'
          ),
          array (
            'name' => 'Water park operation'
          )
        )
      ),
      array (
        'name' => 'Amusement and other recreation activities (not elsewhere classified)',
        'code' => '93400',
        'cover_plus' => '0.76',
        'cover_plus_extra' => '1.03',
        'activities' => array (
          array (
            'name' => 'Adventure sea-diving operation',
            'bic_code' => 'R913905'
          ),
          array (
            'name' => 'Amusement activity (not elsewhere classified)',
            'bic_code' => 'R913910'
          ),
          array (
            'name' => 'Black water rafting',
            'bic_code' => 'R913915'
          ),
          array (
            'name' => 'Bungy jumping operation',
            'bic_code' => 'R913920'
          ),
          array (
            'name' => 'Bush walking operation',
            'bic_code' => 'R913930'
          ),
          array (
            'name' => 'Cave diving operation',
            'bic_code' => 'R913950'
          ),
          array (
            'name' => 'Dive boat tours',
            'bic_code' => 'R913973'
          ),
          array (
            'name' => 'Guided ATV adventure operation',
            'bic_code' => 'P821170'
          ),
          array (
            'name' => 'Outdoor adventure operation (not elsewhere classified)',
            'bic_code' => 'R913977'
          ),
          array (
            'name' => 'Outdoor education (not elsewhere classified)'
          ),
          array (
            'name' => 'Recreational activity (not elsewhere classified)'
          )
        )
      ),
      array (
        'name' => 'Alpine and white water recreation activities',
        'code' => '93410',
        'cover_plus' => '2.58',
        'cover_plus_extra' => '3.39',
        'activities' => array (
          array (
            'name' => 'Mountain guide service',
            'bic_code' => 'R913990'
          ),
          array (
            'name' => 'White water rafting operation',
            'bic_code' => array (
              'R913970',
              'R913980',
              'R913985'
            )
          )
        )
      ),
      array (
        'name' => 'Casino operation',
        'code' => '93220',
        'cover_plus' => '0.51',
        'cover_plus_extra' => '0.69',
        'activities' => array (
          array (
            'name' => 'Casino operation',
            'bic_code' => 'R920110'
          )
        )
      ),
      array (
        'name' => 'Lottery operation',
        'code' => '93210',
        'cover_plus' => '0.30',
        'cover_plus_extra' => '0.43',
        'activities' => array (
          array (
            'name' => 'Art union operation',
            'bic_code' => 'R920210'
          ),
          array (
            'name' => 'Football pool operation',
            'bic_code' => 'R920220'
          ),
          array (
            'name' => 'Keno operation',
            'bic_code' => 'R920230'
          ),
          array (
            'name' => 'Lottery agency operation'
          ),
          array (
            'name' => 'Lottery operation'
          )
        )
      ),
      array (
        'name' => 'Gambling activities (not elsewhere classified)',
        'code' => '93290',
        'cover_plus' => '0.30',
        'cover_plus_extra' => '0.43',
        'activities' => array (
          array (
            'name' => 'Betting shop operation',
            'bic_code' => 'R920910'
          ),
          array (
            'name' => 'Bookmaker operation',
            'bic_code' => 'R920920'
          ),
          array (
            'name' => 'Gambling activity (not elsewhere classified)'
          ),
          array (
            'name' => 'Internet gambling operation'
          ),
          array (
            'name' => 'TAB operation'
          )
        )
      ),
      array (
        'name' => 'Automotive electrical services',
        'code' => '53220',
        'cover_plus' => '0.96',
        'cover_plus_extra' => '1.28',
        'activities' => array (
          array (
            'name' => 'Air conditioner repair (automotive)',
            'bic_code' => 'S941110'
          ),
          array (
            'name' => 'Auto-electrical garage operation',
            'bic_code' => 'S941120'
          ),
          array (
            'name' => 'Automotive electrical product installation and/or repair',
            'bic_code' => 'S941130'
          ),
          array (
            'name' => 'Car radio or CD-player installation and repair'
          ),
          array (
            'name' => 'Electrical repair (automotive)'
          )
        )
      ),
      array (
        'name' => 'Automotive body, paint, and interior repair and maintenance',
        'code' => '53230',
        'cover_plus' => '0.96',
        'cover_plus_extra' => '1.28',
        'activities' => array (
          array (
            'name' => 'Automotive body repair',
            'bic_code' => 'S941210'
          ),
          array (
            'name' => 'Automotive interior repair',
            'bic_code' => 'S941220'
          ),
          array (
            'name' => 'Automotive reupholstery',
            'bic_code' => 'S941225'
          ),
          array (
            'name' => 'Automotive rustproofing and undercoating',
            'bic_code' => 'S941230'
          ),
          array (
            'name' => 'Automotive spray painting',
            'bic_code' => 'S941240'
          ),
          array (
            'name' => 'Automotive trimming',
            'bic_code' => 'S941250'
          ),
          array (
            'name' => 'Car detailing',
            'bic_code' => 'S941260'
          ),
          array (
            'name' => 'Car wash or cleaning operation',
            'bic_code' => 'S941270'
          ),
          array (
            'name' => 'Motor vehicle restoration (interior and exterior only)',
            'bic_code' => 'S941280'
          ),
          array (
            'name' => 'Panel beating (motor body repairing)'
          ),
          array (
            'name' => 'Smash repair'
          ),
          array (
            'name' => 'Windscreen replacement and/or repair (including window tinting)'
          )
        )
      ),
      array (
        'name' => 'Automotive repair and maintenance (not elsewhere classified)',
        'code' => '53290',
        'cover_plus' => '0.96',
        'cover_plus_extra' => '1.28',
        'activities' => array (
          array (
            'name' => 'Automotive conversion (including non-factory based engine reconditioning services and converting foreign cars from left to right-hand drive)',
            'bic_code' => 'S941910'
          ),
          array (
            'name' => 'Automotive repair garage operation',
            'bic_code' => 'S941920'
          ),
          array (
            'name' => 'Brake repair',
            'bic_code' => 'S941930'
          ),
          array (
            'name' => 'Clutch repair',
            'bic_code' => 'S941940'
          ),
          array (
            'name' => 'Cooling system and/or radiator repair (automotive)',
            'bic_code' => 'S941950'
          ),
          array (
            'name' => 'Engine repair or reconditioning (automotive, except factory reconditioning)',
            'bic_code' => 'S941955'
          ),
          array (
            'name' => 'Exhaust system or muffler repair (automotive)',
            'bic_code' => 'S941960'
          ),
          array (
            'name' => 'Gearbox repair (automotive)',
            'bic_code' => 'S941970'
          ),
          array (
            'name' => 'General automotive repair',
            'bic_code' => 'S941980'
          ),
          array (
            'name' => 'Motor cycle or scooter repair',
            'bic_code' => 'S941990'
          ),
          array (
            'name' => 'Motor vehicle restoration (not elsewhere classified)'
          ),
          array (
            'name' => 'Muffler repair (automotive)'
          ),
          array (
            'name' => 'Trailer repair (boat or box)'
          ),
          array (
            'name' => 'Transmission repair (automotive)'
          ),
          array (
            'name' => 'Truck repair (automotive)'
          )
        )
      ),
      array (
        'name' => 'Domestic appliance repair and maintenance',
        'code' => '52610',
        'cover_plus' => '0.96',
        'cover_plus_extra' => '1.28',
        'activities' => array (
          array (
            'name' => 'Air conditioner repair and maintenance',
            'bic_code' => 'S942110'
          ),
          array (
            'name' => 'Appliance domestic repair',
            'bic_code' => 'S942120'
          ),
          array (
            'name' => 'Heater (domestic) repair',
            'bic_code' => 'S942130'
          ),
          array (
            'name' => 'Microwave oven, domestic, repair',
            'bic_code' => 'S942140'
          ),
          array (
            'name' => 'Radio and stereo repair and maintenance (except automotive)'
          ),
          array (
            'name' => 'Refrigerator, domestic, repair'
          ),
          array (
            'name' => 'Sewing machine (domestic) repair'
          ),
          array (
            'name' => 'Stove and/or oven (domestic) repair'
          ),
          array (
            'name' => 'Television repair and maintenance'
          ),
          array (
            'name' => 'VCR and DVD repair and maintenance'
          ),
          array (
            'name' => 'Washing machine and/or clothes dryer (domestic) repair'
          )
        )
      ),
      array (
        'name' => 'Electronic (except domestic appliance) and precision equipment repair and maintenance',
        'code' => '78330',
        'cover_plus' => '0.41',
        'cover_plus_extra' => '0.57',
        'activities' => array (
          array (
            'name' => 'Communication equipment, repair and maintenance',
            'bic_code' => 'S942210'
          ),
          array (
            'name' => 'Computer and computer peripheral equipment repair and maintenance',
            'bic_code' => 'S942220'
          ),
          array (
            'name' => 'Dental equipment repair and maintenance',
            'bic_code' => 'S942230'
          ),
          array (
            'name' => 'Diagnostic imaging equipment repair and maintenance',
            'bic_code' => 'S942240'
          ),
          array (
            'name' => 'Electrical measuring instrument repair and maintenance',
            'bic_code' => 'S942250'
          ),
          array (
            'name' => 'Facsimile (fax) machine repair and maintenance',
            'bic_code' => 'S942260'
          ),
          array (
            'name' => 'Laboratory instrument repair and maintenance',
            'bic_code' => 'S942270'
          ),
          array (
            'name' => 'Measuring equipment repair and maintenance',
            'bic_code' => 'S942280'
          ),
          array (
            'name' => 'Medical and surgical equipment repair and maintenance',
            'bic_code' => 'S942290'
          ),
          array (
            'name' => 'Meteorological instrument repair and maintenance',
            'bic_code' => 'S942935'
          ),
          array (
            'name' => 'Navigation instrument (including radar and sonar) repair and maintenance'
          ),
          array (
            'name' => 'Office machine repair and maintenance'
          ),
          array (
            'name' => 'Optical instrument (including microscopes and telescopes) repair and maintenance'
          ),
          array (
            'name' => 'Photocopying machine repair and maintenance'
          ),
          array (
            'name' => 'Photographic (including camera) equipment repair'
          ),
          array (
            'name' => 'Precision equipment calibration'
          ),
          array (
            'name' => 'Radar and sonar equipment repair and maintenance'
          ),
          array (
            'name' => 'Scales, professional or scientific, repair and maintenance'
          ),
          array (
            'name' => 'Surgical equipment repair and maintenance'
          ),
          array (
            'name' => 'Surveying equipment repair and maintenance'
          ),
          array (
            'name' => 'Telephone equipment repair and maintenance'
          )
        )
      ),
      array (
        'name' => 'Machinery and equipment repair and maintenance (not elsewhere classified)',
        'code' => '28680',
        'cover_plus' => '0.96',
        'cover_plus_extra' => '1.28',
        'activities' => array (
          array (
            'name' => 'Agricultural or farm machinery and equipment repair and maintenance',
            'bic_code' => 'S942905'
          ),
          array (
            'name' => 'Blade sharpening',
            'bic_code' => 'S942910'
          ),
          array (
            'name' => 'Brushcutter repair and maintenance',
            'bic_code' => 'S942913'
          ),
          array (
            'name' => 'Construction machinery and equipment repair and maintenance',
            'bic_code' => 'S942915'
          ),
          array (
            'name' => 'Electric motor repair and maintenance, including armature rewinding (except factory based)',
            'bic_code' => 'S942920'
          ),
          array (
            'name' => 'Electrical generating and transmission equipment repair and maintenance',
            'bic_code' => 'S942925'
          ),
          array (
            'name' => 'Engine repair (except automotive)',
            'bic_code' => 'S942927'
          ),
          array (
            'name' => 'Food machinery and equipment (industrial) repair and maintenance',
            'bic_code' => 'S942930'
          ),
          array (
            'name' => 'Forestry machinery and equipment repair and maintenance',
            'bic_code' => 'S942945'
          ),
          array (
            'name' => 'Foundry machinery and equipment repair and maintenance',
            'bic_code' => 'S942950'
          ),
          array (
            'name' => 'Heavy machinery and equipment repair and maintenance',
            'bic_code' => 'S942955'
          ),
          array (
            'name' => 'Hydraulic equipment repair and maintenance',
            'bic_code' => 'S942960'
          ),
          array (
            'name' => 'Lawn mower repair and maintenance',
            'bic_code' => 'S942965'
          ),
          array (
            'name' => 'Machine tool repair and maintenance'
          ),
          array (
            'name' => 'Maintenance of non-electric traffic signals'
          ),
          array (
            'name' => 'Material handling equipment repair and maintenance'
          ),
          array (
            'name' => 'Mining machinery and equipment repair and maintenance'
          ),
          array (
            'name' => 'Outboard motor repair'
          ),
          array (
            'name' => 'Paper making and printing trade machinery repair and maintenance'
          ),
          array (
            'name' => 'Pump and compressor repair'
          ),
          array (
            'name' => 'Refrigeration equipment (industrial) repair and maintenance'
          ),
          array (
            'name' => 'Shipping barrel and drum reconditioning and repairing'
          ),
          array (
            'name' => 'Stove and/or oven (industrial) repair and maintenance'
          ),
          array (
            'name' => 'Textile machinery repair and maintenance'
          ),
          array (
            'name' => 'Washing machine and/or clothes dryer (industrial) repair and maintenance'
          ),
          array (
            'name' => 'Welding repair service (including automotive)'
          )
        )
      ),
      array (
        'name' => 'Clothing and footwear repair',
        'code' => '52620',
        'cover_plus' => '0.41',
        'cover_plus_extra' => '0.57',
        'activities' => array (
          array (
            'name' => 'Clothing repair',
            'bic_code' => 'S949105'
          ),
          array (
            'name' => 'Footwear (including boot and shoe) repair',
            'bic_code' => 'S949110'
          )
        )
      ),
      array (
        'name' => 'Repair and maintenance (not elsewhere classified)',
        'code' => '52690',
        'cover_plus' => '0.41',
        'cover_plus_extra' => '0.57',
        'activities' => array (
          array (
            'name' => 'Antique restoration',
            'bic_code' => 'S942940'
          ),
          array (
            'name' => 'Bicycle repair',
            'bic_code' => 'S949910'
          ),
          array (
            'name' => 'Furniture repair',
            'bic_code' => 'S949920'
          ),
          array (
            'name' => 'Furniture restoration',
            'bic_code' => 'S949930'
          ),
          array (
            'name' => 'Jewellery repair',
            'bic_code' => 'S949940'
          ),
          array (
            'name' => 'Key duplicating'
          ),
          array (
            'name' => 'Luggage repair'
          ),
          array (
            'name' => 'Musical instrument tuning and repair'
          ),
          array (
            'name' => 'Sports equipment repair'
          ),
          array (
            'name' => 'Watch repair'
          ),
          array (
            'name' => 'Wheelchair repair and maintenance'
          )
        )
      ),
      array (
        'name' => 'Hairdressing and beauty services',
        'code' => '95260',
        'cover_plus' => '0.47',
        'cover_plus_extra' => '0.64',
        'activities' => array (
          array (
            'name' => 'Barber shop operation',
            'bic_code' => 'S951105'
          ),
          array (
            'name' => 'Beauty service',
            'bic_code' => 'S951110'
          ),
          array (
            'name' => 'Electrolysis service',
            'bic_code' => 'S951115'
          ),
          array (
            'name' => 'Hair restoration service (except hair transplant service)',
            'bic_code' => 'S951125'
          ),
          array (
            'name' => 'Hairdressing service',
            'bic_code' => 'S951130'
          ),
          array (
            'name' => 'Make-up service',
            'bic_code' => 'S951135'
          ),
          array (
            'name' => 'Nail care service',
            'bic_code' => 'S951140'
          ),
          array (
            'name' => 'Skin care service',
            'bic_code' => 'S951145'
          ),
          array (
            'name' => 'Tanning (solarium) service',
            'bic_code' => 'S951150'
          )
        )
      ),
      array (
        'name' => 'Diet and weight-reduction centre operation',
        'code' => '95270',
        'cover_plus' => '0.47',
        'cover_plus_extra' => '0.64',
        'activities' => array (
          array (
            'name' => 'Slimming service (non-medical)',
            'bic_code' => 'S951210'
          ),
          array (
            'name' => 'Weight loss centre operation (non-medical)'
          ),
          array (
            'name' => 'Weight loss service (non-medical)'
          )
        )
      ),
      array (
        'name' => 'Funeral, crematorium, and cemetery services',
        'code' => '95240',
        'cover_plus' => '0.47',
        'cover_plus_extra' => '0.64',
        'activities' => array (
          array (
            'name' => 'Cemetery operation',
            'bic_code' => 'S952010'
          ),
          array (
            'name' => 'Columbarium operation',
            'bic_code' => 'S952020'
          ),
          array (
            'name' => 'Crematorium operation',
            'bic_code' => 'S952030'
          ),
          array (
            'name' => 'Embalming service',
            'bic_code' => 'S952040'
          ),
          array (
            'name' => 'Funeral directing'
          ),
          array (
            'name' => 'Mausoleum operation'
          ),
          array (
            'name' => 'Memorial garden (i.e. burial place) operation'
          ),
          array (
            'name' => 'Mortician service'
          ),
          array (
            'name' => 'Pet cemetery operation'
          ),
          array (
            'name' => 'Undertaking service'
          )
        )
      ),
      array (
        'name' => 'Laundry and dry-cleaning services',
        'code' => '95210',
        'cover_plus' => '1.19',
        'cover_plus_extra' => '1.58',
        'activities' => array (
          array (
            'name' => 'Automatic laundry operation (coin-operated)',
            'bic_code' => 'C133420'
          ),
          array (
            'name' => 'Baby napkin hire service',
            'bic_code' => 'S953110'
          ),
          array (
            'name' => 'Carpet, upholstery and rug cleaning',
            'bic_code' => 'S953120'
          ),
          array (
            'name' => 'Clothing, hat or garment (including leather), cleaning service',
            'bic_code' => 'S953130'
          ),
          array (
            'name' => 'Curtain and drapery cleaning service',
            'bic_code' => 'S953140'
          ),
          array (
            'name' => 'Dry-cleaning agency operation',
            'bic_code' => 'S953150'
          ),
          array (
            'name' => 'Dry-cleaning service',
            'bic_code' => 'S953160'
          ),
          array (
            'name' => 'Laundry agency operation',
            'bic_code' => 'S953170'
          ),
          array (
            'name' => 'Laundry and dry-cleaning service',
            'bic_code' => 'S953180'
          ),
          array (
            'name' => 'Laundry operation',
            'bic_code' => 'S953190'
          ),
          array (
            'name' => 'Linen hire service'
          ),
          array (
            'name' => 'Self-service laundry operation'
          ),
          array (
            'name' => 'Uniform hire service'
          )
        )
      ),
      array (
        'name' => 'Photographic film processing',
        'code' => '95220',
        'cover_plus' => '0.14',
        'cover_plus_extra' => '0.22',
        'activities' => array (
          array (
            'name' => 'Digital photograph processing',
            'bic_code' => 'S953205'
          ),
          array (
            'name' => 'Film developing and printing (except motion picture)',
            'bic_code' => 'S953210'
          ),
          array (
            'name' => 'Photofinishing laboratory operation'
          ),
          array (
            'name' => 'Photofinishing service'
          ),
          array (
            'name' => 'Photographic film processing'
          )
        )
      ),
      array (
        'name' => 'Parking services',
        'code' => '66110',
        'cover_plus' => '0.47',
        'cover_plus_extra' => '0.64',
        'activities' => array (
          array (
            'name' => 'Automobile parking garage or lot operation',
            'bic_code' => 'S953310'
          ),
          array (
            'name' => 'Car park operation',
            'bic_code' => 'S953320'
          ),
          array (
            'name' => 'Parking service'
          ),
          array (
            'name' => 'Parking station operation'
          ),
          array (
            'name' => 'Valet parking service'
          )
        )
      ),
      array (
        'name' => 'Building completion services - all trades subcontracted',
        'code' => '42592',
        'cover_plus' => '1.46',
        'cover_plus_extra' => '1.94',
        'activities' => array (
          array (
            'name' => 'Building completion services where trades work is subcontracted',
            'bic_code' => 'E329920'
          )
        )
      ),
      array (
        'name' => 'Brothel-keeping, massage parlour, and prostitution services',
        'code' => '95300',
        'cover_plus' => '0.47',
        'cover_plus_extra' => '0.64',
        'activities' => array (
          array (
            'name' => 'Brothel operation',
            'bic_code' => 'S953410'
          ),
          array (
            'name' => 'Escort service (prostitution)',
            'bic_code' => 'S953420'
          ),
          array (
            'name' => 'Massage parlour operation',
            'bic_code' => 'S953425'
          ),
          array (
            'name' => 'Prostitution service',
            'bic_code' => 'S953440'
          )
        )
      ),
      array (
        'name' => 'Personal services (not elsewhere classified)',
        'code' => '95290',
        'cover_plus' => '0.47',
        'cover_plus_extra' => '0.64',
        'activities' => array (
          array (
            'name' => 'Astrology service',
            'bic_code' => 'S953905'
          ),
          array (
            'name' => 'Baby sitting service (except in child care centres or preschools)',
            'bic_code' => 'S955910'
          ),
          array (
            'name' => 'Cloak room service',
            'bic_code' => 'S953910'
          ),
          array (
            'name' => 'Domestic service (not elsewhere classified)',
            'bic_code' => 'S953915'
          ),
          array (
            'name' => 'Fortune telling service',
            'bic_code' => 'S953920'
          ),
          array (
            'name' => 'Genealogy service',
            'bic_code' => 'S953925'
          ),
          array (
            'name' => 'Heraldry service',
            'bic_code' => 'S953930'
          ),
          array (
            'name' => 'Introductory agency operation',
            'bic_code' => 'S953935'
          ),
          array (
            'name' => 'Life coach',
            'bic_code' => 'S953940'
          ),
          array (
            'name' => 'Marriage celebrant service',
            'bic_code' => 'S953942'
          ),
          array (
            'name' => 'Nanny service',
            'bic_code' => 'S953945'
          ),
          array (
            'name' => 'Personal fitness training service',
            'bic_code' => 'S953950'
          ),
          array (
            'name' => 'Personal stylist service',
            'bic_code' => 'S953955'
          ),
          array (
            'name' => 'Pet boarding service',
            'bic_code' => 'S953960'
          ),
          array (
            'name' => 'Pet grooming service',
            'bic_code' => 'S953965'
          ),
          array (
            'name' => 'Psychic service',
            'bic_code' => 'S953970'
          ),
          array (
            'name' => 'Sauna bath operation'
          ),
          array (
            'name' => 'Shoe shining'
          ),
          array (
            'name' => 'Tattooing and piercing service'
          ),
          array (
            'name' => 'Turkish bath operation'
          ),
          array (
            'name' => 'Wedding chapel operation (except church)'
          )
        )
      ),
      array (
        'name' => 'Religious organisations and services',
        'code' => '96100',
        'cover_plus' => '0.19',
        'cover_plus_extra' => '0.28',
        'activities' => array (
          array (
            'name' => 'Bible society operation',
            'bic_code' => 'S954010'
          ),
          array (
            'name' => 'Church operation',
            'bic_code' => 'S954020'
          ),
          array (
            'name' => 'Convent operation (except schools)'
          ),
          array (
            'name' => 'Diocesan registry operation'
          ),
          array (
            'name' => 'Missionary society operation'
          ),
          array (
            'name' => 'Monastery operation (except schools)'
          ),
          array (
            'name' => 'Mosque operation'
          ),
          array (
            'name' => 'Religious organisation operation'
          ),
          array (
            'name' => 'Religious shrine operation'
          ),
          array (
            'name' => 'Religious temple operation'
          ),
          array (
            'name' => 'Synagogue operation'
          )
        )
      ),
      array (
        'name' => 'Business and professional association services',
        'code' => '96210',
        'cover_plus' => '0.19',
        'cover_plus_extra' => '0.28',
        'activities' => array (
          array (
            'name' => 'Accountants’ association operation',
            'bic_code' => 'S955110'
          ),
          array (
            'name' => 'Architects’ association operation',
            'bic_code' => 'S955120'
          ),
          array (
            'name' => 'Bankers’ association operation',
            'bic_code' => 'S955130'
          ),
          array (
            'name' => 'Builders’ association operation'
          ),
          array (
            'name' => 'Chamber of Commerce operation'
          ),
          array (
            'name' => 'Chamber of Manufacturers operation'
          ),
          array (
            'name' => 'Chemists’ association operation'
          ),
          array (
            'name' => 'Dentists’ association operation'
          ),
          array (
            'name' => 'Employers’ association operation'
          ),
          array (
            'name' => 'Engineers’ association operation (except trade union)'
          ),
          array (
            'name' => 'Farmers’ association operation'
          ),
          array (
            'name' => 'Lawyers’ association operation'
          ),
          array (
            'name' => 'Manufacturers’ association operation'
          ),
          array (
            'name' => 'Medical association operation'
          ),
          array (
            'name' => 'Mining association operation'
          ),
          array (
            'name' => 'Retail traders’ association operation'
          ),
          array (
            'name' => 'Surveyors’ association operation'
          ),
          array (
            'name' => 'Trade association operation (except trade union)'
          )
        )
      ),
      array (
        'name' => 'Labour association services',
        'code' => '96220',
        'cover_plus' => '0.19',
        'cover_plus_extra' => '0.28',
        'activities' => array (
          array (
            'name' => 'Employees’ association operation',
            'bic_code' => 'S955210'
          ),
          array (
            'name' => 'Industrial union operation',
            'bic_code' => 'S955220'
          ),
          array (
            'name' => 'Trade union operation'
          ),
          array (
            'name' => 'Union association operation'
          )
        )
      ),
      array (
        'name' => 'Interest group services (not elsewhere classified)',
        'code' => '96290',
        'cover_plus' => '0.19',
        'cover_plus_extra' => '0.28',
        'activities' => array (
          array (
            'name' => 'Accident prevention association operation',
            'bic_code' => 'S955920'
          ),
          array (
            'name' => 'Animal (including wildlife) welfare association or league operation',
            'bic_code' => 'S955930'
          ),
          array (
            'name' => 'Association operation (for promotion of community or sectional interests)',
            'bic_code' => 'S955950'
          ),
          array (
            'name' => 'Automobile association operation',
            'bic_code' => 'S955960'
          ),
          array (
            'name' => 'Civil liberty service',
            'bic_code' => 'S955970'
          ),
          array (
            'name' => 'Club operation (for the promotion of community or sectional interests)',
            'bic_code' => 'S955980'
          ),
          array (
            'name' => 'Community association operation',
            'bic_code' => 'S955990'
          ),
          array (
            'name' => 'Conservation (including wildlife) association operation'
          ),
          array (
            'name' => 'Consumers’ association operation'
          ),
          array (
            'name' => 'Disease research (including cancer and heart disease) fundraising'
          ),
          array (
            'name' => 'Human rights association operation'
          ),
          array (
            'name' => 'Interest group service (not elsewhere classified)'
          ),
          array (
            'name' => 'Parent-teachers association operation'
          ),
          array (
            'name' => 'Pensioners’ association operation'
          ),
          array (
            'name' => 'Political party operation'
          ),
          array (
            'name' => 'Social club operation'
          ),
          array (
            'name' => 'Veterans’ association operation'
          ),
          array (
            'name' => 'Welfare fundraising'
          ),
          array (
            'name' => 'Women’s interest group association operation'
          ),
          array (
            'name' => 'Youth club/association (including girl guides and scouts) operation'
          )
        )
      ),
      array (
        'name' => 'Private households employing staff',
        'code' => '97000',
        'cover_plus' => '0.47',
        'cover_plus_extra' => '0.64',
        'activities' => array (
          array (
            'name' => 'Household, private, employing staff',
            'bic_code' => 'S960110'
          ),
          array (
            'name' => 'Private household employing domestic personnel',
            'bic_code' => 'S960120'
          ),
          array (
            'name' => 'Private household employing staff'
          )
        )
      ),
      array (
        'name' => 'Community-based, multi-functional activities (not elsewhere classified)',
        'code' => '96291',
        'cover_plus' => '0.47',
        'cover_plus_extra' => '0.64',
        'bic_code' => 'S955940'
      ),
    );

    return collect($units);
  }
}

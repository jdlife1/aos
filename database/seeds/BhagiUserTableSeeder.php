<?php

use Aos\Models\User;
use Illuminate\Database\Seeder;

class BhagiUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Bhagi',
            'email' => 'bhagi@jdlife.co.nz',
            'password' => 'bhagi'
        ]);
    }
}

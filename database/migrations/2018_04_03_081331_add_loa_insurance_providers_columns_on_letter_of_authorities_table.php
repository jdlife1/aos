<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoaInsuranceProvidersColumnsOnLetterOfAuthoritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('letter_of_authorities', function (Blueprint $table) {
            $table->text('client_insurance_providers')->nullable(true)->after('client_smoking_status');
            $table->text('partner_insurance_providers')->nullable(true)->after('partner_smoking_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('letter_of_authorities', function (Blueprint $table) {
            $table->dropColumn([
                'client_insurance_providers',
                'partner_insurance_providers',
            ]);
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientAndPartnerAccCoverPlanTypeColumnsOnLetterOfAuthoritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('letter_of_authorities', function (Blueprint $table) {
            $table
                ->string('acc_cover_plan_type', 25)
                ->after('income_from_business')
                ->default('cover_plus')
                ->nullable(true);
            $table
                ->string('existing_nominated_cover_amount', 25)
                ->after('acc_cover_plan_type')
                ->nullable(true);

            $table
                ->string('partner_acc_cover_plan_type', 25)
                ->after('partner_taking_from_the_firm')
                ->default('cover_plus')
                ->nullable(true);
            $table
                ->string('partner_existing_nominated_cover_amount', 50)
                ->after('partner_acc_cover_plan_type')
                ->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('letter_of_authorities', function (Blueprint $table) {
            $table->dropColumn([
                'acc_cover_plan_type',
                'existing_nominated_cover_amount',
                'partner_acc_cover_plan_type',
                'partner_existing_nominated_cover_amount',
            ]);
        });
    }
}

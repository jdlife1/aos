<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassificationUnitActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classification_unit_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('classification_unit_id');
            $table->foreign('classification_unit_id')
                ->references('id')
                ->on('classification_units')
                ->onDelete('CASCADE');
            $table->text('name');
            $table->json('bic_code')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classification_unit_activities');
    }
}

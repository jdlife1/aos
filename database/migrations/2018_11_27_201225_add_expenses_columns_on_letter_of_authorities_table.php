<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpensesColumnsOnLetterOfAuthoritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('letter_of_authorities', function (Blueprint $table) {
            $table
                ->string('mortgage_repayments_type', 10)
                ->after('mortgage_repayments')
                ->nullable(true);
            $table
                ->string('mortgage_bank_or_provider', 20)
                ->after('mortgage_repayments_type')
                ->nullable(true);
            $table
                ->string('household_building_expense', 20)
                ->after('living_expenses')
                ->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('letter_of_authorities', function (Blueprint $table) {
            $table->dropColumn([
                'mortgage_repayments_type',
                'mortgage_bank_or_provider',
                'household_building_expense',
            ]);
        });
    }
}

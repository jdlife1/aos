<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassificationUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classification_units', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->unsignedInteger('code')->unique();
            $table->string('cover_plus');
            $table->string('cover_plus_extra');
            $table->string('bic_code')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classification_units');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLetterOfAuthorityAccLevyCalculationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letter_of_authority_acc_levy_calculations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('letter_of_authority_id');
            $table
                ->foreign('letter_of_authority_id', 'levy_calculations_letter_of_authority_id_foreign')
                ->references('id')
                ->on('letter_of_authorities')
                ->onDelete('CASCADE');
            $table->string('type', 50); // client or partner
            $table->string('property'); // plan, on_tools, existing.
            $table->json('values'); // raw response from automation service
            $table->json('coverplus_cover');
            $table->json('coverplus_extra_standard');
            $table->json('coverplus_extra_llwc')->nullable(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('letter_of_authority_acc_levy_calculations');
    }
}

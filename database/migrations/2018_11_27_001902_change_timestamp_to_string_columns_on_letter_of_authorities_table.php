<?php

use Aos\Models\LetterOfAuthority;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTimestampToStringColumnsOnLetterOfAuthoritiesTable extends Migration
{
    /**
     * Array defining the columns to be modified
     *
     * @var array
     */
    protected static $FIELDS = [
        'when_business_started',
        'partner_when_business_started',
    ];

    protected function normalizeColumns()
    {
        $models = (new LetterOfAuthority)->newQueryWithoutRelationships()->get();

        foreach ($models as $model) {
            foreach (self::$FIELDS as $attribute) {
                $value = $model->$attribute;
                if (! in_array($value, ['yes', 'no'])) {
                    $model->forceFill([$attribute => null])->update();
                }
            }
        }
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->normalizeColumns();

        Schema::table('letter_of_authorities', function (Blueprint $table) {
            foreach (self::$FIELDS as $field) {
                $table->string($field, 3)->nullable(true)->change();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('letter_of_authorities', function (Blueprint $table) {
            foreach (self::$FIELDS as $field) {
                $table->timestamp($field)->nullable(true)->change();
            }
        });
    }
}

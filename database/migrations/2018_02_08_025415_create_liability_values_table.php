<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiabilityValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liability_values', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('cover_type', ['cover_plus', 'cover_extra'])->default('cover_plus');
            $table->string('liable_earnings')->nullable(true);
            $table->string('earners_levy_current_portion_fix_rate')->nullable(true);
            $table->string('working_safer_levy_fix_rate')->nullable(true);
            $table->string('estimated_total_amount_payable_fix_rate')->default('1.15');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liability_values');
    }
}

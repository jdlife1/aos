<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovePassiveIncomeColumnsOnLetterOfAuthoritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('letter_of_authorities', function (Blueprint $table) {
            $table->dropColumn([
                'income_protection_covers_with_other_insurance_provider',
                'passive_income_from_business',
                'passive_income_from_rentals',
                'other_passive_income'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('letter_of_authorities', function (Blueprint $table) {
            $table
                ->string('income_protection_covers_with_other_insurance_provider', 100)
                ->after('partner_when_business_started')
                ->nullable(true);
            $table
                ->string('passive_income_from_business', 100)
                ->after('income_protection_covers_with_other_insurance_provider')
                ->nullable(true);
            $table
                ->string('passive_income_from_rentals', 100)
                ->after('passive_income_from_business')
                ->nullable(true);
            $table
                ->string('other_passive_income', 100)
                ->after('passive_income_from_rentals')
                ->nullable(true);
        });
    }
}

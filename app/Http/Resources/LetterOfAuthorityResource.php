<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class LetterOfAuthorityResource extends Resource
{
    /**
     * Contant define the date format.
     *
     * @var String
     */
    const DATE_FORMAT = 'd/m/Y';

    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = null;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
        // return array_merge(parent::toArray($request), [
        //     'business_partners' => (array) json_decode($request->business_partners),
        //     'childrens' => (array) json_decode($request->childrens),
        //     'fatal_entitlement_childrens' => (array) json_decode($request->fatal_entitlement_childrens),
        // ]);
    }
}

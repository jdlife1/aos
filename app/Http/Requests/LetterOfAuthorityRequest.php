<?php

namespace App\Http\Requests;

use Auth;
use Aos\Models\LetterOfAuthority;
use Illuminate\Support\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class LetterOfAuthorityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $inputs = $this->all();

        $rules = [
            'client_full_name' => [
                'required',
            ],
            'income_from_business' => [
            ],
            'nominated_cover_amount' => [
            ]
        ];

        // foreach (array_keys(array_except($this->getModelInstance()->getCasts(), ['id'])) as $field) {
        //     $inputs[$field] = json_encode($inputs[$field]);
        // }

        foreach (['client_date_of_birth', 'partner_date_of_birth'] as $field) {
            if (is_null($field)) continue;
            $inputs[$field] = Carbon::parse($inputs[$field]);
        }

        $this->replace($inputs);

        return $rules;
    }

    protected function getModelInstance()
    {
        return (new LetterOfAuthority);
    }
}

<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class AccLevyCalculationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => [
                'required',
                Rule::in(['client', 'partner']),
            ],
            'property' => [
                'required',
                Rule::in(['plan', 'on_tools', 'existing']),
            ],
            'values' => [
                'required',
            ],
            'coverplus_cover' => [
                'required',
            ],
            'coverplus_extra_standard' => [
                'required',
            ],
            'coverplus_extra_llwc' => [
                'nullable',
            ],
        ];
    }
}

<?php

namespace App\Http\Controllers\Api;

use Aos\Models\LetterOfAuthority;
use Aos\Repositories\LetterOfAuthorityRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\LetterOfAuthorityRequest;
use App\Http\Resources\LetterOfAuthorityResource;
use App\Mail\SendLoaApplication;
use Illuminate\Http\Request;
use Mail;

class LetterOfAuthorizationController extends Controller
{
    /**
     * LetterOfAuthorityRepository instance.
     *
     * @var \Aos\Repositories\LetterOfAuthorityRepository
     */
    protected $repository;

    public function __construct(LetterOfAuthorityRepository $repository)
    {
        parent::__construct();

        $this->repository = $repository;
    }

    public function find($id)
    {
        $model = $this->repository->getModel()->findOrFail($id);

        return (new LetterOfAuthorityResource($model->fresh()));
    }

    public function curate()
    {
        $letters = $this->repository->getModel()->get();

        return (LetterOfAuthorityResource::collection($letters));
    }

    public function persist(LetterOfAuthorityRequest $request)
    {
        $attributes = $request->only(['id']);
        $values = $request->except(['id']);

        $response = $request
                        ->user()
                        ->letterOfAuthorities()
                        ->updateOrCreate($attributes, $values);

        return (new LetterOfAuthorityResource($response->fresh()));
    }

    public function expunge($id)
    {
        $model = $this->repository->getModel()->findOrFail($id);

        if ($model->delete()) {
            return response()->json([
                'id' => $id,
                'message' => 'Success',
                'status' => 200
            ]);
        }

        return response()->json([
            'message' => 'Failed',
            'status' => 500
        ]);
    }

    public function sendNotification($id, Request $request)
    {
        $letter = $request
                        ->user()
                        ->letterOfAuthorities()
                        ->findOrFail($id);

        try {
            Mail::to(env('MAIL_TO_ADMIN_EMAIL'))->send((new SendLoaApplication($letter)));
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'status' => 500
            ]);
        }

        return response()->json([
            'message' => 'Sent',
            'status' => 200
        ]);
    }
}

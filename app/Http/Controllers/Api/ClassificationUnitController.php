<?php

namespace App\Http\Controllers\Api;

use Aos\Models\ClassificationUnit;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClassificationUnitResource;

class ClassificationUnitController extends Controller
{
    public function curate(ClassificationUnit $classificationUnit)
    {
        return new ClassificationUnitResource($classificationUnit);
    }
}

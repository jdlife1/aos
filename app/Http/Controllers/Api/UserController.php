<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;

class UserController extends Controller
{
    public function current()
    {
        return request()->user();
    }

    public function modify(UserUpdateRequest $request)
    {
        $user = request()->user();

        return tap($user, function ($instance) use ($request) {
            $instance->update($request->all());
        });
    }
}

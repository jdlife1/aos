<?php

namespace App\Http\Controllers\Api;

use Aos\Models\LetterOfAuthority;
use App\Http\Controllers\Controller;
use App\Http\Requests\AccLevyCalculationRequest;

class AccCalculationController extends Controller
{
    public function __invoke(LetterOfAuthority $loa, AccLevyCalculationRequest $request)
    {
        $loa->accLevyCalculations()->updateOrCreate([
            'id' => $request->get('id', 0),
            'letter_of_authority_id' => $loa->id
        ], $request->except('id'));

        return $loa->fresh();
    }
}

<?php

namespace App\Http\Controllers\Web;

use PDF;
use App\Http\Controllers\Controller;
use App\Http\Resources\LetterOfAuthorityResource;
use Aos\Repositories\LetterOfAuthorityRepository;

class WebLetterOfAuthorizationController extends Controller
{
    /**
     * LetterOfAuthorityRepository instance.
     *
     * @var \Aos\Repositories\LetterOfAuthorityRepository
     */
    protected $letterOfAuthorityRepository;

    public function __construct(LetterOfAuthorityRepository $letterOfAuthorityRepository)
    {
        parent::__construct();
        $this->letterOfAuthorityRepository = $letterOfAuthorityRepository;
    }

    public function displayPdf($id, $type = null)
    {
        $letter = $this
                    ->letterOfAuthorityRepository
                    ->getModel()
                    ->findOrFail($id);

        $view = view('aos.pdf.index', compact('letter'))->render();

        PDF::SetTitle('Document');
        PDF::SetMargins(10, 10, 10);
        PDF::SetHeaderMargin(10);
        PDF::SetFooterMargin(0);
        PDF::setFontSubsetting(true);
        PDF::SetFont('dejavusans', '', 8, '', true);
        PDF::setTextShadow(['enabled'=>false, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=> [196,196,196], 'opacity'=>1, 'blend_mode'=>'Normal']);

        PDF::AddPage();
        PDF::writeHTML($view, true, false, true, false, '');
        PDF::lastPage();

        return PDF::Output(uniqid(). '_loa.pdf');
    }
}

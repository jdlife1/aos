<?php

namespace Aos\Models;

use Illuminate\Database\Eloquent\Model;

class ClassificationUnitActivity extends Model
{
    protected $fillable = [
        'name',
        'bic_code',
    ];

    protected $casts = [
        'bic_code' => 'array',
    ];

    public function setBicCodeAttribute($value)
    {
        if (is_string($value)) {
            $value = array_wrap($value);
        }

        if (is_null($value)) {
            $value = [];
        }

        $this->attributes['bic_code'] = json_encode($value, true);
    }
}

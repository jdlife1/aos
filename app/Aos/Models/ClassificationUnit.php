<?php

namespace Aos\Models;

use Illuminate\Database\Eloquent\Model;

class ClassificationUnit extends Model
{
    protected $fillable = [
        'code',
        'name',
        'bic_code',
    ];

    public function activities()
    {
        return $this->hasMany(ClassificationUnitActivity::class);
    }
}

<?php

namespace Aos\Models;

use Illuminate\Database\Eloquent\Model;

class LetterOfAuthority extends Model
{
    protected $fillable = [
        'company_full_name',
        'client_full_name',
        'business_partners',
        'income_tax_method',
        'on_tools',
        'has_corporate_partner',
        'classification_unit',
        'income_from_business',
        'acc_cover_plan_type',
        'existing_nominated_cover_amount',
        'nominated_cover_amount',
        'when_business_started',
        'how_many_boys',
        'what_you_do_in_business',
        'acc_number_business',
        'acc_or_ird_number_personal',
        'partner_acc_or_ird_number_personal',
        'accountants_name',
        'firm_name',
        'phone_number',
        'client_date_of_birth',
        'client_address',
        'client_mobile_number',
        'client_email_address',
        'client_smoking_status',
        'client_insurance_providers',
        'partner_date_of_birth',
        'partner_address',
        'partner_mobile_number',
        'partner_email_address',
        'partner_smoking_status',
        'partner_insurance_providers',
        'have_childrens',
        'living_expenses',
        'household_building_expense',
        'mortgage_repayments',
        'mortgage_repayments_type',
        'mortgage_bank_or_provider',
        'total_debts',
        'notes',
        'is_partner_shareholder_or_directory',
        'partner_name',
        'is_partner_account_tax_splitting',
        'partner_income_tax_method',
        'partner_taking_from_the_firm',
        'partner_acc_cover_plan_type',
        'partner_existing_nominated_cover_amount',
        'partner_on_tools',
        'partner_classification_unit',
        'partner_nominated_cover_amount',
        'partner_when_business_started',
        'client_tax_issues_insurance_provider',
        'partner_tax_issues_insurance_provider',
        'fatal_entitlement_for_type',
        'fatal_entitlement_category_type',
        'fatal_entitlement_annual_pre_aggreed_cover_amount',
        'fatal_entitlement_childrens',
        'client_signature',
        'partner_signature'
    ];

    protected $with = ['accLevyCalculations'];

    protected $appends = ['loa_document_url'];

    protected $dates = [
        'client_date_of_birth',
        'partner_date_of_birth',
    ];

    protected $casts = [
        'business_partners' => 'array',
        'fatal_entitlement_childrens' => 'array',
        'classification_unit' => 'integer',
        'partner_classification_unit' => 'integer',
        'client_insurance_providers' => 'array',
        'partner_insurance_providers' => 'array',
    ];

    public function accLevyCalculations()
    {
        return $this->hasMany(LetterOfAuthorityAccLevyCalculation::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getLoaDocumentUrlAttribute()
    {
        return route('web.document.letter-of-authority', ['id' => $this->id]);
    }
}

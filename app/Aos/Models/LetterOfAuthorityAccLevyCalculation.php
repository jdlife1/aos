<?php

namespace Aos\Models;

use Illuminate\Database\Eloquent\Model;

class LetterOfAuthorityAccLevyCalculation extends Model
{
    protected $fillable = [
        'type',
        'values',
        'property',
        'coverplus_cover',
        'coverplus_extra_standard',
        'coverplus_extra_llwc',
    ];

    protected $casts = [
        'values' => 'array',
        'coverplus_cover' => 'array',
        'coverplus_extra_standard' => 'array',
        'coverplus_extra_llwc' => 'array',
    ];

    public function letterOfAuthority()
    {
        return $this->belongsTo(LetterOfAuthority::class);
    }
}

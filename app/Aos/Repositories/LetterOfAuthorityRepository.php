<?php

namespace Aos\Repositories;

use Aos\Models\LetterOfAuthority;

class LetterOfAuthorityRepository extends BaseRepository
{
    public function __construct(LetterOfAuthority $model)
    {
        $this->model = $model;
    }
}

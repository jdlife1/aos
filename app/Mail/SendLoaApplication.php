<?php

namespace App\Mail;

use PDF;
use Aos\Models\LetterOfAuthority;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendLoaApplication extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Constant defining the PDF file name.
     *
     * @var string
     */
    const FILENAME = 'jdlife-application.pdf';

    /**
     * LetterOfAuthority instance.
     *
     * @var \Aos\Models\LetterOfAuthority
     */
    public $letter;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(LetterOfAuthority $letter)
    {
        $this->letter = $letter;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $options = ['mime' => 'application/pdf'];

        $url = route('web.document.letter-of-authority', ['id' => $this->letter->id]);

        return $this
                ->markdown('aos.emails.send-application', compact('url'))
                ->attachData(
                    $this->getPdfData(),
                    self::FILENAME,
                    $options
                );
    }

    public function getPdfData()
    {
        $view = view('aos.pdf.index', ['letter' => $this->letter])->render();

        PDF::SetMargins(10, 10, 10);
        PDF::SetHeaderMargin(10);
        PDF::SetFooterMargin(0);
        PDF::setFontSubsetting(true);
        PDF::SetFont('dejavusans', '', 8, '', true);
        PDF::setTextShadow(['enabled'=>false, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=> [196,196,196], 'opacity'=>1, 'blend_mode'=>'Normal']);

        PDF::AddPage();

        PDF::writeHTML($view, true, false, true, false, '');
        PDF::lastPage();

        return PDF::Output(self::FILENAME, 'S');
    }
}

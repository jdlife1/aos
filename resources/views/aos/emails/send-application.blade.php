@component('mail::message')
# LOA Application Notification

Application has been created

@component('mail::button', ['url' => $url])
View Application
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

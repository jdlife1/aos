<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Letter of Authorirty</title>
    @include('aos.pdf.blocks.styles')
</head>
<body>
    @include('aos.pdf.blocks.header')
    @include('aos.pdf.blocks.list')
    @include('aos.pdf.blocks.details')
    @include('aos.pdf.blocks.license')
    @include('aos.pdf.blocks.signature')
</body>
</html>

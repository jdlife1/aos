<table align="left" border="0" cellspacing="0" cellpadding="0">
  <tr style="color:#002aa0;">
    <th>Party One Signature: </th>
    <th>Party Two Signature: </th>
  </tr>
  <tr>
    <td>
      @unless (empty($letter->client_signature))
        <img src="{!! $letter->client_signature !!}" class="img--sginature">
        <p class="font--small">{{ $letter->created_at->format('d/m/Y') }}</p>
      @else
        <p></p>
      @endunless
    </td>
    <td>
      @unless (empty($letter->partner_signature))
        <img src="{{ $letter->partner_signature }}" class="img--sginature">
        <p class="font--small">{{ $letter->created_at->format('d/m/Y') }}</p>
      @else
        <p></p>
      @endunless
    </td>
  </tr>
</table>

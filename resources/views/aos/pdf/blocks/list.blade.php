<h1 style="color:#002aa0;">Letter of Authority</h1>
<ul>
    <li style="color:#002aa0;">✔ &emsp; Existing Insurance Policy Schedule (including benefit information, loadings &amp; exclusions)</li>
    <li style="color:#002aa0;">✔ &emsp; ACC 1766 (ACC Giving access to your ACC information)</li>
    <li style="color:#002aa0;">✔ &emsp; ACC 5937 (ACC Authority to Act)</li>
    <li style="color:#002aa0;">✔ &emsp; Financial Practitioner Engagement</li>
    <li style="color:#002aa0;">✔ &emsp; Medical Information Request</li>
</ul>

<table class="kevin" cellspacing="0" cellpadding="0" border="0">
    <thead>
        <tr>
            <th>Party One</th>
                <th></th>
            <th>Party Two</th>
                <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
Name:
            </td>
                <td>
                    {{ $letter->client_full_name }}
                </td>
            <td>
Name:
            </td>
                <td>
                    {{ $letter->partner_name }}
                </td>
        </tr>

        <tr>
            <td>
Date of Birth:
            </td>
                <td>
                    {{ $letter->client_date_of_birth->format('d/m/Y') }}
                </td>
            <td>
Date of Birth:
            </td>
                <td>
                    {{ $letter->partner_date_of_birth->format('d/m/Y') }}
                </td>
        </tr>

        <tr>
            <td>
IRD/ACC Number:
            </td>
                <td>
                    {{ $letter->acc_or_ird_number_personal }}
                </td>
            <td>
IRD/ACC Number:
            </td>
                <td>
                    {{ $letter->partner_acc_or_ird_number_personal }}
                </td>
        </tr>

        <tr>
            <td>
Company Name:
            </td>
                <td>
                    {{ $letter->company_full_name }}
                </td>
            <td>
Company Name:
            </td>
                <td>
                    {{ $letter->company_full_name }}
                </td>
        </tr>

        <tr>
            <td>
Address:
            </td>
                <td>
                    {{ $letter->client_address }}
                </td>
            <td>
Address:
            </td>
                <td>
                    {{ $letter->partner_address }}
                </td>
        </tr>

        <tr>
            <td>
Email:
            </td>
                <td>
                    {{ $letter->client_email_address }}
                </td>
            <td>
Email:
            </td>
                <td>
                    {{ $letter->partner_email_address }}
                </td>
        </tr>

        <tr>
            <td>
Phone:
            </td>
                <td>
                    {{ $letter->client_mobile_number }}
                </td>
            <td>
Phone:
            </td>
                <td>
                    {{ $letter->partner_mobile_number }}
                </td>
        </tr>
    </tbody>
</table>
<hr>

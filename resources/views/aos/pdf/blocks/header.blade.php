<table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th colspan="4" valign="baseline">&nbsp;
<h3 color="red"><u>MAIN NOMINATED REPRESENTATIVE</u></h3>
            </th>
            <th>
                <img src="./assets/images/jd-life-logo.png" width="120">
            </th>
        </tr>
        <tr>
            <td width="30%" style="font-size: 90%;">
<h4>Name of Financial Adviser:</h4>
            </td>
<td width="70%">Jaspal (Jaz) Singh Dosanjh</td>
        </tr>
        <tr>
            <td width="30%" style="font-size: 90%;">
<h4>Relationship:</h4>
            </td>
<td width="70%">Consultant &amp; Financial Risk Adviser </td>
        </tr>
        <tr>
            <td width="30%" style="font-size: 90%;">
<h4>FSPR Number:</h4>
            </td>
<td>FSP417986</td>
        </tr>
        <tr>
            <td width="30%" style="font-size: 90%;">
<h4>Trading Name:</h4>
            </td>
<td>JD Life Ltd</td>
        </tr>
        <tr>
            <td width="30%" style="font-size: 90%;">
<h4>Email address:</h4>
            </td>
<td>service@jdlife.co.nz</td>
        </tr>
        <tr>
            <td width="30%" style="font-size: 90%;">
<h4>Telephone number:</h4>
            </td>
<td>027 385 8666</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>
                {{ $letter->created_at->format('d/m/Y') }}
            </td>
            <td>
            </td>
        </tr>
</table>

<table>
    <tbody>
        <tr>
            <td style="text-align: justify;">
                <span style="font-size: 94%">
                    I/We authorise JD Life Ltd to act as my/our agent for ACC levy purposes and for all associated entities. This authorisation allows JD Life Ltd to
                    query and change information on my/our ACC levy account(s) through ACC staff, and through MyACC for Business. This authority will also allow
                    the organisations’ main representative discretion to delegate access to my/our ACC information to other members of the organisation. Other
                    delegated members of the organisation will also be able to query and change information on my/our ACC levy account(s).
                    ACC 1766 Access and change information through ACC Online and directly with ACC Staff: This allows JD Life and its representatives to access
                    and change your ACC levy account online and at their discretion delegate access to your information to other members of the Organisation. This
                    will also allow each member of JD Life to query and change your ACC levy account information through ACC staff. ACC 1766 Declaration: I
                    authorise ACC to carry out or initiate transactions in accordance with this authority. I understand that ACC is not liable for any action done in
                    accordance with this authority. I understand that this authority comes into effect from the date the ACC Business Service Centre receives and
                    processes this form. I understand that by providing authority to JD Life, I am providing authorisation to each representative within JD Life. I
                    understand that I am giving my representative authority to access my account by telephone, email, letter, fax, form or otherwise indicated. I
                    understand that the cancellation of this authority must be made in writing or by telephone. It will not be effective until received by the ACC
                    Business Service Centre. I understand that information provided on this form will only be issued to fulfill the requirements of the Accident
                    Compensation Act 2001, and that ACC complies at all times with the Privacy Act 1993, and the Official Information Act 1982. ACC 5937 – This
                    authority to act covers all my claims currently managed by ACC. Furthermore, I/we acknowledge ACC 5937 Declaration: I authorise ACC to act on
                    the instructions of my nominated person and I understand that ACC is not responsible for any actions of my nominated person using this
                    authority. I understand that this authority comes into effect from the date ACC receives this form. I understand that I am giving my nominated
                    person authority to access my information by telephone, email and letter. I understand I can write to or call ACC at any time to cancel this
                    authority, and ACC will only cancel this authority if I ask them to in this way. Cancellation will not be effective until received by ACC.
                    I/we authorise, nominate JD Life & their representatives to attain my personal medical records/financial information including existing insurance
                    policy schedule, benefit information, alterations to standard policy terms(loadings & or exclusions & or medical information rationale), claims
                    history, accessing my ACC levy account information(from a medical practitioner, specialist, hospital, clinic, counsellor, psychologist, therapist,
                    dentist, alternative health practitioner, Accident Compensation Corporation, legal counsel, accountant and or any other similar organisations)
                    and to release these medical records/financial information to various insurance providers in order to assess my eligibility of attaining insurance
                    coverage. In relation to any other people named on my policy, I confirm that: I am authorised to complete this form on their behalf; I am
                    authorised to disclose to ACC & any relevant insurance provider and to receive from any relevant insurance their personal (including policy
                    schedules, medical, exclusion and loadings) and health information. I have made each of them aware of the contents of this form. Please provide
                    in a timely manner my medical records/financial information and via the medium JD Life Ltd request, whether that be physical copy, via email or
                    both. This authority replaces and revokes any previous authorities given or implied to any agent or adviser. I understand that JD Life comply with
                    the Privacy Act 1993, Health Information Privacy Code 1994, the Official Information Act 1982 and comply with the requirements of the Accident
                    Compensation Act 2001. I do not wish to attend the medical practice/financial/legal practitioner for a visit in order for JD Life to attain my medical
                    records/financial information. I understand that I elect JD Life as my agent to request all information on my behalf, should there be a fee
                    applicable, I/we shall be solely responsible for such a fee incurred. I understand however that under Rule 6 (Right to access personal information)
                    of the Requests for Personal Information by the individual concerned that private sector health agencies generally cannot charge for services for
                    making information available in response to a request under rule 6 other than a reasonable charge for repeat requests (same information
                    requested within last 12 months). They may also make a reasonable charge for providing a copy of an X-ray, video recording or CAT/PET/MRI
                    photo. This charge recognises the expense of copying media, and would not apply if the copy of the photo or recording was provided in an
                    expensive digital form (such as a high resolution scan on a portable storage device like a DVD). Otherwise, a private sector health agency cannot
                    charge for making information available in response to a request under rule 6.
                </span>
            </td>
        </tr>
    </tbody>
</table>

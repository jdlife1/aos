export default {
  props: {
    w: {
      type: String,
      default: '100%'
    },
    h: {
      type: String,
      default: '50vh'
    },
    amount: {
      type: [String, Number],
      required: true
    }
  }
}

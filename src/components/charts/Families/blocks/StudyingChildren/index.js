import Forty from './Forty'
import Eighty from './Eighty'
import OneHundred from './OneHundred'

export {
  Forty,
  Eighty,
  OneHundred
}

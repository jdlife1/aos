import NoChild from './NoChild'
import GreaterThanTwoChildren from './GreaterThanTwoChildren'
import LessThanTwoChildren from './LessThanTwoChildren'
import StudyingChildren from './StudyingChildren'

export {
  NoChild,
  LessThanTwoChildren,
  GreaterThanTwoChildren,
  StudyingChildren
}

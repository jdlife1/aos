export default {
  props: {
    w: {
      type: String,
      default: '100%'
    },
    h: {
      type: String,
      default: '70%'
    },
    coverPlusAmount: {
      type: [String, Number],
      required: true
    },
    coverPlusExtraAmount: {
      type: [String, Number],
      required: true
    },
    changesInCover: {
      type: [String, Number],
      required: true
    }
  }
}

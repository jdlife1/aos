import WithTaxChart from './With'
import WithoutTaxChart from './Without'

export {
  WithTaxChart,
  WithoutTaxChart
}

export default {
  props: {
    w: {
      type: String,
      default: '100%'
    },
    h: {
      type: String,
      default: '50vh'
    },
    amount: {
      type: [String, Number],
      required: true
    },
    viewBox: {
      type: String,
      default: '0 0 592 895'
    }
  }
}

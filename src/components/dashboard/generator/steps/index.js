import LoaGenerate from './LoaGenerate'
import AccOffsets from './AccOffsets/Index'
import TaxSplitting from './TaxSplitting/Index'
import ImportantInfo from './ImportantInfo/Index'
import FatalEntitlements from './FatalEntitlements'
import CommonTaxIssues from './CommonTaxIssues/Index'
import CompanyAndIncomeDetails from './CompanyAndIncomeDetails/Index'

export {
  AccOffsets,
  LoaGenerate,
  TaxSplitting,
  ImportantInfo,
  CommonTaxIssues,
  FatalEntitlements,
  CompanyAndIncomeDetails
}

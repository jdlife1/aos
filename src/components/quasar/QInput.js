import { QInput } from 'quasar'

export default {
  extends: QInput,
  props: {
    color: {
      type: String,
      default: 'positive'
    }
  },
  computed: {
    isTel: function isTel () {
      return this.type === 'tel'
    },
    pattern: function pattern () {
      if (this.isNumber || this.isTel) {
        return '[0-9]*'
      }
    }
  }
}

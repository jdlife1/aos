import Datepicker from 'vuejs-datepicker'

export default {
  name: 'vuejs-datepicker',
  extends: Datepicker,
  props: {
    typeable: {
      type: Boolean,
      default: true
    },
    inline: {
      type: Boolean,
      default: false
    },
    inputClass: {
      type: String,
      default: 'col q-input-target q-no-input-spinner'
    },
    placeholder: {
      type: String,
      default: 'DD/MM/YYYY'
    },
    format: {
      type: String,
      default: 'dd/MM/yyyy'
    },
    cleaveOptions: {
      type: Object,
      default: () => {
        return {
          date: true,
          datePattern: ['d', 'm', 'Y'],
          delimiter: '/'
        }
      }
    }
  }
}

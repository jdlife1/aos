import Vue from 'vue'
import VueRouter from 'vue-router'
import { LocalStorage } from 'quasar'
import { CONSTANT_STORAGE } from 'src/config/constants'

Vue.use(VueRouter)

/**
 * View Components
 */
import MasterLayout from '@/Master'
import AuthIndexComponent from '@/auth/Index'
import DashboardLayout from '@/dashboard/Layout'
import DashboardIndexComponent from '@/dashboard/Index'
import GeneratorIndexComponent from '@/dashboard/generator/Index'

/*
 * Uncomment this section and use "load()" if you want
 * to lazy load routes.
function load (component) {
  // '@' is aliased to src/components
  return () => import(`@/${component}.vue`)
}
*/

const router = new VueRouter({
  /*
   * NOTE! VueRouter "history" mode DOESN'T works for Cordova builds,
   * it is only to be used only for websites.
   *
   * If you decide to go with "history" mode, please also open /config/index.js
   * and set "build.publicPath" to something other than an empty string.
   * Example: '/' instead of current ''
   *
   * If switching back to default "hash" mode, don't forget to set the
   * build publicPath back to '' so Cordova builds work again.
  */
  mode: 'hash',
  routes: [
    {
      path: '/',
      name: 'index',
      component: MasterLayout,
      redirect: {name: 'auth'},
      children: [
        {
          path: 'auth',
          name: 'auth',
          component: AuthIndexComponent
        }
      ]
    },
    {
      path: '/dashboard',
      component: DashboardLayout,
      meta: {requiresAuth: true},
      children: [
        {
          path: '/',
          name: 'dashboard.index',
          component: DashboardIndexComponent,
          meta: {requiresAuth: true}
        },
        {
          path: 'generator/:id?',
          name: 'dashboard.generator.index',
          component: GeneratorIndexComponent,
          meta: {requiresAuth: true}
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!LocalStorage.has(CONSTANT_STORAGE.USER_MODEL) && !LocalStorage.has(CONSTANT_STORAGE.USER_ACCESS_TOKEN)) {
      next({
        path: '/auth',
        query: {
          redirect: to.fullPath
        }
      })
    }
    else {
      next()
    }
  }
  else {
    next()
  }
})

export default router

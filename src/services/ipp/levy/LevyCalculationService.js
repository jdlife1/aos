/* eslint-disable */
import { HTTP } from 'src/services/http/http';
import { isEmpty, isObject, get } from 'lodash';
import { floatFixer, floatDiff } from 'src/config/utils';
import BaseLevyService from 'src/services/aos/levy/BaseLevyService';

export default class LevyCalculationService {
  constructor(cover, income, cuccode, type) {
    if (isObject(cover) && !isEmpty(cover)) {
      const opts = cover;
      this.cover = floatFixer(get(opts, 'cover', 0));
      this.income = floatFixer(get(opts, 'income', 0));
      this.cuccode = window.Number(get(opts, 'cuccode', 0));
      this.type = get(opts, 'type', 'coverplus_cover');
    }
    else {
      this.cover = floatFixer(cover);
      this.income = floatFixer(income);
      this.type = type || 'coverplus_cover';
      this.cuccode = window.Number(cuccode);
    }
    this.response = {};
  }

  async requestCalculations() {
    const { cover, income, cuccode } = this;
    const { data } = await HTTP(`${process.env.IPP_ENDPOINT}/api`).post('acc/calculations', { cover, income, cuccode });
    this.setResponse(data);
    return data;
  }

  setResponse(payload) {
    this.response = payload;

    return this;
  }

  getResponse() {
    return this.response;
  }

  hydrateValues(type = this.type, response = this.response) {
    if (!['coverplus_cover', 'coverplus_extra_standard'].includes(type)
      || isEmpty(response)) return BaseLevyService.getDefaultValues();
    const nominatedCoverAmount = get(response, ['nominated_cover_amount', type], 0);
    const amountPayable = get(response, ['total_cover_plus_levy_excluding_gst', type], 0);
    const amountPayableIncludingGst = get(response, ['total_amount_payable_to_acc_including_gst', type], 0);
    const { rawNominatedCoverAmount, currentIncomeFromBusiness, classificationUnitCode } = this;
    return {
      nominatedCoverAmount,
      classificationUnitCode,
      rawNominatedCoverAmount,
      currentIncomeFromBusiness,
      totalAmountPayableToAcc: amountPayable,
      totalAmountPayableToAccIncludingGst: amountPayableIncludingGst,
      netTotalWorkLevy: get(response, ['net_total_work_levy', type], 0),
      workLevy: get(response, ['work_levy_current_portion_excluding_gst', type], 0),
      earnersLevy: get(response, ['earners_levy_current_portion_excluding_gst', type], 0),
      totalCoverPlusLevy: get(response, ['total_cover_plus_levy_excluding_gst', type], 0),
      workingSaferLevy: get(response, ['net_work_levy_current_portion_excluding_gst', type], 0),
      incomeDifferenceToNominatedAmount: floatDiff(nominatedCoverAmount, rawNominatedCoverAmount),
    };
  }

  get rawNominatedCoverAmount() {
    return this.cover;
  }

  get currentIncomeFromBusiness() {
    return this.income;
  }

  get classificationUnitCode() {
    return this.cuccode;
  }
}

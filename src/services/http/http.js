import axios from 'axios'
import router from 'src/router'
import store from 'store'
import {
  Toast,
  LocalStorage
} from 'quasar'
import { CONSTANT_STORAGE } from 'src/config/constants'

const HTTP_API = (baseURL = null) => {
  let accessToken = LocalStorage.get.item(CONSTANT_STORAGE.USER_ACCESS_TOKEN).access_token

  const httpInstance = axios.create({
    baseURL: baseURL || `${process.env.LARAVEL_API_ENDPOINT}/api`,
    headers: {
      'Authorization': `Bearer ${accessToken}`
    }
  })

  httpInstance.interceptors.response.use((response) => {
    return response
  }, function (error) {
    if (error.response.status === 401) {
      Toast.create.negative('Unauthorized')
      store.commit('user/REMOVE_CURRENT_USER_TOKEN')
      store.commit('user/REMOVE_SET_CURRENT_USER_MODEL')
      router.replace('/auth')
    }
    else if (error.response.status === 422) {
      Toast.create.negative(error.response.data.message)
    }
    else if (error.response.status === 500 || error.response.status === 501) {
      Toast.create.negative('Server Error')
    }
    return Promise.reject(error.response)
  })

  return httpInstance
}

const HTTP = (baseURL = null) => {
  const httpInstance = axios.create({
    baseURL: baseURL || process.env.LARAVEL_API_ENDPOINT
  })

  httpInstance.interceptors.response.use((response) => {
    return response
  }, function (error) {
    if (error.response.status === 401) {
      Toast.create.negative('Unauthorized')
    }
    return Promise.reject(error.response)
  })

  return httpInstance
}

export {
  HTTP,
  HTTP_API
}

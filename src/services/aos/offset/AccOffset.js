/* eslint-disable */
import TaxService from 'src/services/aos/tax/TaxService';
import { floatFixer, floatTotals } from 'src/config/utils';
import LiabilityRates from 'src/store/modules/liabilityRates/state';

class AccOffset {
  setCurrentIncomeFromBusiness(amount) {
    this.currentIncomeFromBusiness = amount;

    return this;
  }

  getCurrentIncomeFromBusiness() {
    return floatFixer(this.currentIncomeFromBusiness);
  }

  setOffsetRates(offsets) {
    this.offsetRates = offsets;

    return this;
  }

  getOffsetRates() {
    return this.offsetRates;
  }

  getMaximumIncomeProtection() {
    // eslint-disable-next-line
    return floatFixer(this.getCurrentIncomeFromBusiness() * this.getOffsetRates().maximum_income_protection_indemnity);
  }

  getMaximumMonthlyMortgageRepaymentCover() {
    // eslint-disable-next-line
    return floatFixer(this.getCurrentIncomeFromBusiness() * this.getOffsetRates().maximum_monthly_mortgage_repayment_cover);
  }

  // eslint-disable-next-line
  getMaximumAccCoverPlusCover() {
    const { taxes: { income, levels } } = LiabilityRates();
    // eslint-disable-next-line
    const liableEarnings = floatFixer(this.getCurrentIncomeFromBusiness() * this.getOffsetRates().maximum_acc_cover_plus_cover);

    const { totalTax } = (new TaxService())
      .setTaxRateLevels(levels)
      .setTaxRateIncome(income)
      .setCurrentIncomeFromBusiness(liableEarnings)
      .getAll();

    return (liableEarnings - totalTax);
  }

  getMaximumAccCoverPlusCoverWithoutTax() {
    return floatFixer(this.getCurrentIncomeFromBusiness() *
      this.getOffsetRates().maximum_acc_cover_plus_cover);
  }

  getTotalClaimAmount() {
    // eslint-disable-next-line
    return floatTotals(this.getMaximumAccCoverPlusCover(), this.getMaximumMonthlyMortgageRepaymentCover());
  }

  getAll() {
    return {
      maximumIncomeProtection: this.getMaximumIncomeProtection(),
      maximumAccCoverPlusCoverWithoutTax: this.getMaximumAccCoverPlusCoverWithoutTax(),
      maximumMonthlyMortgageRepaymentCover: this.getMaximumMonthlyMortgageRepaymentCover(),
      maximumAccCoverPlusCover: this.getMaximumAccCoverPlusCover(),
      totalClaimAmount: this.getTotalClaimAmount(),
    };
  }
}

export default AccOffset;

import BaseLevyService from './BaseLevyService'
import { floatFixer } from 'src/config/utils'

export default class CoverPlusExtraService extends BaseLevyService {
  getNominatedCoverAmount () {
    // eslint-disable-next-line
    return floatFixer(this.nominatedCoverAmount)
  }

  getWorkLevy () {
    // eslint-disable-next-line
    return floatFixer(this.getNominatedCoverAmount() / 100 * parseFloat(this.classificationUnit.cover_plus_extra))
  }

  getEarnersLevy () {
    // eslint-disable-next-line
    return floatFixer(this.getNominatedCoverAmount() / 100 * this.earners_levy_fix_rate / parseFloat(this.liable_earning_rate))
  }
}

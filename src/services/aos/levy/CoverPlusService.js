import BaseLevyService from './BaseLevyService'
import { floatFixer } from 'src/config/utils'

export default class CoverPlusService extends BaseLevyService {
  getNominatedCoverAmount () {
    // eslint-disable-next-line
    return floatFixer(this.getCurrentIncomeFromBusiness() * parseFloat(this.liable_earning_rate))
  }

  getWorkLevy () {
    // eslint-disable-next-line
    return floatFixer(this.getCurrentIncomeFromBusiness() / 100 * parseFloat(this.classificationUnit.cover_plus))
  }

  getEarnersLevy () {
    // eslint-disable-next-line
    return floatFixer(this.getCurrentIncomeFromBusiness() / 100 * this.earners_levy_fix_rate)
  }
}

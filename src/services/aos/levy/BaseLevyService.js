import LeviableContract from './contracts/LeviableContract'
import { floatFixer, floatTotals } from 'src/config/utils'

export default class BaseLevyService extends LeviableContract {
  /* eslint-disable */
  setRateValues (rateValues) {
    let { liable_earning_rate, earners_levy_fix_rate, total_amount_payable_rate } = rateValues
    this.liable_earning_rate = liable_earning_rate
    this.earners_levy_fix_rate = earners_levy_fix_rate
    this.total_amount_payable_rate = total_amount_payable_rate

    return this
  }
  /* eslint-enable */

  setCurrentIncomeFromBusiness (amount) {
    this.currentIncomeFromBusiness = amount

    return this
  }

  getCurrentIncomeFromBusiness () {
    return floatFixer(this.currentIncomeFromBusiness)
  }

  setNominatedCoverAmount (amount) {
    this.nominatedCoverAmount = amount

    return this
  }

  setClassificationUnit (classificationUnit) {
    this.classificationUnit = classificationUnit

    return this
  }

  getNetTotalWorkLevy () {
    // eslint-disable-next-line
    return floatTotals(floatFixer(this.getWorkLevy()), floatFixer(this.getEarnersLevy()))
  }

  getWorkingSaferLevy () {
    // eslint-disable-next-line
    return floatFixer(this.getCurrentIncomeFromBusiness() / 1000) * this.liable_earning_rate
  }

  geTotalCoverPlusLevy () {
    // eslint-disable-next-line
    return floatTotals(this.getNetTotalWorkLevy(), this.getWorkingSaferLevy())
  }

  getTotalAmountPayableToAcc () {
    // eslint-disable-next-line
    return floatFixer(floatTotals(this.getNetTotalWorkLevy(), this.getWorkingSaferLevy()) * floatFixer(this.total_amount_payable_rate))
  }

  getTotalAmountPayableToAccIncludingGst () {
    return floatFixer(this.getTotalAmountPayableToAcc() * 1.15)
  }

  static getDefaultValues () {
    return {
      workLevy: 0,
      earnersLevy: 0,
      workingSaferLevy: 0,
      netTotalWorkLevy: 0,
      totalCoverPlusLevy: 0,
      nominatedCoverAmount: 0,
      rawNominatedCoverAmount: 0,
      totalAmountPayableToAcc: 0,
      currentIncomeFromBusiness: 0,
      incomeDifferenceToNominatedAmount: 0,
      totalAmountPayableToAccIncludingGst: 0,
      classificationUnitCode: 0
    }
  }

  getAll () {
    return {
      workLevy: this.getWorkLevy(),
      earnersLevy: this.getEarnersLevy(),
      workingSaferLevy: this.getWorkingSaferLevy(),
      netTotalWorkLevy: this.getNetTotalWorkLevy(),
      totalCoverPlusLevy: this.geTotalCoverPlusLevy(),
      nominatedCoverAmount: this.getNominatedCoverAmount(),
      totalAmountPayableToAcc: this.getTotalAmountPayableToAcc(),
      currentIncomeFromBusiness: this.getCurrentIncomeFromBusiness(),
      totalAmountPayableToAccIncludingGst: this.getTotalAmountPayableToAccIncludingGst()
    }
  }
}

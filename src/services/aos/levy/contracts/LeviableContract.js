export default class LeviableContract {
  getNominatedCoverAmount () {
    throw new Error('Method must be implemented')
  }

  getWorkLevy () {
    throw new Error('Method must be implemented')
  }

  getEarnersLevy () {
    throw new Error('Method must be implemented')
  }

  getNetTotalWorkLevy () {
    throw new Error('Method must be implemented')
  }

  getWorkingSaferLevy () {
    throw new Error('Method must be implemented')
  }

  geTtotalCoverPlusLevy () {
    throw new Error('Method must be implemented')
  }

  getTotalAmountPayableToAcc () {
    throw new Error('Method must be implemented')
  }
}

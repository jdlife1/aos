/* eslint-disable */
import { upperFirst, camelCase } from 'lodash';
import {
  floatDiff,
  floatFixer,
  floatTotals,
  floatTaxTotal,
} from 'src/config/utils';
import PercentageRange from 'src/services/aos/providers/PercentageRange';
import AiaTaxCalculation from 'src/services/aos/tax/insurance/Aia';
import FidelityTaxCalculation from 'src/services/aos/tax/insurance/Fidelity';

export default class TaxService {
  constructor() {
    this.isUsingIndemnityAmount = false;
  }

  setInsuranceProvider(provider) {
    this.insuranceProvider = provider;

    return this;
  }

  getInsuranceProvider() {
    return this.insuranceProvider;
  }

  setCurrentIncomeFromBusiness(amount) {
    this.currentIncomeFromBusiness = amount;

    return this;
  }

  getCurrentIncomeFromBusiness() {
    return floatFixer(this.currentIncomeFromBusiness, 0);
  }

  setTaxRateLevels(levels) {
    this.taxRateLevels = levels;

    return this;
  }

  getTaxRateLevels() {
    return this.taxRateLevels;
  }

  setTaxRateIncome(income) {
    this.taxRateIncome = income;

    return this;
  }

  getTaxRateIncome() {
    return this.taxRateIncome;
  }

  setIsUsingIndemnityAmount() {
    this.isUsingIndemnityAmount = true;

    return this;
  }

  getIsUsingIndemnityAmount() {
    return this.isUsingIndemnityAmount;
  }

  getIncomeProtectionIndemnityAmount() {
    // eslint-disable-next-line
    return floatFixer(this.getCurrentIncomeFromBusiness() * this.getTaxRateIncome().income_protection_indemnity);
  }

  getIncomeProtectionAgreedAmount() {
    const type = `calculate${upperFirst(camelCase(this.getInsuranceProvider()))}`;
    const income = this.getCurrentIncomeFromBusiness();
    if (typeof this.constructor[type] === 'function') return floatFixer(this.constructor[type](income));
    const { income_protection_agreed: incomeAgreed } = this.getTaxRateIncome();
    return floatFixer(income * incomeAgreed);
  }

  static calculateFidelity(income) {
    return (new FidelityTaxCalculation(income)).totalAgreedValue;
  }

  static calculatePartnersLife(income) {
    try {
      return (income * (new PercentageRange(income)).partnersLife());
    }
    catch (e) {} // eslint-disable-line
    return PercentageRange.partnersLifeLimit();
  }

  static calculateAia(income) {
    return (new AiaTaxCalculation(income)).totalAgreedValue;
  }

  get determinedIncomeProtectionIndemnityAmount() {
    return this.getIsUsingIndemnityAmount()
      ? this.getIncomeProtectionIndemnityAmount()
      : this.getCurrentIncomeFromBusiness();
  }

  getCalculatedTax() {
    const levels = this.getTaxRateLevels();
    const income = this.determinedIncomeProtectionIndemnityAmount;
    let netIncome = 0;

    if (income > levels.fourth.income_requirement.max) {
      {
        // console.log('income > levels.fourth.income_requirement.max')
        netIncome = floatDiff(income, levels.fourth.income_requirement.max);
        const taxResults = [
          floatTaxTotal(levels.first.income_requirement.max, levels.first.rate),
          floatTaxTotal(levels.second.income_requirement.max, levels.second.rate),
          floatTaxTotal(levels.third.income_requirement.min, levels.third.rate),
          floatTaxTotal(netIncome, levels.fourth.rate),
        ];
        return {
          rates: [
            levels.first.rate,
            levels.second.rate,
            levels.third.rate,
            levels.fourth.rate,
          ],
          brackets: [
            levels.first.income_requirement.max,
            levels.second.income_requirement.max,
            levels.third.income_requirement.min,
            levels.fourth.income_requirement.min,
          ],
          taxResults,
          total: floatTotals(taxResults),
        };
      }
    }

    if (income > levels.first.income_requirement.max // eslint-disable-line
      && income > levels.third.income_requirement.max // eslint-disable-line
      && income > levels.third.income_requirement.max) { // eslint-disable-line
      {
        netIncome = floatDiff(income, levels.fourth.income_requirement.max);
        const taxResults = [
          floatTaxTotal(levels.first.income_requirement.max, levels.first.rate),
          floatTaxTotal(levels.second.income_requirement.max, levels.second.rate),
          floatTaxTotal(levels.third.income_requirement.min, levels.third.rate),
          floatTaxTotal(netIncome, levels.fourth.rate),
        ];
        return {
          rates: [
            levels.first.rate,
            levels.second.rate,
            levels.third.rate,
            levels.fourth.rate,
          ],
          brackets: [
            floatFixer(levels.first.income_requirement.max),
            floatFixer(levels.second.income_requirement.max),
            floatFixer(levels.third.income_requirement.min),
            floatFixer(netIncome),
          ],
          taxResults,
          total: floatTotals(taxResults),
        };
      }
    }

    if (income >= levels.first.income_requirement.max
      && income > levels.third.income_requirement.max) {
      // console.log('income >= levels.first.income_requirement.max')
      {
        netIncome = floatDiff(income, levels.third.income_requirement.max);
        const taxResults = [
          floatTaxTotal(levels.first.income_requirement.max, levels.first.rate),
          floatTaxTotal(levels.second.income_requirement.max, levels.second.rate),
          floatTaxTotal(netIncome, levels.third.rate),
          0,
        ];
        return {
          rates: [
            levels.first.rate,
            levels.second.rate,
            levels.third.rate,
            0,
          ],
          brackets: [
            floatFixer(levels.first.income_requirement.max),
            floatFixer(levels.second.income_requirement.max),
            floatFixer(netIncome),
            0,
          ],
          taxResults,
          total: floatTotals(taxResults),
        };
      }
    }

    if (income > levels.first.income_requirement.max) {
      {
        netIncome = floatDiff(income, levels.first.income_requirement.max);
        const taxResults = [
          floatTaxTotal(levels.first.income_requirement.max, levels.first.rate, 0),
          floatTaxTotal(netIncome, levels.second.rate),
          0,
          0,
        ];
        return {
          rates: [
            levels.first.rate,
            levels.second.rate,
            0,
            0,
          ],
          brackets: [
            floatFixer(levels.first.income_requirement.max),
            floatFixer(netIncome),
            0,
            0,
          ],
          taxResults,
          total: floatTotals(taxResults),
        };
      }
    }

    netIncome = income;
    const taxResults = [
      floatTaxTotal(netIncome, levels.first.rate),
      0,
      0,
      0,
    ];
    return {
      rates: [
        levels.first.rate,
        0,
        0,
        0,
      ],
      brackets: [
        floatFixer(netIncome),
        0,
        0,
        0,
      ],
      taxResults,
      total: floatTotals(taxResults),
    };
  }

  getTotalCalculatedTaxAmount() {
    const { total } = this.getCalculatedTax();
    return total;
  }

  getTotalClaimAmount() {
    const precision = 0;
    const calculatedTaxAmount = this.getTotalCalculatedTaxAmount();
    const indemnityAmount = this.getIncomeProtectionIndemnityAmount();
    return floatDiff(indemnityAmount, calculatedTaxAmount, precision);
  }

  getAll() {
    const {
      total,
      rates,
      brackets,
      taxResults,
    } = this.getCalculatedTax();
    return {
      taxResults,
      taxRates: rates,
      totalTax: total,
      taxBrackets: brackets,
      totalClaimAmount: this.getTotalClaimAmount(),
      insuranceProvider: this.getInsuranceProvider(),
      isUsingIndemnityAmount: this.getIsUsingIndemnityAmount(),
      incomeProtectionAgreedAmount: this.getIncomeProtectionAgreedAmount(),
      incomeProtectionIndemnityAmount: this.getIncomeProtectionIndemnityAmount(),
      determinedIncomeProtectionIndemnityAmount: this.determinedIncomeProtectionIndemnityAmount,
    };
  }
}

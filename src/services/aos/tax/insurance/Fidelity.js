/* eslint-disable */
import { take } from 'lodash';
import { floatTotals, floatFixer } from 'src/config/utils';

export default class Fidelity {
  constructor(income, mortgageAmount = 0, unearnedAmount = 0) {
    this.income = income;
    this.unearnedOrInvestmentIncome = unearnedAmount;
    this.mortgageAndIncomeProtection = mortgageAmount;
  }

  setIncome(income) {
    this.income = income;

    return this;
  }

  getIncome() {
    return this.income;
  }

  setMortgageAndIncomeProtection(mortgageAndIncomeProtection) {
    this.mortgageAndIncomeProtection = mortgageAndIncomeProtection;

    return this;
  }

  getMortgageAndIncomeProtection() {
    return this.mortgageAndIncomeProtection;
  }

  getUnearnedOrInvestmentIncome() {
    return this.unearnedOrInvestmentIncome;
  }

  get totalAdjustedNetIncome() {
    return (this.getUnearnedOrInvestmentIncome() > (this.getIncome() * 0.10))
      ? (this.getIncome() + this.getUnearnedOrInvestmentIncome()) : this.getIncome();
  }

  get lessIncomeAllocatedToMIP() {
    return (this.getMortgageAndIncomeProtection() / 0.625);
  }

  get totalLessIncomeAllocatedToMIP() {
    return (this.lessIncomeAllocatedToMIP > this.getIncome())
      ? 0 : this.getIncome() - this.lessIncomeAllocatedToMIP;
  }

  get determinedMortgageAndIncomeProtection() {
    return this.getMortgageAndIncomeProtection()
      || (this.getIncome() * 0.40);
  }

  get totalLessUnearnedOrInvestmentIncome() {
    return (this.getUnearnedOrInvestmentIncome() > (this.getIncome() * 0.10))
      ? this.getUnearnedOrInvestmentIncome() : 0;
  }

  // Result property
  get calculations() {
    const totals = [];

    if (this.totalAdjustedNetIncome < 70001) {
      {
        const value = this.totalAdjustedNetIncome - this.lessIncomeAllocatedToMIP;
        totals.push({
          value,
          total: value * 0.625,
        });
      }
    }
    else {
      {
        const value = 70000 - this.lessIncomeAllocatedToMIP;
        totals.push({
          value,
          total: value * 0.625,
        });
      }
    }

    if (this.totalAdjustedNetIncome > 100000) {
      {
        const value = 30000;
        totals.push({
          value,
          total: value * 0.6,
        });
      }
    }
    else {
      {
        const value = (
          this.totalAdjustedNetIncome - (totals[0].value + this.lessIncomeAllocatedToMIP)
        );
        totals.push({
          value,
          total: value * 0.6,
        });
      }
    }

    if (this.totalAdjustedNetIncome > 320000) {
      {
        const value = 220000;
        totals.push({
          value,
          total: value * 0.55,
        });
      }
    }
    else {
      {
        const value = (
          this.totalAdjustedNetIncome -
          (this.lessIncomeAllocatedToMIP +
            floatTotals((take(totals, 2).map(({ value: sum }) => sum))))
        );
        totals.push({
          value,
          total: value * 0.55,
        });
      }
    }

    if (this.totalAdjustedNetIncome > 560000) {
      {
        const value = 240000;
        totals.push({
          value,
          total: value * 0.35,
        });
      }
    }
    else {
      {
        const value = (
          this.totalAdjustedNetIncome -
          (this.lessIncomeAllocatedToMIP +
            floatTotals((take(totals, 3).map(({ value: sum }) => sum))))
        );
        totals.push({
          value,
          total: value * 0.35,
        });
      }
    }

    if (this.totalAdjustedNetIncome > 560000) {
      {
        const value = this.totalAdjustedNetIncome - 560000;
        totals.push({
          value,
          total: value * 0.2,
        });
      }
    }
    else {
      totals.push({
        value: 0,
        total: 0 * 0.2,
      });
    }

    return totals;
  }

  get totalAgreedValue() {
    return floatFixer(floatTotals(this.calculations.map(({ total }) => total)));
  }

  get totalMaximumBenefit() {
    return (this.totalAgreedValue < 0)
      ? 0 : this.totalAgreedValue - this.totalLessUnearnedOrInvestmentIncome;
  }
}

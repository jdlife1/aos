import { floatFixer, floatTotals, floatDiff } from 'src/config/utils'
import AccOffset from 'src/services/aos/offset/AccOffset'
import _ from 'lodash'

export default class PassiveIncome extends AccOffset {
  setArrayOfPassiveIncomes (incomes) {
    this.totalPassiveIncome = floatTotals(_.map(incomes, (n) => {
      if (!_.isNull(n)) {
        return floatFixer(n)
      }
    }))

    return this
  }

  getTotalPassiveIncome () {
    return window.parseInt(floatFixer(this.totalPassiveIncome))
  }

  setTaxRateIncome (income) {
    this.taxRateIncome = income

    return this
  }

  getTaxRateIncome () {
    return this.taxRateIncome
  }

  getIncomeProtectionIndemnityAmount () {
    return floatDiff(floatFixer(this.getCurrentIncomeFromBusiness() * this.getTaxRateIncome().income_protection_indemnity), this.getTotalPassiveIncome())
  }

  getIncomeProtectionAgreedAmount () {
    return floatDiff(floatFixer(this.getCurrentIncomeFromBusiness() * this.getTaxRateIncome().income_protection_agreed), this.getTotalPassiveIncome())
  }

  getAll () {
    return _.merge(super.getAll(), {
      incomeProtectionIndemnityAmount: this.getIncomeProtectionIndemnityAmount(),
      incomeProtectionAgreedAmount: this.getIncomeProtectionAgreedAmount(),
      totalPassiveIncome: this.getTotalPassiveIncome()
    })
  }
}

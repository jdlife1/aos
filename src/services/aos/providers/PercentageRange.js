/* eslint-disable */
import { floatFixer } from 'src/config/utils'

export default class PercentageRange {
  constructor(income) {
    this.income = income
  }

  getIncome() {
    return floatFixer(this.income)
  }

  partnersLife() {
    const limit = PercentageRange.partnersLifeLimit
    if (this.getIncome() >= limit) throw new Error(`Insurance Provider Limit Reached $${limit}`)
    return 0.625
  }

  static partnersLifeLimit() {
    return 480000
  }

  aia() {
    const income = this.getIncome()
    if (income <= 70000) return 0.625
    if (income >= 70001 && income <= 100000) return 0.60
    if (income >= 100001 && income <= 320000) return 0.55
    if (income >= 320001 && income <= 560000) return 0.35
    if (income >= 560001) return 0.20
    throw new Error(`Out of range income $${income}`)
  }

  fidelity() {
    const income = this.getIncome()
    if (income < 70000) return 0.625
    if (income > 70001 && income < 99999) return 0.60
    if (income >= 100000) return 0.55
    throw new Error(`Out of range income $${income}`)
  }
}

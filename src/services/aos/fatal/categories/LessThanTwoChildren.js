import _ from 'lodash'
import { floatFixer, floatTotals, floatDiff } from 'src/config/utils'
import BaseCategory from 'src/services/aos/fatal/categories/BaseCategory'

/**
 * Constant defined if children is studying.
 *
 * @type {Boolean}
 */
const CHILDREN_IS_STUDYING = false // eslint-disable-line

class LessThanTwoChildren extends BaseCategory {
  /**
   * Get the "cover plus" spouse total benifit amount.
   *
   * @return Number|String
   */
  _getCoverPlusSpouseBenifitAmount () {
    return floatFixer((this.getLiableEarnings() * this.fatalRate.spouse) * (this.fatalRate.age_income_requirement - this.getMinimumChildrenAge()))
  }

  /**
   * Get the "cover plus" children(s) total benifit amount.
   *
   * @return Number|String
   */
  _getCoverPlusChildrenTotalBenifitAmount () {
    let _count = 0
    let total = 0

    _.each(this.getChildrens(), (n) => {
      total += (floatFixer((this.fatalRate.age_income_requirement - this.getChildrens()[_count].age) * (this.getLiableEarnings() * this.fatalRate.child)))
      _count += 1
    })

    return total
  }

  getCoverPlusTotalBenifitAmount () {
    return floatTotals(this._getCoverPlusChildrenTotalBenifitAmount(), this._getCoverPlusSpouseBenifitAmount())
  }

  /**
   * Get the "cover plus extra" spouse total benifit amount.
   *
   * @return Number|String
   */
  _getCoverPlusExtraSpouseBenifitAmount () {
    return floatFixer((this.getAnnualPreAgreedCoverAmount() * this.fatalRate.spouse) * (this.fatalRate.age_income_requirement - this.getMinimumChildrenAge()))
  }

  /**
   * Get the "cover plus extra" children(s) total benifit amount.
   *
   * @return Number|String
   */
  _getCoverPlusExtraChildrenTotalBenifitAmount () {
    let _count = 0
    let total = 0

    _.each(this.getChildrens(), () => {
      total += (floatFixer((this.getAnnualPreAgreedCoverAmount() * this.fatalRate.child) * (this.fatalRate.age_income_requirement - this.getChildrens()[_count].age)))
      _count += 1
    })

    return total
  }

  getCoverPlusExtraTotalBenifitAmount () {
    return floatTotals(this._getCoverPlusExtraChildrenTotalBenifitAmount(), this._getCoverPlusExtraSpouseBenifitAmount())
  }

  /**
   * Get "both" total changes.
   *
   * @return Number|String
   */
  getChangesInCoverAmount () {
    return floatDiff(this.getCoverPlusTotalBenifitAmount(), this.getCoverPlusExtraTotalBenifitAmount())
  }
}

export default LessThanTwoChildren

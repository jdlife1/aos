import _ from 'lodash'
import { floatFixer, floatTotals, floatDiff } from 'src/config/utils'
import BaseCategory from 'src/services/aos/fatal/categories/BaseCategory'

/**
 * Constant defined if children is studying.
 *
 * @type {Boolean}
 */
const CHILDREN_IS_STUDYING = false

class GreaterThanTwoChildren extends BaseCategory {
  /**
   * Get each dependents benifit amount percentage
   *
   * @return Number|String
   */
  getBenifitAmountPercentage () {
    let value = 100 / (floatFixer(this.getChildrensWhoIsStudying(CHILDREN_IS_STUDYING).length) + 3)
    return Math.round(value * 100) / 100
  }

  /**
   * Get the "cover plus" spouse total benifit amount.
   *
   * @return Number|String
   */
  _getCoverPlusSpouseBenifitAmount () {
    return floatFixer(this.fatalRate.age_income_requirement - this.getMinimumChildrenAge()) * (this.getLiableEarnings() * (this.getBenifitAmountPercentage() / 100)) * 3
  }

  /**
   * Get the "cover plus" children(s) total benifit amount.
   *
   * @return Number|String
   */
  _getCoverPlusChildrenTotalBenifitAmount () {
    let total = 0
    let values = []

    _.each(this.getChildrensWhoIsStudying(CHILDREN_IS_STUDYING), (o) => {
      let { age } = o
      total = floatFixer(this.fatalRate.age_income_requirement - age) * (this.getLiableEarnings() * (this.getBenifitAmountPercentage() / 100))
      values.push(total)
    })

    return floatTotals(values)
  }

  /**
   * Get the "cover plus extra" spouse total benifit amount.
   *
   * @return Number|String
   */
  _getCoverPlusExtraSpouseBenifitAmount () {
    return floatFixer(this.getAnnualPreAgreedCoverAmount() * (this.getBenifitAmountPercentage() / 100 * 3) * (this.fatalRate.age_income_requirement - this.getMinimumChildrenAge()))
  }

  /**
   * Get the "cover plus extra" children(s) total benifit amount.
   *
   * @return Number|String
   */
  _getCoverPlusExtraChildrenTotalBenifitAmount () {
    let total = 0
    let values = []

    _.each(this.getChildrensWhoIsStudying(CHILDREN_IS_STUDYING), (o) => {
      let { age } = o
      total = floatFixer((this.getAnnualPreAgreedCoverAmount() * (this.getBenifitAmountPercentage() / 100)) * (this.fatalRate.age_income_requirement - age))
      values.push(total)
    })

    return floatTotals(values)
  }

  getCoverPlusTotalBenifitAmount () {
    return floatTotals(this._getCoverPlusChildrenTotalBenifitAmount(), this._getCoverPlusSpouseBenifitAmount())
  }

  getCoverPlusExtraTotalBenifitAmount () {
    return floatTotals(this._getCoverPlusExtraChildrenTotalBenifitAmount(), this._getCoverPlusExtraSpouseBenifitAmount())
  }

  getChangesInCoverAmount () {
    return floatDiff(this.getCoverPlusTotalBenifitAmount(), this.getCoverPlusExtraTotalBenifitAmount())
  }
}

export default GreaterThanTwoChildren

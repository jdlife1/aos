import BaseCategory from 'src/services/aos/fatal/categories/BaseCategory'
import { floatFixer } from 'src/config/utils'

class NoChild extends BaseCategory {
  getCoverPlusTotalBenifitAmount () {
    return floatFixer((this.getLiableEarnings() * this.fatalRate.spouse) * this.fatalRate.years_covered)
  }

  getCoverPlusExtraTotalBenifitAmount () {
    return floatFixer((this.getAnnualPreAgreedCoverAmount() * this.fatalRate.spouse) * this.fatalRate.years_covered)
  }

  getChangesInCoverAmount () {
    return floatFixer(this.getCoverPlusTotalBenifitAmount() - this.getCoverPlusExtraTotalBenifitAmount())
  }
}

export default NoChild

import GreaterThanTwoChildren from 'services/aos/fatal/categories/GreaterThanTwoChildren'
import { floatFixer, floatTotals, floatDiff } from 'src/config/utils' // eslint-disable-line
import _ from 'lodash'

/**
 * Constant defined if children is studying.
 *
 * @type {Boolean}
 */
const CHILDREN_IS_STUDYING = true // eslint-disable-line

class StudyingChildren extends GreaterThanTwoChildren {
  /**
   * Get each dependents benifit amount percentage
   *
   * @return Number|String
   */
  getBenifitAmountPercentage () {
    let value = 100 / (floatFixer(this.getChildrens().length) + 3)
    return Math.round(value * 100) / 100
  }

  /**
   * Get the "cover plus" children(s) total benifit amount.
   *
   * @return Number|String
   */
  _getCoverPlusChildrenTotalBenifitAmount () {
    let total = 0
    let values = []

    // Studying
    _.each(this.getChildrensWhoIsStudying(CHILDREN_IS_STUDYING), (o) => {
      total = floatFixer(this.fatalRate.age_who_is_studying_income_requirement - o.age) * (this.getLiableEarnings() * (this.getBenifitAmountPercentage() / 100))
      values.push(total)
    })

    // Not Studying
    _.each(this.getChildrensWhoIsStudying(!CHILDREN_IS_STUDYING), (o) => {
      total = floatFixer(this.fatalRate.age_income_requirement - o.age) * (this.getLiableEarnings() * (this.getBenifitAmountPercentage() / 100))
      values.push(total)
    })

    return floatTotals(values)
  }

  /**
   * Get the "cover plus extra" children(s) total benifit amount.
   *
   * @return Number|String
   */
  _getCoverPlusExtraChildrenTotalBenifitAmount () {
    let total = 0
    let values = []

    // Studying (Above Child Age)
    _.each(this.getChildrensWhoIsStudying(CHILDREN_IS_STUDYING), (o) => {
      total = floatFixer((this.getAnnualPreAgreedCoverAmount() * (this.getBenifitAmountPercentage() / 100)) * (this.fatalRate.age_who_is_studying_income_requirement - o.age))
      values.push(total)
    })

    // Not Studying (Below Child Age)
    _.each(this.getChildrensWhoIsStudying(!CHILDREN_IS_STUDYING), (o) => {
      total = floatFixer((this.getAnnualPreAgreedCoverAmount() * (this.getBenifitAmountPercentage() / 100)) * (this.fatalRate.age_income_requirement - o.age))
      values.push(total)
    })

    return floatTotals(values)
  }
}

export default StudyingChildren

import _ from 'lodash'
import { floatFixer } from 'src/config/utils'

export default class BaseCategory {
  constructor (income, annualPreAgreedCoverAmount) {
    this.setCurrentIncomeFromBusiness(income)
      .setAnnualPreAgreedCoverAmount(annualPreAgreedCoverAmount)
  }

  setChildrens (childrens = []) {
    this.arrayOfChildrens = childrens

    return this
  }

  /**
   * Get below children ages.
   *
   * @return array
   */
  getMinimumChildrenAge () {
    return _.get(_.minBy(this.getChildrens(), 'age'), 'age', 0)
  }

  /**
   * Get above children ages.
   *
   * @return array
   */
  getMaximumChildrenAge (childrens = []) {
    return _.get(_.maxBy(this.getChildrens(), 'age'), 'age', 0)
  }

  getChildrens () {
    return this.arrayOfChildrens
  }

  getChildrensWhoIsStudying (studying = true) {
    return _.filter(this.getChildrens(), ['studying', studying])
  }

  setCurrentIncomeFromBusiness (amount) {
    this.currentIncomeFromBusiness = amount

    return this
  }

  getCurrentIncomeFromBusiness () {
    return floatFixer(this.currentIncomeFromBusiness)
  }

  setAnnualPreAgreedCoverAmount (amount) {
    this.annualPreAgreedCoverAmount = amount

    return this
  }

  setFatalRates (fatalRate) {
    this.fatalRate = fatalRate

    return this
  }

  setRateValues (rateValues) {
    this.rateValues = rateValues

    return this
  }

  getLiableEarnings () {
    return floatFixer(this.getCurrentIncomeFromBusiness() * this.rateValues.liable_earning_rate)
  }

  getAnnualPreAgreedCoverAmount () {
    return floatFixer(this.annualPreAgreedCoverAmount)
  }

  getCoverPlusTotalBenifitAmount () {
    throw new Error('Method must be implemented')
  }

  getCoverPlusExtraTotalBenifitAmount () {
    throw new Error('Method must be implemented')
  }

  getChangesInCoverAmount () {
    throw new Error('Method must be implemented')
  }

  _getCoverPlusSpouseBenifitAmount () {
    return 0
  }

  _getCoverPlusExtraSpouseBenifitAmount () {
    return 0
  }

  getAll () {
    return {
      coverPlusTotalSpouseBenifitAmount: this._getCoverPlusSpouseBenifitAmount(),
      coverPlusExtraTotalSpouseBenifitAmount: this._getCoverPlusExtraSpouseBenifitAmount(),
      coverPlusTotalBenifitAmount: this.getCoverPlusTotalBenifitAmount(),
      coverPlusExtraTotalBenifitAmount: this.getCoverPlusExtraTotalBenifitAmount(),
      changesInCoverAmount: this.getChangesInCoverAmount()
    }
  }
}

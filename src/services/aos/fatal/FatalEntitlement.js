import _ from 'lodash'
import NoChild from 'services/aos/fatal/categories/NoChild'
import LessThanTwoChildren from 'services/aos/fatal/categories/LessThanTwoChildren'
import GreaterThanTwoChildren from 'services/aos/fatal/categories/GreaterThanTwoChildren'
import StudyingChildren from 'services/aos/fatal/categories/StudyingChildren'

const classes = {
  NoChild,
  LessThanTwoChildren,
  GreaterThanTwoChildren,
  StudyingChildren
}

class FatalEntitlement {
  constructor (income, annualPreAgreedCoverAmount, category, childrens = [], fatalRate, rateValues) {
    this.childrens = childrens
    this.currentIncomeFromBusiness = income
    this.annualPreAgreedCoverAmount = annualPreAgreedCoverAmount
    this.currentCategory = category
    this.fatalRate = fatalRate
    this.rateValues = rateValues
  }

  handle () {
    let type = _.upperFirst(_.camelCase(this.currentCategory))

    if (!type) return

    let classInstance = (new classes[type](this.currentIncomeFromBusiness, this.annualPreAgreedCoverAmount))

    return classInstance
      .setChildrens(this.childrens)
      .setFatalRates(this.fatalRate)
      .setRateValues(this.rateValues)
      .getAll()
  }
}

export default FatalEntitlement

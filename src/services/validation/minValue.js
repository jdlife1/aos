import _ from 'lodash'
import { numberWithCommas } from 'src/config/utils'
import { req, withParams } from 'vuelidate/lib/validators/common'

export default (min) => withParams(
  { type: 'minValue', min }, value => {
    if (_.isString(value)) {
      value = numberWithCommas(value, false)
    }
    return !req(value) || ((!/\s/.test(value) || value instanceof Date) && +value >= +min)
  }
)

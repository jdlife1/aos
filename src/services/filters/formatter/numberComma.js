import { numberWithCommas } from 'src/config/utils'

function numberComma (value) {
  if (!value && value !== 0) return ''
  value = value.toString()
  return numberWithCommas(value).toString()
}

export default numberComma

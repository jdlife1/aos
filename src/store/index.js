import Vue from 'vue'
import Vuex from 'vuex'

import app from './modules/app'
import user from './modules/user'
import liabilityRates from './modules/liabilityRates'
import insuranceProviders from './modules/insuranceProviders'
import classificationUnit from './modules/classificationUnit'
import letterOfAuthority from './modules/letterOfAuthority/index'
import clientCalculations from './modules/calculations/client/index'
import partnerCalculations from './modules/calculations/partner/index'
import letterOfAuthorityCollection from './modules/letterOfAuthorityCollection'
import { persist, autosave } from './plugins'

Vue.use(Vuex)

function debug () {
  return process.env.NODE_ENV !== 'production'
}

const store = new Vuex.Store({
  strict: debug(),
  modules: {
    app,
    user,
    letterOfAuthorityCollection,
    classificationUnit,
    letterOfAuthority,
    clientCalculations,
    partnerCalculations,
    liabilityRates,
    insuranceProviders
  },
  plugins: [persist.plugin, autosave]
})

export default store

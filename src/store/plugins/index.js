import persist from './persist'
import autosave from './autosave'

export {
  persist,
  autosave
}

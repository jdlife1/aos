/* eslint-disable */
import { noop } from 'lodash';

/** @type {Boolean} determines if the browser is online. */
const isOnline = window.navigator.onLine;

async function persistPlanner(store, letterOfAuthority, { payload }) {
  const data = await store.dispatch('letterOfAuthority/submitApplicationRequest', letterOfAuthority);
  await store.commit('letterOfAuthorityCollection/ADD_LOA_COLLECTION', data);
  return data;
}

export default (store) => {
  store.subscribe((mutation, { letterOfAuthority }) => {
    switch (mutation.type) {
      case 'letterOfAuthority/UPDATE_LOA_FIELD_VALUE':
        if (letterOfAuthority.id && isOnline) {
          try { persistPlanner(store, letterOfAuthority, mutation); }
          catch (e) {}
        }
        break;
      default: noop();
    }
  });
};

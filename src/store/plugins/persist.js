import VuexPersistence from 'vuex-persist'

export default new VuexPersistence({
  key: '@aos',
  storage: window.localStorage,
  modules: [
    'classificationUnit',
    'letterOfAuthority'
  ]
})

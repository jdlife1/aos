export default () => ({
  rateValues: {
    liable_earning_rate: 0.8,
    earners_levy_fix_rate: 1.21,
    total_amount_payable_rate: 1.15
  },
  offsets: {
    maximum_income_protection_indemnity: 0.75,
    maximum_monthly_mortgage_repayment_cover: 0.40,
    maximum_acc_cover_plus_cover: 0.80
  },
  taxes: {
    income: {
      income_protection_indemnity: 0.75,
      income_protection_agreed: 0.625
    },
    levels: {
      first: {
        income_requirement: {
          min: 0,
          max: 14000
        },
        rate: 10.50
      },
      second: {
        income_requirement: {
          min: 14000,
          max: 34000
        },
        rate: 17.50
      },
      third: {
        income_requirement: {
          min: 22000,
          max: 48000
        },
        rate: 30.00
      },
      fourth: {
        income_requirement: {
          min: 5000,
          max: 70000
        },
        rate: 33.00
      }
    }
  },
  requirements: {
    income_from_business: {
      minimum: 32760,
      maximum: 122053
    },
    nominated_cover_amount: {
      minimum: 26208,
      maximum: 101029
    }
  },
  fatal: {
    spouse: 0.6,
    child: 0.2,
    weeks_in_years: 52,
    age_income_requirement: 18,
    age_who_is_studying_income_requirement: 21,
    years_covered: 5
  }
})

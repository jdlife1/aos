export const rateValues = (state) =>
  state.rateValues

export const requirements = (state) =>
  state.requirements

export const offsets = (state) =>
  state.offsets

export const taxRateLevels = ({ taxes }) =>
  taxes.levels

export const taxRateIncome = ({ taxes }) =>
  taxes.income

export const fatal = (state) =>
  state.fatal

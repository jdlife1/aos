import { LocalStorage } from 'quasar'
import { CONSTANT_STORAGE } from 'src/config/constants'

const state = {
  currentTokenData: {},
  currentModel: {}
}

const mutations = {
  SET_CURRENT_USER_TOKEN (state, payload) {
    LocalStorage.set(CONSTANT_STORAGE.USER_ACCESS_TOKEN, payload)
    state.currentTokenData = payload
  },
  REMOVE_CURRENT_USER_TOKEN (state, payload) {
    LocalStorage.remove(CONSTANT_STORAGE.USER_ACCESS_TOKEN)
    state.currentTokenData = {}
  },
  SET_CURRENT_USER_MODEL (state, payload) {
    LocalStorage.set(CONSTANT_STORAGE.USER_MODEL, payload)
    state.currentModel = payload
  },
  REMOVE_SET_CURRENT_USER_MODEL (state, payload) {
    LocalStorage.remove(CONSTANT_STORAGE.USER_MODEL)
    state.currentModel = {}
  }
}

const actions = {}

const getters = {
  currentUserModel (state) {
    return state.currentModel
  },
  currentTokenDataUser (state) {
    return state.currentTokenData
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

const state = {
  config: {
    site: {
      title: 'AOS - ACC',
      company: 'JdLife'
    },
    input: {
      date: {
        fotmat: 'DD/MM/YYYY'
      }
    },
    signature: {
      format: 'image/png',
      option: {
        penColor: 'rgb(0, 0, 0)'
      }
    }
  }
}

const mutations = {}

const actions = {}

const getters = {
  config (state) {
    return state.config
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

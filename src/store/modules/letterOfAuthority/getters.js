import { isEmpty, isArray, castArray, find } from 'lodash'
import { floatTotals, floatFixer } from 'src/config/utils'

export const hasClientIncome = ({ income_from_business: income, nominated_cover_amount: cover }) =>
  !!floatTotals([
    floatFixer(income),
    floatFixer(cover)
  ])

export const hasClientSignature = (state) =>
  !isEmpty(state.client_signature) || !isEmpty(state.partner_signature)

export const hasPartnerIncome = ({ partner_taking_from_the_firm: income, partner_nominated_cover_amount: cover }) =>
  !!floatTotals([
    floatFixer(income),
    floatFixer(cover)
  ])

export const getClientOrPartner = (state) => ([
  {
    label: isEmpty(state.client_full_name) ? 'Client' : state.client_full_name,
    value: 'client'
  },
  {
    label: isEmpty(state.partner_name) ? 'Partner' : state.partner_name,
    value: 'partner'
  }
])

export const getAccLevyCalculation = ({ acc_levy_calculations: calculations }) =>
  (type, property) => find(calculations, { type, property }) || null

export const clientInsuranceProviders = ({ client_insurance_providers: providers }) =>
  isArray(providers) ? providers : castArray(providers)

export const partnerInsuranceProviders = ({ partner_insurance_providers: providers }) =>
  isArray(providers) ? providers : castArray(providers)

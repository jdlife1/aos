const initialState = () => ({
  // Company and Income Details
  id: 0,
  company_full_name: null,
  client_full_name: null,
  business_partners: [], // @TODO: Relational Model
  income_tax_method: 'drawings',
  on_tools: 'yes',
  has_corporate_partner: null, // 'no'
  classification_unit: 41110,
  income_from_business: null,

  acc_levy_calculations: [],

  acc_cover_plan_type: 'cover_plus',
  existing_nominated_cover_amount: null,

  nominated_cover_amount: null,
  when_business_started: null,
  how_many_boys: null,
  what_you_do_in_business: null,

  // IMPORTANT INFO
  acc_number_business: null,
  acc_or_ird_number_personal: null,
  partner_acc_or_ird_number_personal: null,
  accountants_name: null,
  firm_name: null,
  phone_number: null, // telephone

  client_date_of_birth: null,
  client_address: null,
  client_mobile_number: null,
  client_email_address: null,
  client_smoking_status: null, // 'no'
  client_insurance_providers: [],

  // TAX SPLITTING
  partner_date_of_birth: null,
  partner_address: null,
  partner_mobile_number: null,
  partner_email_address: null,
  partner_smoking_status: null, // 'no'
  partner_insurance_providers: [],
  have_childrens: null, // 'no'

  living_expenses: null,
  household_building_expense: null, // 'rental-payments'
  mortgage_repayments: null,
  mortgage_repayments_type: null, // 'monthly'
  mortgage_bank_or_provider: null,
  total_debts: null,
  notes: null,

  is_partner_shareholder_or_directory: null, // 'yes'
  partner_name: null,
  is_partner_account_tax_splitting: null, // 'yes'
  partner_income_tax_method: 'drawings',
  partner_taking_from_the_firm: '0',

  partner_acc_cover_plan_type: 'cover_plus',
  partner_existing_nominated_cover_amount: null,

  partner_on_tools: 'yes',
  partner_classification_unit: 41110,
  partner_nominated_cover_amount: null,
  partner_when_business_started: null,

  client_tax_issues_insurance_provider: null,
  partner_tax_issues_insurance_provider: null,

  fatal_entitlement_for_type: null,
  fatal_entitlement_category_type: null,
  fatal_entitlement_annual_pre_aggreed_cover_amount: null,
  fatal_entitlement_childrens: [],

  client_signature: null,
  partner_signature: null,

  loa_document_url: null
})

export { initialState }

export default initialState

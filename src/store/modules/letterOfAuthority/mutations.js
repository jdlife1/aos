import { set, each, get, cloneDeep, isEmpty } from 'lodash'

export const UPDATE_LOA_FIELD_VALUE = (state, { value, field }) => {
  if ((value instanceof Object && value.constructor === Object) && isEmpty(field)) {
    Object.entries(value).forEach(([k, v]) => {
      const payload = Array.isArray(state[k]) ? cloneDeep(v) : v
      set(state, k, payload)
    })
  }
  else {
    set(state, field, value)
  }
}

export const ASSIGN_LOA_STATE = (state, payload) => {
  each(get(payload, 'data', payload), (value, key) => {
    set(state, key, value)
  })
}

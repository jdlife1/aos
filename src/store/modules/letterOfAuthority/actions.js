import { HTTP_API } from 'src/services/http/http'

export const assignLoaState = ({ commit }, payload) =>
  commit('ASSIGN_LOA_STATE', payload)

export const updateLoaFieldAction = ({ state, commit }, payload) => {
  commit('UPDATE_LOA_FIELD_VALUE', payload)
  return state
}

export const submitApplicationRequest = async ({ dispatch }, payload) => {
  const { data } = await HTTP_API().post('/letter-of-authorization/persist', payload)
  await dispatch('assignLoaState', data)
  return data
}

export const removeApplicationRequest = async (context, { id }) => {
  if (!id) throw new Error('Failed to sent application notification.')
  const { data } = await HTTP_API().post(`/letter-of-authorization/expunge/${id}/application`)
  return data
}

export const sendApplicationNotification = (context, { id }) => {
  if (!id) throw new Error('Failed to sent application notification.')
  return HTTP_API().post(`/letter-of-authorization/send/${id}/notification`)
}

export const persistAccLevyCalculation = async ({ state: { id } }, payload) => {
  const { data } = await HTTP_API().post(`acc/calculations/${id}/persist`, payload)
  return data
}

export const findApplication = async ({ state }, id) => {
  const payload = id || state.id
  const { data } = await HTTP_API().get(`/letter-of-authorization/find/${payload}`)
  return data
}

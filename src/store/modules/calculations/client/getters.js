/* eslint-disable */
import _ from 'lodash'
import { floatFixer, floatDiff, getPercentageValue } from 'src/config/utils'
import CoverPlusService from 'src/services/aos/levy/CoverPlusService'
import CoverPlusExtraService from 'src/services/aos/levy/CoverPlusExtraService'
import AccOffsetService from 'src/services/aos/offset/AccOffset'
// import PassiveIncomeService from 'src/services/aos/income/PassiveIncome'
import TaxService from 'src/services/aos/tax/TaxService'
import FatalEntitlement from 'src/services/aos/fatal/FatalEntitlement'

const LIABILITY_RATES_NAMESPACE = 'liabilityRates'
const LOA_NAMESPACE = 'letterOfAuthority'

export const isAlreadyOnAccCoverPlusExtra = (state, getters, rootState) =>
  _.eq(rootState[LOA_NAMESPACE].acc_cover_plan_type, 'cover_plus_extra')

export const getDeterminedIncomeAmount = (state, getters, rootState) =>
  floatFixer(rootState[LOA_NAMESPACE].income_from_business)

export const getDeterminedNominatedAmount = (state, getters, rootState) =>
  floatFixer(rootState[LOA_NAMESPACE].nominated_cover_amount)

export const getAccLevyCalculationExistingFromServer = (state, getters, rootState, rootGetters) =>
  rootGetters[`${LOA_NAMESPACE}/getAccLevyCalculation`]('client', 'existing')

export const getAccLevyCalculationOnToolsFromServer = (state, getters, rootState, rootGetters) =>
  rootGetters[`${LOA_NAMESPACE}/getAccLevyCalculation`]('client', 'on_tools')

export const getAccLevyCalculationPlanFromServer = (state, getters, rootState, rootGetters) =>
  rootGetters[`${LOA_NAMESPACE}/getAccLevyCalculation`]('client', 'plan')

export const getFatalEntitlementDetermineCalculationByCategory = (state, getters, rootState, rootGetters) => {
  let income = getters.getFatalEntitlementDetermineActualEstimatedEarningsAmount
  let category = rootState.letterOfAuthority.fatal_entitlement_category_type

  let annualPreAgreedCoverAmount = rootState.letterOfAuthority.fatal_entitlement_annual_pre_aggreed_cover_amount
  let childrens = rootState.letterOfAuthority.fatal_entitlement_childrens
  if (_.isEmpty(annualPreAgreedCoverAmount)) return null

  let fatalRate = rootGetters[`${LIABILITY_RATES_NAMESPACE}/fatal`]
  let rateValues = rootGetters[`${LIABILITY_RATES_NAMESPACE}/rateValues`]

  return (new FatalEntitlement(
    income,
    annualPreAgreedCoverAmount,
    category,
    childrens,
    fatalRate,
    rateValues
  )).handle()
}

export const getPigSavingsAmount = (state, { getCoverPlusCalculation, getDeterminedCoverPlusExtraCalculation }) =>
  floatDiff(
    _.get(getCoverPlusCalculation, 'totalAmountPayableToAccIncludingGst', 0),
    _.get(getDeterminedCoverPlusExtraCalculation, 'totalAmountPayableToAccIncludingGst', 0)
  )

export const isTheSameClassificationUnit = (state, getters, { letterOfAuthority }) =>
  _.eq(letterOfAuthority.on_tools, 'yes');

export const getFatalEntitlementDetermineActualEstimatedEarningsAmount = (state, getters, rootState) => {
  let determinedIncomeByType = _.eq(rootState.letterOfAuthority.fatal_entitlement_for_type, 'client') ? 'income_from_business' : 'partner_taking_from_the_firm'
  return floatFixer(rootState.letterOfAuthority[determinedIncomeByType])
}

// export const getPassiveIncomeCalculation = (state, getters, rootState, rootGetters) => {
//   let passiveIncomeServiceInstance = new PassiveIncomeService()

//   let {
//     income_from_business,
//     income_protection_covers_with_other_insurance_provider,
//     passive_income_from_business,
//     passive_income_from_rentals,
//     other_passive_income
//   } = rootState.letterOfAuthority

//   if (_.isEmpty(income_from_business)) return null

//   passiveIncomeServiceInstance
//     .setOffsetRates(rootGetters[`${LIABILITY_RATES_NAMESPACE}/offsets`])
//     .setTaxRateIncome(rootGetters[`${LIABILITY_RATES_NAMESPACE}/taxRateIncome`])
//     .setCurrentIncomeFromBusiness(income_from_business)
//     .setArrayOfPassiveIncomes([
//       income_protection_covers_with_other_insurance_provider,
//       passive_income_from_business,
//       passive_income_from_rentals,
//       other_passive_income
//     ])

//   return passiveIncomeServiceInstance.getAll()
// }

export const getNetIncomeTaxCalculation = (state, { getDeterminedIncomeAmount }, { letterOfAuthority }, rootGetters) =>
  (new TaxService())
    .setIsUsingIndemnityAmount()
    .setInsuranceProvider(letterOfAuthority.client_tax_issues_insurance_provider)
    .setTaxRateLevels(rootGetters[`${LIABILITY_RATES_NAMESPACE}/taxRateLevels`])
    .setTaxRateIncome(rootGetters[`${LIABILITY_RATES_NAMESPACE}/taxRateIncome`])
    .setCurrentIncomeFromBusiness(getDeterminedIncomeAmount)
    .getAll()

export const getAccOffsetCalculation = (state, { getDeterminedIncomeAmount }, rootState, rootGetters) =>
  (new AccOffsetService())
    .setOffsetRates(rootGetters[`${LIABILITY_RATES_NAMESPACE}/offsets`])
    .setCurrentIncomeFromBusiness(getDeterminedIncomeAmount)
    .getAll()

export const getSelectedClassificationunitOnTools = (state, getters, { classificationUnit, letterOfAuthority }) =>
  _.find(classificationUnit.data, ['code', letterOfAuthority.what_you_do_in_business]) || null

export const getPigSavingsPercentage = (state, { getCoverPlusCalculation, getDeterminedCoverPlusExtraCalculation }) =>
  getPercentageValue(
    _.get(getCoverPlusCalculation, 'totalAmountPayableToAccIncludingGst', 0),
    _.get(getDeterminedCoverPlusExtraCalculation, 'totalAmountPayableToAccIncludingGst', 0),
  );

export const getSelectedClassificationUnit = (state, getters, { classificationUnit, letterOfAuthority }) => {
  let classificationUnitsData = classificationUnit.data
  if (_.isEmpty(classificationUnitsData)) return null

  let foundClassificationUnitObject = _.find(classificationUnitsData, ['code', letterOfAuthority.classification_unit])
  if (_.isEmpty(foundClassificationUnitObject)) return null

  return foundClassificationUnitObject
}

export const getCoverPlusExtraCalculationOnTools = (state, { getDeterminedIncomeAmount, getDeterminedNominatedAmount, getSelectedClassificationunitOnTools, getAccLevyCalculationOnToolsFromServer }) => {
  const classificationUnit = getSelectedClassificationunitOnTools;
  const calculationFromServer = getAccLevyCalculationOnToolsFromServer;
  if (!_.isEmpty(calculationFromServer)
    && !_.isEmpty(classificationUnit)) {
    const { code } = classificationUnit;
    const { coverplus_extra_standard: result } = calculationFromServer;
    return (_.eq(result.classificationUnitCode, code)
      && _.eq(result.currentIncomeFromBusiness, getDeterminedIncomeAmount)
      && _.eq(result.rawNominatedCoverAmount, getDeterminedNominatedAmount)) ? result : null;
  }

  return null;
  // let coverPlusExtraService = new CoverPlusExtraService()

  // let { getSelectedClassificationunitOnTools } = getters
  // coverPlusExtraService.setRateValues(rootGetters[`${LIABILITY_RATES_NAMESPACE}/rateValues`])

  // let { income_from_business, nominated_cover_amount } = rootState.letterOfAuthority
  // if (_.isEmpty(income_from_business) || _.isEmpty(nominated_cover_amount)) return null

  // coverPlusExtraService
  //   .setCurrentIncomeFromBusiness(income_from_business)
  //   .setNominatedCoverAmount(nominated_cover_amount)

  // if (_.isEmpty(getSelectedClassificationunitOnTools)) return null
  // coverPlusExtraService.setClassificationUnit(getSelectedClassificationunitOnTools)

  // return coverPlusExtraService.getAll()
}



export const showCalculatedLevy = (state, { getCoverPlusCalculation, getDeterminedCoverPlusExtraCalculation }) =>
  (!_.isEmpty(getCoverPlusCalculation) && !_.isEmpty(getDeterminedCoverPlusExtraCalculation));

export const getDeterminedCalculationHeaderLabel = (state, { getSelectedClassificationunitOnTools, getSelectedClassificationUnit, isTheSameClassificationUnit }) => {
  if (!isTheSameClassificationUnit
    && !_.isEmpty(getSelectedClassificationUnit)
    && !_.isEmpty(getSelectedClassificationunitOnTools)) {
    return `From ${_.get(getSelectedClassificationUnit, 'name')} to ${_.get(getSelectedClassificationunitOnTools, 'name')}`;
  }
  return _.get(getSelectedClassificationUnit, 'name');
}

export const getDeterminedCoverPlusExtraCalculation = (state, { getCoverPlusExtraCalculationOnTools, getCoverPlusExtraCalculation, isTheSameClassificationUnit }) =>
  isTheSameClassificationUnit
    ? getCoverPlusExtraCalculation
    : getCoverPlusExtraCalculationOnTools;

export const getCoverPlusExtraCalculation = (state, { getDeterminedIncomeAmount, getDeterminedNominatedAmount, getSelectedClassificationUnit, getAccLevyCalculationPlanFromServer }) => {

  const calculationFromServer = getAccLevyCalculationPlanFromServer;
  if (!_.isEmpty(calculationFromServer)) {
    const { code } = getSelectedClassificationUnit;
    const { coverplus_extra_standard: result } = calculationFromServer;
    return (_.eq(result.classificationUnitCode, code)
      && _.eq(result.currentIncomeFromBusiness, getDeterminedIncomeAmount)
      && _.eq(result.rawNominatedCoverAmount, getDeterminedNominatedAmount)) ? result : null;
  }

  return null;

  // let coverPlusExtraService = new CoverPlusExtraService()
  // coverPlusExtraService.setRateValues(rootGetters[`${LIABILITY_RATES_NAMESPACE}/rateValues`])

  // let { income_from_business, nominated_cover_amount } = rootState.letterOfAuthority
  // if (_.isEmpty(income_from_business) || _.isEmpty(nominated_cover_amount)) return null

  // coverPlusExtraService
  //   .setCurrentIncomeFromBusiness(income_from_business)
  //   .setNominatedCoverAmount(nominated_cover_amount)

  // let { getSelectedClassificationUnit } = getters
  // if (_.isEmpty(getSelectedClassificationUnit)) return null
  // coverPlusExtraService.setClassificationUnit(getSelectedClassificationUnit)

  // return coverPlusExtraService.getAll()
}

export const getCoverPlusCalculation = (state, { getDeterminedIncomeAmount, getDeterminedNominatedAmount, getSelectedClassificationUnit, getExistingCoverPlusExtraCalculation, isAlreadyOnAccCoverPlusExtra, getAccLevyCalculationPlanFromServer }) => {

  if (isAlreadyOnAccCoverPlusExtra) return getExistingCoverPlusExtraCalculation;

  const calculationFromServer = getAccLevyCalculationPlanFromServer;
  if (!_.isEmpty(calculationFromServer)) {
    const { code } = getSelectedClassificationUnit;
    const { coverplus_cover: result } = calculationFromServer;
    return (_.eq(result.classificationUnitCode, code)
      && _.eq(result.currentIncomeFromBusiness, getDeterminedIncomeAmount)
      && _.eq(result.rawNominatedCoverAmount, getDeterminedNominatedAmount)) ? result : null;
  }

  return null;

  // let coverPlusService = new CoverPlusService()
  // coverPlusService.setRateValues(rootGetters[`${LIABILITY_RATES_NAMESPACE}/rateValues`])

  // let { income_from_business, nominated_cover_amount } = rootState.letterOfAuthority
  // if (_.isEmpty(income_from_business) || _.isEmpty(nominated_cover_amount)) return null

  // coverPlusService
  //   .setCurrentIncomeFromBusiness(income_from_business)
  //   .setNominatedCoverAmount(nominated_cover_amount)

  // let { getSelectedClassificationUnit } = getters
  // if (_.isEmpty(getSelectedClassificationUnit)) return null
  // coverPlusService.setClassificationUnit(getSelectedClassificationUnit)

  // return coverPlusService.getAll()
}

export const getExistingCoverPlusExtraCalculation = (state, { getDeterminedIncomeAmount, getSelectedClassificationUnit, isAlreadyOnAccCoverPlusExtra, getAccLevyCalculationExistingFromServer }, { letterOfAuthority }, rootGetters) => {
  if (!isAlreadyOnAccCoverPlusExtra) return null;
  const { existing_nominated_cover_amount: existingNominatedCoverAmount } = letterOfAuthority;

  const calculationFromServer = getAccLevyCalculationExistingFromServer;
  if (!_.isEmpty(calculationFromServer)) {
    const { code } = getSelectedClassificationUnit;
    const { coverplus_extra_standard: result } = calculationFromServer;
    return (_.eq(result.classificationUnitCode, code)
      && _.eq(result.currentIncomeFromBusiness, getDeterminedIncomeAmount)
      && _.eq(result.rawNominatedCoverAmount, floatFixer(existingNominatedCoverAmount)))
      ? result : null;
  }

  return null;
}

export const getTotalCoverEstimatedSavingsAmount = (state, { getCoverPlusCalculation, getCoverPlusExtraCalculation }, rootState, rootGetters) => {
  if (_.isEmpty(getCoverPlusCalculation)
    || _.isEmpty(getCoverPlusExtraCalculation)) return 0

  let coverPlusTotal = _.get(getCoverPlusCalculation, 'totalAmountPayableToAcc', 0)
  let coverPlusExtraTotal = _.get(getCoverPlusExtraCalculation, 'totalAmountPayableToAcc', 0)

  return floatDiff(coverPlusTotal, coverPlusExtraTotal)
}

export const getTotalIncomeProtectionAmount = (state, { getCoverPlusCalculation, getCoverPlusExtraCalculation , getDeterminedNominatedAmount}) => {
  if (_.isEmpty(getCoverPlusCalculation)
    || _.isEmpty(getCoverPlusExtraCalculation)) return 0
  return floatDiff(getCoverPlusCalculation['nominatedCoverAmount'], getDeterminedNominatedAmount)
}

export const getTotalSavingsFromAccLeviesAmount = (state, { getCoverPlusCalculation, getCoverPlusExtraCalculation }) => {
  if (_.isEmpty(getCoverPlusCalculation)
    || _.isEmpty(getCoverPlusExtraCalculation)) return 0

  let coverPlusTotal = _.get(getCoverPlusCalculation, 'totalAmountPayableToAcc', 0)
  let coverPlusExtraTotal = _.get(getCoverPlusExtraCalculation, 'totalAmountPayableToAcc', 0)

  return floatDiff(coverPlusTotal, coverPlusExtraTotal)
}
/* eslint-enable */

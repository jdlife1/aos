/* eslint-disable */
import _ from 'lodash'
import CoverPlusService from 'src/services/aos/levy/CoverPlusService'
import { floatFixer, floatDiff, getPercentageValue } from 'src/config/utils'
import CoverPlusExtraService from 'src/services/aos/levy/CoverPlusExtraService'
import TaxService from 'src/services/aos/tax/TaxService'
import AccOffsetService from 'src/services/aos/offset/AccOffset'

const LIABILITY_RATES_NAMESPACE = 'liabilityRates'
const LOA_NAMESPACE = 'letterOfAuthority'
const CLIENT_CALCULATIONS_NAMESPACE = 'clientCalculations'

export const getDeterminedIncomeAmount = (state, getters, rootState) =>
  floatFixer(rootState[LOA_NAMESPACE].partner_taking_from_the_firm)

export const getDeterminedNominatedAmount = (state, getters, rootState) =>
  floatFixer(rootState[LOA_NAMESPACE].partner_nominated_cover_amount)

export const isAlreadyOnAccCoverPlusExtra = (state, getters, rootState) =>
  _.eq(rootState[LOA_NAMESPACE].partner_acc_cover_plan_type, 'cover_plus_extra')

export const getAccLevyCalculationExistingFromServer = (state, getters, rootState, rootGetters) =>
  rootGetters[`${LOA_NAMESPACE}/getAccLevyCalculation`]('partner', 'existing')

export const getAccLevyCalculationOnToolsFromServer = (state, getters, rootState, rootGetters) =>
  rootGetters[`${LOA_NAMESPACE}/getAccLevyCalculation`]('partner', 'on_tools')

export const getAccLevyCalculationPlanFromServer = (state, getters, rootState, rootGetters) =>
  rootGetters[`${LOA_NAMESPACE}/getAccLevyCalculation`]('partner', 'plan')

export const getNetIncomeTaxCalculation = (state, { getDeterminedIncomeAmount }, { letterOfAuthority }, rootGetters) =>
  (new TaxService())
    .setIsUsingIndemnityAmount()
    .setInsuranceProvider(letterOfAuthority.partner_tax_issues_insurance_provider)
    .setTaxRateLevels(rootGetters[`${LIABILITY_RATES_NAMESPACE}/taxRateLevels`])
    .setTaxRateIncome(rootGetters[`${LIABILITY_RATES_NAMESPACE}/taxRateIncome`])
    .setCurrentIncomeFromBusiness(getDeterminedIncomeAmount)
    .getAll()

export const getAccOffsetCalculation = (state, { getDeterminedIncomeAmount }, rootState, rootGetters) =>
  (new AccOffsetService())
    .setOffsetRates(rootGetters[`${LIABILITY_RATES_NAMESPACE}/offsets`])
    .setCurrentIncomeFromBusiness(getDeterminedIncomeAmount)
    .getAll()

export const isTheSameClassificationUnit = (state, getters, rootState, rootGetters) =>
  _.eq(rootState.letterOfAuthority.partner_on_tools, 'yes')

export const getClientSelectedClassificationUnit = (state, getters, rootState, rootGetters) =>
  _.get(rootGetters, `${CLIENT_CALCULATIONS_NAMESPACE}/getSelectedClassificationUnit`, {});

export const getDeterminedCoverPlusExtraCalculation = (state, { getCoverPlusExtraCalculationOnTools, getCoverPlusExtraCalculation, isTheSameClassificationUnit }) =>
  isTheSameClassificationUnit
    ? getCoverPlusExtraCalculation
    : getCoverPlusExtraCalculationOnTools

export const getPigSavingsPercentage = (state, { getCoverPlusCalculationTotalAmountPayableToAcc, getDeterminedCoverPlusExtraCalculation }) =>
  getPercentageValue(
    getCoverPlusCalculationTotalAmountPayableToAcc,
    _.get(getDeterminedCoverPlusExtraCalculation, 'totalAmountPayableToAccIncludingGst', 0),
  )

export const getPigSavingsAmount = (state, { getCoverPlusCalculation, getDeterminedCoverPlusExtraCalculation }) =>
  floatDiff(
    _.get(getCoverPlusCalculation, 'totalAmountPayableToAccIncludingGst', 0),
    _.get(getDeterminedCoverPlusExtraCalculation, 'totalAmountPayableToAccIncludingGst', 0),
  )

export const showCalculatedLevy = (state, { getCoverPlusCalculation, getDeterminedCoverPlusExtraCalculation }, { letterOfAuthority }) => {
  if (_.eq(letterOfAuthority.is_partner_shareholder_or_directory, 'no')) return false;
  return (!_.isEmpty(getCoverPlusCalculation) && !_.isEmpty(getDeterminedCoverPlusExtraCalculation));
}

export const getCoverPlusCalculationTotalAmountPayableToAcc = (state, { getCoverPlusCalculation }) =>
  _.get(getCoverPlusCalculation, 'totalAmountPayableToAccIncludingGst', 0)

export const getSelectedClassificationUnit = (state, { isTheSameClassificationUnit }, { letterOfAuthority, classificationUnit }) => {
  const { classification_unit: fromClient, partner_classification_unit: fromPartner } = letterOfAuthority;
  const code = isTheSameClassificationUnit
    ? fromClient
    : fromPartner;
  return _.find(classificationUnit.data, ['code', code]) || null;
}

export const getSelectedClassificationunitOnTools = (state, getters, { letterOfAuthority, classificationUnit }) =>
  _.find(classificationUnit.data, ['code', letterOfAuthority.partner_classification_unit]) || null;

export const getCoverPlusExtraCalculationOnTools = (state, { getDeterminedIncomeAmount, getDeterminedNominatedAmount, getSelectedClassificationunitOnTools,getAccLevyCalculationOnToolsFromServer }) => {
  const classificationUnit = getSelectedClassificationunitOnTools;
  const calculationFromServer = getAccLevyCalculationOnToolsFromServer;
  if (!_.isEmpty(calculationFromServer)
    && !_.isEmpty(classificationUnit)) {
    const { code } = classificationUnit;
    const { coverplus_extra_standard: result } = calculationFromServer;
    return (_.eq(result.classificationUnitCode, code)
      && _.eq(result.currentIncomeFromBusiness, getDeterminedIncomeAmount)
      && _.eq(result.rawNominatedCoverAmount, getDeterminedNominatedAmount)) ? result : null;
  }

  return null;
}

export const getDeterminedCalculationHeaderLabel = (state, { getSelectedClassificationUnit, isTheSameClassificationUnit, getClientSelectedClassificationUnit }, rootState, rootGetters) => {
  if (!isTheSameClassificationUnit
    && !_.isEmpty(getSelectedClassificationUnit)
    && !_.isEmpty(getClientSelectedClassificationUnit)) {
    return `From ${get(getClientSelectedClassificationUnit, 'name', null)} to ${get(getSelectedClassificationUnit, 'name', null)}`;
  }
  return _.isEmpty(getClientSelectedClassificationUnit)
    ? _.get(getSelectedClassificationUnit, 'name', null)
    : _.get(getClientSelectedClassificationUnit, 'name', null);
}

export const getCoverPlusExtraCalculation = (state, { getDeterminedIncomeAmount, getDeterminedNominatedAmount, getClientSelectedClassificationUnit, getAccLevyCalculationPlanFromServer }, rootState, rootGetters) => {
  const calculationFromServer = getAccLevyCalculationPlanFromServer;
  if (!_.isEmpty(calculationFromServer)) {
    const code = _.get(getClientSelectedClassificationUnit, 'code', 0);
    const { coverplus_extra_standard: result } = calculationFromServer;
    return (_.eq(result.classificationUnitCode, code)
      && _.eq(result.currentIncomeFromBusiness, getDeterminedIncomeAmount)
      && _.eq(result.rawNominatedCoverAmount, getDeterminedNominatedAmount)) ? result : null;
  }

  return null;
  // let coverPlusExtraService = new CoverPlusExtraService()
  // coverPlusExtraService.setRateValues(rootGetters[`${LIABILITY_RATES_NAMESPACE}/rateValues`])

  // let { partner_taking_from_the_firm, partner_nominated_cover_amount } = rootState.letterOfAuthority
  // if (_.isEmpty(partner_taking_from_the_firm) || _.isEmpty(partner_nominated_cover_amount)) return null
  // coverPlusExtraService.setCurrentIncomeFromBusiness(partner_taking_from_the_firm)
  //   .setNominatedCoverAmount(partner_nominated_cover_amount)

  // let { getSelectedClassificationUnit } = getters
  // if (_.isEmpty(getSelectedClassificationUnit)) return null
  // coverPlusExtraService.setClassificationUnit(getSelectedClassificationUnit)

  // return coverPlusExtraService.getAll()
}

export const getExistingCoverPlusExtraCalculation = (state, { getDeterminedIncomeAmount, getClientSelectedClassificationUnit, isAlreadyOnAccCoverPlusExtra, getAccLevyCalculationExistingFromServer }, { letterOfAuthority }, rootGetters) => {
  const { partner_existing_nominated_cover_amount: existingNominatedCoverAmount } = letterOfAuthority;
  const calculationFromServer = getAccLevyCalculationExistingFromServer;
  if (!_.isEmpty(calculationFromServer)) {
    const { code } = getClientSelectedClassificationUnit;
    const { coverplus_extra_standard: result } = calculationFromServer;
    return (_.eq(result.classificationUnitCode, code)
      && _.eq(result.currentIncomeFromBusiness, getDeterminedIncomeAmount)
      && _.eq(result.rawNominatedCoverAmount, floatFixer(existingNominatedCoverAmount)))
      ? result : null;
  }

  return null;
}

export const getCoverPlusCalculation = (state, { getDeterminedIncomeAmount, getDeterminedNominatedAmount, getClientSelectedClassificationUnit, isAlreadyOnAccCoverPlusExtra, getExistingCoverPlusExtraCalculation, getAccLevyCalculationPlanFromServer, isTheSameClassificationUnit, getClientCoverPlusCalculation }, rootState, rootGetters) => {
  // We are ignoring the cover plus calculations
  // since the partner is already on Cover Plus Extra plan
  if (isAlreadyOnAccCoverPlusExtra) return getExistingCoverPlusExtraCalculation;

  // We are going to check if the partner is on tools
  // This means that the classification unit will be
  // The same as the client either the calculation
  if (isTheSameClassificationUnit) return getClientCoverPlusCalculation;

  // Unless if the partner is not on tools
  // Resulting that the partner has been
  // Re-coded to another cu code
  const calculationFromServer = getAccLevyCalculationPlanFromServer;
  if (_.isEmpty(calculationFromServer)) {
    const { code } = getClientSelectedClassificationUnit;
    const { coverplus_cover: result } = calculationFromServer;
    return (_.eq(result.classificationUnitCode, code)
      && _.eq(result.currentIncomeFromBusiness, getDeterminedIncomeAmount)
      && _.eq(result.rawNominatedCoverAmount, getDeterminedNominatedAmount)) ? result : null;
  }

  return null;
  // let coverPlusService = new CoverPlusService()
  // coverPlusService.setRateValues(rootGetters[`${LIABILITY_RATES_NAMESPACE}/rateValues`])

  // let currentIncomeFromBusiness = rootState.letterOfAuthority.partner_taking_from_the_firm
  // let currentNominatedCoverAmount = rootState.letterOfAuthority.partner_nominated_cover_amount
  // if (_.isEmpty(currentIncomeFromBusiness) || _.isEmpty(currentNominatedCoverAmount)) return null
  // coverPlusService.setCurrentIncomeFromBusiness(currentIncomeFromBusiness)
  //   .setNominatedCoverAmount(currentNominatedCoverAmount)

  // let foundClassificationUnitObject = getters.getSelectedClassificationUnit
  // if (_.isEmpty(foundClassificationUnitObject)) return null
  // coverPlusService.setClassificationUnit(foundClassificationUnitObject)

  // return coverPlusService.getAll()
}

export const getClientCoverPlusCalculation = (state, { getClientSelectedClassificationUnit }, rootState, rootGetters) => {
  // Get the client cover plus calculation regardless
  // If the client calculation is already on cover plus extra
  const calculationFromServer = rootGetters[`${CLIENT_CALCULATIONS_NAMESPACE}/getAccLevyCalculationPlanFromServer`];
  const income = rootGetters[`${CLIENT_CALCULATIONS_NAMESPACE}/getDeterminedIncomeAmount`];
  const cover = rootGetters[`${CLIENT_CALCULATIONS_NAMESPACE}/getDeterminedNominatedAmount`];

  if (!_.isEmpty(calculationFromServer)) {
    const { code } = getClientSelectedClassificationUnit;
    const { coverplus_cover: result } = calculationFromServer;
    return (_.eq(result.classificationUnitCode, code)
      && _.eq(result.currentIncomeFromBusiness, income)
      && _.eq(result.rawNominatedCoverAmount, cover)) ? result : null;
  }

  return null;
}

export const getTotalCoverEstimatedSavingsAmount = (state, getters, rootState, rootGetters) => {
  if (_.isEmpty(getters.getCoverPlusCalculation) || _.isEmpty(getters.getCoverPlusExtraCalculation)) return null

  let coverPlusTotal = getters.getCoverPlusCalculation['totalAmountPayableToAcc']
  let coverPlusExtraTotal = getters.getCoverPlusExtraCalculation['totalAmountPayableToAcc']

  return (floatFixer(coverPlusTotal) - floatFixer(coverPlusExtraTotal))
}

export const getTotalIncomeProtectionAmount = (state, getters, rootState) => {
  if (_.isEmpty(getters.getCoverPlusCalculation) || _.isEmpty(getters.getCoverPlusExtraCalculation)) return null

  let totalNewCover = getters.getCoverPlusCalculation['nominatedCoverAmount']
  let nominatedCoverPlusExtraAmount = rootState.letterOfAuthority.partner_nominated_cover_amount

  return (floatFixer(totalNewCover) - floatFixer(nominatedCoverPlusExtraAmount))
}

export const getTotalSavingsFromAccLeviesAmount = (state, getters, rootState, rootGetters) => {
  if (_.isEmpty(getters.getCoverPlusCalculation) || _.isEmpty(getters.getCoverPlusExtraCalculation)) return null

  let coverPlusTotal = getters.getCoverPlusCalculation['totalAmountPayableToAcc']
  let coverPlusExtraTotal = getters.getCoverPlusExtraCalculation['totalAmountPayableToAcc']

  return (floatFixer(coverPlusTotal) - floatFixer(coverPlusExtraTotal))
}
/* eslint-enable */

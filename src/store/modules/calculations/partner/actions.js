/* eslint-disable */
import { merge, isEmpty, get, toString } from 'lodash';
import LevyCalculationService from 'src/services/ipp/levy/LevyCalculationService';

/**
 * Constant defining store namespace module.
 *
 * @type {String}
 */
const LOA_NAMESPACE = 'letterOfAuthority';
const LOA_COLLECTION_NAMESPACE = 'letterOfAuthorityCollection';

export const requestLevyCalculationOnTools = async ({ dispatch, commit, getters }) => {
  const classificationUnit = get(getters, 'getSelectedClassificationunitOnTools');
  if (isEmpty(classificationUnit) && getters.isAlreadyOnAccCoverPlusExtra) return Promise.resolve();
  const options = {
    income: getters.getDeterminedIncomeAmount,
    cover: getters.getDeterminedNominatedAmount,
    cuccode: get(classificationUnit, 'code', null),
  };
  const service = new LevyCalculationService(options);
  const responseValues = await service.requestCalculations();
  let payload = {
    id: 0,
    letter_of_authority_id: 0,
    type: 'partner',
    property: 'on_tools',
    values: responseValues,
    coverplus_cover: service.hydrateValues('coverplus_cover'),
    coverplus_extra_standard: service.hydrateValues('coverplus_extra_standard'),
  };
  const calculationFromServer = getters.getAccLevyCalculationOnToolsFromServer;
  if (!isEmpty(calculationFromServer)) {
    const { id, letter_of_authority_id: planner } = calculationFromServer;
    payload = merge(payload, { id, letter_of_authority_id: planner });
  }
  const reponse = await dispatch(`${LOA_NAMESPACE}/persistAccLevyCalculation`, payload, { root: true });
  commit(`${LOA_NAMESPACE}/ASSIGN_LOA_STATE`, reponse, { root: true });
  commit(`${LOA_COLLECTION_NAMESPACE}/ADD_LOA_COLLECTION`, reponse, { root: true });
  return Promise.resolve(reponse);
};

// eslint-disable-next-line object-curly-newline
export const requestLevyCalculationExisting = async ({ commit, dispatch, getters, rootState }) => {
  const cover = get(rootState, [LOA_NAMESPACE, 'partner_existing_nominated_cover_amount'], 0);
  // We are getting the current (plan) classification unit from the client
  const classificationUnit = getters.getClientSelectedClassificationUnit;
  if (isEmpty(toString(cover))
    || isEmpty(classificationUnit)
    || !getters.isAlreadyOnAccCoverPlusExtra) return Promise.resolve();
  const options = {
    cover,
    income: getters.getDeterminedIncomeAmount,
    cuccode: get(classificationUnit, 'code', 0),
  };
  const service = new LevyCalculationService(options);
  const responseValues = await service.requestCalculations();
  let payload = {
    id: 0,
    letter_of_authority_id: 0,
    type: 'partner',
    property: 'existing',
    values: responseValues,
    coverplus_cover: service.hydrateValues('coverplus_cover'),
    coverplus_extra_standard: service.hydrateValues('coverplus_extra_standard'),
  };
  const calculationFromServer = getters.getAccLevyCalculationExistingFromServer;
  if (!isEmpty(calculationFromServer)) {
    const { id, letter_of_authority_id: planner } = calculationFromServer;
    payload = merge(payload, { id, letter_of_authority_id: planner });
  }
  const reponse = await dispatch(`${LOA_NAMESPACE}/persistAccLevyCalculation`, payload, { root: true });
  commit(`${LOA_NAMESPACE}/ASSIGN_LOA_STATE`, reponse, { root: true });
  commit(`${LOA_COLLECTION_NAMESPACE}/ADD_LOA_COLLECTION`, reponse, { root: true });
  return Promise.resolve(reponse);
};

export const requestLevyCalculationPlan = async ({ dispatch, commit, getters }) => {
  const classificationUnit = get(getters, 'getClientSelectedClassificationUnit');
  const cover = getters.getDeterminedNominatedAmount;
  const income = getters.getDeterminedIncomeAmount;
  if (isEmpty(classificationUnit)
    || isEmpty(toString(cover))
    || isEmpty(toString(income))) return Promise.resolve();
  const options = {
    cover,
    income,
    cuccode: get(classificationUnit, 'code', null),
  };
  const service = new LevyCalculationService(options);
  const responseValues = await service.requestCalculations();
  let payload = {
    id: 0,
    letter_of_authority_id: 0,
    type: 'partner',
    property: 'plan',
    values: responseValues,
    coverplus_cover: service.hydrateValues('coverplus_cover'),
    coverplus_extra_standard: service.hydrateValues('coverplus_extra_standard'),
  };
  const calculationFromServer = getters.getAccLevyCalculationPlanFromServer;
  if (!isEmpty(calculationFromServer)) {
    const { id, letter_of_authority_id: planner } = calculationFromServer;
    payload = merge(payload, { id, letter_of_authority_id: planner });
  }
  const reponse = await dispatch(`${LOA_NAMESPACE}/persistAccLevyCalculation`, payload, { root: true });
  commit(`${LOA_NAMESPACE}/ASSIGN_LOA_STATE`, reponse, { root: true });
  commit(`${LOA_COLLECTION_NAMESPACE}/ADD_LOA_COLLECTION`, reponse, { root: true });
  return Promise.resolve(reponse);
};

export const requestAllLevyCalculations = ({ dispatch }) =>
  Promise.all([
    dispatch('requestLevyCalculationPlan'),
    dispatch('requestLevyCalculationOnTools'),
    dispatch('requestLevyCalculationExisting'),
  ]);

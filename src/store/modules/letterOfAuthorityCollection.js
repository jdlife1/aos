import _ from 'lodash'
import { HTTP_API } from 'src/services/http/http'

const state = {
  offlineData: [], // offline data
  data: [],
  search: {
    data: [],
    query: null
  }
}

const mutations = {
  SAVE_OFFLINE_DATA (state, payload) {
    let instance = _.filter(state.offlineData, ['client_full_name', payload.client_full_name])
    if (!_.isEmpty(instance)) {
      _.set(state, ['offlineData', window.parseInt(_.findKey(state.offlineData, ['client_full_name', _.head(instance).client_full_name]))], payload)
    }
    else {
      state.offlineData = _.concat(state.offlineData, payload)
    }
  },
  REMOVE_OFFLINE_DATA (state, payload) {
    let { client_full_name } = payload //eslint-disable-line
    state.offlineData = _.reject(state.offlineData, (o) => {
      return _.eq(o.client_full_name, _.toString(client_full_name)) //eslint-disable-line
    })
  },
  SAVE_LOA_DATA (state, payload) {
    payload = payload.data || payload
    _.set(state, 'data', payload)
  },
  UPDATE_FIELD (state, payload) {
    let { value, path } = payload
    _.set(state, path, value)
  },
  REMOVE_BY_LOA_COLLECTION_BY_INDEX (state, payload) {
    let { id } = payload
    state.data = _.reject(state.data, (o) => {
      return _.eq(o.id, id)
    })
  },
  ADD_LOA_COLLECTION (state, payload) {
    payload = payload.data || payload
    const items = state.data
    /* eslint-disable */
    const result = [...items.concat(_.castArray(payload))
      .reduce((r, o) => {
        r.has(o.id) || r.set(o.id, {});

        const item = r.get(o.id);

        Object.entries(o).forEach(([k, v]) =>
          // We are removing the reactivity from vue if it is array
          item[k] = Array.isArray(item[k]) ? _.cloneDeep(v) : v
        );

        return r;
      }, new Map()).values()];
    /* eslint-enable */
    _.set(state, 'data', result)
  }
}

const actions = {
  requestAllLetterOfAuthorityFromApi ({commit}, payload) {
    return HTTP_API().get('/letter-of-authorization/curate')
      .then(({data}) => {
        commit('SAVE_LOA_DATA', data)
        return data
      })
  }
}

const getters = {
  getFilteredResults (state) {
    let { data } = state
    let { query } = state.search
    if (_.isEmpty(query)) return []
    if (_.isEmpty(data)) return []
    return _.filter(data, (n) => {
      let { company_full_name, client_email_address, client_full_name } = n // eslint-disable-line
      return _.eq(company_full_name, query)
        || _.eq(client_email_address, query) // eslint-disable-line
        || _.eq(client_full_name, query) // eslint-disable-line
    })
  },
  getLoaApplications (state, getters) {
    return _.isEmpty(getters.getFilteredResults) ? state.data : getters.getFilteredResults
  },
  getLoaOfflineApplications (state) {
    let { offlineData } = state
    return offlineData
  },
  getLoaApplicationInstanceById (state, getters, { route }) {
    let { id } = route.params
    id = _.toNumber(id)
    if (!_.isNumber(id)) return false
    let { getLoaApplications } = getters
    if (!getLoaApplications.length) return false
    return _.find(getLoaApplications, ['id', id])
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

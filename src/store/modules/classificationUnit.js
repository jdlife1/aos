import _ from 'lodash'
import { HTTP_API } from 'src/services/http/http'

const state = {
  data: []
}

const mutations = {
  SAVE_DATA (state, payload) {
    _.assign(state, payload)
  }
}

const actions = {
  requestAllClassificationUnitFromApi ({commit}, payload) {
    return HTTP_API().get('/classification/unit/curate')
      .then(({data}) => {
        commit('SAVE_DATA', data)
        return data
      })
  }
}

const getters = {
  mapClassificationValues (state) {
    let { data } = state
    return _.map(data, (v) => {
      return {
        label: v.name,
        value: v.code
      }
    })
  },
  isClasificationUitEmpty (state) {
    return _.isEmpty(state.data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

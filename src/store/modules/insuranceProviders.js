import { HTTP } from 'src/services/http/http'
import { set, map, merge, filter, includes } from 'lodash'

const state = {
  data: []
}

const mutations = {
  ASSIGN_INSURANCE_PROVIDERS (state, payload) {
    payload = payload.data || payload
    set(state, 'data', payload)
  }
}

const actions = {
  async requestInsuranceProviders ({ commit }) {
    const { IPP_ENDPOINT } = process.env
    const { data } = await HTTP(IPP_ENDPOINT).get('/api/insurance/providers')
    commit('ASSIGN_INSURANCE_PROVIDERS', data)
    return data
  }
}

const getters = {
  providers ({ data }) {
    return data
  },
  mapProviders (state, { providers: data }) {
    const excepts = ['First Appointment', 'First Meeting Report']
    const providers = filter(data, ({ name }) => !includes(excepts, name))
    return map(providers, ({ name }) => merge({}, { label: name, value: name }))
  },
  getFilteredInsuranceProvider (state, { mapProviders }) {
    return providers =>
      filter(mapProviders, ({ value }) => includes(providers, value))
  },
  getInsuranceProvidersForTaxIssues (state, { getFilteredInsuranceProvider }) {
    return getFilteredInsuranceProvider(['Fidelity', 'AIA', 'Partners Life'])
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

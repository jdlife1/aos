export default {
  blue: 'rgb(54, 162, 235) ',
  blueLight: 'rgb(54, 162, 235, 0.4)',
  green: 'rgb(75, 192, 192) ',
  greenLight: 'rgb(75, 192, 192, 0.4)',
  grey: 'rgb(201, 203, 207) ',
  greyLight: 'rgb(201, 203, 207, 0.4)',
  orange: 'rgb(255, 159, 64) ',
  orangeLight: 'rgb(255, 159, 64, 0.4)',
  purple: 'rgb(153, 102, 255) ',
  red: 'rgb(255, 99, 132) ',
  redLight: 'rgba(255,99,132,0.4)',
  yellow: 'rgb(255, 205, 86)',
  yellowLight: 'rgb(255, 205, 86, 0.4)'
}

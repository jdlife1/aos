export const mortgageRepaymentMethods = () => ([
  { label: 'Weekly', value: 'weekly' },
  { label: 'Fortnightly', value: 'fortnightly' },
  { label: 'Monthly', value: 'monthly' },
  { label: 'Annually', value: 'annually' }
])

export const propertyTypeOptions = () => ([
  { label: 'Mortgage Repayments', value: 'mortgage-repayments' },
  { label: 'Rental Payments', value: 'rental-payments' }
])

export const accCoverPlanTypes = () => ([
  { label: 'ACC Cover Plus/Work Place Cover', value: 'cover_plus' },
  { label: 'ACC Cover Plus Extra', value: 'cover_plus_extra' }
])

export const getStepList = () => ([
  { label: 'Company And Income Details', value: 'company-income-details', color: 'positive' },
  { label: 'Tax Splitting', value: 'tax-splitting', color: 'positive' },
  { label: 'Important Info', value: 'important-info', color: 'positive' },
  { label: 'ACC Offsets', value: 'acc-offsets', color: 'positive' },
  { label: 'Common Tax Issues', value: 'common-tax-issues', color: 'positive' },
  { label: 'Fatal Entitlements', value: 'fatal-entitlements', color: 'positive' },
  { label: 'Generate', value: 'loa-generate', color: 'positive' }
])

export const getFatalEntitlementCategories = () => { // make this a model
  return [
    { label: 'With Partner and No Child', value: 'no-child' },
    { label: 'With Partner and Less Than Two children younger than 18 years old', value: 'less-than-two-children' },
    { label: 'With Partner and Greater Than Two children younger than 18 years old', value: 'greater-than-two-children' },
    { label: 'With Partner and studying children above than 18 years old', value: 'studying-children' }
  ]
}

export const getClientOrPartner = () => ([
  { label: 'Client', value: 'client' },
  { label: 'Partner', value: 'partner' }
])

export const getYesOrNo = () => ([
  { label: 'Yes', value: 'yes' },
  { label: 'No', value: 'no' }
])

export const getTaxMethods = () => {
  return [
    {
      label: 'Drawings/Shareholder salary (i.e non paye income)',
      value: 'drawings'
    },
    {
      label: 'PAYE',
      value: 'paye'
    },
    {
      label: 'Combination of Drawings/Shareholder salary (i.e none paye income) and PAYE',
      value: 'combination-of-drawings-and-paye'
    }
  ]
}

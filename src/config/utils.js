/* eslint-disable */
import { toNumber, isArray, reduce } from 'lodash';

export const floatFixer = (x, precision = 2, comma = false) =>
  toNumber(window.parseFloat(numberWithCommas(x, comma)).toFixed(precision));

export const floatTotals = (x, y = 0, precision = 2) => {
  if (isArray(x)) return reduce(x, (sum, n) => sum + n, 0);
  return floatFixer(floatFixer(x) + floatFixer(y), precision);
};

export const floatDiff = (x, y, precision = 2) =>
 (floatFixer(floatFixer(x) - floatFixer(y), precision));

export const numberWithCommas = (x, comma = true) => {
  if (!x) return 0;
  if (comma) return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  return toNumber(x.toString().split(',').join(''));
};

export const floatTaxTotal = (x, y, precision = 2, percent = 100) =>
  floatFixer(floatFixer(x, precision) * (y / percent), precision);

export const getPercentageValue = (x, y, p = 100) =>
  Math.ceil(((x - y) / x * p));

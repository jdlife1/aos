const CONSTANT_STORAGE = {
  USER_ACCESS_TOKEN: 'USER_ACCESS_TOKEN_DATA',
  USER_MODEL: 'USER_MODEL'
}

const CONSTANT_URL = {
  DOCUMENT: {
    PDF: `${process.env.LARAVEL_API_ENDPOINT}/document/:id/letter-of-authority`
  }
}

export {
  CONSTANT_STORAGE,
  CONSTANT_URL
}

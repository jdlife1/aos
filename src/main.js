// === DEFAULT / CUSTOM STYLE ===
// WARNING! always comment out ONE of the two require() calls below.
// 1. use next line to activate CUSTOM STYLE (./src/themes)
// require(`./themes/app.${__THEME}.styl`)
// 2. or, use next line to activate DEFAULT QUASAR STYLE
require(`quasar/dist/quasar.${__THEME}.css`)
// ==============================

// Uncomment the following lines if you need IE11/Edge support
// require(`quasar/dist/quasar.ie`)
// require(`quasar/dist/quasar.ie.${__THEME}.css`)

import Vue from 'vue'
import Quasar from 'quasar'
import router from './router'
import App from './App'
import store from './store'
import axios from 'axios'
import money from '@/vmoney'
import VueSignature from 'vue-signature'
import AOSFilters from 'services/filters'
import { sync } from 'vuex-router-sync'
import VueOnlinePlugin from 'vue-navigator-online'

sync(store, router)
Vue.use(AOSFilters)
Vue.use(VueOnlinePlugin)
Vue.use(VueSignature)
Vue.use(money, { precision: 0 })

Vue.use(Quasar) // Install Quasar Framework

Vue.prototype.$filters = Vue.options.filters
Vue.config.productionTip = false
Vue.prototype.$http = axios

if (__THEME === 'mat') {
  require('quasar-extras/roboto-font')
}
import 'quasar-extras/material-icons'
// import 'quasar-extras/ionicons'
// import 'quasar-extras/fontawesome'

import 'quasar-extras/animate/fadeIn.css'
import 'quasar-extras/animate/fadeOut.css'
// Or import them all -- but notice that your
// bundle will probably include unused ones,
// so more KB over the wire that never get used.
// import 'quasar-extras/animate'

Quasar.start(() => {
  /* eslint-disable no-new */
  new Vue({
    el: '#q-app',
    router,
    store,
    render: h => h(App)
  })
})

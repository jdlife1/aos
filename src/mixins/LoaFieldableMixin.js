import _ from 'lodash'
import { mapActions, mapState } from 'vuex'

const LOA_STORE_NAME = 'letterOfAuthority'

export default {
  methods: {
    ...mapActions(LOA_STORE_NAME, ['updateLoaFieldAction']),
    updateLoaField: _.debounce(function (value, field) {
      value = _.isArray(value) ? _.cloneDeep(value) : value
      return this.updateLoaFieldAction({ value, field })
    }, 800)
  },
  computed: {
    ...mapState(LOA_STORE_NAME, {
      loaField: state => state
    }),
    clientName () {
      const { client_full_name: name } = this.loaField
      return name || 'Client'
    },
    partnerName () {
      const { partner_name: name } = this.loaField
      return name || 'Partner'
    },
    getChartDataOptions () {
      return {
        scales: {
          yAxes: [{
            stacked: true,
            ticks: {
              beginAtZero: true,
              callback (value, index, values) {
                return `$ ${value}`
              }
            }
          }],
          xAxes: [{
            gridLines: {
              display: false
            }
          }]
        },
        responsive: true,
        maintainAspectRatio: false
      }
    }
  }
}

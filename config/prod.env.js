module.exports = {
  NODE_ENV: JSON.stringify('production'),
  IPP_ENDPOINT: JSON.stringify('https://jdserver.net/ipp/public'),
  LARAVEL_API_ENDPOINT: JSON.stringify('https://jdserver.net/aos/public')
}

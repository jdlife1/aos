var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: JSON.stringify('development'),
  IPP_ENDPOINT: JSON.stringify('http://localhost:9090'),
  LARAVEL_API_ENDPOINT: JSON.stringify('http://aos.localhost')
})
